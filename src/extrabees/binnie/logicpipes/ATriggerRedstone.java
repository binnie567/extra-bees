package binnie.logicpipes;

import java.util.ArrayList;
import java.util.List;

public class ATriggerRedstone extends LogicTrigger implements ILogicTrigger {

	@Override
	public String getTextureFile() {
		return "/gfx/buildcraft/gui/triggers.png";
	}

	@Override
	public int getIndexInTexture() {
		return 0;
	}

	@Override
	public String getDescription() {
		return "Redstone Signal";
	}

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();
		list.add("Any Direction");
		return list;
	}

}
