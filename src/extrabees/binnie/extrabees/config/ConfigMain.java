package binnie.extrabees.config;

public class ConfigMain extends ConfigBase {

	ConfigMain() {
		super("main");
	}

	public static final String CATEOGORY_WORLDGEN = "world_generation";

	/*
	 * Config option is equal to hiveSpawnRate. + name provided
	 */
	public int hiveRate(String name, int defaultValue) {
		return getInt("hiveSpawnRate." + name, CATEOGORY_WORLDGEN, defaultValue);
	}

}
