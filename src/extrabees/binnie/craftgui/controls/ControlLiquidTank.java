package binnie.craftgui.controls;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import binnie.core.BinnieCore;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityMachine;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ControlLiquidTank extends Control implements ITooltip {

	/*
	 * 
	 * @Override public void onMouseClick(int button) { ItemStack item =
	 * ((Window) getSuperParent()).getHeldItemStack();
	 * 
	 * boolean empty = tank == null || tank.getLiquid() == null ||
	 * tank.getLiquid().amount == 0;
	 * 
	 * if (item != null && item.stackSize == 1) { if
	 * (LiquidManager.isEmptyContainer(item) && !empty) { LiquidContainer
	 * container = LiquidManager.getEmptyContainer( item, tank.getLiquid()); if
	 * (tank.drain(container.liquid.amount, false) != null &&
	 * tank.drain(container.liquid.amount, false).amount ==
	 * container.liquid.amount) { tank.drain(container.liquid.amount, true);
	 * ((Window) getSuperParent()) .setHeldItemStack(container.filled); }
	 * 
	 * } else if (LiquidManager.getLiquidContainer(item) != null) {
	 * LiquidContainer container = LiquidManager .getLiquidContainer(item); if
	 * (tank.fill(container.liquid, false) > 0) { tank.fill(container.liquid,
	 * true); ((Window) getSuperParent()) .setHeldItemStack(container.empty); }
	 * } } }
	 */

	int tankID;

	public ControlLiquidTank(IWidget parent, int x, int y) {
		super(parent, x, y, 18, 60);
		canMouseOver = true;
	}

	public void setTankID(int tank) {
		this.tankID = tank;
	}

	public ILiquidTank getTank() {
		if (getWindow().getInventory() instanceof TileEntityMachine) {
			return ((TileEntityMachine) getWindow().getInventory())
					.getTank(this.tankID);
		}
		return null;
	}

	@Override
	public void onRenderBackground() {

		getRenderer().renderRect(0, 0, 18, 60, 152, 0);

		ILiquidTank tank = getTank();

		if (tank != null && tank.getLiquid() != null
				&& tank.getLiquid().amount > 0) {

			Object content = null;

			int liquidImgIndex = 0;
			int liquidId = tank.getLiquid().itemID;
			int squaled = (int) (58.0 * (tank.getLiquid().amount) / (tank
					.getCapacity()));

			if (liquidId <= 0)
				return;

			if (liquidId < Block.blocksList.length
					&& Block.blocksList[liquidId] != null) {
				content = Block.blocksList[liquidId];
				liquidImgIndex = Block.blocksList[liquidId].blockIndexInTexture;
			} else {
				content = Item.itemsList[liquidId];
				liquidImgIndex = Item.itemsList[liquidId].getIconFromDamage(0);
			}

			if (content instanceof Block) {
				BinnieCore.proxy
						.bindTexture(((Block) content).getTextureFile());
			} else if (content instanceof Item) {
				BinnieCore.proxy.bindTexture(((Item) content).getTextureFile());
			} else {
				BinnieCore.proxy.bindTexture("/terrain.png");
			}

			int imgLine = liquidImgIndex / 16;
			int imgColumn = liquidImgIndex - imgLine * 16;

			imgLine *= 16;
			imgColumn *= 16;

			int yPos = 59;

			int block = Math.min(squaled, 16);

			GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0F);

			while (block > 0) {

				getRenderer().renderRect(1, yPos - block, 16, block, imgColumn,
						imgLine + 16 - block);

				squaled -= 16;
				yPos -= 16;
				block = Math.min(squaled, 16);
			}

			BinnieCore.proxy.bindTexture(getTextureFile());

			getRenderer().renderRect(1, 0, 16, 60, 170, 0);

		}

	}

	@Override
	public List<String> getTooltip() {

		ILiquidTank tank = getTank();

		List<String> list = new ArrayList<String>();
		if (tank != null && tank.getLiquid() != null
				&& tank.getLiquid().amount > 0) {
			ItemStack liquid = new ItemStack(tank.getLiquid().itemID, 1,
					tank.getLiquid().itemMeta);

			List<String> info = (liquid.getTooltip(
					((Window) getSuperParent()).getPlayer(), false));

			int percentage = (int) (100.0 * (tank.getLiquid().amount) / (tank
					.getCapacity()));

			list.add(info.get(0));
			info.remove(0);
			list.add(percentage + "% full");
			list.addAll(info);

			return list;
		}

		list.add("Empty");
		list.add("0% full");
		return list;
	}

}
