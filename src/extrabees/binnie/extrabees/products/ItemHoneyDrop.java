package binnie.extrabees.products;

import java.util.List;

import denoflionsx.API.PfFEvents;
import denoflionsx.API.Annotations.PfFEventTypes;
import denoflionsx.API.Annotations.PfFSubscribe;
import denoflionsx.API.Events.EventFuelCreated;

import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraftforge.liquids.LiquidStack;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.liquids.ItemLiquid;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public class ItemHoneyDrop extends Item {
	public ItemHoneyDrop(int i) {
		super(i);
		maxStackSize = 64;
		setMaxDamage(0);
		setHasSubtypes(true);
		setTextureFile("/gfx/forestry/items/items.png");
		setCreativeTab(CreativeTabs.tabMaterials);
		if (PfFEvents.fuelEvent != null)
			PfFEvents.fuelEvent.register(this);
	}

	public static void addSubtypes() {
		EnumType.ENERGY.init("Energy Drop", 0x9C4972, 0xE37171);
		EnumType.ACID.init("Acid Drop", 0x4BB541, 0x49DE3C);
		EnumType.ACID
				.addLiquid(new LiquidStack(
						ItemLiquid.liquids[ItemLiquid.EnumType.ACID.ordinal()].shiftedIndex,
						200, 0));
		EnumType.POISON.init("Venom Drop", 0xD106B9, 0xFF03E2);
		EnumType.POISON
				.addLiquid(new LiquidStack(
						ItemLiquid.liquids[ItemLiquid.EnumType.POISON.ordinal()].shiftedIndex,
						200, 0));

		EnumType.APPLE.init("Apple Drop", 0xC75252, 0xC92A2A);
		EnumType.APPLE.addLiquid(new LiquidStack(ItemInterface.getItem(
				"liquidJuice").getItem(), 200));

		EnumType.CITRUS.init("Citric Drop", 0xE1E655, 0xF6FF00);

		EnumType.ICE.init("Glacial Drop", 0xAEE8E2, 0x96FFF5);
		EnumType.ICE
				.addLiquid(new LiquidStack(
						ItemLiquid.liquids[ItemLiquid.EnumType.GLACIAL
								.ordinal()].shiftedIndex, 200, 0));

		EnumType.MILK.init("Milk Drop", 0xE0E0E0, 0xFFFFFF);
		EnumType.MILK.addLiquid(new LiquidStack(ItemInterface.getItem(
				"liquidMilk").getItem(), 200));

		EnumType.SEED.init("Seed Drop", 0x7CC272, 0xC2BEA7);
		EnumType.SEED.addLiquid(new LiquidStack(ItemInterface.getItem(
				"liquidSeedOil").getItem(), 200));
		EnumType.SEED.addRemenants(new ItemStack(Item.seeds, 1, 0), 25);

		EnumType.ALCOHOL.init("Alcoholic Drop", 0xDBE84D, 0xA5E84D);
		EnumType.ALCOHOL.addLiquid(new LiquidStack(ItemInterface.getItem(
				"liquidMead").getItem(), 200));

		EnumType.FRUIT.init("Fruit Drop", 0xAD0034, 0xCC6E8A);

		EnumType.VEGETABLE.init("Vegetable Drop", 0xFF6200, 0xAD7A5A);
		EnumType.PUMPKIN.init("Pumpkin Drop", 0xFFBF00, 0xFFDE7D);
		EnumType.MELON.init("Melon Drop", 0xFF3700, 0xFFA991);

	}

	@PfFSubscribe(Event = PfFEventTypes.FUEL)
	public void PfFInit(EventFuelCreated event) {
		String name = event.getFuel().getLiquid().getItemName();
		ItemStack liquid = event.getFuel().getLiquid();

		if (name.equals("item.citrusJuice"))
			EnumType.CITRUS
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							200, liquid.getItemDamage()));

		if (name.equals("item.pumpkinjuice"))
			EnumType.PUMPKIN
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							200, liquid.getItemDamage()));

		if (name.equals("item.melonjuice"))
			EnumType.MELON
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							200, liquid.getItemDamage()));

		if (name.equals("item.fruitJuice"))
			EnumType.FRUIT
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							200, liquid.getItemDamage()));

		if (name.equals("item.veggieJuice"))
			EnumType.VEGETABLE
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							200, liquid.getItemDamage()));

	}

	@Override
	public boolean isDamageable() {
		return false;
	}

	@Override
	public boolean isRepairable() {
		return false;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return ItemHoneyDrop.EnumType.getTypeFromID(itemstack.getItemDamage()).name;
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	@Override
	public int getColorFromItemStack(ItemStack itemStack, int j) {
		int i = itemStack.getItemDamage();
		if (j == 0) {
			return EnumType.getTypeFromID(i).primaryColor;
		}

		return EnumType.getTypeFromID(i).secondaryColor;
	}

	// Image ID's

	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		if (j > 0) {
			return 110;
		}

		return 109;
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (EnumType type : EnumType.values()) {
			if (!type.isSecret) {
				itemList.add(new ItemStack(this, 1, type.ordinal()));
			}
		}
	}

	public static enum EnumType {
		ENERGY, ACID, POISON, APPLE, CITRUS, ICE, MILK, SEED, ALCOHOL, FRUIT, VEGETABLE, PUMPKIN, MELON,

		;

		public static EnumType getTypeFromID(int id) {
			return EnumType.class.getEnumConstants()[id];
		}

		public String name;
		public int primaryColor;
		public int secondaryColor;
		public boolean isSecret = false;
		int time;
		LiquidStack liquid;
		ItemStack remenants;
		int chance;

		EnumType() {
			this.name = "??? Drop";
			this.primaryColor = 0xFFFFFF;
			this.secondaryColor = 0xFFFFFF;
			this.isSecret = false;
			this.time = 10;
			this.chance = 0;
			this.remenants = new ItemStack(Block.dirt, 1, 0);
			liquid = null;
		}

		public void init(String name, int primaryColor, int secondaryColor) {
			this.name = name;
			this.primaryColor = primaryColor;
			this.secondaryColor = secondaryColor;
		}

		public EnumType setIsSecret() {
			this.isSecret = true;
			return this;
		}

		public void addLiquid(LiquidStack item) {
			liquid = item;
			addRecipe();
		}

		public void addRemenants(ItemStack item, int chance) {
			this.remenants = item;
			this.chance = chance;
		}

		public void addRecipe() {
			if (liquid != null) {
				RecipeManagers.squeezerManager.addRecipe(time,
						new ItemStack[] { new ItemStack(ExtraBeeItem.honeyDrop,
								1, this.ordinal()) }, liquid, remenants,
						this.chance);
			}
		}
	}

	public static ItemStack getItem(ItemHoneyDrop.EnumType acid) {
		return new ItemStack(ExtraBeeItem.honeyDrop, 1, acid.ordinal());
	}
}
