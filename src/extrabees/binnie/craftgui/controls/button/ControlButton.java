package binnie.craftgui.controls.button;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventButtonClicked;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.window.IRendererWindow;

public class ControlButton extends Control {

	@Override
	public void onMouseClick(EventMouseClick event) {
		callEvent(new EventButtonClicked(this));
	}

	private ControlText textWidget;
	private String text;

	public ControlButton(IWidget parent, int x, int y, int width, int height) {
		super(parent, x, y, width, height);

		this.canMouseOver = true;
	}

	public ControlButton(IWidget parent, int x, int y, int width, int height,
			String text) {
		this(parent, x, y, width, height);

		int textHeight = ((IRendererWindow) getSuperParent().getRenderer())
				.getTextHeight();

		this.text = text;
		this.textWidget = new ControlText(this, width / 2,
				(height - textHeight) / 2, text, ControlText.Alignment.Center);
	}

	@Override
	public void onRenderBackground() {

		int v = 0;
		if (this.isMouseOver())
			v += 40;
		else if (this.isEnabled())
			v += 20;

		((IRendererWindow) getSuperParent().getRenderer()).renderRectBordered(
				0, 0, (int) this.getSize().x, (int) this.getSize().y, 0, v,
				128, 20, 2, 2, 2, 3);
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
		if (textWidget != null)
			textWidget.setText(getText());
	}

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-controls.png";
	}

	public String getText() {
		return text;
	}

}
