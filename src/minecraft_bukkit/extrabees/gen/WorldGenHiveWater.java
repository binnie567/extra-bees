package extrabees.gen;

import extrabees.core.ExtraBeeBlock;
import java.util.Random;
import net.minecraft.server.*;

public class WorldGenHiveWater extends WorldGenerator
{
    public WorldGenHiveWater()
    {
    }

    public boolean a(World world, Random random, int i, int j, int k)
    {
        BiomeBase biomebase = world.getWorldChunkManager().getBiome(i, k);
        int l = i;
        int i1 = j;
        int j1 = k;

        if (world.getTypeId(l, i1, j1) == Block.STATIONARY_WATER.id && world.getTypeId(l, i1, j1) == Block.STATIONARY_WATER.id && (world.getMaterial(l, i1 - 1, j1) == Material.SAND || world.getMaterial(l, i1 - 1, j1) == Material.CLAY || world.getMaterial(l, i1 - 1, j1) == Material.EARTH || world.getMaterial(l, i1 - 1, j1) == Material.STONE))
        {
            world.setRawTypeIdAndData(l, i1, j1, ExtraBeeBlock.hive.id, 0);
        }

        return true;
    }
}
