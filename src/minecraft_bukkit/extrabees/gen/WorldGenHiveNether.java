package extrabees.gen;

import extrabees.core.ExtraBeeBlock;
import java.util.Random;
import net.minecraft.server.*;

public class WorldGenHiveNether extends WorldGenerator
{
    public WorldGenHiveNether()
    {
    }

    public boolean a(World world, Random random, int i, int j, int k)
    {
        BiomeBase biomebase = world.getWorldChunkManager().getBiome(i, k);
        BiomeBase _tmp = biomebase;

        if (biomebase.id != BiomeBase.HELL.id)
        {
            return true;
        }

        int l = i;
        int i1 = j;
        int j1 = k;

        if (embedInWall(world, Block.NETHERRACK.id, l, i1, j1))
        {
            world.setRawTypeIdAndData(l, i1, j1, ExtraBeeBlock.hive.id, 2);
        }

        return true;
    }

    public boolean embedInWall(World world, int i, int j, int k, int l)
    {
        return world.getTypeId(j, k, l) == i && world.getTypeId(j, k + 1, l) == i && world.getTypeId(j, k - 1, l) == i && (world.isEmpty(j + 1, k, l) || world.isEmpty(j - 1, k, l) || world.isEmpty(j, k, l + 1) || world.isEmpty(j, k, l - 1));
    }
}
