package binnie.craftgui.core;

public class Vector2f {

	public float x = 0.0f;
	public float y = 0.0f;

	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2f(Vector2f o) {
		this.x = o.x;
		this.y = o.y;
	}

	public static Vector2f add(Vector2f a, Vector2f b) {
		return new Vector2f(a.x + b.x, a.y + b.y);
	}

	public static Vector2f sub(Vector2f a, Vector2f b) {
		return new Vector2f(a.x - b.x, a.y - b.y);
	}

}
