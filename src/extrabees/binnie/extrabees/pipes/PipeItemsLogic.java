package binnie.extrabees.pipes;

import net.minecraft.src.ModLoader;
import net.minecraft.src.NBTTagCompound;
import net.minecraftforge.common.ForgeDirection;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.logicpipes.LogicGate;
import buildcraft.transport.Pipe;
import buildcraft.transport.pipes.PipeItemsWood;

public class PipeItemsLogic extends PipeItemsWood {

	int baseTexture = 70;
	int plainTexture = 71;

	public PipeItemsLogic(int itemID) {
		super(itemID);

		this.gate = new LogicGate(this);

		try {
			ModLoader.setPrivateValue(Pipe.class, this, "logic",
					new PipeLogicLogic());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getTextureFile() {
		return ExtraBeeTexture.Main.getTexture();
	}

	@Override
	public int getTextureIndex(ForgeDirection direction) {
		if (direction == ForgeDirection.UNKNOWN)
			return baseTexture;
		else {
			int metadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);

			if (metadata == direction.ordinal())
				return plainTexture;
			else
				return baseTexture;
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		if (nbttagcompound.hasKey("Gate")) {
			nbtGate = nbttagcompound.getCompoundTag("Gate");
		}
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		if (nbtGate != null) {
			gate = new LogicGate(this);
			gate.readFromNBT(nbtGate);
		}
	}

	NBTTagCompound nbtGate;

}
