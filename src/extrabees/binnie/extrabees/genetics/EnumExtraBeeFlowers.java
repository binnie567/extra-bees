package binnie.extrabees.genetics;

import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.World;
import forestry.api.apiculture.IAlleleFlowers;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IFlowerProvider;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.ILegacyHandler;

public enum EnumExtraBeeFlowers implements IFlowerProvider, IAlleleFlowers {
	WATER, SUGAR, ROCK, BOOK, NONE, REDSTONE;

	String uid = "";
	String name = "";
	boolean dominant = true;;

	@Override
	public String getUID() {
		return "extrabees.flower." + uid;
	}

	@Override
	public boolean isDominant() {
		return dominant;
	}

	@Override
	public IFlowerProvider getProvider() {
		return this;
	}

	@Override
	public boolean isAcceptedFlower(World world, IBeeGenome genome, int x,
			int y, int z) {
		switch (this) {
		case WATER:
			return world.getBlockId(x, y, z) == Block.waterlily.blockID;

		case ROCK:
			return world.getBlockMaterial(x, y, z) == Material.rock;

		case SUGAR:
			return world.getBlockId(x, y, z) == Block.reed.blockID;

		case BOOK:
			return world.getBlockId(x, y, z) == Block.bookShelf.blockID;

		case REDSTONE:
			return world.getBlockId(x, y, z) == Block.torchRedstoneActive.blockID;

		case NONE:
			return true;

		default:
			return false;
		}
	}

	@Override
	public boolean growFlower(World world, IBeeGenome genome, int x, int y,
			int z) {
		switch (this) {
		case WATER:
			if (world.isAirBlock(x, y, z)
					&& (world.getBlockId(x, y - 1, z) == Block.waterStill.blockID)) {
				return world.setBlockWithNotify(x, y, z,
						Block.waterlily.blockID);
			}
			return false;

		case SUGAR:
			if ((world.getBlockId(x, y, z) == Block.reed.blockID)
					&& world.isAirBlock(x, y + 1, z)) {
				return world
						.setBlockWithNotify(x, y + 1, z, Block.reed.blockID);
			}
			return false;

		default:
			return true;
		}
	}

	@Override
	public String getDescription() {
		return name;
	}

	public void init(String name, int id) {
		this.name = name;
		this.uid = toString().toLowerCase();
		((ILegacyHandler) AlleleManager.alleleRegistry).registerLegacyMapping(
				id, uid);
	}

	public void register() {
		AlleleManager.alleleRegistry.registerAllele(this);
	}

	public static void doInit() {
		int id = 1950;
		WATER.init("Lily Pad", id++);
		SUGAR.init("Reeds", id++);
		ROCK.init("Rock", id++);
		BOOK.init("Books", id++);
		NONE.init("None", id++);
		REDSTONE.init("Redstone", id++);

		for (EnumExtraBeeFlowers effect : EnumExtraBeeFlowers.values()) {
			effect.register();
		}
	}

	@Override
	public ItemStack[] affectProducts(World world, IBeeGenome genome, int x,
			int y, int z, ItemStack[] products) {
		return products;
	}

	@Override
	public ItemStack[] getItemStacks() {
		switch (this) {
		case WATER:
			return new ItemStack[] { new ItemStack(Block.waterlily) };
		case SUGAR:
			return new ItemStack[] { new ItemStack(Block.reed) };
		case ROCK:
			return new ItemStack[] { new ItemStack(Block.cobblestone) };
		case BOOK:
			return new ItemStack[] { new ItemStack(Block.bookShelf) };
		case REDSTONE:
			return new ItemStack[] { new ItemStack(Block.torchRedstoneActive) };
		default:
			break;
		}
		return null;
	}

}
