package binnie.extrabees.core;

public interface IExtraBeeCore {

	public void preInit();

	public void doInit();

	public void postInit();

}
