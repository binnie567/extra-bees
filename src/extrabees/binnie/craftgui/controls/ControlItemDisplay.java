package binnie.craftgui.controls;

import java.util.List;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.window.Window;

import net.minecraft.src.ItemStack;

public class ControlItemDisplay extends Control implements ITooltip {

	ItemStack itemStack = null;

	boolean tooltip = false;

	public void setTooltip() {
		tooltip = true;
	}

	public ControlItemDisplay(IWidget parent, int x, int y) {
		super(parent, x, y, 16, 16);
		canMouseOver = true;
	}

	@Override
	public void onRenderBackground() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRenderForeground() {
		if (itemStack != null) {
			getRenderer().renderItem(0, 0, itemStack);
		}

	}

	@Override
	public void onRenderOverlay() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdate() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getTextureFile() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setItemStack(ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	@Override
	public List<String> getTooltip() {
		if (tooltip && itemStack != null) {
			return itemStack.getTooltip(
					((Window) getSuperParent()).getPlayer(), false);
		}
		return null;
	}

}
