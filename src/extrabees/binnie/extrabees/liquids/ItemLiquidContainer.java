package binnie.extrabees.liquids;

import java.util.List;

import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.liquids.ItemLiquid.EnumType;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemLiquidContainer extends Item {
	private String name;
	private int textBase;
	private int textOverlay;

	public ItemLiquidContainer(int i, String name, int base, int overlay) {
		super(i);
		maxStackSize = 64;
		setMaxDamage(0);
		setHasSubtypes(true);
		this.name = name;
		textBase = base;
		textOverlay = overlay;
		setTextureFile(ExtraBeeTexture.Containers.getTexture());
		setCreativeTab(CreativeTabs.tabMaterials);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		if (itemstack == null)
			return "Unknown Liquid " + name;
		if (itemstack.getItemDamage() < ItemLiquid.liquids.length)
			return ItemLiquid.liquids[itemstack.getItemDamage()]
					.getItemDisplayName(itemstack) + " " + name;
		return "Unknown Liquid " + name;
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (EnumType type : EnumType.values()) {
			if (!type.deprecated)
				itemList.add(new ItemStack(this, 1, type.ordinal()));
		}
	}

	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		if (j > 0) {
			return textOverlay;
		}

		return textBase;
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	@Override
	public int getColorFromItemStack(ItemStack itemStack, int j) {
		int i = itemStack.getItemDamage();
		if (j == 0) {
			return 0xFFFFFF;
		}

		return ItemLiquid.EnumType.values()[i].colour;
	}

}
