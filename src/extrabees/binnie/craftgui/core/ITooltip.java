package binnie.craftgui.core;

import java.util.List;

public interface ITooltip {

	public List<String> getTooltip();

}
