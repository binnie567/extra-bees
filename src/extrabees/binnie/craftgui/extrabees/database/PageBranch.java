package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IBeeBranch;
import binnie.craftgui.controls.ControlPage;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

public abstract class PageBranch extends ControlPage {

	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if (event instanceof EventBranchChanged) {
			onBranchChanged(((EventBranchChanged) event).getBranch());
		}
	}

	public abstract void onBranchChanged(IBeeBranch branch);

	public PageBranch(IWidget parent) {
		super(parent, parent.getPosition().x, parent.getPosition().y, parent
				.getSize().x, parent.getSize().y);
	}

}
