package binnie.extrabees.products;

import binnie.extrabees.core.ExtraBeeItem;

import net.minecraft.src.ItemStack;

public class ItemHoneyCrystalEmpty extends ItemHoneyCrystal {
	public ItemHoneyCrystalEmpty(int id) {
		super(id);
		setIconIndex(17);
		setMaxDamage(0);
		setMaxStackSize(64);
	}

	@Override
	public int getChargedItemId() {
		return ExtraBeeItem.honeyCrystal.shiftedIndex;
	}

	@Override
	public int getIconFromDamage(int i) {
		return this.iconIndex;
	}

	@Override
	public String getItemDisplayName(ItemStack i) {
		return "Depleted Honey Crystal";
	}
}
