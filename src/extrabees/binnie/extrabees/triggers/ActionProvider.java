package binnie.extrabees.triggers;

import java.util.LinkedList;

import binnie.extrabees.machines.ICustomGates;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionProvider;

import net.minecraft.src.Block;
import net.minecraft.src.TileEntity;

public class ActionProvider implements IActionProvider {

	@Override
	public LinkedList<IAction> getNeighborActions(Block block, TileEntity tile) {
		if (tile instanceof ICustomGates)
			return ((ICustomGates) tile).getCustomActions();
		return null;
	}

}
