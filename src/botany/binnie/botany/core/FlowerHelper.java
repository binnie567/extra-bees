package binnie.botany.core;

import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.EnumFlowerChromosome;
import binnie.botany.genetics.Flower;
import binnie.botany.genetics.FlowerGenome;
import binnie.botany.genetics.FlowerSpecies;
import binnie.botany.genetics.IFlower;
import binnie.botany.genetics.IFlowerGenome;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.World;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.core.config.ForestryItem;
import forestry.core.genetics.Chromosome;
import forestry.plugins.PluginBotany;

public class FlowerHelper implements IFlowerInterface {

	@Override
	public boolean isFlower(ItemStack stack) {
		if (stack == null)
			return false;

		return stack.itemID == PluginBotany.itemBotany.shiftedIndex;
	}

	@Override
	public IFlower getFlower(ItemStack stack) {
		if (!isFlower(stack))
			return null;
		return new Flower(stack.getTagCompound());
	}

	@Override
	public IFlower getFlower(IFlowerGenome genome, int age) {
		return new Flower(genome, age);
	}

	@Override
	public ItemStack getFlowerStack(IFlower flower) {

		NBTTagCompound nbttagcompound = new NBTTagCompound();
		flower.writeToNBT(nbttagcompound);
		ItemStack beeStack = new ItemStack(PluginBotany.itemBotany, 1,
				flower.getDamage());
		beeStack.setTagCompound(nbttagcompound);
		return beeStack;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] template) {
		Chromosome[] chromosomes = new Chromosome[template.length];
		for (int i = 0; i < template.length; i++)
			if (template[i] != null)
				chromosomes[i] = new Chromosome(template[i]);

		return chromosomes;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] templateActive,
			IAllele[] templateInactive) {
		Chromosome[] chromosomes = new Chromosome[templateActive.length];
		for (int i = 0; i < templateActive.length; i++)
			if (templateActive[i] != null)
				chromosomes[i] = new Chromosome(templateActive[i],
						templateInactive[i]);

		return chromosomes;
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] template) {
		return new FlowerGenome(templateAsChromosomes(template));
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive) {
		return new FlowerGenome(templateAsChromosomes(templateActive,
				templateInactive));
	}

	public IAllele[] getDefaultFlowerTemplate() {
		IAllele[] template = new IAllele[2];
		template[0] = FlowerSpecies.Default;
		template[1] = AlleleFlowerStem.Thorn;
		return template;

	}

	public IFlowerGenome getDefaultFlowerGenome() {
		return templateAsGenome(getDefaultFlowerTemplate());
	}

}
