package binnie.botany.core;

import org.lwjgl.opengl.GL11;

import binnie.botany.genetics.IFlower;
import binnie.core.BinnieCore;

import forestry.plugins.PluginBotany;
import net.minecraft.src.Block;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Item;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;
import net.minecraft.src.RenderBlocks;
import net.minecraft.src.RenderEngine;
import net.minecraft.src.Tessellator;
import net.minecraftforge.client.IItemRenderer;

public class ItemRendererBotany implements IItemRenderer {

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return item.getItem().shiftedIndex == PluginBotany.itemBotany.shiftedIndex;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return false;
	}

	/**
	 * Called to render an item in a GUI inventory slot. If rendering as a 3D
	 * block, the appropriate OpenGL translations and scaling have already been
	 * applied, and the rendering should be done in local coordinates from
	 * (0,0,0)-(1,1,1). If rendering as a 2D texture, the rendering should be in
	 * GUI pixel coordinates from (0, 0, 0)-(16, 16, 0).
	 * 
	 * Data parameters: RenderBlocks render - The RenderBlocks instance
	 */

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		if (type == ItemRenderType.INVENTORY)
			renderItemInventory(item, (RenderBlocks) data[0]);
	}

	public void renderItemInventory(ItemStack item, RenderBlocks render) {

		if (!BotanyCore.flowerInterface.isFlower(item))
			return;

		IFlower flower = BotanyCore.flowerInterface.getFlower(item);

		Tessellator tessellator = Tessellator.instance;

		BinnieCore.proxy.bindTexture(flower.getFlowerGenome()
				.getPrimaryAsFlower().getTextureFile());

		int pos = flower.getFlowerGenome().getPrimaryAsFlower()
				.getPrimaryIconIndex() * 8;
		int age = flower.getAge();
		int x = age + pos % 16;
		int y = pos / 16;
		double xPos = (double) x / 16.0;
		double yPos = (double) y / 16.0;

		int colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getPrimaryColor();
		int r = colour / (256 * 256);
		colour = colour % (256 * 256);
		int g = colour / 256;
		colour = colour % 256;
		int b = colour;

		tessellator.startDrawingQuads();
		tessellator.setColorOpaque(r, g, b);
		tessellator.addVertexWithUV(0.0, 16.0, 0.0, xPos, yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 16.0, 0.0, xPos + 0.0625,
				yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 0.0, 0.0, xPos + 0.0625, yPos);
		tessellator.addVertexWithUV(0.0, 0.0, 0.0, xPos, yPos);
		tessellator.draw();

		pos = flower.getFlowerGenome().getPrimaryAsFlower()
				.getSecondaryIconIndex() * 8;
		age = flower.getAge();
		x = age + pos % 16;
		y = pos / 16;
		xPos = (double) x / 16.0;
		yPos = (double) y / 16.0;

		colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getSecondaryColor();
		r = colour / (256 * 256);
		colour = colour % (256 * 256);
		g = colour / 256;
		colour = colour % 256;
		b = colour;

		tessellator.startDrawingQuads();
		tessellator.setColorOpaque(r, g, b);
		tessellator.addVertexWithUV(0.0, 16.0, 0.0, xPos, yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 16.0, 0.0, xPos + 0.0625,
				yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 0.0, 0.0, xPos + 0.0625, yPos);
		tessellator.addVertexWithUV(0.0, 0.0, 0.0, xPos, yPos);
		tessellator.draw();

		BinnieCore.proxy.bindTexture(flower.getFlowerGenome().getStem()
				.getTextureFile());

		pos = flower.getFlowerGenome().getStem().getIconIndex() * 8;
		x = age + pos % 16;
		y = pos / 16;

		xPos = (double) x / 16.0;
		yPos = (double) y / 16.0;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(0.0, 16.0, 0.0, xPos, yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 16.0, 0.0, xPos + 0.0625,
				yPos + 0.0625);
		tessellator.addVertexWithUV(16.0, 0.0, 0.0, xPos + 0.0625, yPos);
		tessellator.addVertexWithUV(0.0, 0.0, 0.0, xPos, yPos);

		tessellator.draw();

	}

}
