package binnie.extrabees.gen;

import java.util.ArrayList;

import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeType;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IHiveDrop;
import forestry.api.genetics.IAllele;

public class HiveDrop implements IHiveDrop {

	private IAllele[] template;
	private ArrayList<ItemStack> additional = new ArrayList<ItemStack>();
	private int chance;

	public HiveDrop(IAlleleBeeSpecies species, int chance) {
		this(BeeManager.breedingManager.getBeeTemplate(species.getUID()),
				new ItemStack[0], chance);
	}

	public HiveDrop(IAllele[] template, ItemStack[] bonus, int chance) {
		if (template == null)
			template = BeeManager.breedingManager.getDefaultBeeTemplate();
		this.template = template;
		this.chance = chance;

		for (ItemStack stack : bonus)
			this.additional.add(stack);
	}

	@Override
	public ItemStack getPrincess(World world, int x, int y, int z, int fortune) {
		return BeeManager.beeInterface.getBeeStack(
				BeeManager.beeInterface.getBee(world,
						BeeManager.beeInterface.templateAsGenome(template)),
				EnumBeeType.PRINCESS);
	}

	@Override
	public ArrayList<ItemStack> getDrones(World world, int x, int y, int z,
			int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		ret.add(BeeManager.beeInterface.getBeeStack(
				BeeManager.beeInterface.getBee(world,
						BeeManager.beeInterface.templateAsGenome(template)),
				EnumBeeType.DRONE));
		return ret;
	}

	@Override
	public ArrayList<ItemStack> getAdditional(World world, int x, int y, int z,
			int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		for (ItemStack stack : additional)
			ret.add(stack.copy());

		return ret;
	}

	@Override
	public int getChance(World world, int x, int y, int z) {
		return chance;
	}

}
