package extrabees.liquids;

import extrabees.core.ExtraBeeCore;
import forge.ITextureProvider;
import java.util.ArrayList;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

public class ItemLiquidContainer extends Item implements ITextureProvider
{
    private String name;
    private int textBase;
    private int textOverlay;

    public ItemLiquidContainer(int i, String s, int j, int k)
    {
        super(i);
        maxStackSize = 64;
        setMaxDurability(0);
        a(true);
        name = s;
        textBase = j;
        textOverlay = k;
    }

    public String getTextureFile()
    {
        return ExtraBeeCore.getTextureFileLiquid();
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return (new StringBuilder()).append(ItemLiquid.liquids[itemstack.getData()].getItemDisplayName(itemstack)).append(" ").append(name).toString();
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        ItemLiquid.EnumType aenumtype[] = ItemLiquid.EnumType.values();
        int i = aenumtype.length;

        for (int j = 0; j < i; j++)
        {
            ItemLiquid.EnumType enumtype = aenumtype[j];
            arraylist.add(new ItemStack(this, 1, enumtype.ordinal()));
        }
    }

    public int func_46057_a(int i, int j)
    {
        if (j > 0)
        {
            return textOverlay;
        }
        else
        {
            return textBase;
        }
    }

    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }

    public int getColorFromDamage(int i, int j)
    {
        if (j == 0)
        {
            return 0xffffff;
        }
        else
        {
            return ItemLiquid.EnumType.values()[i].colour;
        }
    }
}
