package binnie.extrabees.fx;


import net.minecraft.src.EntityFX;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.machines.TileEntityGenepool;
import binnie.extrabees.machines.TileEntitySequencer;

public class EntityGenepoolFX extends EntityDNAFX {

	public EntityGenepoolFX(TileEntityGenepool tile) {

		super(tile);
		
		this.motionY = 0.05f;
		this.particleMaxAge = 16;

	}
	
	@Override
	public void onUpdate() {
		//motionY -= 0.005f;
	}

}
