package binnie.extrabees.products;

import binnie.extrabees.config.Config;
import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.ModLoader;

public class ExtraBeeProductsCore {

	public static void doInit() {
		ExtraBeeItem.honeyCrystal = new ItemHoneyCrystal(Config.Main.getItemID(
				"honeyCrystal", ExtraBeeItem.honeyCrystalID));
		ExtraBeeItem.honeyCrystalEmpty = new ItemHoneyCrystalEmpty(
				Config.Main.getItemID("honeyCrystalEmpty",
						ExtraBeeItem.honeyCrystalEmptyID));
		ExtraBeeItem.honeyDrop = new ItemHoneyDrop(Config.Main.getItemID(
				"honeyDrop", ExtraBeeItem.honeyDropID));
		ItemHoneyComb.setupCombs(Config.Main.getItemID("comb",
				ExtraBeeItem.combID));
		ExtraBeeItem.propolis = new ItemPropolis(Config.Main.getItemID(
				"propolis", ExtraBeeItem.propolisID));
		ItemHoneyDrop.addSubtypes();
		ItemHoneyComb.addSubtypes();
		ItemPropolis.addSubtypes();
		ModLoader.addName(ExtraBeeItem.honeyCrystal, "Honey Crystal");
		ModLoader.addName(ExtraBeeItem.honeyCrystalEmpty,
				"Depleted Honey Crystal");
	}

	public static void postInit() {

	}

}
