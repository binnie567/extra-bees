package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.button.ControlButton;
import binnie.craftgui.core.IWidget;

public class ControlScrollButton extends ControlButton {

	enum Position {
		Top, Bottom
	}

	public ControlScrollButton(IWidget parent, Position position) {
		super(parent, 0, 0, 0, 0, "");
		setSize((int) parent.getSize().x, (int) parent.getSize().x);
		if (position == Position.Top) {
			setPosition(0, 0);
		} else {
			setPosition(0, (int) (parent.getSize().y - parent.getSize().x));
		}

	}

}
