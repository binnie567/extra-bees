package binnie.extrabees.genetics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBranch;

public class ModuleGeneticsXML {
	
	public static void writeBeeToXML(IBeeGenome genome, BufferedWriter bw)
			throws IOException {

		bw.write("<species>\n");

		String name = "[Unnamed]";
		String uid = "[Unknown]";
		String branch = "None";
		int outline = 0xFFFFFF;
		int body = 0xFFFFFF;
		String binomial;
		String description = "";

		String authority;
		String temperature;
		String humidity;

		IAlleleBeeSpecies species = genome.getPrimaryAsBee();

		name = species.getName();

		uid = species.getUID();

		if (species.getBranch() != null) {
			branch = species.getBranch().getName();
		}

		outline = species.getPrimaryColor();
		body = species.getSecondaryColor();

		binomial = species.getBinomial();

		if (species.getDescription() != null)
			description = species.getDescription();

		authority = species.getAuthority();
		temperature = species.getTemperature().name;
		humidity = species.getHumidity().name;

		bw.write("<name>" + name + "</name>\n");

		bw.write("<uid>" + uid + "</uid>\n");

		bw.write("<branch>" + branch + "</branch>\n");

		bw.write("<outlineColour>" + outline + "</outlineColour>\n");

		bw.write("<bodyColour>" + body + "</bodyColour>\n");

		bw.write("<binomial>" + binomial + "</binomial>\n");

		bw.write("<description>" + description + "</description>\n");

		bw.write("<authority>" + authority + "</authority>\n");

		bw.write("<temperature>" + temperature + "</temperature>\n");

		bw.write("<humidity>" + humidity + "</humidity>\n");

		bw.write("<genome>" + "</genome>\n");

		bw.write("</species>\n");

	}

	public static void writeToXML() {
		List<IBeeGenome> noBranches = new ArrayList<IBeeGenome>();

		for (IAllele allele : AlleleManager.alleleRegistry
				.getRegisteredAlleles().values()) {
			if (allele instanceof IAlleleBeeSpecies) {
				IAllele[] template = BeeManager.breedingManager
						.getBeeTemplate(allele.getUID());
				if (template != null
						&& ((IAlleleSpecies) allele).getBranch() == null) {
					noBranches.add(BeeManager.beeInterface
							.templateAsGenome(template));
				}
			}
		}

		try {
			File file = new File("species.xml");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("<branches>\n");

			for (IBranch branch : AlleleManager.alleleRegistry
					.getRegisteredBranches().values()) {
				if (branch instanceof IBeeBranch) {
					bw.write("<branch>\n");
					bw.write("<name>" + branch.getName() + "</name>\n");
					bw.write("<uid>" + branch.getUID() + "</uid>\n");
					bw.write("<description>" + branch.getDescription()
							+ "</description>\n");
					bw.write("<scientific>" + branch.getScientific()
							+ "</scientific>\n");
					for (IAlleleSpecies allele : branch.getMembers()) {
						IAllele[] template = BeeManager.breedingManager
								.getBeeTemplate(allele.getUID());
						if (template != null) {
							writeBeeToXML(
									BeeManager.beeInterface
											.templateAsGenome(template),
									bw);
						}
					}
					bw.write("</branch>\n");
				}

			}

			bw.write("</branches>\n");

			bw.write("<nobranch>\n");
			for (IBeeGenome genome : noBranches) {
				writeBeeToXML(genome, bw);
			}
			bw.write("</nobranch>\n");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
