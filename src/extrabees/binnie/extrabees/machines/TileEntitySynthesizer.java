package binnie.extrabees.machines;

import net.minecraft.src.IInventory;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.genetics.ModuleEngineering;
import buildcraft.api.power.IPowerReceptor;

public class TileEntitySynthesizer extends TileEntityMachine implements
		ITankContainer, IInventory, IPowerReceptor {

	public static final int SlotSerum = 2;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Synthesizer.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Synthesizer;
	}

	public TileEntitySynthesizer() {
		super(25000, 500, 40000);

		addSlot(SlotSerum);

		getSlot(SlotSerum).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));

		this.addTank(0, new LiquidTank(8000));
	}

	@Override
	public void onFinishTask() {

		getSlot(SlotSerum).getItemStack().setItemDamage(
				getSlot(SlotSerum).getItemStack().getItemDamage() - 1);
		getTank(0).drain(100, true);

		ModuleEngineering.changeQuality(getSlot(SlotSerum).getItemStack(),
				-worldObj.rand.nextInt(2));

	}

	@Override
	public ErrorState canWork() {

		if (getSlot(SlotSerum).getItemStack() == null)
			return new ErrorState("No Serum");

		if (getSlot(SlotSerum).getItemStack().getItemDamage() == 0)
			return new ErrorState("Serum already Full");

		return super.canWork();
	}

	@Override
	public ErrorState canProgress() {
		if (!canDrain(0, 100))
			return new ErrorState("Not enough Liquid DNA");

		return super.canProgress();

	}

	public boolean canDrain(int index, int amount) {
		return getTank(index).drain(amount, false) != null
				&& getTank(index).drain(amount, false).amount == amount;
	}

	@Override
	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	@Override
	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}
}
