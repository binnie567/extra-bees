package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventCycleChanged extends Event {

	public EventCycleChanged(IWidget origin, int value) {
		super(origin);
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public int value;

}
