package binnie.extrabees.products;

import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import ic2.api.IElectricItem;

public class ItemHoneyCrystal extends Item implements IElectricItem {
	private int maxCharge = 8000;
	private int transferLimit = 500;
	private int tier = 1;

	public ItemHoneyCrystal(int id) {
		super(id);
		// setIconIndex(18);
		// setMaxDamage(13);
		setMaxDamage(27);
		setMaxStackSize(1);
		setTextureFile(ExtraBeeTexture.Main.getTexture());
		setCreativeTab(CreativeTabs.tabRedstone);
	}

	@Override
	public String getItemDisplayName(ItemStack i) {
		return "Honey Crystal";
	}

	public static NBTTagCompound getOrCreateNbtData(ItemStack itemStack) {
		NBTTagCompound ret = itemStack.getTagCompound();

		if (ret == null) {
			ret = new NBTTagCompound();

			itemStack.setTagCompound(ret);
		}

		return ret;
	}

	public static int charge(ItemStack itemStack, int amount, int tier,
			boolean ignoreTransferLimit, boolean simulate) {
		if ((!(itemStack.getItem() instanceof IElectricItem)) || (amount < 0)
				|| (itemStack.stackSize > 1))
			return 0;

		IElectricItem item = (IElectricItem) itemStack.getItem();

		if (item.getTier() > tier)
			return 0;
		if ((amount > item.getTransferLimit()) && (!ignoreTransferLimit))
			amount = item.getTransferLimit();

		NBTTagCompound nbtData = getOrCreateNbtData(itemStack);
		int charge = nbtData.getInteger("charge");

		if (amount > item.getMaxCharge() - charge)
			amount = item.getMaxCharge() - charge;
		charge += amount;

		if (!simulate) {
			nbtData.setInteger("charge", charge);

			itemStack.itemID = (charge > 0 ? item.getChargedItemId() : item
					.getEmptyItemId());

			if ((itemStack.getItem() instanceof IElectricItem)) {
				item = (IElectricItem) itemStack.getItem();

				if (itemStack.getMaxDamage() > 2)
					itemStack.setItemDamage(1 + (item.getMaxCharge() - charge)
							* (itemStack.getMaxDamage() - 2)
							/ item.getMaxCharge());
				else
					itemStack.setItemDamage(0);
			} else {
				itemStack.setItemDamage(0);
			}
		}

		return amount;
	}

	public static int discharge(ItemStack itemStack, int amount, int tier,
			boolean ignoreTransferLimit, boolean simulate) {
		if ((!(itemStack.getItem() instanceof IElectricItem)) || (amount < 0)
				|| (itemStack.stackSize > 1))
			return 0;

		IElectricItem item = (IElectricItem) itemStack.getItem();

		if (item.getTier() > tier)
			return 0;
		if ((amount > item.getTransferLimit()) && (!ignoreTransferLimit))
			amount = item.getTransferLimit();

		NBTTagCompound nbtData = getOrCreateNbtData(itemStack);
		int charge = nbtData.getInteger("charge");

		if (amount > charge)
			amount = charge;
		charge -= amount;

		if (!simulate) {
			nbtData.setInteger("charge", charge);

			itemStack.itemID = (charge > 0 ? item.getChargedItemId() : item
					.getEmptyItemId());

			if ((itemStack.getItem() instanceof IElectricItem)) {
				item = (IElectricItem) itemStack.getItem();

				if (itemStack.getMaxDamage() > 2)
					itemStack.setItemDamage(1 + (item.getMaxCharge() - charge)
							* (itemStack.getMaxDamage() - 2)
							/ item.getMaxCharge());
				else
					itemStack.setItemDamage(0);
			} else {
				itemStack.setItemDamage(0);
			}
		}

		return amount;
	}

	public static boolean canUse(ItemStack itemStack, int amount) {
		NBTTagCompound nbtData = getOrCreateNbtData(itemStack);

		return nbtData.getInteger("charge") >= amount;
	}

	@Override
	public boolean canProvideEnergy() {
		return true;
	}

	@Override
	public int getChargedItemId() {
		return this.shiftedIndex;
	}

	@Override
	public int getEmptyItemId() {
		if (this.shiftedIndex == ExtraBeeItem.honeyCrystal.shiftedIndex) {
			return ExtraBeeItem.honeyCrystalEmpty.shiftedIndex;
		}
		return this.shiftedIndex;
	}

	@Override
	public int getMaxCharge() {
		return this.maxCharge;
	}

	@Override
	public int getTier() {
		return this.tier;
	}

	@Override
	public int getTransferLimit() {
		return this.transferLimit;
	}

	@Override
	public int getIconFromDamage(int i) {
		return 18;
	}
}
