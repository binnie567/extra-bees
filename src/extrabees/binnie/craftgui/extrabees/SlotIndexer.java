package binnie.craftgui.extrabees;

import forestry.api.apiculture.BeeManager;
import binnie.craftgui.controls.CustomSlot;
import binnie.extrabees.machines.IIndexer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;

public class SlotIndexer extends CustomSlot {

	@Override
	public boolean isItemValid(ItemStack par1ItemStack) {
		return BeeManager.beeInterface.isBee(par1ItemStack);
	}

	int page = 0;
	int index = 0;

	public void setPage(int page) {
		this.page = page;
	}

	public SlotIndexer(IInventory par1iInventory, int par2, int par3, int par4,
			int pageLength) {
		super(par1iInventory, par2, par3, par4);
		this.index = par2;
		this.pageLength = pageLength;
	}

	int pageLength;

	@Override
	public int getSlotIndex() {
		return index + page * pageLength;
	}

	@Override
	public ItemStack decrStackSize(int par1) {
		return this.inventory.decrStackSize(getSlotIndex(), par1);
	}

	/**
	 * returns true if this slot is in par2 of par1
	 */
	@Override
	public boolean isSlotInInventory(IInventory par1IInventory, int par2) {
		return par1IInventory == this.inventory && par2 == this.getSlotIndex();
	}

	@Override
	public ItemStack getStack() {
		return ((IIndexer) this.inventory).getIndexerStackInSlot(this
				.getSlotIndex());
	}

	@Override
	public void putStack(ItemStack par1ItemStack) {
		this.inventory.setInventorySlotContents(this.getSlotIndex(),
				par1ItemStack);
		this.onSlotChanged();
	}

}
