package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesResultant extends PageSpecies {

	public PageSpeciesResultant(IWidget parent) {
		super(parent);

		pageSpeciesResultant_Title = new ControlText(this, 72, 8,
				"Resultant Mutations", ControlText.Alignment.Center);

		pageSpeciesResultant_List = new ControlMutationBox(this, 4, 20, 136,
				152, ControlMutationBox.Type.Resultant);
	}

	ControlText pageSpeciesResultant_Title;
	ControlMutationBox pageSpeciesResultant_List;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		pageSpeciesResultant_List.setSpecies(species);
	}

}
