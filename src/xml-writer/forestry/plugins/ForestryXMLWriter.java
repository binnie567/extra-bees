package forestry.plugins;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.ICommand;
import net.minecraft.src.World;
import cpw.mods.fml.common.network.IGuiHandler;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeMutation;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.core.IOreDictionaryHandler;
import forestry.api.core.IPacketHandler;
import forestry.api.core.IPickupHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ISaveEventHandler;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.EnumTolerance;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBranch;
import forestry.core.genetics.ClimateHelper;

public class ForestryXMLWriter implements IPlugin {

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
		writeToXML();
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
	}

	@Override
	public IGuiHandler getGuiHandler() {
		return null;
	}

	@Override
	public IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

	@Override
	public ISaveEventHandler getSaveEventHandler() {
		return null;
	}

	@Override
	public IOreDictionaryHandler getDictionaryHandler() {
		return null;
	}

	@Override
	public ICommand[] getConsoleCommands() {
		return null;
	}
	
	public ArrayList<EnumTemperature> getToleratedTemperatures(IBeeGenome genome) {
		EnumTemperature temperature = genome.getPrimaryAsBee().getTemperature();
		EnumTolerance temperatureTolerance = genome.getToleranceTemp();

		return ClimateHelper.getToleratedTemperature(temperature, temperatureTolerance);
	}
	
	public ArrayList<EnumHumidity> getToleratedHumdities(IBeeGenome genome) {
		EnumHumidity humidity = genome.getPrimaryAsBee().getHumidity();
		EnumTolerance humidityTolerance = genome.getToleranceHumid();

		return ClimateHelper.getToleratedHumidity(humidity, humidityTolerance);

	}
	
	public ArrayList<Integer> getSuitableBiomes(IBeeGenome genome) {
		ArrayList<EnumTemperature> toleratedTemperatures = getToleratedTemperatures(genome);
		ArrayList<EnumHumidity> toleratedHumidities = getToleratedHumdities(genome);
		
		
		ArrayList<Integer> biomeIdsTemp = new ArrayList<Integer>();
		for (EnumTemperature temp : toleratedTemperatures)
			biomeIdsTemp.addAll(EnumTemperature.getBiomeIds(temp));

		ArrayList<Integer> biomeIdsHumid = new ArrayList<Integer>();
		for (EnumHumidity humid : toleratedHumidities)
			biomeIdsHumid.addAll(EnumHumidity.getBiomeIds(humid));

		biomeIdsTemp.retainAll(biomeIdsHumid);

		return biomeIdsTemp;
	}
	
	public void writeBeeToXML(IBeeGenome genome, BufferedWriter bw)
			throws IOException {

		bw.write("<species>\n");

		String name = "[Unnamed]";
		String uid = "[Unknown]";
		String branch = "None";
		int outline = 0xFFFFFF;
		int body = 0xFFFFFF;
		String binomial;
		String description = "";

		String authority;
		String temperature;
		String humidity;

		IAlleleBeeSpecies species = genome.getPrimaryAsBee();

		name = species.getName();

		uid = species.getUID();

		if (species.getBranch() != null) {
			branch = species.getBranch().getName();
		}

		outline = species.getPrimaryColor();
		body = species.getSecondaryColor();

		binomial = species.getBinomial();

		if (species.getDescription() != null)
			description = species.getDescription();

		authority = species.getAuthority();
		temperature = species.getTemperature().name;
		humidity = species.getHumidity().name;

		bw.write("<name>" + name + "</name>\n");

		bw.write("<uid>" + uid + "</uid>\n");

		bw.write("<branch>" + branch + "</branch>\n");

		bw.write("<outlineColour>" + outline + "</outlineColour>\n");

		bw.write("<bodyColour>" + body + "</bodyColour>\n");

		bw.write("<binomial>" + binomial + "</binomial>\n");

		bw.write("<description>" + description + "</description>\n");

		bw.write("<authority>" + authority + "</authority>\n");

		bw.write("<temperature>" + temperature + "</temperature>\n");

		bw.write("<humidity>" + humidity + "</humidity>\n");

		bw.write("<genome>" + "</genome>\n");
		
		bw.write("<tolerance>\n");
		
		bw.write("<temperature\n>");
		
		for(EnumTemperature tempTol : getToleratedTemperatures(genome)) {
			bw.write("<"+tempTol.name+"/>\n");
		}
		
		bw.write("</temperature>\n");
		
		bw.write("<humidity>\n");
		
		for(EnumHumidity humidityTol : getToleratedHumdities(genome)) {
			bw.write("<"+humidityTol.name+"/>\n");
		}
		
		bw.write("</humidity>\n");
		
		bw.write("<biomes>\n");
		
		for(int i : getSuitableBiomes(genome)) {
			BiomeGenBase biome = BiomeGenBase.biomeList[i];
			bw.write("<"+biome.biomeName.replaceAll(" ", "")+"/>\n");
		}
		
		bw.write("</biomes>\n");
		
		bw.write("</tolerance>\n");

		bw.write("</species>\n");

	}

	public void writeToXML() {
		List<IBeeGenome> noBranches = new ArrayList<IBeeGenome>();

		for (IAllele allele : AlleleManager.alleleRegistry
				.getRegisteredAlleles().values()) {
			if (allele instanceof IAlleleBeeSpecies) {
				IAllele[] template = BeeManager.breedingManager
						.getBeeTemplate(allele.getUID());
				if (template != null
						&& ((IAlleleSpecies) allele).getBranch() == null) {
					noBranches.add(BeeManager.beeInterface
							.templateAsGenome(template));
				}
			}
		}

		try {

			String content = "This is the content to write into file";

			File file = new File("species.xml");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("<branches>\n");

			for (IBranch branch : AlleleManager.alleleRegistry
					.getRegisteredBranches().values()) {
				if (branch instanceof IBeeBranch) {
					bw.write("<branch>\n");
					bw.write("<name>" + branch.getName() + "</name>\n");
					bw.write("<uid>" + branch.getUID() + "</uid>\n");
					bw.write("<description>" + branch.getDescription()
							+ "</description>\n");
					bw.write("<scientific>" + branch.getScientific()
							+ "</scientific>\n");
					for (IAlleleSpecies allele : branch.getMembers()) {
						IAllele[] template = BeeManager.breedingManager
								.getBeeTemplate(allele.getUID());
						if (template != null) {
							writeBeeToXML(
									BeeManager.beeInterface
											.templateAsGenome(template),
									bw);
						}
					}
					bw.write("</branch>\n");
				}

			}

			bw.write("</branches>\n");

			bw.write("<nobranch>\n");
			for (IBeeGenome genome : noBranches) {
				writeBeeToXML(genome, bw);
			}
			bw.write("</nobranch>\n");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeMutationsToXML() {

		List<IBeeMutation> templates = new ArrayList<IBeeMutation>();
		templates.addAll(BeeManager.beeMutations);

		try {

			File file = new File("bee-mutations.xml");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			bw.write("<!DOCTYPE root-element SYSTEM \"bee-mutations.dtd\">\n");
			bw.write("<mutations>\n");
			for (IBeeMutation mutation : templates) {

				IAlleleBeeSpecies species0 = (IAlleleBeeSpecies) mutation
						.getAllele0();
				IAlleleBeeSpecies species1 = (IAlleleBeeSpecies) mutation
						.getAllele1();
				IAlleleBeeSpecies speciesResult = (IAlleleBeeSpecies) mutation
						.getTemplate()[0];
				int chance = mutation.getBaseChance();

				bw.write("<mutation>\n");

				bw.write("<species-0>");
				bw.write("<uid>" + species0.getUID() + "</uid>");
				bw.write("</species-0>");
				bw.write("<species-1>");
				bw.write("<uid>" + species1.getUID() + "</uid>");
				bw.write("</species-1>");
				bw.write("<species-result>");
				bw.write("<uid>" + speciesResult.getUID() + "</uid>");
				bw.write("</species-result>");
				bw.write("<basechance>");
				bw.write("<value>" + chance + "</value>");
				bw.write("</basechance>");

				bw.write("</mutation>\n");
			}

			bw.write("</mutations>\n");

			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
