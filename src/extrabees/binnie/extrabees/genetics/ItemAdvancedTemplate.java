package binnie.extrabees.genetics;

import java.util.List;

import binnie.core.BinnieCore;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IGenome;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class ItemAdvancedTemplate extends ItemAdvancedTemplateBlank {

	public ItemAdvancedTemplate(int id) {
		super(id);
		setMaxStackSize(1);
		setIconIndex(36);
		setMaxDamage(32);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer,
			List list, boolean par4) {
		super.addInformation(itemStack, entityPlayer, list, par4);

		int quality = getQuality(itemStack);

		Quality type = Quality.Average;

		switch (quality) {
		case 0:
		case 1:
			type = Quality.Awful;
			break;

		case 2:
		case 3:
			type = Quality.Poor;
			break;

		case 7:
		case 8:
			type = Quality.Good;
			break;

		case 9:
		case 10:
			type = Quality.Excellent;
			break;

		default:
			break;
		}

		list.add(type.color + "\u00A7o" + type.toString() + " Quality \u00A7r");

		IGenome genome = getGenome(itemStack);
		if (genome != null) {
			IBee bee = BeeManager.beeInterface.getBee(
					BinnieCore.proxy.getWorld(), (IBeeGenome) genome);
			bee.analyze();
			bee.addTooltip(list);
		}

	}

	@Override
	public int getColorFromItemStack(ItemStack itemstack, int j) {
		int color = getColor(itemstack);
		if (j == 0) {
			return 0xFFFFFF;
		}
		return color;
	}

	// Image ID's
	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		if (j > 0) {
			return 37;
		}

		return 36;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		IAlleleSpecies species = getGenome(itemstack).getPrimary();

		if (species == null)
			return "Corrupt Template";

		String contents = "";
		if (itemstack.getItemDamage() == 0)
			contents = "Empty ";

		return contents + species.getName() + " Template";

	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		// for (IAllele allele : AlleleManager.alleleRegistry
		// .getRegisteredAlleles().values()) {
		// if ((allele instanceof IAlleleBeeSpecies)
		// && (!((IAlleleSpecies) allele).isSecret())) {
		// ItemStack stack = new ItemStack(this, 1, 0);
		//
		// stack.setItemDamage(31);
		//
		// NBTTagCompound nbt = new NBTTagCompound();
		// stack.writeToNBT(nbt);
		// nbt.setInteger("quality", 5);
		// nbt.setString("name", "Pure " + ((IAlleleSpecies)allele).getName());
		// nbt.setInteger("color", ((IAlleleSpecies)allele).getPrimaryColor());
		//
		// NBTTagCompound genomeNBT = nbt.getCompoundTag("genome");
		// IGenome genome =
		// BeeManager.beeInterface.templateAsGenome(BeeManager.breedingManager.getBeeTemplate(
		// ((IAlleleSpecies)allele).getUID()));
		// genome.writeToNBT(genomeNBT);
		// nbt.setCompoundTag("genome", genomeNBT);
		// stack.setTagCompound(nbt);
		// itemList.add(stack);
		// }
		//
		// }
	}

	@Override
	public boolean hasEffect(ItemStack itemstack) {
		IAlleleSpecies species = getGenome(itemstack).getPrimary();
		if (species != null)
			return species.hasEffect();
		else
			return false;
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	public static int getQuality(ItemStack item) {
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("quality"))
			return nbt.getInteger("quality");
		else
			return 5;
	}

	public static int getColor(ItemStack item) {
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("color"))
			return nbt.getInteger("color");
		else
			return 0xFFFFFF;
	}

	public static String getName(ItemStack item) {
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("name"))
			return nbt.getString("name");
		else
			return "Unnamed Template";
	}

	enum Quality {
		Awful("\u00A7c"), Poor("\u00A76"), Average("\u00A7e"), Good("\u00A7a"), Excellent(
				"\u00A7b"), ;
		String color;

		Quality(String color) {
			this.color = color;
		}
	}

	public static IGenome getGenome(ItemStack item) {
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("genome")) {
			NBTTagCompound genomeNBT = nbt.getCompoundTag("genome");
			IGenome genome = BeeManager.beeInterface
					.templateAsGenome(BeeManager.breedingManager
							.getDefaultBeeTemplate());
			genome.readFromNBT(genomeNBT);
			return genome;
		}

		else
			return null;
	}

}
