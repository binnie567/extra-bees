package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntitySequencer;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAlleleSpecies;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowSequencer extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntitySequencer machine;

	ControlSlot slotBee;
	ControlSlotArray slotReserve;

	ControlSlot slotTemplate;
	ControlSlot slotTemplateFinished;

	ControlExtraBeeProgress sequenceProgress;
	ControlExtraBeeProgress dnaProgress;

	ControlText speciesUID;

	public WindowSequencer(EntityPlayer player, IInventory inventory, Side side) {
		super(244, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntitySequencer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowSequencer(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Sequencer");

		playerInventory = new ControlPlayerInventory(this);

		speciesUID = new ControlText(this, 125, 67, "",
				ControlText.Alignment.Center);
		speciesUID.setColour(0xA0A0A0);

		sequenceProgress = new ControlExtraBeeProgress(this, 79, 53, 98, 9, 64,
				114, 64, 123, ControlExtraBeeProgress.Direction.Right);
		dnaProgress = new ControlExtraBeeProgress(this, 186, 24, 45, 72, 64,
				42, 109, 42, ControlExtraBeeProgress.Direction.Up);

		slotBee = new ControlSlot(this, 55, 50);

		slotReserve = new ControlSlotArray(this, 11, 32, 2, 3);

		slotTemplate = new ControlSlot(this, 199, 78);

		slotTemplateFinished = new ControlSlot(this, 199, 24);

		slotBee.create(machine, TileEntitySequencer.SlotBee);
		slotReserve.create(machine, TileEntitySequencer.SlotReserves[0]);
		slotTemplate.create(machine, TileEntitySequencer.SlotTemplateBlank);
		slotTemplateFinished.create(machine,
				TileEntitySequencer.SlotTemplateWritten);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		new ControlErrorState(this, 70, 80);

	}

	@Override
	public void onUpdate() {
		sequenceProgress.setProgress(machine.getCurrentProgress() / 100.0f);
		dnaProgress.setProgress(machine.DNAProgress / 100.0f);
		String uid = machine.SpeciesUID;
		if (AlleleManager.alleleRegistry.getAllele(uid) == null)
			speciesUID.setText("None");
		else
			speciesUID.setText(((IAlleleSpecies) AlleleManager.alleleRegistry
					.getAllele(uid)).getName());
	}

};
