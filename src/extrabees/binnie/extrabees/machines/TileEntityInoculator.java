package binnie.extrabees.machines;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.genetics.ItemSerum;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityInoculator extends TileEntityMachine implements
		IInventory, IPowerReceptor {

	public static final int SlotBee = 2;

	public static final int[] SlotBeeReserve = new int[] { 3, 4, 5, 6, 7, 8 };

	public static final int[] SlotBeeDone = new int[] { 9, 10, 11, 12, 13, 14 };

	public static final int SlotSerum = 15;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Inoculator.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Inoculator;
	}

	public TileEntityInoculator() {
		super(50000, 2000, 75000);

		addSlot(SlotBee);
		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));

		addSlot(SlotSerum);
		getSlot(SlotSerum).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));

		addSlotArray(SlotBeeReserve);

		for (int slot : SlotBeeReserve) {
			getSlot(slot).setValidator(
					new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		}

		addSlotArray(SlotBeeDone);

		for (int slot : SlotBeeDone) {
			getSlot(slot).setReadOnly();
			getSlot(slot).setValidator(
					new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		}

	}

	public InventorySlot[] getFreeBeeDoneSlots() {
		List<InventorySlot> slots = new ArrayList<InventorySlot>();
		for (InventorySlot slot : getSlots(SlotBeeDone)) {
			if (slot.getItemStack() == null)
				slots.add(slot);
		}
		return slots.toArray(new InventorySlot[0]);
	}

	public InventorySlot[] getBeeReserveSlots() {
		List<InventorySlot> slots = new ArrayList<InventorySlot>();
		for (InventorySlot slot : getSlots(SlotBeeReserve)) {
			if (slot.getItemStack() != null)
				slots.add(slot);
		}
		return slots.toArray(new InventorySlot[0]);
	}

	@Override
	public void onFinishTask() {
		String UID = ItemSerum.getUID(getSlot(SlotSerum).getItemStack());
		IAllele allele = AlleleManager.alleleRegistry.getAllele(UID);
		IBee bee = BeeManager.beeInterface.getBee(getSlot(SlotBee)
				.getItemStack());
		if (allele == null || bee == null)
			return;

		int chromosomeID = getSlot(SlotSerum).getItemStack().getTagCompound()
				.getInteger("chromosome");
		;

		int id = worldObj.rand.nextBoolean() ? 1 : 0;

		ItemStack beeItem = getSlot(SlotBee).getItemStack().copy();

		NBTTagCompound beeNBT = beeItem.getTagCompound();

		NBTTagCompound genomeNBT = beeNBT.getCompoundTag("Genome");

		NBTTagList chromosomes = genomeNBT.getTagList("Chromosomes");

		NBTTagCompound chromosome = (NBTTagCompound) chromosomes
				.tagAt(chromosomeID);

		chromosome.setString("UID" + id, allele.getUID());

		beeItem.setTagCompound(beeNBT);

		getSlot(SlotSerum).getItemStack().damageItem(1, null);

		getSlot(SlotBee).setItemStack(beeItem);

		if (getFreeBeeDoneSlots().length > 0 && !canInoculate()) {
			ItemStack transf = getSlot(SlotBee).getItemStack().copy();
			getSlot(SlotBee).setItemStack(null);
			getFreeBeeDoneSlots()[0].setItemStack(transf);
			if (getBeeReserveSlots().length > 0) {
				ItemStack transf2 = getBeeReserveSlots()[0].getItemStack()
						.copy();
				getBeeReserveSlots()[0].setItemStack(null);
				getSlot(SlotBee).setItemStack(transf2);
			}
		}

		this.onInventoryChanged();
	}

	@Override
	public ErrorState canWork() {

		if (getSlot(SlotSerum).getItemStack() == null)
			return new ErrorState("No Serum");

		if (getSlot(SlotBee).getItemStack() == null)
			return new ErrorState("No Bee");

		if (getSlot(SlotSerum).getItemStack().getItemDamage() >= ExtraBeeItem.serum
				.getMaxDamage())
			return new ErrorState("Serum is Empty");

		if (!canInoculate())
			return new ErrorState("Can't innoculate current bee");

		if (getFreeBeeDoneSlots().length == 0)
			return new ErrorState("No Space for Inoculated Bees");

		return super.canWork();

	}

	public boolean canInoculate() {
		if (getSlot(SlotSerum).getItemStack() == null
				|| getSlot(SlotBee).getItemStack() == null)
			return false;

		ItemStack serum = getSlot(SlotSerum).getItemStack();
		String uid = ItemSerum.getUID(serum);
		EnumBeeChromosome chromosome = ItemSerum.getChromosome(serum);
		IAllele allele = AlleleManager.alleleRegistry.getAllele(uid);
		if (allele == null || chromosome == null)
			return false;

		IBee bee = BeeManager.beeInterface.getBee(getSlot(SlotBee)
				.getItemStack());
		IBeeGenome genome = bee.getGenome();
		return genome.getActiveAllele(chromosome.ordinal()) != allele
				|| genome.getInactiveAllele(chromosome.ordinal()) != allele;

	}

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();

		if (getSlot(SlotBee).getItemStack() == null
				&& getBeeReserveSlots().length > 0) {
			ItemStack transf3 = getBeeReserveSlots()[0].getItemStack().copy();
			getBeeReserveSlots()[0].setItemStack(null);
			getSlot(SlotBee).setItemStack(transf3);
			return;
		}
	}

}
