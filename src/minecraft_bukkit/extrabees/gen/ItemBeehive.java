package extrabees.gen;

import java.util.ArrayList;
import net.minecraft.server.ItemBlock;
import net.minecraft.server.ItemStack;

public class ItemBeehive extends ItemBlock
{
    public ItemBeehive(int i)
    {
        super(i);
        setMaxDurability(0);
        a(true);
    }

    /**
     * Returns the metadata of the block which this Item (ItemBlock) can place
     */
    public int filterData(int i)
    {
        return i;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return a(itemstack);
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        for (int i = 0; i < 4; i++)
        {
            arraylist.add(new ItemStack(this, 1, i));
        }
    }

    public String a(ItemStack itemstack)
    {
        switch (itemstack.getData())
        {
            case 0:
                return "Water Hive";

            case 1:
                return "Rock Hive";

            case 2:
                return "Nether Hive";

            case 3:
                return "Marble Hive";
        }

        return "??? Hive";
    }
}
