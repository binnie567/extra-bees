package extrabees.liquids;

import forestry.api.core.ItemInterface;
import forestry.api.liquids.*;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

public class EBLiquidContainer extends LiquidContainer
{
    public EBLiquidContainer(ItemLiquid.EnumType enumtype, int i, Item item, ItemStack itemstack)
    {
        super(new LiquidStack(ItemLiquid.liquids[enumtype.ordinal()].id, i, 0), new ItemStack(item, 1, enumtype.ordinal()), itemstack, itemstack.getItem() == Item.BUCKET);
        LiquidManager.registerLiquidContainer(this);

        if (itemstack == ItemInterface.getItem("waxCapsule"))
        {
            ExtraBeeLiquidsCore.injectWaxContainer(this);
        }
        else if (itemstack == ItemInterface.getItem("refractoryEmpty"))
        {
            ExtraBeeLiquidsCore.injectRefractoryContainer(this);
        }
        else if (itemstack == ItemInterface.getItem("canEmpty"))
        {
            ExtraBeeLiquidsCore.injectTinContainer(this);
        }
        else
        {
            ExtraBeeLiquidsCore.injectLiquidContainer(this);
        }
    }
}
