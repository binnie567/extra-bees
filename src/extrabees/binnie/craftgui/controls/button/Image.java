package binnie.craftgui.controls.button;

public class Image {

	public Image(String textureFile, int x, int y, int width, int height,
			int u, int v) {
		super();
		this.textureFile = textureFile;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.u = u;
		this.v = v;
	}

	String textureFile;
	int x;
	int y;
	int width;
	int height;
	int u;
	int v;

}
