package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;

import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.controls.button.ControlButton;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowNEIBreeder extends Window {

	//
	// @Event(event = Widget.eventMouseClicked)
	// public void onMouseClickInChild(IWidget child, Object[] parameters) {
	// if (child == guiButton) {
	// ItemStack[] stacks = new ItemStack[] { slotDrone.getItemStack(),
	// slotPrincess.getItemStack() };
	//
	// boolean hasDrone = false;
	// IBee drone = null;
	//
	// boolean hasQueen = false;
	// IBee queen = null;
	//
	// boolean hasPrincess = false;
	// IBee princess = null;
	//
	// for (ItemStack stack : stacks) {
	// if (BeeManager.beeInterface.isDrone(stack) && !hasDrone) {
	// hasDrone = true;
	// drone = BeeManager.beeInterface.getBee(stack);
	// } else if (BeeManager.beeInterface.isMated(stack) && !hasQueen) {
	// hasQueen = true;
	// queen = BeeManager.beeInterface.getBee(stack);
	// } else if (!BeeManager.beeInterface.isDrone(stack)
	// && !BeeManager.beeInterface.isMated(stack)
	// && BeeManager.beeInterface.isBee(stack) && !hasPrincess) {
	// hasPrincess = true;
	// princess = BeeManager.beeInterface.getBee(stack);
	// }
	// }
	//
	// if (hasPrincess && princess != null & hasDrone & drone != null) {
	// princess.mate(drone);
	// hasQueen = true;
	// queen = princess;
	// }
	//
	// if (hasQueen && queen != null) {
	// ArrayList<ItemStack> output = new ArrayList<ItemStack>();
	// EntityPlayer player = getPlayer();
	// BiomeGenBase biome = player.worldObj.getBiomeGenForCoords(
	// (int) player.posX, (int) player.posZ);
	//
	// /*
	//
	// for (IBee droneOutput : queen.spawnDrones(player.worldObj,
	// biome.biomeID, (int) player.posX, (int) player.posY,
	// (int) player.posZ)) {
	// droneOutput.analyze();
	// output.add(BeeManager.beeInterface.getBeeStack(droneOutput,
	// EnumBeeType.DRONE));
	// }
	// IBee princessOutput = queen.spawnPrincess(player.worldObj,
	// biome.biomeID, (int) player.posX, (int) player.posY,
	// (int) player.posZ);
	//
	// */
	//
	// //princessOutput.analyze();
	// //output.add(BeeManager.beeInterface.getBeeStack(princessOutput,
	// // EnumBeeType.PRINCESS));
	// slotOutput.setItemStacks(output.toArray(new ItemStack[0]));
	// }
	//
	// }
	// }

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	ControlSlot slotPrincess;
	ControlSlot slotDrone;
	ControlSlotArray slotOutput;

	ControlButton guiButton;

	public WindowNEIBreeder(EntityPlayer player, Side side) {
		super(176, 128, player, null, side);
		this.player = player;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null)
			return null;
		return new WindowNEIBreeder(player, side);
	}

	@Override
	public void initialize() {

		setTitle("Automatic Breeder");

		playerInventory = new ControlPlayerInventory(this);
		slotDrone = new ControlSlot(this, 7, 23);

		slotPrincess = new ControlSlot(this, 29, 23);

		slotOutput = new ControlSlotArray(this, 61, 23, 6, 1);

		guiButton = new ControlButton(this, 50, 28, 8, 8, "");

		slotDrone.create(getWindowInventory(), 0);
		slotPrincess.create(getWindowInventory(), 1);
		slotOutput.create(getWindowInventory(), 2);
	}

	@Override
	public void onClose() {
		playerInventory.addInventory(getWindowInventory());
	}

};
