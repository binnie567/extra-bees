package binnie.botany.core;

import java.util.List;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import net.minecraft.src.Block;
import net.minecraft.src.BlockDirt;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;

public class BlockLoam extends BlockDirt {

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List par3List) {
		for (int i = 0; i < 9; i++) {
			par3List.add(new ItemStack(this, 1, i));
		}
	}

	@Override
	public int getBlockTextureFromSideAndMetadata(int par1, int meta) {
		if (meta >= 3)
			meta += 13;

		if (meta >= 19)
			meta += 13;

		return meta;
	}

	public BlockLoam(int id) {
		super(id, 0);
		setTextureFile("/gfx/botany/botany-blocks.png");
	}

	public static String getName(int meta) {
		String name = "";
		int pH = meta / 3;
		int moisture = meta % 3;
		if (moisture == 0)
			name += "Moist ";
		else if (moisture == 2)
			name += "Dry ";
		if (pH == 0)
			name += "Acidic ";
		else if (pH == 2)
			name += "Alkaline ";
		name += "Loam";
		return name;
	}

}
