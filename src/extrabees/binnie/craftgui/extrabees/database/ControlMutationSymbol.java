package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeMutation;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.extrabees.VirtualBeeHousing;
import binnie.extrabees.genetics.ModuleGenetics;

public class ControlMutationSymbol extends Control implements ITooltip {

	@Override
	public void onRenderBackground() {
		super.onRenderBackground();
		getRenderer().setColour(getColour());
		getRenderer().renderRect(0, 0, 16 + type * 16, 16, type * 16, 0);
	}

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/extrabees-gui.png";
	}

	protected ControlMutationSymbol(IWidget parent, int x, int y, int type) {
		super(parent, x, y, 16+type * 16, 16);
		this.value = null;
		this.type = type;
		this.canMouseOver = true;
	}

	public void setValue(IBeeMutation value) {
		this.value = value;
		this.discovered = ModuleGenetics.isMutationDiscovered(value, getWindow().getWorld(), 
				((WindowDictionary)getSuperParent()).isNEI());
		if(discovered)
			setColour(0xFFFFFF);
		else
			setColour(0x777777);
	}

	IBeeMutation value;
	boolean discovered;
	int type;

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();
		if (type == 1 && discovered) {
			IAlleleBeeSpecies species1 = (IAlleleBeeSpecies) value.getAllele0();
			IAlleleBeeSpecies species2 = (IAlleleBeeSpecies) value.getAllele1();
			int chance = value.getChance(new VirtualBeeHousing(getWindow()
					.getPlayer()), species1, species2, BeeManager.beeInterface
					.templateAsGenome(BeeManager.breedingManager
							.getBeeTemplate(species1.getUID())),
					BeeManager.beeInterface
							.templateAsGenome(BeeManager.breedingManager
									.getBeeTemplate(species2.getUID())));
			tooltip.add("Current Chance - " + chance + "%");
		}
		return tooltip;
	}

}
