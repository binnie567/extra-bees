package binnie.logicpipes;

public interface ILogicStatement {
	
	ILogicTrigger getTrigger();
	ILogicAction getAction();
	boolean isInverse();

}
