package binnie.extrabees.pipes;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import binnie.core.BinnieCore;
import buildcraft.api.transport.PipeManager;
import buildcraft.transport.BlockGenericPipe;
import buildcraft.transport.Pipe;

public class ExtraBeePipes {

	public static void initPipes() {
		//ExtraBeePipesCore.pipeItemsRandom = createPipe(5253,
		//		PipeItemsRandom.class, "Random Extraction Pipe");
		//ExtraBeePipesCore.pipeItemsRedstoneExtraction = createPipe(5254,
		//		PipeItemsRedstoneExtraction.class, "Redstone Extraction Pipe");
		ExtraBeePipesCore.pipeItemsRedstoneExtraction = createPipe(5255,
		PipeItemsLogic.class, "Logic Pipe");
		//PipeManager.registerExtractionHandler(new RedstoneExtractionHandler());
	}

	public static Item createPipe(int id, Class<? extends Pipe> clas,
			String description) {

		Item pipe = BlockGenericPipe.registerPipe(id, clas);
		pipe.setCreativeTab(CreativeTabs.tabMisc);
		pipe.setItemName(clas.getSimpleName());
		BinnieCore.proxy.createPipe(pipe);
		return pipe;
	}

}
