package binnie.extrabees.machines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import binnie.core.network.PacketPayload;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.SetList;
import binnie.extrabees.network.PacketIndexerChanged;

import forestry.api.apiculture.BeeManager;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;

public class TileEntityIndexer extends TileEntityMachine implements IIndexer {

	@Override
	public void writeToPacket(PacketPayload data) {
		super.writeToPacket(data);
	}

	@Override
	public void readFromPacket(PacketPayload data) {
		super.readFromPacket(data);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		NBTTagList indexerNBT = new NBTTagList();

		for (ItemStack item : this.indexerInventory) {
			NBTTagCompound itemNBT = new NBTTagCompound();
			item.writeToNBT(itemNBT);
			indexerNBT.appendTag(itemNBT);
		}

		nbttagcompound.setTag("indexer", indexerNBT);

	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);

		NBTTagList indexerNBT = nbttagcompound.getTagList("indexer");

		indexerInventory.clear();

		for (int i = 0; i < indexerNBT.tagCount(); i++) {
			NBTTagCompound itemNBT = (NBTTagCompound) indexerNBT.tagAt(i);
			indexerInventory.add(ItemStack.loadItemStackFromNBT(itemNBT));
		}

		needsSorting = true;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		Sort();
	}

	List<ItemStack> indexerInventory = new SetList<ItemStack>();
	List<Integer> sortedInventory = new SetList<Integer>();

	public enum Mode {
		None, Species, Type, ;
	}

	Mode sortingMode = Mode.None;

	boolean needsSorting = true;

	public void Sort() {

		for (int i = 0; i < indexerInventory.size();) {
			if (indexerInventory.get(i) == null)
				indexerInventory.remove(i);
			else
				i++;
		}

		if (!needsSorting) {
			return;
		} else {
			needsSorting = false;
		}

		switch (sortingMode) {
		case Species:
		case Type: {
			class SpeciesList {
				public List<Integer> drones = new ArrayList<Integer>();
				public List<Integer> queens = new ArrayList<Integer>();
				public List<Integer> princesses = new ArrayList<Integer>();
				public List<ItemStack> bees = new ArrayList<ItemStack>();

				public void add(ItemStack stack) {
					bees.add(stack);
				}
			}

			Map<Integer, SpeciesList> speciesList = new HashMap<Integer, SpeciesList>();

			for (ItemStack itemStack : indexerInventory) {
				int species = itemStack.getItemDamage();
				if (!speciesList.containsKey(species)) {
					speciesList.put(species, new SpeciesList());
				}
				speciesList.get(species).add(itemStack);
			}

			for (SpeciesList sortableList : speciesList.values()) {
				for (ItemStack beeStack : sortableList.bees) {
					if (BeeManager.beeInterface.isDrone(beeStack)) {
						sortableList.drones.add(indexerInventory
								.indexOf(beeStack));
					} else if (BeeManager.beeInterface.isMated(beeStack)) {
						sortableList.queens.add(indexerInventory
								.indexOf(beeStack));
					} else {
						sortableList.princesses.add(indexerInventory
								.indexOf(beeStack));
					}
				}
			}
			sortedInventory = new SetList<Integer>();

			switch (sortingMode) {
			case Species:
				for (int i = 0; i < 1024; i++) {
					if (speciesList.containsKey(i)) {
						sortedInventory.addAll(speciesList.get(i).queens);
						sortedInventory.addAll(speciesList.get(i).princesses);
						sortedInventory.addAll(speciesList.get(i).drones);
					}
				}
				break;
			case Type:
				for (int i = 0; i < 1024; i++) {
					if (speciesList.containsKey(i)) {
						sortedInventory.addAll(speciesList.get(i).queens);
					}
				}
				for (int i = 0; i < 1024; i++) {
					if (speciesList.containsKey(i)) {
						sortedInventory.addAll(speciesList.get(i).princesses);
					}
				}
				for (int i = 0; i < 1024; i++) {
					if (speciesList.containsKey(i)) {
						sortedInventory.addAll(speciesList.get(i).drones);
					}
				}
				break;
			default:
				break;
			}

		}
			break;
		default:
			sortedInventory.clear();
			for (int i = 0; i < indexerInventory.size(); i++)
				sortedInventory.add(i);
		}
	}

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Indexer.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Indexer;
	}

	@Override
	public int getSizeInventory() {
		return indexerInventory.size() + 39;
	}

	@Override
	public ItemStack getStackInSlot(int index) {

		if (index >= 0 && index < indexerInventory.size())
			return indexerInventory.get(index);
		return null;
	}

	@Override
	public ItemStack getIndexerStackInSlot(int index) {

		if (index >= 0 && index < sortedInventory.size())
			return sortedInventory.get(index) < indexerInventory.size() ? indexerInventory
					.get(sortedInventory.get(index)) : null;
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {

		if (index >= 0) {
			ItemStack returnStack = getStackInSlot(index).copy();
			setInventorySlotContents(index, null);
			return returnStack;
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack itemStack) {

		if (index >= 0 && index < indexerInventory.size()) {
			indexerInventory.set(index, itemStack);
		} else if (itemStack != null) {
			indexerInventory.add(itemStack);
		}

		this.needsSorting = true;
		onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {

	}

	@Override
	public void closeChest() {

	}

	public void setMode(Mode mode) {
		this.sortingMode = mode;

		this.needsSorting = true;
	}

	public Mode getMode() {
		return sortingMode;
	}

	@Override
	public ItemStack shiftClick(ItemStack stack) {
		if (BeeManager.beeInterface.isBee(stack)) {
			setInventorySlotContents(this.indexerInventory.size(), stack.copy());
			return null;
		} else {
			return stack;
		}

	}

	public int getNumberOfPages(int i) {
		return 1 + (indexerInventory.size() / 39);
	}

	public void loadFromPacket(PacketIndexerChanged packet) {
		// TODO Auto-generated method stub

	}

}
