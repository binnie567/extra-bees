package binnie.extrabees.platform;

import binnie.core.BinnieCore;
import net.minecraft.src.RenderEngine;
import cpw.mods.fml.client.FMLTextureFX;

public class TextureLiquidsFX extends FMLTextureFX {
	private final String texture;
	private final int rMin;
	private final int rMax;
	private final int gMin;
	private final int gMax;
	private final int bMin;
	private final int bMax;
	protected float[] red;
	protected float[] green;
	protected float[] blue;
	protected float[] alpha;

	public TextureLiquidsFX(int rMin, int rMax, int gMin, int gMax, int bMin,
			int bMax, int iconIndex, String textureFile) {
		super(iconIndex);
		this.texture = textureFile;
		this.rMin = rMin;
		this.rMax = rMax;
		this.gMin = gMin;
		this.gMax = gMax;
		this.bMin = bMin;
		this.bMax = bMax;
		setup();
	}

	@Override
	public void setup() {
		super.setup();
		this.red = new float[this.tileSizeSquare];
		this.green = new float[this.tileSizeSquare];
		this.blue = new float[this.tileSizeSquare];
		this.alpha = new float[this.tileSizeSquare];
	}

	@Override
	public void bindImage(RenderEngine renderengine) {
		BinnieCore.proxy.bindTexture(this.texture);
	}

	@Override
	public void onTick() {
		for (int var1 = 0; var1 < this.tileSizeBase; var1++) {
			for (int var2 = 0; var2 < this.tileSizeBase; var2++) {
				float var3 = 0.0F;

				for (int var4 = var1 - 1; var4 <= var1 + 1; var4++) {
					int r = var4 & this.tileSizeMask;
					int g = var2 & this.tileSizeMask;
					var3 += this.red[(r + g * this.tileSizeBase)];
				}

				this.green[(var1 + var2 * this.tileSizeBase)] = (var3 / 3.3F + this.blue[(var1 + var2
						* this.tileSizeBase)] * 0.8F);
			}
		}

		for (int var1 = 0; var1 < this.tileSizeBase; var1++) {
			for (int var2 = 0; var2 < this.tileSizeBase; var2++) {
				this.blue[(var1 + var2 * this.tileSizeBase)] += this.alpha[(var1 + var2
						* this.tileSizeBase)] * 0.05F;

				if (this.blue[(var1 + var2 * this.tileSizeBase)] < 0.0F) {
					this.blue[(var1 + var2 * this.tileSizeBase)] = 0.0F;
				}

				this.alpha[(var1 + var2 * this.tileSizeBase)] -= 0.1F;

				if (Math.random() < 0.05D) {
					this.alpha[(var1 + var2 * this.tileSizeBase)] = 0.5F;
				}
			}
		}

		float[] var12 = this.green;
		this.green = this.red;
		this.red = var12;

		for (int var2 = 0; var2 < this.tileSizeSquare; var2++) {
			float var3 = this.red[var2];

			if (var3 > 1.0F) {
				var3 = 1.0F;
			}

			if (var3 < 0.0F) {
				var3 = 0.0F;
			}

			float var13 = var3 * var3;
			int r = (int) (this.rMin + var13 * (this.rMax - this.rMin));
			int g = (int) (this.gMin + var13 * (this.gMax - this.gMin));
			int b = (int) (this.bMin + var13 * (this.bMax - this.bMin));
			int a = (int) (146.0F + var13 * 50.0F);

			if (this.anaglyphEnabled) {
				int var9 = (r * 30 + g * 59 + b * 11) / 100;
				int var10 = (r * 30 + g * 70) / 100;
				int var11 = (r * 30 + b * 70) / 100;
				r = var9;
				g = var10;
				b = var11;
			}

			this.imageData[(var2 * 4 + 0)] = (byte) r;
			this.imageData[(var2 * 4 + 1)] = (byte) g;
			this.imageData[(var2 * 4 + 2)] = (byte) b;
			this.imageData[(var2 * 4 + 3)] = (byte) a;
		}
	}
}