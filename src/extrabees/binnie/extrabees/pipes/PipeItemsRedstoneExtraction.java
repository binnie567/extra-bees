package binnie.extrabees.pipes;

import net.minecraftforge.common.ForgeDirection;
import binnie.extrabees.core.ExtraBeeTexture;
import buildcraft.transport.pipes.PipeItemsWood;

public class PipeItemsRedstoneExtraction extends PipeItemsWood {

	int baseTexture = 66;
	int plainTexture = 67;

	public PipeItemsRedstoneExtraction(int itemID) {
		super(itemID);

		/*
		 * try { ModLoader.setPrivateValue(Pipe.class, this, "logic", new
		 * PipeLogicRandom()); } catch (Exception e) { e.printStackTrace(); }
		 */
	}

	@Override
	public String getTextureFile() {
		return ExtraBeeTexture.Main.getTexture();
	}

	@Override
	public int getTextureIndex(ForgeDirection direction) {
		if (direction == ForgeDirection.UNKNOWN)
			return baseTexture;
		else {
			int metadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);

			if (metadata == direction.ordinal())
				return plainTexture;
			else
				return baseTexture;
		}
	}

	boolean redstone = false;
	boolean redstoneSignal = false;

	@Override
	public void updateEntity() {
		super.updateEntity();
		boolean hasRedstone = worldObj.isBlockIndirectlyGettingPowered(xCoord,
				yCoord, zCoord);
		if (hasRedstone && !redstone) {
			redstoneSignal = true;
		}
		redstone = hasRedstone;
	}

}
