package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityInoculator;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowInoculator extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityInoculator machine;

	ControlSlot slotSerum;
	ControlSlot slotBee;
	ControlSlotArray slotBeeReserve;
	ControlSlotArray slotBeeDone;

	ControlExtraBeeProgress progessWidget;

	public WindowInoculator(EntityPlayer player, IInventory inventory, Side side) {
		super(260, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityInoculator) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowInoculator(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Inoculator");

		playerInventory = new ControlPlayerInventory(this);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		slotSerum = new ControlSlot(this, 44, 56);
		slotSerum.create(machine, TileEntityInoculator.SlotSerum);

		int x = 120;
		int y = 30;

		slotBee = new ControlSlot(this, x + 45, y + 18 + 8);
		slotBee.create(machine, TileEntityInoculator.SlotBee);

		slotBeeDone = new ControlSlotArray(this, x, y + 36 + 16, 6, 1);
		slotBeeDone.create(machine, TileEntityInoculator.SlotBeeDone[0]);

		slotBeeReserve = new ControlSlotArray(this, x, y, 6, 1);
		slotBeeReserve.create(machine, TileEntityInoculator.SlotBeeReserve[0]);

		progessWidget = new ControlExtraBeeProgress(this, 67, 57, 95, 16, 74,
				132, 74, 148, ControlProgress.Direction.Right);

		new ControlErrorState(this, x + 45 + 30, y + 18 + 9);

	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
