package binnie.botany.core;

import java.util.List;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import net.minecraft.src.Block;
import net.minecraft.src.BlockFlower;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ItemStack;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockBotany extends BlockFlower {

	@Override
	public int getRenderType() {
		return -1;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean hasTileEntity(int metadata) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityPlant();
	}

	public BlockBotany(int id) {
		super(id, 0);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	protected boolean canThisPlantGrowOnThisBlockID(int par1) {
		return Block.blocksList[par1] != null
				&& Block.blocksList[par1].isOpaqueCube();
	}

}
