package binnie.extrabees.genetics;

import net.minecraft.src.World;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;
import forestry.api.apiculture.IBeeMutation;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IGenome;

//		"bees.species.forest"
//		"bees.species.meadows"
//		"bees.species.common"
//		"bees.species.cultivated"
//		"bees.species.noble"
//		"bees.species.majestic"
//		"bees.species.imperial"
//		"bees.species.diligent"
//		"bees.species.unweary"
//		"bees.species.industrious"
//		"bees.species.steadfast"
//		"bees.species.valiant"
//		"bees.species.heroic"
//		"bees.species.sinister"
//		"bees.species.fiendish"
//		"bees.species.demonic"
//		"bees.species.modest"
//		"bees.species.frugal"
//		"bees.species.austere"
//		"bees.species.tropical"
//		"bees.species.exotic"
//		"bees.species.edenic"
//		"bees.species.ender"
//		"bees.species.wintry"
//		"bees.species.icy"
//		"bees.species.glacial"
//		"bees.species.vindictive"
//		"bees.species.vengeful"
//		"bees.species.avenging"
//		"bees.species.darkened"
//		"bees.species.reddened"
//		"bees.species.omega"
//		"bees.species.leporine"
//		"bees.species.rural"
//		"bees.species.marshy"

public enum EnumExtraBeeMutation implements IBeeMutation {
	AridA,

	BarrenA,

	DesolateA,

	RottenA,

	BoneA,

	CreeperA,

	StoneA,

	GraniteA,

	MineralA,

	IronA,

	CopperA,

	TinA,

	BronzeA,

	SilverA,

	GoldA,

	UnstableA,

	NucleurA,

	RadioactiveA,

	AncientA,

	PrimevalA,

	PrehistoricA,

	RelicA,

	CoalA,

	ResinA,

	OilA,

	PeatA,

	DistilledA,

	FuelA,

	CreosoteA,

	LatexA,

	RiverA,

	OceanA,

	InkA,

	SweetA,

	SugarA,

	FruitA,

	AlcoholA,

	FarmA,

	MilkA,

	CoffeeA,

	CitrusA,

	MintA,

	SwampA,

	BoggyA,

	FungalA,

	CommonA, CommonB, CommonC, CommonD, CommonE, CommonF, CommonG, CommonH, CommonI, CommonJ, CommonK, CommonL, CommonM, CommonN, CommonO, CommonP, CommonQ, CommonR, CommonS, CommonT, CommonU, CommonV, CommonW, CommonX, CommonY, CommonZ, CommonAA, CommonAB, CommonAC, CommonAD,

	CultivatedA, CultivatedB, CultivatedC, CultivatedD,

	RomanA, GreekA, ClassicalA,

	TemperedA, AngryA, VolcanicA,

	MaliciousA, InfectiousA, VirulentA,

	ViscousA, GlutinousA, StickyA,

	CorrosiveA, CausticA, AcidicA,

	ExcitedA, EnergeticA, EcstaticA,

	ArticA, FreezingA,

	ShadowA, DarkenedA, AbyssA,

	;

	public static void doInit() {

		AridA.init(EnumExtraBeeSpecies.getVanilla("Meadows"),
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.ARID, 10);

		BarrenA.init(EnumExtraBeeSpecies.ARID,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.BARREN, 8);

		DesolateA.init(EnumExtraBeeSpecies.ARID, EnumExtraBeeSpecies.BARREN,
				EnumExtraBeeSpecies.DESOLATE, 8);

		RottenA.init(EnumExtraBeeSpecies.DESOLATE,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.ROTTEN, 15);

		BoneA.init(EnumExtraBeeSpecies.DESOLATE,
				EnumExtraBeeSpecies.getVanilla("Frugal"),
				EnumExtraBeeSpecies.BONE, 15);

		CreeperA.init(EnumExtraBeeSpecies.DESOLATE,
				EnumExtraBeeSpecies.getVanilla("Austere"),
				EnumExtraBeeSpecies.CREEPER, 15);

		StoneA.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.STONE, 15);

		GraniteA.init(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.STONE,
				EnumExtraBeeSpecies.GRANITE, 15);

		MineralA.init(EnumExtraBeeSpecies.STONE, EnumExtraBeeSpecies.GRANITE,
				EnumExtraBeeSpecies.MINERAL, 15);

		IronA.init(EnumExtraBeeSpecies.MINERAL,
				EnumExtraBeeSpecies.getVanilla("Cultivated"),
				EnumExtraBeeSpecies.IRON, 5);

		CopperA.init(EnumExtraBeeSpecies.MINERAL,
				EnumExtraBeeSpecies.getVanilla("Cultivated"),
				EnumExtraBeeSpecies.COPPER, 5);

		TinA.init(EnumExtraBeeSpecies.MINERAL,
				EnumExtraBeeSpecies.getVanilla("Cultivated"),
				EnumExtraBeeSpecies.TIN, 5);

		BronzeA.init(EnumExtraBeeSpecies.COPPER, EnumExtraBeeSpecies.TIN,
				EnumExtraBeeSpecies.BRONZE, 5);

		SilverA.init(EnumExtraBeeSpecies.IRON, EnumExtraBeeSpecies.TIN,
				EnumExtraBeeSpecies.SILVER, 5);

		GoldA.init(EnumExtraBeeSpecies.BRONZE, EnumExtraBeeSpecies.SILVER,
				EnumExtraBeeSpecies.GOLD, 5);

		UnstableA.init(EnumExtraBeeSpecies.getVanilla("Austere"),
				EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.UNSTABLE, 5);

		NucleurA.init(EnumExtraBeeSpecies.UNSTABLE, EnumExtraBeeSpecies.IRON,
				EnumExtraBeeSpecies.NUCLEAR, 5);

		RadioactiveA.init(EnumExtraBeeSpecies.NUCLEAR,
				EnumExtraBeeSpecies.GOLD, EnumExtraBeeSpecies.RADIOACTIVE, 5);

		AncientA.init(EnumExtraBeeSpecies.getVanilla("Noble"),
				EnumExtraBeeSpecies.getVanilla("Diligent"),
				EnumExtraBeeSpecies.ANCIENT, 10);

		PrimevalA.init(EnumExtraBeeSpecies.ANCIENT,
				EnumExtraBeeSpecies.getVanilla("Noble"),
				EnumExtraBeeSpecies.PRIMEVAL, 8);

		PrehistoricA.init(EnumExtraBeeSpecies.PRIMEVAL,
				EnumExtraBeeSpecies.getVanilla("Majestic"),
				EnumExtraBeeSpecies.PREHISTORIC, 8);

		RelicA.init(EnumExtraBeeSpecies.PREHISTORIC,
				EnumExtraBeeSpecies.getVanilla("Imperial"),
				EnumExtraBeeSpecies.RELIC, 8);

		CoalA.init(EnumExtraBeeSpecies.PRIMEVAL,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.COAL, 8);

		ResinA.init(EnumExtraBeeSpecies.PRIMEVAL,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.RESIN, 8);

		OilA.init(EnumExtraBeeSpecies.PRIMEVAL,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.OIL, 8);

		PeatA.init(EnumExtraBeeSpecies.PRIMEVAL, EnumExtraBeeSpecies.BOGGY,
				EnumExtraBeeSpecies.PEAT, 8);

		DistilledA.init(EnumExtraBeeSpecies.OIL,
				EnumExtraBeeSpecies.getVanilla("Industrious"),
				EnumExtraBeeSpecies.DISTILLED, 8);

		FuelA.init(EnumExtraBeeSpecies.OIL, EnumExtraBeeSpecies.DISTILLED,
				EnumExtraBeeSpecies.FUEL, 8);

		CreosoteA.init(EnumExtraBeeSpecies.FUEL, EnumExtraBeeSpecies.COAL,
				EnumExtraBeeSpecies.CREOSOTE, 8);

		LatexA.init(EnumExtraBeeSpecies.FUEL, EnumExtraBeeSpecies.RESIN,
				EnumExtraBeeSpecies.LATEX, 8);

		RiverA.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.RIVER, 10);

		OceanA.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.OCEAN, 10);

		InkA.init(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.OCEAN,
				EnumExtraBeeSpecies.INK, 8);

		SweetA.init(EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanilla("Diligent"),
				EnumExtraBeeSpecies.SWEET, 15);

		SugarA.init(EnumExtraBeeSpecies.SWEET,
				EnumExtraBeeSpecies.getVanilla("Diligent"),
				EnumExtraBeeSpecies.SUGAR, 15);

		FruitA.init(EnumExtraBeeSpecies.SUGAR,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.FRUIT, 5);

		AlcoholA.init(EnumExtraBeeSpecies.FRUIT,
				EnumExtraBeeSpecies.getVanilla("Rural"),
				EnumExtraBeeSpecies.ALCOHOL, 10);

		FarmA.init(EnumExtraBeeSpecies.getVanilla("Cultivated"),
				EnumExtraBeeSpecies.getVanilla("Rural"),
				EnumExtraBeeSpecies.FARM, 10);

		MilkA.init(EnumExtraBeeSpecies.getVanilla("Rural"),
				EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.MILK, 10);

		CoffeeA.init(EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanilla("Rural"),
				EnumExtraBeeSpecies.COFFEE, 10);

		CitrusA.init(EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.FARM, EnumExtraBeeSpecies.CITRUS, 10);

		MintA.init(EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.FARM, EnumExtraBeeSpecies.MINT, 10);

		SwampA.init(EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.SWAMP, 10);

		BoggyA.init(EnumExtraBeeSpecies.SWAMP,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.BOGGY, 8);

		FungalA.init(EnumExtraBeeSpecies.BOGGY, EnumExtraBeeSpecies.SWAMP,
				EnumExtraBeeSpecies.FUNGAL, 8);

		CommonA.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonB.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Meadows"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonC.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonD.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonE.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonF.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonM.init(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonN.init(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonO.init(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonG.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonH.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Meadows"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonI.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonJ.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonK.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonL.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonP.init(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonQ.init(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonR.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonS.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Meadows"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonT.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonU.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonV.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonW.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonX.init(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonY.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Forest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonZ.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Meadows"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonAA.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Modest"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonAB.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonAC.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CommonAD.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Marshy"),
				EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15);

		CultivatedA.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12);

		CultivatedB.init(EnumExtraBeeSpecies.ROCK,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12);

		CultivatedC.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12);

		CultivatedD.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Common"),
				EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12);

		RomanA.init(EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.getVanilla("Heroic"),
				EnumExtraBeeSpecies.ROMAN, 10);

		GreekA.init(EnumExtraBeeSpecies.ROMAN, EnumExtraBeeSpecies.MARBLE,
				EnumExtraBeeSpecies.GREEK, 8);

		ClassicalA.init(EnumExtraBeeSpecies.GREEK, EnumExtraBeeSpecies.ROMAN,
				EnumExtraBeeSpecies.CLASSICAL, 8);

		TemperedA.init(EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.getVanilla("Sinister"),
				EnumExtraBeeSpecies.TEMPERED, 10);

		AngryA.init(EnumExtraBeeSpecies.TEMPERED, EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.ANGRY, 8);

		VolcanicA.init(EnumExtraBeeSpecies.ANGRY, EnumExtraBeeSpecies.TEMPERED,
				EnumExtraBeeSpecies.VOLCANIC, 8);

		MaliciousA.init(EnumExtraBeeSpecies.getVanilla("Sinister"),
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.MALICIOUS, 10);

		InfectiousA.init(EnumExtraBeeSpecies.MALICIOUS,
				EnumExtraBeeSpecies.getVanilla("Tropical"),
				EnumExtraBeeSpecies.INFECTIOUS, 8);

		VirulentA
				.init(EnumExtraBeeSpecies.MALICIOUS,
						EnumExtraBeeSpecies.INFECTIOUS,
						EnumExtraBeeSpecies.VIRULENT, 8);

		ViscousA.init(EnumExtraBeeSpecies.WATER,
				EnumExtraBeeSpecies.getVanilla("Exotic"),
				EnumExtraBeeSpecies.VISCOUS, 10);

		GlutinousA.init(EnumExtraBeeSpecies.VISCOUS,
				EnumExtraBeeSpecies.getVanilla("Exotic"),
				EnumExtraBeeSpecies.GLUTINOUS, 8);

		StickyA.init(EnumExtraBeeSpecies.VISCOUS,
				EnumExtraBeeSpecies.GLUTINOUS, EnumExtraBeeSpecies.STICKY, 8);

		CorrosiveA.init(EnumExtraBeeSpecies.VIRULENT,
				EnumExtraBeeSpecies.STICKY, EnumExtraBeeSpecies.CORROSIVE, 10);

		CausticA.init(EnumExtraBeeSpecies.CORROSIVE,
				EnumExtraBeeSpecies.getVanilla("Fiendish"),
				EnumExtraBeeSpecies.CAUSTIC, 8);

		AcidicA.init(EnumExtraBeeSpecies.CORROSIVE,
				EnumExtraBeeSpecies.CAUSTIC, EnumExtraBeeSpecies.ACIDIC, 8);

		ExcitedA.init(EnumExtraBeeSpecies.getVanilla("Cultivated"),
				EnumExtraBeeSpecies.getVanilla("Valiant"),
				EnumExtraBeeSpecies.EXCITED, 10);

		EnergeticA.init(EnumExtraBeeSpecies.EXCITED,
				EnumExtraBeeSpecies.getVanilla("Valiant"),
				EnumExtraBeeSpecies.ENERGETIC, 8);

		EcstaticA.init(EnumExtraBeeSpecies.EXCITED,
				EnumExtraBeeSpecies.ENERGETIC, EnumExtraBeeSpecies.ECSTATIC, 8);

		ArticA.init(EnumExtraBeeSpecies.getVanilla("Wintry"),
				EnumExtraBeeSpecies.getVanilla("Diligent"),
				EnumExtraBeeSpecies.ARTIC, 10);

		FreezingA.init(EnumExtraBeeSpecies.OCEAN, EnumExtraBeeSpecies.ARTIC,
				EnumExtraBeeSpecies.FREEZING, 10);

		ShadowA.init(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.ANGRY,
				EnumExtraBeeSpecies.SHADOW, 10);

		DarkenedA.init(EnumExtraBeeSpecies.SHADOW, EnumExtraBeeSpecies.BASALT,
				EnumExtraBeeSpecies.DARKENED, 8);

		AbyssA.init(EnumExtraBeeSpecies.SHADOW, EnumExtraBeeSpecies.DARKENED,
				EnumExtraBeeSpecies.ABYSS, 8);

		for (IBeeMutation mutation : EnumExtraBeeMutation.values()) {
			BeeManager.beeMutations.add(mutation);
		}

	}

	public void init(IAlleleBeeSpecies allele0, IAlleleBeeSpecies allele1,
			EnumExtraBeeSpecies mutation, int chance) {
		this.chance = chance;
		species0 = allele0;
		species1 = allele1;
		template = mutation.getTemplate();
	}

	public void init(IAlleleBeeSpecies allele0, IAlleleBeeSpecies allele1,
			IAllele[] mutation, int chance) {
		this.chance = chance;
		species0 = allele0;
		species1 = allele1;
		template = mutation;
	}

	IAlleleBeeSpecies species0 = null;
	IAlleleBeeSpecies species1 = null;
	IAllele[] template = new IAllele[0];
	int chance = 80;

	@Override
	public IAllele getAllele0() {
		return species0;
	}

	@Override
	public IAllele getAllele1() {
		return species1;
	}

	@Override
	public IAllele[] getTemplate() {
		return template;
	}

	@Override
	public int getBaseChance() {
		return chance;
	}

	@Override
	public boolean isPartner(IAllele allele) {
		return (allele.getUID().equals(species0.getUID()))
				|| (allele.getUID().equals(species1.getUID()));
	}

	@Override
	public IAllele getPartner(IAllele allele) {
		return (allele.getUID().equals(species0.getUID())) ? species1
				: species0;
	}

	@Override
	public boolean isSecret() {
		return false;
	}

	@Override
	public int getChance(IBeeHousing housing, IAllele allele0, IAllele allele1,
			IGenome genome0, IGenome genome1) {

		World world = housing.getWorld();

		int processedChance = Math.round(chance
				* housing.getMutationModifier((IBeeGenome) genome0,
						(IBeeGenome) genome1)
				* BeeManager.breedingManager.getBeekeepingMode(world)
						.getMutationModifier((IBeeGenome) genome0,
								(IBeeGenome) genome1));

		if (this.species0.getUID().equals(allele0.getUID())
				&& this.species1.getUID().equals(allele1.getUID()))
			return processedChance;
		if (this.species1.getUID().equals(allele0.getUID())
				&& this.species0.getUID().equals(allele1.getUID()))
			return processedChance;

		return 0;

	}

}
