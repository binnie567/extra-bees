package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventTextEdit extends Event {

	public EventTextEdit(IWidget origin, String text) {
		super(origin);
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public String text;

}
