package binnie.extrabees.fx;


import net.minecraft.src.EntityFX;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.machines.TileEntitySequencer;

public class EntitySequencerFX extends EntityBinnieFX {

	public int blendmode = 1;
	private String texture = ExtraBeeTexture.FX.getTexture();

	public EntitySequencerFX(TileEntitySequencer tile) {

		super(tile.worldObj, 
				new Vector3f(
						(float) tile.xCoord + 0.2f + 0.6f*tile.worldObj.rand.nextFloat(),
						(float) tile.yCoord + 1f,
						(float) tile.zCoord + 0.2f + 0.6f*tile.worldObj.rand.nextFloat()),
				new Vector3f(0f, 0f, 0f));
		
		this.setParticleTextureIndex(0);
		this.setSize(0.1F, 0.1F);
		this.particleScale *= 0.5F;
		this.particleMaxAge = 10 + worldObj.rand.nextInt(6);
		this.noClip = true;

		this.motionX *= 0.119999999552965164D;
		this.motionY *= 0.119999999552965164D;
		this.motionZ *= 0.119999999552965164D;
		
		this.motionY = 0.05f;
		
		this.particleMaxAge = 16;
		
		newLetter();

	}
	
	
	
	private void newLetter() {
		letter = Letter.values()[worldObj.rand.nextInt(4)];
		setParticleTextureIndex(letter.ordinal());
		int color = letter.colour;
		particleRed = (color >> 16 & 255) / 255.0F;
		particleGreen = (color >> 8 & 255) / 255.0F;
		particleBlue = (color & 255) / 255.0F;
	}



	Letter letter;
	
	enum Letter {
		A(0xFF0000),
		G(0xFFFF00),
		T(0x00FF00),
		C(0x0000FF)
		;
		int colour;
		Letter(int colour) {
			this.colour = colour;
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate() {

		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		this.moveEntity(this.motionX, this.motionY, this.motionZ);

		if(this.particleMaxAge < 20) {
			this.setSize(0.005f*particleMaxAge, 0.005f*particleMaxAge);
		}
		
		if (this.particleMaxAge-- <= 0)
			this.setDead();
		
		if(worldObj.rand.nextInt(20)==0) {
			newLetter();
		}

	}

}
