package binnie.botany.core;

import java.util.HashMap;
import java.util.Map;

import forestry.api.genetics.IAllele;

import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.EnumFlowerChromosome;
import binnie.botany.genetics.Flower;
import binnie.botany.genetics.FlowerSpecies;
import binnie.botany.genetics.IAlleleFlowerSpecies;
import binnie.botany.genetics.IAlleleFlowerStem;
import binnie.botany.genetics.IFlower;

public class BotanyCore {

	Map<String, IAlleleFlowerSpecies> flowerSpecies = new HashMap<String, IAlleleFlowerSpecies>();

	public static IFlowerInterface flowerInterface = new FlowerHelper();

	public static int ageFromDamage(int damage) {
		return damage % 16;
	}

	public static IAlleleFlowerSpecies speciesFromDamage(int damage) {

		int index = (damage % 1024) / 16;
		if ((index < 0) || (index >= FlowerSpecies.values().length))
			return null;
		return FlowerSpecies.values()[index];
	}

	public static IAlleleFlowerStem stemFromDamage(int damage) {
		int index = damage / 1024;
		if ((index < 0) || (index >= AlleleFlowerStem.values().length))
			return null;
		return AlleleFlowerStem.values()[index];
	}

	public static IFlower flowerFromDamage(int damage) {
		return null;
	}

	public static int damageFromFlower(IFlower flower) {
		return 0;
	}

}
