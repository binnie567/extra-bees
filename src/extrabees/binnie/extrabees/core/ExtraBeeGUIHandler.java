package binnie.extrabees.core;

import binnie.craftgui.extrabees.WindowAcclimatiser;
import binnie.craftgui.extrabees.WindowGenepool;
import binnie.craftgui.extrabees.WindowIndexer;
import binnie.craftgui.extrabees.WindowAdvancedIndexer;
import binnie.craftgui.extrabees.WindowIndustrialAlveary;
import binnie.craftgui.extrabees.WindowInoculator;
import binnie.craftgui.extrabees.WindowIsolator;
import binnie.craftgui.extrabees.WindowPurifier;
import binnie.craftgui.extrabees.WindowReplicator;
import binnie.craftgui.extrabees.WindowSequencer;
import binnie.craftgui.extrabees.WindowSplicer;
import binnie.craftgui.extrabees.WindowAdvancedSequencer;
import binnie.craftgui.extrabees.WindowSynthesizer;
import binnie.craftgui.extrabees.database.WindowDictionary;
import binnie.craftgui.window.Window;
import binnie.logicpipes.WindowLogicPipe;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.network.IGuiHandler;

public class ExtraBeeGUIHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world,
			int x, int y, int z) {

		Window window = getWindow(id, player, world, x, y, z, Side.SERVER);

		if (window == null)
			return null;

		window.initGui();

		return window.getContainer();

	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world,
			int x, int y, int z) {

		Window window = getWindow(id, player, world, x, y, z, Side.CLIENT);

		if (window == null)
			return null;

		return window.getGui();

	}

	public Window getWindow(int id, EntityPlayer player, World world, int x,
			int y, int z, Side side) {

		if (id < 0 || id >= ExtraBeeGUI.values().length)
			return null;

		ExtraBeeGUI ID = ExtraBeeGUI.values()[id];

		Window window = null;

		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

		IInventory object = null;

		if (tileEntity instanceof IInventory) {
			object = (IInventory) tileEntity;
		}

		switch (ID) {
		case Acclimatiser:
			window = WindowAcclimatiser.create(player, object, side);
			break;
		case Database:
		case DatabaseNEI:
			window = WindowDictionary.create(player, side, ID==ExtraBeeGUI.Database ? false : true);
			break;
		case Indexer:
			window = WindowIndexer.create(player, object, side);
			break;
		case AdvancedIndexer:
			window = WindowAdvancedIndexer.create(player, object, side);
			break;
		case Genepool:
			window = WindowGenepool.create(player, object, side);
			break;
		case Sequencer:
			window = WindowSequencer.create(player, object, side);
			break;
		case Splicer:
			window = WindowSplicer.create(player, object, side);
			break;
		case AdvancedSequencer:
			window = WindowAdvancedSequencer.create(player, object, side);
			break;
		case IndustrialAlveary:
			window = WindowIndustrialAlveary.create(player, object, side);
			break;
		case LogicPipe:
			window = WindowLogicPipe.create(player, tileEntity, side);
			break;
		case Isolator:
			window = WindowIsolator.create(player, object, side);
			break;
		case Purifier:
			window = WindowPurifier.create(player, object, side);
			break;
		case Replicator:
			window = WindowReplicator.create(player, object, side);
			break;
		case Inoculator:
			window = WindowInoculator.create(player, object, side);
			break;
		case Synthesizer:
			window = WindowSynthesizer.create(player, object, side);
			break;
		}

		return window;

	}

}
