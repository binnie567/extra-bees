package binnie.extrabees.genetics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import binnie.extrabees.core.ExtraBeeItem;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IAlleleBeeEffect;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IAlleleFlowers;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleEffect;
import forestry.api.genetics.IAlleleSpecies;

public class ModuleEngineering {

	enum Quality {
		Awful("\u00A7c"), Poor("\u00A76"), Average("\u00A7e"), Good("\u00A7a"), Excellent(
				"\u00A7b");
		String color;

		Quality(String color) {
			this.color = color;
		}
	}

	public static void changeQuality(ItemStack itemStack, int dif) {
		if (dif == 0)
			return;

		NBTTagCompound nbt = itemStack.getTagCompound();

		int q = nbt.getInteger("quality");

		q += dif;
		if (q > 10)
			q = 10;
		if (q < 0)
			q = 0;

		nbt.setInteger("quality", q);

		itemStack.setTagCompound(nbt);
	}

	public enum Lifespan {
		Shortest, Short, Shortened, Normal, Elongated, Long, Longer, Longest, ;
		public String getUID() {
			return "forestry.lifespan" + this.toString();
		}

		public String getName() {
			return this.toString() + " Lifespan";
		}
	}

	public enum Speed {
		Slowest, Slower, Slow, Norm, Fast, Faster, Fastest;
		public String getUID() {
			return "forestry.speed" + this.toString();
		}

		public String getName() {
			String name = this.toString();
			if (this == Norm)
				name += "al";
			return name + " Productivity";
		}
	}

	public enum Fertility {
		Low, Normal, High, Maximum;
		public String getUID() {
			return "forestry.fertility" + this.toString();
		}

		public String getName() {
			return this.toString() + " Fertility";
		}
	}

	public enum Flowering {
		Slowest, Slower, Slow, Maximum;
		public String getUID() {
			return "forestry.flowering" + this.toString();
		}

		public String getName() {
			return this.toString() + " Flowering";
		}
	}

	public enum Territory {
		Default, Large, Larger, Largest;
		public String getUID() {
			return "forestry.territory" + this.toString();
		}

		public String getName() {
			return this.toString() + " Territory";
		}
	}

	public enum Tolerance {

		None("No"), Both1("Both 1"), Both2("Both 2"), Both3("Both 3"), Both4(
				"Both 4"), Both5("Both 5"), Up1("Up 1"), Up2("Up 2"), Up3(
				"Up 3"), Up4("Up 4"), Up5("Up 5"), Down1("Down 1"), Down2(
				"Down 2"), Down3("Down 3"), Down4("Down 4"), Down5("Down 5"),

		;

		String name;

		Tolerance(String name) {
			this.name = name;
		}

		public String getUID() {
			return "forestry.tolerance" + this.toString();
		}

		public String getName(String type) {
			return name + " " + type + " Tol.";
		}
	}

	public static int getColour(IAllele allele, EnumBeeChromosome chromosome,
			int pass) {
		switch (chromosome) {
		case SPECIES:
			if (pass == 1)
				return 0x9C3535;
			else if (pass == 3)
				return ((IAlleleSpecies) allele).getPrimaryColor();
			break;
		case CAVE_DWELLING:
			if (pass == 0 || pass == 1 || pass == 3)
				return 0x95969C;
			break;
		case EFFECT:
			if (pass == 1)
				return 0xB3AE22;
			break;
		case FERTILITY:
			if (pass == 1)
				return 0xE041B1;
			break;
		case FLOWERING:
			if (pass == 1)
				return 0xB365BA;
			break;
		case FLOWER_PROVIDER:
			if (pass == 1)
				return 0x58A367;
			break;
		case HUMIDITY:
			break;
		case HUMIDITY_TOLERANCE:
			if (pass == 1)
				return 0x21A9C4;
			break;
		case LIFESPAN:
			if (pass == 1)
				return 0x4BA9B8;
			break;
		case NOCTURNAL:
			if (pass == 0 || pass == 1 || pass == 3)
				return 0x20255C;
			break;
		case SPEED:
			if (pass == 1)
				return 0xB7BF17;
			break;
		case TEMPERATURE_TOLERANCE:
			if (pass == 1)
				return 0x8A3636;
			break;
		case TERRITORY:
			if (pass == 1)
				return 0x352AA8;
			break;
		case TOLERANT_FLYER:
			if (pass == 0 || pass == 1 || pass == 3)
				return 0x3F47A1;
			break;
		default:
			break;

		}
		return -1;
	}

	public static String getName(IAllele allele, EnumBeeChromosome chromosome) {
		switch (chromosome) {
		case SPECIES:
			return ((IAlleleSpecies) allele).getName() + " Species";
		case CAVE_DWELLING:
			return "Cave Dwelling";
		case EFFECT:
			IAlleleBeeEffect eAllele = ((IAlleleBeeEffect) allele);
			if (eAllele.getIdentifier().equals("None"))
				return "Effect Cancelation";
			else
				return eAllele.getIdentifier() + " Effect";
		case FERTILITY:
			for (Fertility lspan : Fertility.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case FLOWERING:
			for (Flowering lspan : Flowering.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case FLOWER_PROVIDER:
			IAlleleFlowers fAllele = ((IAlleleFlowers) allele);
			if (allele.getUID().equals("extrabees.flower.none"))
				return "Non-pollination";
			else
				return fAllele.getProvider().getDescription() + " Pollination";
		case HUMIDITY:
			for (Speed lspan : Speed.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case HUMIDITY_TOLERANCE:
			for (Tolerance lspan : Tolerance.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName("Humid.");
			break;
		case LIFESPAN:
			for (Lifespan lspan : Lifespan.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case NOCTURNAL:
			return "Nocturnal";
		case SPEED:
			for (Speed lspan : Speed.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case TEMPERATURE_TOLERANCE:
			for (Tolerance lspan : Tolerance.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName("Temp.");
			break;
		case TERRITORY:
			for (Territory lspan : Territory.values())
				if (lspan.getUID().equals(allele.getUID()))
					return lspan.getName();
			break;
		case TOLERANT_FLYER:
			return "Rainfall";
		default:
			break;
		}
		return "Corrupted";
	}

	public static void getQualityInfo(ItemStack itemStack, List list) {
		int quality = getQuality(itemStack);

		Quality type = Quality.Average;

		switch (quality) {
		case 0:
		case 1:
			type = Quality.Awful;
			break;

		case 2:
		case 3:
			type = Quality.Poor;
			break;

		case 7:
		case 8:
			type = Quality.Good;
			break;

		case 9:
		case 10:
			type = Quality.Excellent;
			break;

		default:
			break;
		}

		list.add(type.color + "\u00A7o" + type.toString() + " Quality \u00A7r");
	}

	public static int getQuality(ItemStack item) {
		if (!item.hasTagCompound())
			return 0;
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("quality"))
			return nbt.getInteger("quality");
		else
			return 0;
	}

	public static Collection<ItemStack> getAllSerums() {
		List<ItemStack> itemList = new ArrayList<ItemStack>();

		for (IAllele allele : AlleleManager.alleleRegistry
				.getRegisteredAlleles().values()) {

			String name = "DNA";

			EnumBeeChromosome chromosome = null;

			if (allele instanceof IAlleleBeeSpecies) {
				chromosome = EnumBeeChromosome.SPECIES;
				name = ((IAlleleSpecies) allele).getName();
			}

			else if (allele instanceof IAlleleBeeEffect) {
				chromosome = EnumBeeChromosome.EFFECT;
				name = ((IAlleleEffect) allele).getIdentifier();
			}

			else if (allele instanceof IAlleleFlowers) {
				chromosome = EnumBeeChromosome.FLOWER_PROVIDER;
				name = ((IAlleleFlowers) allele).getProvider().getDescription();
			}

			if (name != "DNA") {
				itemList.add(createSerum(allele, chromosome, 5));
			}

		}

		for (Lifespan lspan : Lifespan.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.LIFESPAN, 5);
			if (serum != null)
				itemList.add(serum);
		}

		for (Flowering lspan : Flowering.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.FLOWERING, 5);
			if (serum != null)
				itemList.add(serum);
		}

		for (Fertility lspan : Fertility.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.FERTILITY, 5);
			if (serum != null)
				itemList.add(serum);
		}

		for (Territory lspan : Territory.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.TERRITORY, 5);
			if (serum != null)
				itemList.add(serum);
		}

		for (Speed lspan : Speed.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.SPEED, 5);
			if (serum != null)
				itemList.add(serum);
		}

		for (Tolerance lspan : Tolerance.values()) {
			ItemStack serum = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.TEMPERATURE_TOLERANCE, 5);
			ItemStack serum2 = tryCreateSerum(
					AlleleManager.alleleRegistry.getAllele(lspan.getUID()),
					EnumBeeChromosome.HUMIDITY_TOLERANCE, 5);
			if (serum != null)
				itemList.add(serum);
			if (serum2 != null)
				itemList.add(serum2);
		}

		itemList.add(createSerum(AlleleManager.getAllele("boolTrue"),
				EnumBeeChromosome.NOCTURNAL, 5));

		itemList.add(createSerum(AlleleManager.getAllele("boolTrue"),
				EnumBeeChromosome.CAVE_DWELLING, 5));

		itemList.add(createSerum(AlleleManager.getAllele("boolTrue"),
				EnumBeeChromosome.TOLERANT_FLYER, 5));

		return itemList;

	}

	public static ItemStack tryCreateSerum(IAllele allele,
			EnumBeeChromosome chromosome, int quality) {
		if (allele == null || chromosome == null)
			return null;

		switch (chromosome) {
		case SPECIES:
			if (allele instanceof IAlleleBeeSpecies)
				return createSerum(allele, chromosome, quality);
			return null;
		case CAVE_DWELLING:
			if (allele.getUID().equals("forestry.boolTrue"))
				return createSerum(allele, chromosome, quality);
			return null;
		case EFFECT:
			if (allele instanceof IAlleleBeeEffect)
				return createSerum(allele, chromosome, quality);
			return null;
		case FERTILITY:
			for (Fertility lspan : Fertility.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case FLOWERING:
			for (Flowering lspan : Flowering.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case FLOWER_PROVIDER:
			if (allele instanceof IAlleleFlowers)
				return createSerum(allele, chromosome, quality);
			return null;
		case HUMIDITY:
			return null;
		case HUMIDITY_TOLERANCE:
		case TEMPERATURE_TOLERANCE:
			for (Tolerance lspan : Tolerance.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case LIFESPAN:
			for (Lifespan lspan : Lifespan.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case NOCTURNAL:
			if (allele.getUID().equals("forestry.boolTrue"))
				return createSerum(allele, chromosome, quality);
			return null;
		case SPEED:
			for (Speed lspan : Speed.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case TERRITORY:
			for (Territory lspan : Territory.values())
				if (lspan.getUID().equals(allele.getUID()))
					return createSerum(allele, chromosome, quality);
			return null;
		case TOLERANT_FLYER:
			if (allele.getUID().equals("forestry.boolTrue"))
				return createSerum(allele, chromosome, quality);
			return null;

		}
		return null;
	}

	public static ItemStack createSerum(IAllele allele,
			EnumBeeChromosome chromosome, int quality) {
		int i = chromosome.ordinal();
		if (i > EnumBeeChromosome.HUMIDITY.ordinal())
			i--;
		ItemStack stack = new ItemStack(ExtraBeeItem.serum, 1, 0);
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger("quality", quality);
		nbt.setString("uid", allele.getUID());
		nbt.setInteger("chromosome", i);
		stack.setTagCompound(nbt);
		return stack;
	}

	public static Collection<ItemStack> getAllGenomes() {
		List<ItemStack> list = new ArrayList<ItemStack>();
		for (IAllele allele : AlleleManager.alleleRegistry
				.getRegisteredAlleles().values()) {
			IAllele[] template = BeeManager.breedingManager
					.getBeeTemplate(allele.getUID());
			if (template != null) {
				IBeeGenome genome = BeeManager.beeInterface
						.templateAsGenome(template);
				NBTTagCompound nbt = new NBTTagCompound();
				genome.writeToNBT(nbt);
				NBTTagCompound itemNBT = new NBTTagCompound();
				itemNBT.setCompoundTag("genome", nbt);
				itemNBT.setInteger("quality", 5);
				ItemStack item = new ItemStack(ExtraBeeItem.genome, 1, 0);
				item.setTagCompound(itemNBT);
				list.add(item);
			}
		}
		return list;
	}
	
	
	
	
	
	
	
	
	
	
	

}
