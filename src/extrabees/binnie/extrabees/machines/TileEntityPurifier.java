package binnie.extrabees.machines;

import net.minecraft.src.IInventory;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.genetics.ModuleEngineering;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;

import buildcraft.api.power.IPowerReceptor;

public class TileEntityPurifier extends TileEntityMachine implements
		IInventory, IPowerReceptor, ITankContainer {

	public static final int SlotSerum = 2;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Purifier.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Purifier;
	}

	public TileEntityPurifier() {
		super(30000, 300, 50000);

		addSlot(SlotSerum);

		getSlot(SlotSerum).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));

		this.addTank(0, new LiquidTank(4000));
	}

	@Override
	public void onFinishTask() {

		int amount = (ExtraBeeItem.serum.getMaxDamage() - getSlot(SlotSerum)
				.getItemStack().getItemDamage()) * 100;

		getTank(0).drain(amount, true);

		ModuleEngineering.changeQuality(getSlot(SlotSerum).getItemStack(), 1);
	}

	@Override
	public ErrorState canWork() {
		if (getSlot(SlotSerum).getItemStack() == null)
			return new ErrorState("No Serum");

		if (ModuleEngineering.getQuality(getSlot(SlotSerum).getItemStack()) == 10)
			return new ErrorState("Serum already highest quality");

		return super.canWork();
	}

	@Override
	public ErrorState canProgress() {
		if (getTank(0).drain(500, false) != null
				&& getTank(0).drain(500, false).amount == 500)
			return super.canProgress();

		return new ErrorState("Not enough Liquid DNA");
	}

	@Override
	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	@Override
	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}

}
