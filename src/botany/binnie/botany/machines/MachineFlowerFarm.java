package binnie.botany.machines;

import net.minecraft.src.TileEntity;
import forestry.core.gadgets.Machine;
import forestry.core.gadgets.MachineFactory;
import forestry.core.gadgets.TileMachine;
import forestry.core.gadgets.TileMill;
import forestry.factory.gadgets.MillRainmaker;

public class MachineFlowerFarm extends BotanyMachine {

	public static class Factory extends MachineFactory {
		@Override
		public Machine createMachine(TileEntity tile) {
			return new MachineFlowerFarm((TileBotanyMachine) tile);
		}
	}

	public MachineFlowerFarm(TileMachine tile) {
		super(tile);
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean doWork() {
		return false;
	}

	@Override
	public boolean isWorking() {
		return false;
	}

}
