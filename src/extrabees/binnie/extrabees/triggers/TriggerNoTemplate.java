package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntitySplicer;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerNoTemplate extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "No Template";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntitySplicer) {
			TileEntitySplicer machine = (TileEntitySplicer) tile;
			return machine.getSlot(TileEntitySplicer.SlotTemplate).isEmpty();
		}
		return false;
	}

	public TriggerNoTemplate() {
		super();
	}

}
