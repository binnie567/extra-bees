package binnie.extrabees.machines;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import net.minecraft.src.World;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;
import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.fx.EntitySequencerFX;
import binnie.extrabees.fx.EntitySplicerFX;
import buildcraft.api.power.IPowerReceptor;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.AlleleManager;

public class TileEntitySplicer extends TileEntityMachine implements
		IPowerReceptor, ITankContainer {

	public static final int[] SlotReserve = new int[] { 6, 7, 8, 9, 10, 11 };
	public static final int[] SlotFinished = new int[] { 12, 13, 14, 15, 16, 17 };
	public static final int SlotBee = 5;
	public static final int SlotBeeAfter = 4;
	public static final int SlotTemplate = 2;
	
	int fxCounter = 0;
	
	@SideOnly(Side.CLIENT)
	public void onFX(World world, int x, int y, int z, float efficiency) {
		if(fxCounter++==3) {
			EntityBinnieFX.createFX(new EntitySplicerFX(this, 0));
			EntityBinnieFX.createFX(new EntitySplicerFX(this, 1));
			fxCounter=0;
		}
	}

	public TileEntitySplicer() {
		super(15000, 1000, 25000);
		addSlot(SlotBee);
		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));

		addSlotArray(SlotReserve);
		for (InventorySlot slot : getSlots(SlotReserve)) {
			slot.setValidator(new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		}

		addSlotArray(SlotFinished);
		for (InventorySlot slot : getSlots(SlotFinished)) {
			slot.setValidator(new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
			slot.setReadOnly();
		}

		addSlot(SlotBeeAfter);
		getSlot(SlotBeeAfter).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		getSlot(SlotBeeAfter).setReadOnly();

		addSlot(SlotTemplate);
		getSlot(SlotTemplate).setValidator(
				new SlotValidatorTemplate(SlotValidatorTemplate.Mode.Written2));

		this.addTank(0, new LiquidTank(2000));
	}

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Splicer.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Splicer;
	}

	@Override
	public ErrorState canWork() {

		if (getStackInSlot(SlotBee) == null)
			return new ErrorState("No Bee");
		if (getStackInSlot(SlotTemplate) == null)
			return new ErrorState("No Template");
		if (getStackInSlot(SlotBeeAfter) != null)
			return new ErrorState("Finished Bee Slots Full");

		return super.canWork();
	}
	
	@Override
	public ErrorState canProgress() {

		if (getTank(0).drain(500, false) == null || getTank(0).drain(500, false).amount != 500)
			return new ErrorState("Not enough Liquid DNA");

		return super.canProgress();
	}

	@Override
	public void onFinishTask() {
		ItemStack template = getStackInSlot(SlotTemplate);
		int speciesID = template.getItemDamage();
		IAlleleBeeSpecies species = (IAlleleBeeSpecies) AlleleManager.alleleRegistry
				.getAllele(AlleleManager.alleleRegistry.getFromMetaMap(
						speciesID).getUID());
		
		

		/*
		 * if (species != null) { ItemStack newBee =
		 * getStackInSlot(SlotBee).copy(); LogicMachine.spliceBee(newBee,
		 * species); newBee.setItemDamage(speciesID);
		 * setInventorySlotContents(SlotBee, null);
		 * setInventorySlotContents(SlotBeeAfter, newBee); }
		 */

		IBee bee = BeeManager.beeInterface.getBee(getSlot(SlotBee)
				.getItemStack());

		if (species == null || bee == null)
			return;

		IBeeGenome genome = BeeManager.beeInterface.templateAsGenome(BeeManager.breedingManager.getBeeTemplate(species.getUID()));
		
		ItemStack beeItem = getSlot(SlotBee).getItemStack().copy();

		NBTTagCompound beeNBT = beeItem.getTagCompound();

		NBTTagCompound genomeNBT = beeNBT.getCompoundTag("Genome");

		NBTTagList chromosomes = genomeNBT.getTagList("Chromosomes");

		EnumBeeChromosome[] possibleGenes = new EnumBeeChromosome[] {
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,

				EnumBeeChromosome.EFFECT, EnumBeeChromosome.EFFECT,
				EnumBeeChromosome.EFFECT,

				EnumBeeChromosome.FLOWER_PROVIDER,
				EnumBeeChromosome.FLOWER_PROVIDER,
				EnumBeeChromosome.FLOWER_PROVIDER,

				EnumBeeChromosome.NOCTURNAL, EnumBeeChromosome.TOLERANT_FLYER,
				EnumBeeChromosome.CAVE_DWELLING, EnumBeeChromosome.NOCTURNAL,
				EnumBeeChromosome.TOLERANT_FLYER,
				EnumBeeChromosome.CAVE_DWELLING,

				EnumBeeChromosome.SPEED, EnumBeeChromosome.SPEED,
				EnumBeeChromosome.SPEED,

				EnumBeeChromosome.LIFESPAN, EnumBeeChromosome.LIFESPAN,

				EnumBeeChromosome.TEMPERATURE_TOLERANCE,
				EnumBeeChromosome.TEMPERATURE_TOLERANCE,

				EnumBeeChromosome.HUMIDITY_TOLERANCE,
				EnumBeeChromosome.HUMIDITY_TOLERANCE,

				EnumBeeChromosome.FERTILITY, EnumBeeChromosome.FERTILITY,

				EnumBeeChromosome.FLOWERING, EnumBeeChromosome.TERRITORY, };
		
		int n = worldObj.rand.nextInt(4)+4;
		for(int i = 0; i < n; i++) {
			int id = worldObj.rand.nextBoolean() ? 1 : 0;
			EnumBeeChromosome chrom = possibleGenes[worldObj.rand.nextInt(possibleGenes.length)];
			NBTTagCompound chromosome = (NBTTagCompound) chromosomes
					.tagAt(chrom.ordinal() > 6 ? chrom.ordinal()-1 : chrom.ordinal());

			chromosome.setString("UID" + id, genome.getActiveAllele(chrom.ordinal()).getUID());
		}
		
		

		beeItem.setTagCompound(beeNBT);

		bee = BeeManager.beeInterface.getBee(beeItem);
		
		if(bee.getGenome().getPrimaryAsBee().getUID().equals(species.getUID())) {
			getSlot(SlotBee).setItemStack(null);
			getSlot(SlotBeeAfter).setItemStack(beeItem);
		}
		
		if(worldObj.rand.nextInt(10)==0) {
			getSlot(SlotTemplate).setItemStack(null);
		}

		this.onInventoryChanged();
	}

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		if (getSlot(SlotBee).getItemStack() == null) {
			for (int i : SlotReserve) {
				if (getSlot(i).getItemStack() != null) {
					transferItem(i, SlotBee);
					break;
				}
			}
		}
		if (getSlot(SlotBeeAfter).getItemStack() != null) {
			for (int i : SlotFinished) {
				if (getSlot(i).getItemStack() == null) {
					transferItem(SlotBeeAfter, i);
					break;
				}
			}
		}
	}

	@Override
	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	@Override
	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}

}
