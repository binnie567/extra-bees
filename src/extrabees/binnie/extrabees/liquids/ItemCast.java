package binnie.extrabees.liquids;

import java.util.List;

import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.core.ExtraBeeTexture;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import forestry.api.core.ItemInterface;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;

public class ItemCast extends Item {

	public ItemCast(int par1) {
		super(par1);
		this.setHasSubtypes(true);
		this.setMaxStackSize(16);
		this.setTextureFile(ExtraBeeTexture.Main.getTexture());
		this.setCreativeTab(CreativeTabs.tabMaterials);
	}
	
	public static void setupRecipes() {
		ItemStack beeswax = ItemInterface.getItem("beeswax");
		ItemStack cast = ItemInterface.getItem("waxCast");
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Helmet.ordinal()), 
				new Object[] {"bbb", "bcb", "   ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Chestplate.ordinal()), 
				new Object[] {"b b", "bcb", "bbb", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Legging.ordinal()), 
				new Object[] {"bbb", "bcb", "b b", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Boots.ordinal()), 
				new Object[] {"   ", "bcb", "b b", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Axe.ordinal()), 
				new Object[] {"bb ", "bc ", "   ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Pickaxe.ordinal()), 
				new Object[] {"bbb", " c ", "   ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Shovel.ordinal()), 
				new Object[] {" b ", " c ", "   ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Hoe.ordinal()), 
				new Object[] {"bb ", " c ", "   ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Gear.ordinal()), 
				new Object[] {" b ", "bcb", " b ", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.moltenCast, 1, CastType.Block.ordinal()), 
				new Object[] {"bbb", "bcb", "bbb", Character.valueOf('b'), beeswax,
			Character.valueOf('c'), cast});
		
		
		}

	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List par3List) {
		for(int i = 0; i < CastType.values().length; i++)
			par3List.add(new ItemStack(this.shiftedIndex, 1, i));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getIconFromDamage(int par1) {
		return 112 + par1;
	}

	@Override
	public String getItemDisplayName(ItemStack itemStack) {
		if(itemStack==null) return "";
		return CastType.values()[itemStack.getItemDamage()].toString() + " Cast";
	}
	
	enum CastType {
		Helmet(5f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Chestplate(8f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Legging(7f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Boots(4f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Axe(3f, new Object[] {"   ", " S ", " S ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Pickaxe(3f, new Object[] {"   ", " S ", " S ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Shovel(1f, new Object[] {"   ", " S ", " S ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Hoe(2f, new Object[] {"   ", " S ", " S ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Gear(4f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		Block(9f, new Object[] {"   ", "   ", "   ", Character.valueOf('S'), new ItemStack(Item.stick) }),
		;
		float amount;
		Object[] recipe;
		CastType(float amount, Object[] recipe) {
			this.amount = amount;
			this.recipe = recipe;
		}
		public Object[] getRecipe() {
			return recipe;
		}
		public Object[] getGearRecipe(ItemStack gear) {
			return new Object[] {"   ", " G ", "   ", Character.valueOf('G'), gear};
		}
	}

}
