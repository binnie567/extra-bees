package binnie.botany.genetics;

import binnie.botany.core.BotanyCore;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IChromosome;
import forestry.apiculture.genetics.BeeTemplates;
import forestry.core.genetics.Chromosome;
import forestry.core.genetics.Genome;

public class FlowerGenome extends Genome implements IFlowerGenome {

	public FlowerGenome(NBTTagCompound nbttagcompound) {
		super(BotanyCore.flowerInterface.getDefaultFlowerTemplate(),
				nbttagcompound);
	}

	public FlowerGenome(IChromosome[] chromosomes) {
		super(BotanyCore.flowerInterface.getDefaultFlowerTemplate(),
				chromosomes);
	}

	@Override
	public IAlleleSpecies getPrimary() {
		return (IAlleleSpecies) getChromosomes()[EnumFlowerChromosome.SPECIES
				.ordinal()].getPrimaryAllele();
	}

	@Override
	public IAlleleSpecies getSecondary() {
		return (IAlleleSpecies) getChromosomes()[EnumFlowerChromosome.SPECIES
				.ordinal()].getSecondaryAllele();
	}

	@Override
	public IAlleleFlowerSpecies getPrimaryAsFlower() {
		return (IAlleleFlowerSpecies) getPrimary();
	}

	@Override
	public IAlleleFlowerStem getStem() {
		return (IAlleleFlowerStem) getChromosomes()[EnumFlowerChromosome.STALK
				.ordinal()].getActiveAllele();
	}

}
