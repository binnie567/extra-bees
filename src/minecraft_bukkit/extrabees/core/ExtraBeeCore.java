package extrabees.core;

import extrabees.gen.*;
import extrabees.genetics.*;
import extrabees.liquids.ItemLiquid;
import extrabees.platform.ExtraBeePlatform;
import extrabees.products.*;
import forestry.api.apiculture.BeeManager;
import forestry.api.core.ForestryBlock;
import forestry.api.core.ItemInterface;
import forge.MinecraftForge;
import ic2.api.Ic2Recipes;
import ic2.api.Items;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.logging.Logger;

import buildcraft.api.API;
import net.minecraft.server.*;
import extrabees.core.ExtraBeeBlock;
import extrabees.products.ItemHoneyComb;
import extrabees.products.ItemHoneyDrop;
import extrabees.products.ItemPropolis;

public class ExtraBeeCore
{
    public static boolean modIC2;
    public static boolean modRailcraft;
    public static boolean modBuildcraft;
    public static boolean modRedpower2;
    public static boolean modBetterFarming;
    public static boolean modAddonsForForestry;
    public static ArrayList hiveDrops[];
    public static String beeBreedingMode;

    public ExtraBeeCore()
    {
    }

    public static String getTextureFileLiquid()
    {
        return "/gfx/extrabees/extrabees_liquids.png";
    }

    public static String getTextureFileMain()
    {
        return "/gfx/extrabees/extrabees.png";
    }

    public static boolean isAvailable()
    {
        return true;
    }

    public static void preInit()
    {
    }

    public static void loadConfig()
    {
        ModLoader.getLogger().fine("Loading Extra Bees Config");
        Config.load();
    }

    public static void checkExternalMods()
    {
        modIC2 = ModLoader.isModLoaded("mod_IC2");
        modRailcraft = ModLoader.isModLoaded("mod_Railcraft");
        modBuildcraft = ModLoader.isModLoaded("mod_BuildCraftCore") || ModLoader.isModLoaded("net.minecraft.server.mod_BuildCraftCore") || ModLoader.isModLoaded("net.minecraft.server.mod_BuildCraftCore");
        modRedpower2 = ModLoader.isModLoaded("mod_RedPower2");
        modBetterFarming = ModLoader.isModLoaded("mod_BetterFarming");

        if (modIC2)
        {
            ModLoader.getLogger().fine("Found IndustrialCraft 2");
        }

        if (modRailcraft)
        {
            ModLoader.getLogger().fine("Found Railcraft");
        }

        if (modBuildcraft)
        {
            ModLoader.getLogger().fine("Found Buildcraft");

            try
            {
                if (ExtraBeePlatform.isBukkit())
                {
                    ExtraBeeExtMod.bcOil = (Block)Class.forName("net.minecraft.server.BuildCraftEnergy").getField("oilStill").get(null);
                    ExtraBeeExtMod.bcFuel = (Item)Class.forName("net.minecraft.server.BuildCraftEnergy").getField("fuel").get(null);
                }
                else
                {
                    ExtraBeeExtMod.bcOil = (Block)Class.forName("BuildCraftEnergy").getField("oilStill").get(null);
                    ExtraBeeExtMod.bcFuel = (Item)Class.forName("BuildCraftEnergy").getField("fuel").get(null);
                }
            }
            catch (Exception exception)
            {
                ModLoader.getLogger().fine("Oil or fuel not found.");
                ExtraBeeExtMod.bcOil = null;
                ExtraBeeExtMod.bcFuel = null;
            }
        }

        if (modRedpower2)
        {
            ModLoader.getLogger().fine("Found RedPower 2");
        }

        if (modBetterFarming)
        {
            ModLoader.getLogger().fine("Found Better Farming");
        }
    }

    public static void loadForestryConfigs()
    {
        try
        {
            Object obj = Class.forName("forestry.core.config.Config").getField("beeBreedingMode").get(null);
            Class class1 = Class.forName("forestry.core.EnumBreedingMode");
            beeBreedingMode = (String)class1.getMethod("toString", new Class[0]).invoke(obj, new Object[0]);
            ModLoader.getLogger().fine((new StringBuilder()).append("Config option beeBreedingMode loaded from Forestry - ").append(beeBreedingMode).toString());
        }
        catch (Exception exception)
        {
            ModLoader.getLogger().warning((new StringBuilder()).append("Could not load forestry config, due to java reflection failing!\n").append(exception.getMessage()).toString());
            ModLoader.getLogger().fine("Config option beeBreedingMode defaulting to NORMAL");
            beeBreedingMode = "NORMAL";
        }
    }

    public static void setupBlocks()
    {
        ExtraBeeBlock.materialBeehive = new MaterialBeehive();
        ModLoader.getLogger().fine((new StringBuilder()).append("Hive Block ID set to ").append(Config.hiveID).toString());
        ExtraBeeBlock.hive = new BlockExtraBeeHive(Config.hiveID);
        ModLoader.registerBlock(ExtraBeeBlock.hive, extrabees.gen.ItemBeehive.class);
        ExtraBeeBlock.ectoplasm = new BlockEctoplasm(Config.ectoplasmID);
        ModLoader.registerBlock(ExtraBeeBlock.ectoplasm);
        ModLoader.addName(ExtraBeeBlock.ectoplasm, "Ectoplasm");
        MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 0, "scoop", 0);
        MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 1, "scoop", 0);
        MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 2, "scoop", 0);
        MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 3, "scoop", 0);
        ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 0), "Water Hive");
        ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 1), "Rock Hive");
        ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 2), "Nether Hive");
        ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 3), "Marble Hive");
        
        
        if(!Config.canQuarryMineHives) {
			try {
				boolean[] blocks = (boolean[]) Class.forName("buildcraft.api.BuildCraftAPI").getField("softBlocks").get(null);
				
				blocks[ExtraBeeBlock.hive.id] = true;
				blocks[ForestryBlock.beehives.id] = true;
			}
			catch(Exception e) {
				
			}
			try {
				boolean[] blocks = (boolean[]) Class.forName("buildcraft.api.API").getField("softBlocks").get(null);
				
				blocks[ExtraBeeBlock.hive.id] = true;
				blocks[ForestryBlock.beehives.id] = true;
			}
			catch(Exception e) {
				
			}
		}
    }

    public static void setupLiquids()
    {
        ItemLiquid.setup(Config.liquidID, Config.containerID);
    }

    public static void setupItems()
    {
        ExtraBeeItem.honeyCrystal = new ItemHoneyCrystal(Config.honeyCrystalID);
        ExtraBeeItem.honeyCrystalEmpty = new ItemHoneyCrystalEmpty(Config.honeyCrystalEmptyID);
        ExtraBeeItem.honeyDrop = new ItemHoneyDrop(Config.honeyDropID);
        ItemHoneyComb.setupCombs(Config.combID);
        ExtraBeeItem.propolis = new ItemPropolis(Config.propolisID);
        ItemHoneyDrop.addSubtypes();
        ItemHoneyComb.addSubtypes();
        ItemPropolis.addSubtypes();
        ModLoader.addName(ExtraBeeItem.honeyCrystal, "Honey Crystal");
        ModLoader.addName(ExtraBeeItem.honeyCrystalEmpty, "Depleted Honey Crystal");
        ModLoader.getLogger().fine((new StringBuilder()).append("Comb ID set to ").append(Config.combID).toString());
        ModLoader.getLogger().fine((new StringBuilder()).append("Propolis ID set to ").append(Config.propolisID).toString());
    }

    public static void setupGenetics()
    {
        EnumExtraBeeSpecies.setup();
        EnumExtraBeeEffect.setup(1625);
        EnumExtraBeeFlowers.setup(1950);
    }

    public static void addHiveDrops()
    {
        hiveDrops = new ArrayList[4];
        hiveDrops[0] = new ArrayList();
        hiveDrops[0].add(new HiveDrop(EnumExtraBeeSpecies.WATER.getTemplate(), 80));
        hiveDrops[0].add(new HiveDrop(EnumExtraBeeSpecies.getVanillaTemplate("Valiant"), 3));
        hiveDrops[1] = new ArrayList();
        hiveDrops[1].add(new HiveDrop(EnumExtraBeeSpecies.ROCK.getTemplate(), 80));
        hiveDrops[1].add(new HiveDrop(EnumExtraBeeSpecies.getVanillaTemplate("Valiant"), 3));
        hiveDrops[2] = new ArrayList();
        hiveDrops[2].add(new HiveDrop(EnumExtraBeeSpecies.BASALT.getTemplate(), 80));
        hiveDrops[2].add(new HiveDrop(EnumExtraBeeSpecies.getVanillaTemplate("Valiant"), 3));
        hiveDrops[3] = new ArrayList();
        hiveDrops[3].add(new HiveDrop(EnumExtraBeeSpecies.MARBLE.getTemplate(), 80));
        hiveDrops[3].add(new HiveDrop(EnumExtraBeeSpecies.getVanillaTemplate("Valiant"), 3));
    }

    public static void addRecipes()
    {
        ModLoader.addRecipe(new ItemStack(ExtraBeeItem.honeyCrystalEmpty), new Object[]
                {
                    "#@#", "@#@", "#@#", '@', ItemInterface.getItem("honeyDrop"), '#', ItemHoneyDrop.getItem(extrabees.products.ItemHoneyDrop.EnumType.ENERGY)
                });


        for (ItemHoneyComb.EnumType info : ItemHoneyComb.EnumType.values()) {
			info.addRecipe();
		}

		for (ItemPropolis.EnumType info : ItemPropolis.EnumType.values()) {
			info.addRecipe();
		}

		for (ItemHoneyDrop.EnumType info : ItemHoneyDrop.EnumType.values()) {
			info.addRecipe();
		}

        if (modIC2)
        {
            Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canWater"), Items.getItem("waterCell"));
            Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canLava"), Items.getItem("lavaCell"));
            Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canBiomass"), Items.getItem("bioCell"));
            Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canBiofuel"), Items.getItem("biofuelCell"));
            Ic2Recipes.addExtractorRecipe(new ItemStack(ItemLiquid.can, 1, extrabees.liquids.ItemLiquid.EnumType.GLACIAL.ordinal()), Items.getItem("coolingCell"));
        }
    }

    public static void registerMutations()
    {
        EnumExtraBeeMutation aenumextrabeemutation[] = EnumExtraBeeMutation.values();
        int i = aenumextrabeemutation.length;

        for (int j = 0; j < i; j++)
        {
            EnumExtraBeeMutation enumextrabeemutation = aenumextrabeemutation[j];
            BeeManager.beeMutations.add(enumextrabeemutation);
        }
    }
}
