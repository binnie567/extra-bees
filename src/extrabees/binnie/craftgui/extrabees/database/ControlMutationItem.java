package binnie.craftgui.extrabees.database;

import binnie.craftgui.controls.listbox.ControlAbstractOption;
import binnie.craftgui.controls.listbox.ControlAbstractOptionSet;
import binnie.extrabees.genetics.ModuleGenetics;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeMutation;
import forestry.api.genetics.IMutation;

public class ControlMutationItem extends ControlAbstractOption<IMutation> {

	ControlBeeDisplay itemWidget1;
	ControlBeeDisplay itemWidget2;
	ControlBeeDisplay itemWidget3;
	ControlMutationSymbol addSymbol;
	ControlMutationSymbol arrowSymbol;


	public ControlMutationItem(ControlAbstractOptionSet<IMutation> controlOptions, float x, float y,
			float width, float height, IMutation value) {
		super(controlOptions, x, y, width, height, value);
		itemWidget1 = new ControlBeeDisplay(this, 4, 4);
		itemWidget2 = new ControlBeeDisplay(this, 44, 4);
		itemWidget3 = new ControlBeeDisplay(this, 104, 4);
		addSymbol = new ControlMutationSymbol(this, 24, 4, 0);
		arrowSymbol = new ControlMutationSymbol(this, 64, 4, 1);
		
		if (value != null) {
			boolean discovered = ModuleGenetics.isMutationDiscovered(value, getWindow().getWorld(), 
					((WindowDictionary)getSuperParent()).isNEI());
			itemWidget1.setSpecies((IAlleleBeeSpecies) value.getAllele0(),
					!discovered);
			itemWidget2.setSpecies((IAlleleBeeSpecies) value.getAllele1(),
					!discovered);
			itemWidget3.setSpecies((IAlleleBeeSpecies) value.getTemplate()[0],
					!discovered);
			addSymbol.setValue((IBeeMutation) value);
			arrowSymbol.setValue((IBeeMutation) value);
		}
	}

}
