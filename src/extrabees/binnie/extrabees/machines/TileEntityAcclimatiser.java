package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeGUI;
import buildcraft.api.power.IPowerReceptor;
import net.minecraft.src.ItemStack;

public class TileEntityAcclimatiser extends TileEntityMachine implements
		IPowerReceptor {

	public final static int SlotItem = 2;
	public final static int SlotBee = 3;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Acclimatiser.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Acclimatiser;
	}

	@Override
	public ErrorState canWork() {
		if (this.getStackInSlot(SlotBee) == null)
			return new ErrorState("No Bee");

		if (!LogicAcclimatiser
				.isAcclimatiserItem(this.getStackInSlot(SlotItem)))
			return new ErrorState("Cannot Acclimatise using this item");
		
		if (!LogicAcclimatiser
				.canAcclimatise(this.getStackInSlot(SlotBee), this.getStackInSlot(SlotItem)))
			return new ErrorState("Cannot Acclimatise this bee with this item");

		return super.canWork();
	}

	@Override
	public void onFinishTask() {
		ItemStack bee = getSlot(SlotBee).getItemStack();
		ItemStack item = getSlot(SlotItem).getItemStack();
		if (LogicAcclimatiser.canAcclimatise(bee, item)) {
			LogicAcclimatiser.acclimatise(bee, item);
			decrStackSize(SlotItem, 1);
		}

	}

	public TileEntityAcclimatiser() {
		super(500, 200, 1000);
		addSlot(SlotBee);
		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		addSlot(SlotItem);
		getSlot(SlotItem).setValidator(new SlotValidatorAcclimatiser());
	}

}
