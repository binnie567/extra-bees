package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageBranchOverview extends PageBranch {

	public PageBranchOverview(IWidget parent) {
		super(parent);

		pageBranchOverview_branchName = new ControlText(this, 72, 8, "",
				ControlText.Alignment.Center);

		pageBranchOverview_branchScientific = new ControlText(this, 72, 32, "",
				ControlText.Alignment.Center);
		pageBranchOverview_branchAuthority = new ControlText(this, 72, 44, "",
				ControlText.Alignment.Center);

		
	}

	ControlText pageBranchOverview_branchName;
	ControlText pageBranchOverview_branchScientific;
	ControlText pageBranchOverview_branchAuthority;
	List<ControlText> pageBranchOverview_branchDescription = new ArrayList<ControlText>();

	@Override
	public void onBranchChanged(IBeeBranch branch) {
		pageBranchOverview_branchName.setText("\u00a7n"+branch.getName()+" Branch\u00a7r");
		pageBranchOverview_branchScientific.setText("\u00a7oApidae " + branch.getScientific()+"\u00a7r");
		pageBranchOverview_branchAuthority.setText("Discovered by \u00a7l"+branch.getMembers()[0].getAuthority()+"\u00a7r");
		
		for(IWidget widget : pageBranchOverview_branchDescription)
			deleteChild(widget);
		
		pageBranchOverview_branchDescription.clear();
		
		String desc = branch.getDescription();
		if(desc == null || desc == "")
			desc = "No Description Provided.";
		
		String line = "";
		
		List<String> descLines = new ArrayList<String>();
		
		for(String str : desc.split(" ")) {
			if(getRenderer().getTextWidth(line + " " + str)>134) {
				descLines.add("\u00a7o" + line + "\u00a7r");
				line = "";
			}
			line = line + " " + str;
		}
		descLines.add(line);

		int i = 0;
		
		for(String dLine : descLines)
			pageBranchOverview_branchDescription.add(new ControlText(this, 72, 84+12*i++,
					dLine, ControlText.Alignment.Center));
	}

}
