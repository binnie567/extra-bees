package binnie.extrabees.machines;

public class TileEntityGeneticMachine extends TileEntityMachine {

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/GeneticMachine.png";
	}

}
