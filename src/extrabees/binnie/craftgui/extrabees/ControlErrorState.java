package binnie.craftgui.extrabees;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.src.IInventory;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.extrabees.machines.TileEntityMachine;
import binnie.extrabees.machines.TileEntityMachine.ErrorState;

public class ControlErrorState extends Control implements ITooltip {

	@Override
	public void onRenderBackground() {
		if (errorState == null) {
			getRenderer().renderRect(0, 0, 16, 16, 160, 78);
		} else if (type == 0) {
			getRenderer().renderRect(0, 0, 16, 16, 128, 78);
		} else {
			getRenderer().renderRect(0, 0, 16, 16, 144, 78);
		}
		super.onRenderBackground();
	}

	@Override
	public void onUpdate() {
		IInventory inventory = getWindow().getInventory();
		if (inventory instanceof TileEntityMachine) {
			ErrorState state = ((TileEntityMachine) inventory).canWork();
			if (state != null) {
				this.type = 0;
				this.errorState = state;
			} else {
				state = ((TileEntityMachine) inventory).canProgress();
				this.type = 1;
				this.errorState = state;
			}
		}
	}

	TileEntityMachine.ErrorState errorState;
	int type = 0;

	protected ControlErrorState(IWidget parent, float x, float y) {
		super(parent, x, y, 16, 16);
		canMouseOver = true;
	}

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();
		if (errorState != null)
			list.add(errorState.toString());
		return list;
	}

}
