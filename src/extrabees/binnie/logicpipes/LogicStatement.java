package binnie.logicpipes;

public class LogicStatement implements ILogicStatement {
	
	public boolean isInverse() {
		return isInverse;
	}
	public void setInverse(boolean isInverse) {
		this.isInverse = isInverse;
	}
	public ILogicAction getAction() {
		return action;
	}
	public ILogicTrigger getTrigger() {
		return trigger;
	}
	public LogicStatement(ILogicTrigger trigger, ILogicAction action) {
		super();
		this.action = action;
		this.trigger = trigger;
	}
	ILogicAction action;
	ILogicTrigger trigger;
	boolean isInverse;

}
