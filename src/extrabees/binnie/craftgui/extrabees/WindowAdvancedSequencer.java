package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlTextEdit;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventTextEdit;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityAdvancedSequencer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowAdvancedSequencer extends Window {

	@Override
	public void onHandleEvent(Event event) {
		if ((event instanceof EventTextEdit) && event.isOrigin(name))
			machine.name = ((EventTextEdit) event).getText();
	}

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityAdvancedSequencer machine;

	ControlSlot slotBee;

	ControlSlot slotTemplate;
	ControlSlot slotTemplateFinished;

	ControlExtraBeeProgress sequenceProgress;
	ControlExtraBeeProgress dnaProgress;

	ControlText speciesUID;

	public WindowAdvancedSequencer(EntityPlayer player, IInventory inventory,
			Side side) {
		super(244, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityAdvancedSequencer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowAdvancedSequencer(player, inventory, side);
	}

	ControlTextEdit name;

	@Override
	public void initialize() {

		setTitle("Genome Sequencer");

		playerInventory = new ControlPlayerInventory(this);

		sequenceProgress = new ControlExtraBeeProgress(this, 79, 43, 98, 9, 64,
				114, 64, 123, ControlExtraBeeProgress.Direction.Right);
		dnaProgress = new ControlExtraBeeProgress(this, 186, 24, 45, 72, 64,
				42, 109, 42, ControlExtraBeeProgress.Direction.Up);

		slotBee = new ControlSlot(this, 55, 40);

		slotTemplate = new ControlSlot(this, 199, 78);

		slotTemplateFinished = new ControlSlot(this, 199, 24);

		name = new ControlTextEdit(this, 79, 63, 100);
		name.setText(this.machine.name);

		slotBee.create(machine, TileEntityAdvancedSequencer.SlotBee);
		slotTemplate.create(machine,
				TileEntityAdvancedSequencer.SlotTemplateBlank);
		slotTemplateFinished.create(machine,
				TileEntityAdvancedSequencer.SlotTemplateWritten);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		new ControlErrorState(this, 34, 72);

	}

	@Override
	public void onUpdate() {
	}

};
