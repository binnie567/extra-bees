package binnie.extrabees.gen;

import java.util.Random;

import binnie.extrabees.core.ExtraBeeBlock;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.World;
import net.minecraft.src.WorldGenerator;
import forestry.api.core.GlobalManager;

public class WorldGenHiveMarble extends WorldGenerator {
	@Override
	public boolean generate(World world, Random random, int i, int j, int k) {
		BiomeGenBase biome = world.getWorldChunkManager().getBiomeGenAt(i, k);
		int i1 = i;
		int j1 = j;
		int k1 = k;

		if (world.isAirBlock(i1, j1, k1)
				&& (world.isAirBlock(i1, j1 + 1, k1) && (GlobalManager.dirtBlockIds
						.contains(Integer.valueOf(world.getBlockId(i1, j1 - 1,
								k1)))))) {
			world.setBlockAndMetadata(i1, j1, k1, ExtraBeeBlock.hive.blockID, 3);
		}

		return true;
	}
}
