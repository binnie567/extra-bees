package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

public class EventSpeciesChanged extends Event {

	IAlleleBeeSpecies species;

	public IAlleleBeeSpecies getSpecies() {
		return species;
	}

	public EventSpeciesChanged(IWidget origin, IAlleleBeeSpecies species) {
		super(origin);
		this.species = species;
	}

}
