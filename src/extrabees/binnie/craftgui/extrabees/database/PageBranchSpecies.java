package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventValueChanged;

public class PageBranchSpecies extends PageBranch {

	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if(event instanceof EventValueChanged && event.isOrigin(pageBranchSpecies_speciesList)) {
			((WindowDictionary)getSuperParent()).gotoSpecies(((EventValueChanged<IAlleleBeeSpecies>)event).getValue());
		}
	}

	public PageBranchSpecies(IWidget parent) {
		super(parent);

		pageBranchSpecies_title = new ControlText(this, 72, 8, "Species",
				ControlText.Alignment.Center);
		
//		ControlListBox<String> options = new ControlListBox<String>(this, 4, 20, 136, 152);
//
//		List<String> opt = new ArrayList<String>();
//		opt.add("Hello");
//		opt.add("Goodbye");
//		opt.add("Au revoir");
//		opt.add("King");
//		opt.add("() !! XX");
//		
//		options.setOptions(opt);
		
		pageBranchSpecies_speciesList = new ControlSpeciesBox(this, 4, 20, 136,
				152);
	}

	ControlText pageBranchSpecies_title;
	ControlSpeciesBox pageBranchSpecies_speciesList;

	@Override
	public void onBranchChanged(IBeeBranch branch) {
		pageBranchSpecies_speciesList.setBranch(branch);
	}


}
