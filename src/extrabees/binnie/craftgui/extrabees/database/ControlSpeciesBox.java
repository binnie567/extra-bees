package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import binnie.core.BinnieCore;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import net.minecraft.src.ItemStack;

public class ControlSpeciesBox extends
		ControlListBox<IAlleleBeeSpecies> {

	@Override
	public IWidget createOption(int index, int height, IAlleleBeeSpecies value) {
		return new ControlSpeciexBoxOption(controlOptions, 0, height, getSize().x-14, 22, value);
	}

	private int index;

	public ControlSpeciesBox(IWidget parent, int x, int y, int width, int height) {
		super(parent, x, y, width, height);
	}

	IBeeBranch branch = null;

	public void setBranch(IBeeBranch branch) {
		if (branch != this.branch) {
			this.branch = branch;
			List<IAlleleBeeSpecies> speciesList = new ArrayList<IAlleleBeeSpecies>();
			this.index = 0;
			movePercentage(-100.0f);
			setOptions(speciesList);
			if (branch != null) {
				for (IAlleleSpecies species : branch.getMembers()) {
					speciesList.add((IAlleleBeeSpecies) species);
				}
			}
			setOptions(speciesList);
		}

	}

}
