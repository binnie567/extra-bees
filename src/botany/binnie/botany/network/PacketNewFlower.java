package binnie.botany.network;

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.TileEntity;
import binnie.core.network.PacketTileNBT;
import binnie.botany.core.TileEntityPlant;

public class PacketNewFlower extends PacketTileNBT {
	
	public PacketNewFlower(TileEntityPlant tile) {
		super(PacketID.NewFlower.ordinal(), tile);
	}
	
	public PacketNewFlower() { super(); }

}
