package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.controls.ControlItemDisplay;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.genetics.ModuleGenetics;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.genetics.IMutation;

public class ControlBeeDisplay extends ControlItemDisplay implements ITooltip {

	@Override
	public void onRenderForeground() {
		switch (discovered) {
		case 0:
			super.onRenderForeground();
			break;
		case 1:
			getRenderer().setTexture(ExtraBeeTexture.Icons.getTexture());
			getRenderer().renderRect(0, 0, 16, 16, 64, 16);
			break;
		case 2:
			getRenderer().setTexture(ExtraBeeTexture.Icons.getTexture());
			getRenderer().renderRect(0, 0, 16, 16, 64, 0);
		default:
			break;
		}
	}

	@Override
	public String getTextureFile() {
		return super.getTextureFile();
	}

	public void setSpecies(IAlleleBeeSpecies species, boolean hidden) {
		super.setItemStack(ModuleGenetics.getBeeIcon(species));
		this.species = species;
		if (!hidden) {
			discovered = ModuleGenetics.isSpeciesDiscovered(species, getWindow().getWorld(), 
					((WindowDictionary)getSuperParent()).isNEI()) ? 0 : 2;
		} else {
			discovered = ModuleGenetics.isSpeciesDiscovered(species, getWindow().getWorld(), 
					((WindowDictionary)getSuperParent()).isNEI()) ? 1 : 2;
		}
		canMouseOver = true;
	}

	IAlleleBeeSpecies species = null;
	int discovered = 0; // 0 - discovered, 1 - known, 2 - unkown

	public ControlBeeDisplay(IWidget parent, int x, int y) {
		super(parent, x, y);
	}

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();
		if (species != null) {
			switch (discovered) {
			case 0:
				tooltip.add(species.getName());
				break;
			case 1:
				tooltip.add("Discovered Species");
				break;
			case 2:
				tooltip.add("Undiscovered Species");
			default:
				break;
			}
		}

		return tooltip;
	}

}
