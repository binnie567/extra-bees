package binnie.craftgui.extrabees;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;

public class VirtualBeeHousing implements IBeeHousing {

	EntityPlayer player;

	public VirtualBeeHousing(EntityPlayer player) {
		this.player = player;
	}

	@Override
	public float getTerritoryModifier(IBeeGenome genome) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public float getProductionModifier(IBeeGenome genome) {
		// TODO Auto-generated method stub
		return 1.0f;
	}

	@Override
	public int getXCoord() {
		// TODO Auto-generated method stub
		return player.serverPosX;
	}

	@Override
	public int getYCoord() {
		// TODO Auto-generated method stub
		return player.serverPosY;
	}

	@Override
	public int getZCoord() {
		// TODO Auto-generated method stub
		return player.serverPosZ;
	}

	@Override
	public ItemStack getQueen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack getDrone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setQueen(ItemStack itemstack) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDrone(ItemStack itemstack) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getBiomeId() {
		// TODO Auto-generated method stub
		return player.worldObj.getBiomeGenForCoords(player.serverPosX,
				player.serverPosY).biomeID;
	}

	@Override
	public float getTemperature() {
		// TODO Auto-generated method stub
		return BiomeGenBase.biomeList[getBiomeId()].temperature;
	}

	@Override
	public float getHumidity() {
		// TODO Auto-generated method stub
		return BiomeGenBase.biomeList[getBiomeId()].rainfall;
	}

	@Override
	public World getWorld() {
		// TODO Auto-generated method stub
		return player.worldObj;
	}

	@Override
	public EntityPlayer getOwnerEntity() {
		// TODO Auto-generated method stub
		return player;
	}

	@Override
	public void setErrorState(int state) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getErrorOrdinal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean canBreed() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean addProduct(ItemStack product, boolean all) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void wearOutEquipment(int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onQueenChange(ItemStack queen) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isSealed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSelfLighted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSunlightSimulated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHellish() {
		// TODO Auto-generated method stub
		return getBiomeId() == BiomeGenBase.hell.biomeID;
	}

}
