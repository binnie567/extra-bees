package binnie.botany.genetics;

public enum AlleleFlowerStem implements IAlleleFlowerStem {

	Thorn("Thorny", 2), Leaf("Leafy", 4), Reed("Sugar", 6), Cactus("Desert", 8),

	;

	private int iconIndex;

	private AlleleFlowerStem(String name, int iconIndex) {
		this.name = name;
		this.iconIndex = iconIndex;
	}

	String name = "";
	String texture = "/gfx/botany/botany.png";

	@Override
	public String getUID() {
		return "botany.flowers.stem." + this.toString();
	}

	@Override
	public boolean isDominant() {
		return false;
	}

	@Override
	public String getTextureFile() {
		return texture;
	}

	@Override
	public String getAdjective() {
		return name;
	}

	@Override
	public int getIconIndex() {
		return iconIndex;
	}

}
