package binnie.craftgui.extrabees;

import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.core.IWidget;

public class ControlExtraBeeProgress extends ControlProgress {

	protected ControlExtraBeeProgress(IWidget parent, int x, int y, int width,
			int height, int u1, int v1, int u2, int v2,
			ControlProgress.Direction dir, String textureFile) {
		super(parent, x, y, width, height, u1, v1, u2, v2, dir);
		texture = textureFile;
	}

	protected ControlExtraBeeProgress(IWidget parent, int x, int y, int width,
			int height, int u1, int v1, int u2, int v2,
			ControlProgress.Direction dir) {
		super(parent, x, y, width, height, u1, v1, u2, v2, dir);
		texture = "/gfx/extrabees/extrabees-processes.png";
	}

}
