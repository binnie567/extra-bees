package extrabees.core;

import extrabees.gen.ExtraBeeGenCore;
import extrabees.genetics.EnumExtraBeeMutation;
import extrabees.genetics.EnumExtraBeeSpecies;
import extrabees.platform.ExtraBeePlatform;
import extrabees.products.ItemHoneyComb;
import extrabees.products.ItemPropolis;
import forestry.api.core.*;
import forge.*;
import java.util.Random;
import java.util.logging.Logger;
import net.minecraft.server.ModLoader;
import net.minecraft.server.World;

public class ExtraBeePlugin implements IPlugin
{
    public ExtraBeePlugin()
    {
    }

    public boolean isAvailable()
    {
        return ExtraBeeCore.isAvailable();
    }

    public void preInit()
    {
        ExtraBeeCore.loadConfig();
        ExtraBeePlatform.preInit();
    }

    public void doInit()
    {
        ModLoader.getLogger().fine("Starting Initializing of Extra Bees:");
        ExtraBeeCore.checkExternalMods();
        ExtraBeeCore.loadForestryConfigs();
        ExtraBeePlatform.doInit();
        ExtraBeeCore.setupBlocks();
        ExtraBeeCore.setupLiquids();
        ExtraBeeCore.setupItems();
        ExtraBeeCore.setupGenetics();
        ExtraBeeCore.addHiveDrops();
        ExtraBeeCore.addRecipes();
        ExtraBeeCore.registerMutations();
        ModLoader.getLogger().fine((new StringBuilder()).append(EnumExtraBeeSpecies.values().length).append(" species of bee added to Minecraft").toString());
        ModLoader.getLogger().fine((new StringBuilder()).append(extrabees.products.ItemHoneyComb.EnumType.values().length).append(" honey combs added to Minecraft").toString());
        ModLoader.getLogger().fine((new StringBuilder()).append(extrabees.products.ItemPropolis.EnumType.values().length).append(" propolises added to Minecraft").toString());
        ModLoader.getLogger().fine((new StringBuilder()).append(EnumExtraBeeMutation.values().length).append(" bee mutations added to Minecraft").toString());
        ModLoader.getLogger().fine("Finishing Initialization of Extra Bees");
    }

    public void postInit()
    {
        ExtraBeePlatform.postInit();
    }

    public String getDescription()
    {
        return "Extra Bees";
    }

    public void generateSurface(World world, Random random, int i, int j)
    {
        ExtraBeeGenCore.generateSurface(world, random, i, j);
    }

    public IGuiHandler getGuiHandler()
    {
        return null;
    }

    public IPacketHandler getPacketHandler()
    {
        return null;
    }

    public IPickupHandler getPickupHandler()
    {
        return null;
    }

    public IAchievementHandler getAchievementHandler()
    {
        return null;
    }

    public IResupplyHandler getResupplyHandler()
    {
        return null;
    }

    public static void modLoaded()
    {
    }
}
