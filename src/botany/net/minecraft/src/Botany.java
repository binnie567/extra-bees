package net.minecraft.src;

import binnie.botany.network.BotanyPacketHandler;
import binnie.botany.proxy.Proxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(modid = "Botany", name = "Botany", version = "0.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = { "BOT" }, packetHandler = BotanyPacketHandler.class)
public class Botany {

	@Instance("Botany")
	public static Botany instance;

	@SidedProxy(clientSide = "binnie.botany.proxy.ProxyClient", serverSide = "binnie.botany.proxy.ProxyServer")
	public static Proxy proxy;

	public static String channel = "BOT";

	@Init
	public void load(FMLInitializationEvent evt) {
		this.instance = this;
	}

}
