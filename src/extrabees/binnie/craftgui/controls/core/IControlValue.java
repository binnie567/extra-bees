package binnie.craftgui.controls.core;

public interface IControlValue<T> {

	T getValue();
	void setValue(T value);
	
}
