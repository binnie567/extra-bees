package extrabees.genetics;

import extrabees.core.Config;
import extrabees.products.ItemHoneyComb;
import forestry.api.apiculture.*;
import forestry.api.core.*;
import forestry.api.genetics.*;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.logging.Logger;
import net.minecraft.server.*;

public enum EnumExtraBeeSpecies implements IAlleleBeeSpecies
{
    ARID,
    BARREN,
    DESOLATE,
    ROTTEN,
    BONE,
    CREEPER,
    ROCK,
    STONE,
    GRANITE,
    MINERAL,
    IRON,
    GOLD,
    COPPER,
    TIN,
    SILVER,
    BRONZE,
    UNSTABLE,
    NUCLEAR,
    RADIOACTIVE,
    ANCIENT,
    PRIMEVAL,
    PREHISTORIC,
    RELIC,
    COAL,
    RESIN,
    OIL,
    DISTILLED,
    FUEL,
    WATER,
    RIVER,
    OCEAN,
    INK,
    SWEET,
    SUGAR,
    FRUIT,
    ALCOHOL,
    FARM,
    MILK,
    SWAMP,
    BOGGY,
    FUNGAL,
    CREOSOTE,
    LATEX,
    MARBLE,
    ROMAN,
    GREEK,
    CLASSICAL,
    BASALT,
    TEMPERED,
    ANGRY,
    VOLCANIC,
    MALICIOUS,
    INFECTIOUS,
    VIRULENT,
    VISCOUS,
    GLUTINOUS,
    STICKY,
    CORROSIVE,
    CAUSTIC,
    ACIDIC,
    EXCITED,
    ENERGETIC,
    ECSTATIC,
    COFFEE,
    ARTIC,
    FREEZING;

    private String name;
    private int primaryColor;
    private int secondaryColor;
    private EnumTemperature temperature;
    private EnumHumidity humidity;
    private boolean effect;
    private boolean secret;
    private boolean counted;
    private String binomial;
    private Achievement achievement;
    private int id;
    private boolean dominant;
    private HashMap products;
    private HashMap specialty;
    private IAllele template[];

    public static IAlleleBeeSpecies getVanilla(String s)
    {
        return (IAlleleBeeSpecies)AlleleManager.getAllele((new StringBuilder()).append("species").append(s).toString());
    }

    public static IAllele[] getVanillaTemplate(String s)
    {
        try
        {
            Class class1 = Class.forName("forestry.apiculture.genetics.BeeTemplates");
            Method method = class1.getMethod((new StringBuilder()).append("get").append(s).append("Template").toString(), new Class[0]);
            Object obj = method.invoke(null, new Object[0]);
            return (IAllele[])obj;
        }
        catch (Exception exception)
        {
            ModLoader.getLogger().warning((new StringBuilder()).append("Could not retrieve vanilla template - get").append(s).append("Template, due to java reflection failing!\n").append(exception.getMessage()).toString());
        }

        return ExtraBeeGeneticsCore.getDefaultTemplate();
    }

    public static void setup()
    {
        ARID.init("Arid", "aridus", 0xbee854);
        ARID.setHumidity(EnumHumidity.ARID);
        ARID.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        ARID.importVanillaTemplate("AustereBranch");
        ARID.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.NONE;
        BARREN.init("Barren", "infelix", 0xe0d263);
        BARREN.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        BARREN.importTemplate(ARID);
        DESOLATE.init("Desolate", "desolo", 0xd1b890);
        DESOLATE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        DESOLATE.importTemplate(BARREN);
        DESOLATE.recessive();
        ROTTEN.init("Decaying", "caries", 0xbfe0b6);
        ROTTEN.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        ROTTEN.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.ROTTEN), Integer.valueOf(10));
        ROTTEN.importTemplate(DESOLATE);
        ROTTEN.template[EnumBeeChromosome.NOCTURNAL.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROTTEN.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROTTEN.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROTTEN.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_ZOMBIE;
        ROTTEN.template[EnumBeeChromosome.FERTILITY.ordinal()] = AlleleManager.getAllele("fertilityLow");
        BONE.init("Skeletal", "os", 0xe9ede8);
        BONE.importTemplate(ROTTEN);
        BONE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        BONE.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BONE), Integer.valueOf(10));
        BONE.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_SKELETON;
        CREEPER.init("Creepy", "erepo", 0x2ce615);
        CREEPER.importTemplate(ROTTEN);
        CREEPER.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(30));
        CREEPER.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.POWDERY), Integer.valueOf(10));
        CREEPER.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_CREEPER;
        ROCK.init("Rocky", "saxum", 0xa8a8a8);
        ROCK.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(30));
        ROCK.setIsSecret(false);
        ROCK.template[EnumBeeChromosome.NOCTURNAL.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROCK.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROCK.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager.getAllele("boolTrue");
        ROCK.template[EnumBeeChromosome.TEMPERATURE_TOLERANCE.ordinal()] = AlleleManager.getAllele("toleranceBoth2");
        ROCK.template[EnumBeeChromosome.HUMIDITY_TOLERANCE.ordinal()] = AlleleManager.getAllele("toleranceBoth2");
        ROCK.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.ROCK;
        ROCK.template[EnumBeeChromosome.FERTILITY.ordinal()] = AlleleManager.getAllele("fertilityLow");
        ROCK.setSecondaryColor(0x999999);
        STONE.init("Stone", "lapis", 0x757575);
        STONE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(30));
        STONE.importTemplate(ROCK);
        STONE.recessive();
        STONE.setSecondaryColor(0x999999);
        GRANITE.init("Granite", "granum", 0x695555);
        GRANITE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(30));
        GRANITE.importTemplate(STONE);
        GRANITE.setSecondaryColor(0x999999);
        MINERAL.init("Mineral", "minerale", 0x6e757d);
        MINERAL.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(30));
        MINERAL.importTemplate(GRANITE);
        MINERAL.setSecondaryColor(0x999999);
        IRON.init("Ferrous", "ferrous", 0xa87058);
        IRON.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        IRON.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.IRON), Integer.valueOf(10));
        IRON.importTemplate(MINERAL);
        IRON.recessive();
        IRON.setSecondaryColor(0x999999);
        GOLD.init("Aureus", "aureus", 0xe6cc0b);
        GOLD.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        GOLD.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.GOLD), Integer.valueOf(10));
        GOLD.importTemplate(MINERAL);
        GOLD.setHasEffect(true);
        GOLD.setSecondaryColor(0x999999);
        COPPER.init("Cuprous", "cuprous", 0xd16308);
        COPPER.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        COPPER.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.COPPER), Integer.valueOf(10));
        COPPER.importTemplate(MINERAL);
        COPPER.setSecondaryColor(0x999999);
        TIN.init("Stannic", "stannus", 0xbdb1bd);
        TIN.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        TIN.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.TIN), Integer.valueOf(10));
        TIN.importTemplate(MINERAL);
        TIN.setSecondaryColor(0x999999);
        SILVER.init("Argenti", "argentus", 0xdbdbdb);
        SILVER.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        SILVER.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.SILVER), Integer.valueOf(10));
        SILVER.importTemplate(MINERAL);
        SILVER.recessive();
        SILVER.recessive();
        SILVER.setSecondaryColor(0x999999);
        BRONZE.init("Aeneus", "pyropus", 0xd48115);
        BRONZE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.STONE), Integer.valueOf(20));
        BRONZE.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BRONZE), Integer.valueOf(10));
        BRONZE.importTemplate(MINERAL);
        BRONZE.recessive();
        BRONZE.setSecondaryColor(0x999999);
        UNSTABLE.init("Unstable", "levis", 0x3e8c34);
        UNSTABLE.importTemplate(ROCK);
        UNSTABLE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(20));
        UNSTABLE.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager.getAllele("effectRadioactive");
        UNSTABLE.recessive();
        UNSTABLE.setSecondaryColor(0x999999);
        NUCLEAR.init("Nuclear", "nucleus", 0x41cc2f);
        NUCLEAR.importTemplate(UNSTABLE);
        NUCLEAR.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(20));
        NUCLEAR.recessive();
        NUCLEAR.setSecondaryColor(0x999999);
        RADIOACTIVE.init("Radioactive", "fervens", 0x1eff00);
        RADIOACTIVE.importTemplate(NUCLEAR);
        RADIOACTIVE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BARREN), Integer.valueOf(20));
        RADIOACTIVE.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.URANIUM), Integer.valueOf(5));
        RADIOACTIVE.setHasEffect(true);
        RADIOACTIVE.recessive();
        RADIOACTIVE.setSecondaryColor(0x999999);
        ANCIENT.init("Ancient", "antiquus", 0xf2db8f);
        ANCIENT.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(30));
        ANCIENT.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager.getAllele("lifespanElongated");
        PRIMEVAL.init("Primeval", "priscus", 0xb3a67b);
        PRIMEVAL.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(30));
        PRIMEVAL.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager.getAllele("lifespanLong");
        PREHISTORIC.init("Prehistoric", "pristinus", 0x6e5a40);
        PREHISTORIC.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(30));
        PREHISTORIC.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager.getAllele("lifespanLonger");
        PREHISTORIC.recessive();
        RELIC.init("Relic", "sapiens", 0x4d3e16);
        RELIC.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(30));
        RELIC.setHasEffect(true);
        RELIC.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager.getAllele("lifespanLongest");
        COAL.init("Fossil", "carbo", 0x7a7648);
        COAL.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(20));
        COAL.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.COAL), Integer.valueOf(10));
        RESIN.init("Preserved", "lacrima", 0xa6731b);
        RESIN.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(20));
        RESIN.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.RESIN), Integer.valueOf(10));
        RESIN.recessive();
        OIL.init("Oil", "lubricus", 0x574770);
        OIL.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OLD), Integer.valueOf(20));
        OIL.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OIL), Integer.valueOf(10));
        DISTILLED.init("Distilled", "distilli", 0x356356);
        DISTILLED.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OIL), Integer.valueOf(30));
        DISTILLED.recessive();
        FUEL.init("Refined", "refina", 0xffc003);
        FUEL.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OIL), Integer.valueOf(25));
        FUEL.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.FUEL), Integer.valueOf(10));
        FUEL.setHasEffect(true);
        CREOSOTE.init("Tarry", "creosota", 0x979e13);
        CREOSOTE.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OIL), Integer.valueOf(25));
        CREOSOTE.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.CREOSOTE), Integer.valueOf(10));
        CREOSOTE.setHasEffect(true);
        LATEX.init("Latex", "latex", 0x494a3e);
        LATEX.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.OIL), Integer.valueOf(25));
        LATEX.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.LATEX), Integer.valueOf(10));
        LATEX.setHasEffect(true);
        WATER.init("Water", "aqua", 0x94a2ff);
        WATER.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.WATER), Integer.valueOf(30));
        WATER.setIsSecret(false);
        WATER.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager.getAllele("boolTrue");
        WATER.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager.getAllele("boolTrue");
        WATER.template[EnumBeeChromosome.HUMIDITY_TOLERANCE.ordinal()] = AlleleManager.getAllele("toleranceBoth1");
        WATER.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.WATER;
        WATER.setHumidity(EnumHumidity.DAMP);
        RIVER.init("River", "flumen", 0x83b3d4);
        RIVER.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.WATER), Integer.valueOf(30));
        RIVER.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.CLAY), Integer.valueOf(15));
        RIVER.importTemplate(WATER);
        OCEAN.init("Ocean", "mare", 0x1d2ead);
        OCEAN.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.WATER), Integer.valueOf(30));
        OCEAN.importTemplate(WATER);
        OCEAN.recessive();
        INK.init("Inkling", "atramentum", 0xe1447);
        INK.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.WATER), Integer.valueOf(30));
        INK.addSpecialty(new ItemStack(Item.INK_SACK, 1, 0), Integer.valueOf(15));
        INK.importTemplate(WATER);
        SWEET.init("Sweet", "mellitus", 0xfc51f1);
        SWEET.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(40));
        SWEET.addProduct(new ItemStack(Item.SUGAR, 1, 0), Integer.valueOf(10));
        SWEET.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.SUGAR;
        SUGAR.init("Sugary", "dulcis", 0xe6d3e0);
        SUGAR.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(40));
        SUGAR.addProduct(new ItemStack(Item.SUGAR, 1, 0), Integer.valueOf(20));
        SUGAR.importTemplate(SWEET);
        FRUIT.init("Fruity", "pomum", 0xdb5876);
        FRUIT.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        FRUIT.addProduct(new ItemStack(Item.SUGAR, 1, 0), Integer.valueOf(10));
        FRUIT.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.FRUIT), Integer.valueOf(15));
        FRUIT.setHasEffect(true);
        FRUIT.importTemplate(SUGAR);
        ALCOHOL.init("Fermented", "vinum", 0xe88a61);
        ALCOHOL.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        ALCOHOL.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.ALCOHOL), Integer.valueOf(15));
        ALCOHOL.importVanillaTemplate("AgrarianBranch");
        ALCOHOL.recessive();
        FARM.init("Farmed", "ager", 0x75db60);
        FARM.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        FARM.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.SEED), Integer.valueOf(15));
        FARM.importVanillaTemplate("AgrarianBranch");
        MILK.init("Bovine", "lacteus", 0xe3e8e8);
        MILK.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        MILK.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.MILK), Integer.valueOf(15));
        MILK.importVanillaTemplate("AgrarianBranch");
        COFFEE.init("Coffee", "arabica", 0x8c5e30);
        COFFEE.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        COFFEE.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.COFFEE), Integer.valueOf(15));
        COFFEE.importVanillaTemplate("AgrarianBranch");
        SWAMP.init("Swamp", "paludis", 0x356933);
        SWAMP.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.MOSSY), Integer.valueOf(30));
        SWAMP.importVanillaTemplate("BoggyBranch");
        SWAMP.setHumidity(EnumHumidity.DAMP);
        BOGGY.init("Boggy", "lama", 0x785c29);
        BOGGY.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.MOSSY), Integer.valueOf(30));
        BOGGY.importTemplate(SWAMP);
        BOGGY.recessive();
        FUNGAL.init("Fungal", "boletus", 0xd16200);
        FUNGAL.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.MOSSY), Integer.valueOf(30));
        FUNGAL.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.FUNGAL), Integer.valueOf(15));
        FUNGAL.importTemplate(BOGGY);
        FUNGAL.setHasEffect(true);
        MARBLE.init("Marbled", "marbla", 0xd6c9cf);
        MARBLE.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        MARBLE.importVanillaTemplate("Noble");
        ROMAN.init("Roman", "roman", 0xad8bb0);
        ROMAN.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        ROMAN.importTemplate(MARBLE);
        GREEK.init("Greek", "greco", 0x854c8a);
        GREEK.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        GREEK.recessive();
        GREEK.importTemplate(ROMAN);
        CLASSICAL.init("Classical", "classica", 0x831d8c);
        CLASSICAL.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.HONEY), Integer.valueOf(30));
        CLASSICAL.addSpecialty(ItemInterface.getItem("royalJelly"), Integer.valueOf(25));
        CLASSICAL.setHasEffect(true);
        CLASSICAL.importTemplate(GREEK);
        BASALT.init("Embittered", "aceri", 0x8c6969);
        BASALT.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.PARCHED), Integer.valueOf(25));
        BASALT.importVanillaTemplate("BranchInfernal");
        BASALT.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager.getAllele("effectAggressive");
        BASALT.setSecondaryColor(0x9a2323);
        BASALT.setHumidity(EnumHumidity.ARID);
        BASALT.setTemperature(EnumTemperature.HELLISH);
        TEMPERED.init("Angry", "turbo", 0x8a4848);
        TEMPERED.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SIMMERING), Integer.valueOf(25));
        TEMPERED.importTemplate(BASALT);
        TEMPERED.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager.getAllele("effectIgnition");
        TEMPERED.recessive();
        TEMPERED.setSecondaryColor(0x9a2323);
        ANGRY.init("Furious", "iratus", 0x801f1f);
        ANGRY.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SIMMERING), Integer.valueOf(25));
        ANGRY.importTemplate(TEMPERED);
        ANGRY.setSecondaryColor(0x9a2323);
        VOLCANIC.init("Volcanic", "volcano", 0x4d0c0c);
        VOLCANIC.importTemplate(ANGRY);
        VOLCANIC.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SIMMERING), Integer.valueOf(25));
        VOLCANIC.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.BLAZE), Integer.valueOf(25));
        VOLCANIC.setHasEffect(true);
        VOLCANIC.setSecondaryColor(0x9a2323);
        MALICIOUS.init("Malicious", "acerbus", 0x782a77);
        MALICIOUS.importVanillaTemplate("TropicalBranch");
        MALICIOUS.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        MALICIOUS.setSecondaryColor(0x69764);
        MALICIOUS.setHumidity(EnumHumidity.DAMP);
        MALICIOUS.setTemperature(EnumTemperature.WARM);
        INFECTIOUS.init("Infectious", "contagio", 0xb82eb5);
        INFECTIOUS.importTemplate(MALICIOUS);
        INFECTIOUS.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        INFECTIOUS.setSecondaryColor(0x69764);
        VIRULENT.init("Virulent", "morbus", 0xf013ec);
        VIRULENT.importTemplate(INFECTIOUS);
        VIRULENT.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        VIRULENT.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.VENOMOUS), Integer.valueOf(15));
        VIRULENT.recessive();
        VIRULENT.setHasEffect(true);
        VIRULENT.setSecondaryColor(0x69764);
        VISCOUS.init("Viscous", "liquidus", 0x9470e);
        VISCOUS.importVanillaTemplate("TropicalBranch");
        VISCOUS.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.ECTOPLASM;
        VISCOUS.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        VISCOUS.setSecondaryColor(0x69764);
        VISCOUS.setHumidity(EnumHumidity.DAMP);
        VISCOUS.setTemperature(EnumTemperature.WARM);
        GLUTINOUS.init("Glutinous", "glutina", 0x1d8c27);
        GLUTINOUS.importTemplate(VISCOUS);
        GLUTINOUS.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        GLUTINOUS.setSecondaryColor(0x69764);
        STICKY.init("Sticky", "lentesco", 0x17e328);
        STICKY.importTemplate(GLUTINOUS);
        STICKY.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(25));
        STICKY.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.SLIME), Integer.valueOf(15));
        STICKY.setHasEffect(true);
        STICKY.setSecondaryColor(0x69764);
        CORROSIVE.init("Corrosive", "corrumpo", 0x4a5c0b);
        CORROSIVE.importVanillaTemplate("TropicalBranch");
        CORROSIVE.setHumidity(EnumHumidity.DAMP);
        CORROSIVE.setTemperature(EnumTemperature.WARM);
        CORROSIVE.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.ACID;
        CORROSIVE.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(20));
        CORROSIVE.recessive();
        CORROSIVE.setSecondaryColor(0x69764);
        CAUSTIC.init("Caustic", "torrens", 0x84a11d);
        CAUSTIC.importTemplate(CORROSIVE);
        CAUSTIC.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(20));
        CAUSTIC.setSecondaryColor(0x69764);
        ACIDIC.init("Acidic", "acidus", 0xc0f016);
        ACIDIC.importTemplate(CAUSTIC);
        ACIDIC.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.SILKY), Integer.valueOf(20));
        ACIDIC.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.ACIDIC), Integer.valueOf(15));
        ACIDIC.setHasEffect(true);
        ACIDIC.setSecondaryColor(0x69764);
        EXCITED.init("Excited", "excita", 0xff4545);
        EXCITED.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
        EXCITED.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.REDSTONE), Integer.valueOf(25));
        EXCITED.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager.getAllele("boolTrue");
        EXCITED.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.REDSTONE;
        ENERGETIC.init("Energetic", "energia", 0xe835c7);
        ENERGETIC.importTemplate(EXCITED);
        ENERGETIC.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
        ENERGETIC.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.REDSTONE), Integer.valueOf(25));
        ENERGETIC.recessive();
        ECSTATIC.init("Ecstatic", "ecstatica", 0xaf35e8);
        ECSTATIC.importTemplate(ENERGETIC);
        ECSTATIC.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
        ECSTATIC.addProduct(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.REDSTONE), Integer.valueOf(25));
        ECSTATIC.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.IC2ENERGY), Integer.valueOf(10));
        ECSTATIC.setHasEffect(true);
        ECSTATIC.setIsSecret(true);
        ARTIC.init("Artic", "artica", 0xade0e0);
        ARTIC.importVanillaTemplate("FrozenBranch");
        ARTIC.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.FROZEN), Integer.valueOf(25));
        ARTIC.setTemperature(EnumTemperature.ICY);
        FREEZING.init("Glacial", "glacia", 0x7be3e3);
        FREEZING.importTemplate(ARTIC);
        FREEZING.addProduct(ItemHoneyComb.getVanillaItem(extrabees.products.ItemHoneyComb.EnumVanillaType.FROZEN), Integer.valueOf(20));
        FREEZING.addSpecialty(ItemHoneyComb.getItem(extrabees.products.ItemHoneyComb.EnumType.GLACIAL), Integer.valueOf(15));
        ROCK.setAchievement(new Achievement(8000, "rockBee", -6, 3, new ItemStack(ItemInterface.getItem("beeQueenGE").getItem(), 1, ROCK.ordinal()), null), "Rock and a Hard Place", "Discovered the Rocky Branch");
        EnumExtraBeeSpecies aenumextrabeespecies[] = values();
        int i = aenumextrabeespecies.length;

        for (int j = 0; j < i; j++)
        {
            EnumExtraBeeSpecies enumextrabeespecies = aenumextrabeespecies[j];
            BeeManager.breedingManager.registerBeeTemplate(enumextrabeespecies.getTemplate());
        }
    }

    private void importVanillaTemplate(String s)
    {
        IAllele aiallele[] = getVanillaTemplate(s);

        for (int i = 0; i < 14; i++)
        {
            if (i != EnumBeeChromosome.SPECIES.ordinal())
            {
                template[i] = aiallele[i];
            }
        }
    }

    private void importTemplate(EnumExtraBeeSpecies enumextrabeespecies)
    {
        for (int i = 0; i < 14; i++)
        {
            if (i != EnumBeeChromosome.SPECIES.ordinal())
            {
                template[i] = enumextrabeespecies.template[i];
            }
        }

        setHumidity(enumextrabeespecies.getHumidity());
        setTemperature(enumextrabeespecies.getTemperature());
    }

    private void init(String s, String s1, int i)
    {
        setName(s);
        setBinomial(s1);
        setPrimaryColor(i);
        setId(ordinal() + Config.speciesID);
        AlleleManager.alleleList[getId()] = this;
    }

    private void recessive()
    {
        dominant = false;
    }

    private EnumExtraBeeSpecies()
    {
        name = "???";
        primaryColor = 0xffffff;
        secondaryColor = 0xffffff;
        temperature = EnumTemperature.NORMAL;
        humidity = EnumHumidity.NORMAL;
        effect = false;
        secret = true;
        counted = true;
        binomial = "???";
        achievement = null;
        id = 0;
        dominant = true;
        products = new HashMap();
        specialty = new HashMap();
        template = ExtraBeeGeneticsCore.getDefaultTemplate();
        template[EnumBeeChromosome.SPECIES.ordinal()] = this;
    }

    public void addProduct(ItemStack itemstack, Integer integer)
    {
        products.put(itemstack, integer);
    }

    public void addSpecialty(ItemStack itemstack, Integer integer)
    {
        specialty.put(itemstack, integer);
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setPrimaryColor(int i)
    {
        primaryColor = i;
    }

    public void setSecondaryColor(int i)
    {
        secondaryColor = i;
    }

    public void setTemperature(EnumTemperature enumtemperature)
    {
        temperature = enumtemperature;
    }

    public void setHumidity(EnumHumidity enumhumidity)
    {
        humidity = enumhumidity;
    }

    public void setHasEffect(boolean flag)
    {
        effect = flag;
    }

    public void setIsSecret(boolean flag)
    {
        secret = flag;
    }

    public void setIsCounted(boolean flag)
    {
        counted = flag;
    }

    public void setBinomial(String s)
    {
        binomial = s;
    }

    public void setAchievement(Achievement achievement1, String s, String s1)
    {
    }

    public void setId(int i)
    {
        id = i;
    }

    public void setIsDominant(boolean flag)
    {
        dominant = flag;
    }

    public String getName()
    {
        return name;
    }

    public int getPrimaryColor()
    {
        return primaryColor;
    }

    public int getSecondaryColor()
    {
        return secondaryColor;
    }

    public EnumTemperature getTemperature()
    {
        return temperature;
    }

    public EnumHumidity getHumidity()
    {
        return humidity;
    }

    public boolean hasEffect()
    {
        return effect;
    }

    public boolean isSecret()
    {
        return secret;
    }

    public boolean isCounted()
    {
        return counted;
    }

    public String getBinomial()
    {
        return binomial;
    }

    public Achievement getAchievement()
    {
        return achievement;
    }

    public int getId()
    {
        return id;
    }

    public boolean isDominant()
    {
        return dominant;
    }

    public HashMap getProducts()
    {
        return products;
    }

    public HashMap getSpecialty()
    {
        return specialty;
    }

    public boolean isJubilant(World world, int i, int j, int k, int l)
    {
        return true;
    }

    public IAllele[] getTemplate()
    {
        return template;
    }

    public void setChromosome(EnumBeeChromosome enumbeechromosome, IAllele iallele)
    {
        template[enumbeechromosome.ordinal()] = iallele;
    }
}
