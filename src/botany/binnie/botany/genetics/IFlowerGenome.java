package binnie.botany.genetics;

import forestry.api.apiculture.IAlleleBeeEffect;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IFlowerProvider;
import forestry.api.genetics.EnumTolerance;
import forestry.api.genetics.IGenome;

public interface IFlowerGenome extends IGenome {

	public IAlleleFlowerSpecies getPrimaryAsFlower();

	public IAlleleFlowerStem getStem();

}
