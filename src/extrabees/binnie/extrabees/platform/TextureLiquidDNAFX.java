package binnie.extrabees.platform;

import java.util.Random;

import binnie.extrabees.core.ExtraBeeTexture;

public class TextureLiquidDNAFX extends TextureLiquidsFX {
	public TextureLiquidDNAFX() {
		super(0, 255, 0, 255, 0, 255, 255, ExtraBeeTexture.Liquids.getTexture());
	}

	@Override
	public void setup() {

		Random rand = new Random();

		super.setup();
		for (int var1 = 0; var1 < this.tileSizeSquare; var1++) {
			red[var1] = (float) (rand.nextFloat() * Math.PI);
			green[var1] = (float) (rand.nextFloat() * Math.PI);
			blue[var1] = red[var1] + rand.nextFloat() * 1.0f - 0.5f;
			alpha[var1] = 1.0f;
		}
	}

	@Override
	public void onTick() {

		Random rand = new Random();

		for (int var1 = 0; var1 < this.tileSizeSquare; var1++) {
			float speed = 0.05f;
			red[var1] += speed;
			green[var1] += speed;
			blue[var1] += speed;
			alpha[var1] = 1.0f;

			if (red[var1] > Math.PI)
				red[var1] = 0.0f;

			if (green[var1] > Math.PI)
				green[var1] = 0.0f;

			if (blue[var1] > Math.PI)
				blue[var1] = 0.0f;

			this.imageData[(var1 * 4 + 0)] = (byte) ((byte) 100 + (Math
					.sin(red[var1]) * 155));
			this.imageData[(var1 * 4 + 1)] = (byte) (Math.sin(green[var1]) * 155);
			this.imageData[(var1 * 4 + 2)] = (byte) ((byte) 100 + (Math
					.sin(blue[var1]) * 155));
			this.imageData[(var1 * 4 + 3)] = (byte) (alpha[var1] * 255);
		}

	}
}
