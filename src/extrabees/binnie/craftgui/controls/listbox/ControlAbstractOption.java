package binnie.craftgui.controls.listbox;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlAbstractOption<T> extends Control {

	T value;
	
	protected ControlAbstractOption(IWidget parent, float x, float y, float w,
			float h, T value) {
		super(parent, x, y, w, h);
		this.value = value;
	}
	
	public final T getValue() {
		return value;
	}

}
