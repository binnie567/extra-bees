package binnie.extrabees.machines;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;

public interface IIndexer extends IInventory {

	public ItemStack getIndexerStackInSlot(int index);

}
