package binnie.extrabees.core;

import binnie.extrabees.liquids.ItemMoltenMetal;
import net.minecraft.src.Item;

public class ExtraBeeItem {
	public static Item comb;
	public static Item propolis;
	public static Item honeyDrop;
	public static Item honeyCrystal;
	public static Item honeyCrystalEmpty;

	public static Item dictionary;
	public static Item templateBlank;
	public static Item template;
	public static Item advancedTemplate;
	public static Item advancedTemplateBlank;
	public static Item serum;
	public static Item serumEmpty;
	public static Item genome;
	public static Item genomeEmpty;

	public static Item liquidDNA;
	
	public static Item moltenLiquid;
	public static Item moltenCast;

	public final static int liquidID = 8500;

	public final static int liquidContainerID = 8510;

	public final static int combID = 8520;
	public final static int propolisID = 8525;
	public final static int honeyDropID = 8530;
	public final static int honeyCrystalID = 8535;
	public final static int honeyCrystalEmptyID = 8536;

	public final static int dictionaryID = 8537;
	public final static int templateBlankID = 8542;
	public final static int templateID = 8541;
	public final static int liquidDnaID = 8543;
	public final static int advancedTemplateID = 8545;
	public final static int advancedTemplateBlankID = 8546;
	public final static int serumID = 8547;
	public final static int serumEmptyID = 8548;
	public final static int genomeID = 8549;
	public final static int genomeEmptyID = 8550;

	public final static int hiveFrameID = 8560;
	
	public final static int moltenLiquidID = 8570;
	public final static int moltenCastID = 8571;
}
