package binnie.extrabees.pipes;

import net.minecraft.src.World;
import buildcraft.api.transport.IExtractionHandler;
import buildcraft.api.transport.IPipe;

public class RedstoneExtractionHandler implements IExtractionHandler {

	@Override
	public boolean canExtractItems(IPipe pipe, World world, int i, int j, int k) {
		if (pipe instanceof PipeItemsRedstoneExtraction) {
			if (((PipeItemsRedstoneExtraction) pipe).redstoneSignal) {
				((PipeItemsRedstoneExtraction) pipe).redstoneSignal = false;
				return true;
			}
			return false;
		}
		return true;
	}

	@Override
	public boolean canExtractLiquids(IPipe pipe, World world, int i, int j,
			int k) {
		return true;
	}

}
