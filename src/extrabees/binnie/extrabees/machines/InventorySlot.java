package binnie.extrabees.machines;

import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class InventorySlot {

	ItemStack itemStack = null;

	SlotValidator validator = null;

	boolean readOnly = false;

	public void setReadOnly() {
		readOnly = true;
	}

	public boolean isItemValid(ItemStack item) {
		if (validator != null)
			return !readOnly && validator.isValid(item);
		return !readOnly;
	}

	public ItemStack getItemStack() {
		return itemStack;
	}

	public void setItemStack(ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	public void setValidator(SlotValidator val) {
		validator = val;
	}

	public ItemStack decrStackSize(int amount) {
		if (itemStack == null)
			return null;

		if (itemStack.stackSize <= amount) {
			ItemStack returnStack = itemStack.copy();
			itemStack = null;
			return returnStack;
		} else {
			ItemStack returnStack = itemStack.copy();
			itemStack.stackSize -= amount;
			returnStack.stackSize = amount;
			return returnStack;
		}
	}

	public void readFromNBT(NBTTagCompound slotNBT) {
		if (slotNBT.hasKey("item")) {
			NBTTagCompound itemNBT = slotNBT.getCompoundTag("item");
			itemStack = ItemStack.loadItemStackFromNBT(itemNBT);
		} else
			itemStack = null;
	}

	public void writeToNBT(NBTTagCompound slotNBT) {
		NBTTagCompound itemNBT = new NBTTagCompound();
		if (itemStack != null)
			itemStack.writeToNBT(itemNBT);
		slotNBT.setCompoundTag("item", itemNBT);
	}

	public boolean isEmpty() {
		return itemStack == null;
	}

}
