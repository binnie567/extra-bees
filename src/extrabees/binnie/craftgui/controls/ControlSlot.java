package binnie.craftgui.controls;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.window.Window;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Slot;

public class ControlSlot extends Control implements ITooltip {

	public Slot slot = null;

	@Override
	@SideOnly(Side.CLIENT)
	public void onMouseClick(EventMouseClick event) {
		if (slot != null) {

			((Window) getSuperParent()).getGui().getMinecraft().playerController
					.windowClick(
							((Window) getSuperParent()).getContainer().windowId,
							slot.slotNumber,
							event.getButton(),
							Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) ? 1 : 0,
							((Window) getSuperParent()).getGui().getMinecraft().thePlayer);

		}
	}

	public ControlSlot(IWidget parent, int x, int y) {
		super(parent, x, y, 18, 18);
		canMouseOver = true;
	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderRect(0, 0, 18, 18, 0, 60);

		if (getSuperParent().getMousedOverWidget() == this) {

			getRenderer().renderGradientRect(1, 1, 16, 16, -2130706433,
					-2130706433);
		}

	}

	@Override
	public void onRenderForeground() {
		ItemStack item = getItemStack();
		if (item != null) {
			getRenderer().renderItem(1, 1, item);
		}
	}

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();

		ItemStack item = getItemStack();

		if (item == null)
			return tooltip;

		return item.getTooltip(((Window) getSuperParent()).getPlayer(), false);
	}

	public ItemStack getItemStack() {
		if (slot != null)
			return slot.getStack();
		return null;
	}

	public void create(IInventory inventory, int index) {
		slot = createSlot(inventory, index);
		((Window) getSuperParent()).getContainer().newSlot(slot);
	}

	public Slot createSlot(IInventory inventory, int index) {
		return new CustomSlot(inventory, index, 0, 0);
	}

}
