package extrabees.products;

import extrabees.core.ExtraBeeCore;
import extrabees.core.ExtraBeeItem;
import forge.ITextureProvider;
import ic2.api.IElectricItem;
import java.util.ArrayList;
import net.minecraft.server.*;

public class ItemHoneyCrystal extends Item implements IElectricItem, ITextureProvider
{
    private int maxCharge;
    private int transferLimit;
    private int tier;

    public ItemHoneyCrystal(int i)
    {
        super(i);
        maxCharge = 8000;
        transferLimit = 500;
        tier = 1;
        setMaxDurability(27);
        e(1);
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return "Honey Crystal";
    }

    public static NBTTagCompound getOrCreateNbtData(ItemStack itemstack)
    {
        NBTTagCompound nbttagcompound = itemstack.getTag();

        if (nbttagcompound == null)
        {
            nbttagcompound = new NBTTagCompound();
            itemstack.setTag(nbttagcompound);
        }

        return nbttagcompound;
    }

    public static int charge(ItemStack itemstack, int i, int j, boolean flag, boolean flag1)
    {
        if (!(itemstack.getItem() instanceof IElectricItem) || i < 0 || itemstack.count > 1)
        {
            return 0;
        }

        IElectricItem ielectricitem = (IElectricItem)itemstack.getItem();

        if (ielectricitem.getTier() > j)
        {
            return 0;
        }

        if (i > ielectricitem.getTransferLimit() && !flag)
        {
            i = ielectricitem.getTransferLimit();
        }

        NBTTagCompound nbttagcompound = getOrCreateNbtData(itemstack);
        int k = nbttagcompound.getInt("charge");

        if (i > ielectricitem.getMaxCharge() - k)
        {
            i = ielectricitem.getMaxCharge() - k;
        }

        k += i;

        if (!flag1)
        {
            nbttagcompound.setInt("charge", k);
            itemstack.id = k <= 0 ? ielectricitem.getEmptyItemId() : ielectricitem.getChargedItemId();

            if (itemstack.getItem() instanceof IElectricItem)
            {
                IElectricItem ielectricitem1 = (IElectricItem)itemstack.getItem();

                if (itemstack.i() > 2)
                {
                    itemstack.setData(1 + ((ielectricitem1.getMaxCharge() - k) * (itemstack.i() - 2)) / ielectricitem1.getMaxCharge());
                }
                else
                {
                    itemstack.setData(0);
                }
            }
            else
            {
                itemstack.setData(0);
            }
        }

        return i;
    }

    public static int discharge(ItemStack itemstack, int i, int j, boolean flag, boolean flag1)
    {
        if (!(itemstack.getItem() instanceof IElectricItem) || i < 0 || itemstack.count > 1)
        {
            return 0;
        }

        IElectricItem ielectricitem = (IElectricItem)itemstack.getItem();

        if (ielectricitem.getTier() > j)
        {
            return 0;
        }

        if (i > ielectricitem.getTransferLimit() && !flag)
        {
            i = ielectricitem.getTransferLimit();
        }

        NBTTagCompound nbttagcompound = getOrCreateNbtData(itemstack);
        int k = nbttagcompound.getInt("charge");

        if (i > k)
        {
            i = k;
        }

        k -= i;

        if (!flag1)
        {
            nbttagcompound.setInt("charge", k);
            itemstack.id = k <= 0 ? ielectricitem.getEmptyItemId() : ielectricitem.getChargedItemId();

            if (itemstack.getItem() instanceof IElectricItem)
            {
                IElectricItem ielectricitem1 = (IElectricItem)itemstack.getItem();

                if (itemstack.i() > 2)
                {
                    itemstack.setData(1 + ((ielectricitem1.getMaxCharge() - k) * (itemstack.i() - 2)) / ielectricitem1.getMaxCharge());
                }
                else
                {
                    itemstack.setData(0);
                }
            }
            else
            {
                itemstack.setData(0);
            }
        }

        return i;
    }

    public static boolean canUse(ItemStack itemstack, int i)
    {
        NBTTagCompound nbttagcompound = getOrCreateNbtData(itemstack);
        return nbttagcompound.getInt("charge") >= i;
    }

    public boolean canProvideEnergy()
    {
        return true;
    }

    public int getChargedItemId()
    {
        return id;
    }

    public int getEmptyItemId()
    {
        if (id == ExtraBeeItem.honeyCrystal.id)
        {
            return ExtraBeeItem.honeyCrystalEmpty.id;
        }
        else
        {
            return id;
        }
    }

    public int getMaxCharge()
    {
        return maxCharge;
    }

    public int getTier()
    {
        return tier;
    }

    public int getTransferLimit()
    {
        return transferLimit;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        ItemStack itemstack = new ItemStack(this, 1);
        charge(itemstack, 0x7fffffff, 0x7fffffff, true, false);
        arraylist.add(itemstack);
        arraylist.add(new ItemStack(this, 1, getMaxDurability()));
    }

    public int getIconFromDamage(int i)
    {
        return 18;
    }

    public String getTextureFile()
    {
        return ExtraBeeCore.getTextureFileMain();
    }
}
