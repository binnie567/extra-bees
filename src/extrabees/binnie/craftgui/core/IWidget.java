package binnie.craftgui.core;

import java.util.List;

import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventKeyTyped;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.events.EventMouseMoved;
import binnie.craftgui.events.EventMouseUp;

public interface IWidget {

	public IWidget getParent();
	
	public void deleteChild(IWidget child);

	public ITopLevelWidget getSuperParent();

	public IRenderer getRenderer();

	public boolean isTopLevel();

	public Vector2f getPosition();

	public Vector2f getOriginalPosition();

	public Vector2f getAbsolutePosition();

	public Vector2f getOriginalAbsolutePosition();

	public void setPosition(Vector2f position);

	public void setPosition(float x, float y);

	public Vector2f getSize();

	public void setSize(Vector2f size);

	public void setSize(float x, float y);

	public Vector2f getOffset();

	public void setOffset(Vector2f offset);

	public void setOffset(float x, float y);

	public Vector2f getMousePosition();

	public Vector2f getRelativeMousePosition();

	public void setColour(int colour);

	public int getColour();

	public void renderBackground();

	public void renderForeground();

	public void renderOverlay();

	public void update();

	public void enable();

	public void disable();

	public void show();

	public void hide();

	public IWidget calculateMousedOverWidget();

	public boolean calculateIsMouseOver();

	public boolean isEnabled();

	public boolean isVisible();

	public boolean isFocused();

	public boolean isMouseOver();

	public boolean isDragged();

	public boolean isChildVisible(IWidget child);

	public boolean isChildEnabled(IWidget child);

	public boolean canMouseOver();

	public boolean canFocus();

	public IWidget addWidget(IWidget widget);

	public List<IWidget> getWidgets();

	public String getTextureFile();

	public void callEvent(Event event);

	public void recieveEvent(Event event);

	public void handleEvent(Event event);

	public void mouseClicked(EventMouseClick event);

	public void keyTyped(EventKeyTyped event);

	public void mouseMoved(EventMouseMoved event);

	public void mouseUp(EventMouseUp event);

	public void onRenderBackground();

	public void onRenderForeground();

	public void onRenderOverlay();

	public void onUpdate();

	public void onMouseClick(EventMouseClick event);

	public void onKeyTyped(EventKeyTyped event);

	public void onMouseMoved(EventMouseMoved event);

	public void onMouseUp(EventMouseUp event);

	public void onHandleEvent(Event event);

	public void onEnable();

	public void onDisable();

	public void onShow();

	public void onHide();

	public void onGainFocus();

	public void onLoseFocus();

	public void onStartDrag();

	public void onEndDrag();

	public void onStartMouseOver();

	public void onEndMouseOver();

	public void onChangePosition();

	public void onChangeSize();

	public void onChangeOffset();

	public void onChangeColour();

}
