package binnie.craftgui.window;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;

public class WindowInventory implements IInventory {

	Map<Integer, ItemStack> inventory = new HashMap<Integer, ItemStack>();

	@Override
	public int getSizeInventory() {
		int max = 0;
		for (int i : inventory.keySet()) {
			if (i > max)
				max = i;
		}
		return max + 1;
	}

	@Override
	public ItemStack getStackInSlot(int var1) {
		if (inventory.containsKey(var1))
			return inventory.get(var1);
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		if (inventory.containsKey(index)) {
			ItemStack item = inventory.get(index);
			ItemStack output = item.copy();
			int available = item.stackSize;
			if (amount > available)
				amount = available;
			item.stackSize -= amount;
			output.stackSize = amount;
			return output;
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int var1, ItemStack var2) {
		inventory.put(var1, var2);
	}

	@Override
	public String getInvName() {
		return "window.inventory";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public void onInventoryChanged() {

	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

}
