package binnie.extrabees.machines;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.genetics.ModuleEngineering;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityIsolator extends TileEntityMachine implements
		IInventory, IPowerReceptor {

	public static final int SlotBee = 2;
	public static final int SlotVials = 3;
	public static final int[] SlotVialsDone = { 4, 5, 6, 7, 8, 9, 10, 11, 12 };

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Isolator;
	}

	public TileEntityIsolator() {
		super(20000, 400, 60000);

		addSlot(SlotBee);

		addSlot(SlotVials);
		addSlotArray(SlotVialsDone);

		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));

		getSlot(SlotVials).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Empty));
		for (int slot : SlotVialsDone) {
			getSlot(slot).setValidator(
					new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));
		}

	}

	public InventorySlot[] getFreeVialSlots() {
		List<InventorySlot> slots = new ArrayList<InventorySlot>();
		for (InventorySlot slot : getSlots(SlotVialsDone)) {
			if (slot.getItemStack() == null)
				slots.add(slot);
		}
		return slots.toArray(new InventorySlot[0]);
	}

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
	}

	@Override
	public void onFinishTask() {

		InventorySlot[] freeSlots = getFreeVialSlots();

		EnumBeeChromosome[] chromosomes = new EnumBeeChromosome[] {
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,
				EnumBeeChromosome.SPECIES, EnumBeeChromosome.SPECIES,

				EnumBeeChromosome.EFFECT, EnumBeeChromosome.EFFECT,
				EnumBeeChromosome.EFFECT,

				EnumBeeChromosome.FLOWER_PROVIDER,
				EnumBeeChromosome.FLOWER_PROVIDER,
				EnumBeeChromosome.FLOWER_PROVIDER,

				EnumBeeChromosome.NOCTURNAL, EnumBeeChromosome.TOLERANT_FLYER,
				EnumBeeChromosome.CAVE_DWELLING, EnumBeeChromosome.NOCTURNAL,
				EnumBeeChromosome.TOLERANT_FLYER,
				EnumBeeChromosome.CAVE_DWELLING,

				EnumBeeChromosome.SPEED, EnumBeeChromosome.SPEED,
				EnumBeeChromosome.SPEED,

				EnumBeeChromosome.LIFESPAN, EnumBeeChromosome.LIFESPAN,

				EnumBeeChromosome.TEMPERATURE_TOLERANCE,
				EnumBeeChromosome.TEMPERATURE_TOLERANCE,

				EnumBeeChromosome.HUMIDITY_TOLERANCE,
				EnumBeeChromosome.HUMIDITY_TOLERANCE,

				EnumBeeChromosome.FERTILITY, EnumBeeChromosome.FERTILITY,

				EnumBeeChromosome.FLOWERING, EnumBeeChromosome.TERRITORY, };
		IBeeGenome genome = BeeManager.beeInterface.getBee(
				getSlot(SlotBee).getItemStack()).getGenome();

		EnumBeeChromosome chromosome = chromosomes[worldObj.rand
				.nextInt(chromosomes.length)];

		IAllele allele;

		if (worldObj.rand.nextBoolean())
			allele = genome.getActiveAllele(chromosome.ordinal());
		else
			allele = genome.getInactiveAllele(chromosome.ordinal());

		ItemStack itemStack = ModuleEngineering.tryCreateSerum(allele,
				chromosome, 5);

		if (itemStack == null)
			return;

		itemStack.setItemDamage(itemStack.getItem().getMaxDamage());

		freeSlots[0].setItemStack(itemStack);
		getSlot(SlotVials).decrStackSize(1);

	}

	@Override
	public ErrorState canWork() {
		if (getSlot(SlotBee).getItemStack() == null)
			return new ErrorState("No Bee");

		if (getSlot(SlotVials).getItemStack() == null)
			return new ErrorState("No Empty Vials");

		if (getFreeVialSlots().length == 0)
			return new ErrorState("No Space for New Vials");

		return super.canWork();
	}

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Isolator.png";
	}

}
