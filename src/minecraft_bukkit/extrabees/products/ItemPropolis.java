package extrabees.products;

import extrabees.core.*;
import extrabees.liquids.ItemLiquid;
import forestry.api.core.ItemInterface;
import forestry.api.liquids.LiquidStack;
import forestry.api.recipes.ISqueezerManager;
import forestry.api.recipes.RecipeManagers;
import forge.ITextureProvider;
import java.util.ArrayList;
import net.minecraft.server.*;
import net.minecraft.server.Block;
import net.minecraft.server.ItemStack;
import extrabees.core.ExtraBeeItem;
import extrabees.products.ItemPropolis.EnumType;

public class ItemPropolis extends Item implements ITextureProvider
{
	public static enum EnumType
    {
        WATER, OIL, FUEL, MILK, FRUIT, SEED, ALCOHOL, CREOSOTE, GLACIAL;

        public static EnumType getTypeFromID(int id)
        {
            return EnumType.class.getEnumConstants()[id];
        }

        public String name;
        public int primaryColor;
        public int secondaryColor;
        public boolean isSecret = false;
        int time;
        LiquidStack liquid;
        ItemStack remenants;
        int chance;

        EnumType()
        {
            this.time = 20;
            this.chance = 0;
            this.remenants = new ItemStack(Block.DIRT, 1, 0);
            liquid = null;
            name = "??? Propolis";
            primaryColor = 0xFFFFFF;
            secondaryColor = 0xFFFFFF;
        }

        public void init(String name, int primaryColor, int secondaryColor)
        {
            this.name = name;
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
        }

        public void setIsSecret()
        {
            this.isSecret = true;
        }

        public void addLiquid(LiquidStack item)
        {
            liquid = item;
        }

        public void addRemenants(ItemStack item, int chance)
        {
            this.remenants = item;
            this.chance = chance;
        }

        public void addRecipe()
        {
            if (liquid != null)
            {
                RecipeManagers.squeezerManager.addRecipe(time,
                new ItemStack[] { new ItemStack(ExtraBeeItem.propolis, 1, this.ordinal()) },
                liquid, remenants, this.chance);
            }
        }
    }

    public ItemPropolis(int i)
    {
        super(i);
        maxStackSize = 64;
        setMaxDurability(0);
        a(true);
        d(98);
    }

    public static void addSubtypes()
    {
        EnumType.WATER.init("Watery Propolis", 0x24b3c9, 0xc2bea7);
        EnumType.WATER.addLiquid(new LiquidStack(Block.STATIONARY_WATER, 500));
        EnumType.OIL.init("Oily Propolis", 0x172f33, 0xc2bea7);

        if (ExtraBeeCore.modBuildcraft && ExtraBeeExtMod.bcOil != null)
        {
            EnumType.OIL.addLiquid(new LiquidStack((new ItemStack(ExtraBeeExtMod.bcOil, 1, 0)).getItem(), 500));
        }

        EnumType.FUEL.init("Petroleum Propolis", 0xa38d12, 0xc2bea7);
        EnumType.FUEL.addLiquid(new LiquidStack(ItemInterface.getItem("liquidBiofuel").getItem(), 500));
        EnumType.MILK.init("Milky Propolis", 0xffffff, 0xc2bea7);
        EnumType.MILK.addLiquid(new LiquidStack(ItemInterface.getItem("liquidMilk").getItem(), 500));
        EnumType.FRUIT.init("Fruity Propolis", 0xde2a2a, 0xc2bea7);
        EnumType.FRUIT.addLiquid(new LiquidStack(ItemInterface.getItem("liquidJuice").getItem(), 500));
        EnumType.FRUIT.addRemenants(ItemInterface.getItem("mulch"), 25);
        EnumType.SEED.init("Seedy Propolis", 0x6ad660, 0xc2bea7);
        EnumType.SEED.addLiquid(new LiquidStack(ItemInterface.getItem("liquidSeedOil").getItem(), 500));
        EnumType.SEED.addRemenants(new ItemStack(Item.SEEDS, 1, 0), 25);
        EnumType.ALCOHOL.init("Alcoholic Propolis", 0x418521, 0xc2bea7);
        EnumType.ALCOHOL.addLiquid(new LiquidStack(ItemInterface.getItem("liquidMead").getItem(), 500));
        EnumType.CREOSOTE.init("Wood Tar Propolis", 0x877501, 0xbda613);
        EnumType.CREOSOTE.addLiquid(new LiquidStack(ItemLiquid.liquids[extrabees.liquids.ItemLiquid.EnumType.CREOSOTE.ordinal()].id, 500, 0));
        EnumType.GLACIAL.init("Icy Propolis", 0xdfebeb, 0x9debeb);
        EnumType.GLACIAL.addLiquid(new LiquidStack(ItemLiquid.liquids[extrabees.liquids.ItemLiquid.EnumType.GLACIAL.ordinal()].id, 500, 0));
        EnumType.GLACIAL.addRemenants(new ItemStack(Item.SNOW_BALL, 1, 0), 25);
    }

    public boolean g()
    {
        return false;
    }

    public boolean isRepairable()
    {
        return false;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return EnumType.getTypeFromID(itemstack.getData()).name;
    }

    public int getColorFromDamage(int i, int j)
    {
        if (j == 0)
        {
            return EnumType.getTypeFromID(i).primaryColor;
        }
        else
        {
            return EnumType.getTypeFromID(i).secondaryColor;
        }
    }

    public int func_46057_a(int i, int j)
    {
        return 98;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        EnumType aenumtype[] = EnumType.values();
        int i = aenumtype.length;

        for (int j = 0; j < i; j++)
        {
            EnumType enumtype = aenumtype[j];

            if (!enumtype.isSecret)
            {
                arraylist.add(new ItemStack(this, 1, enumtype.ordinal()));
            }
        }
    }

    public String getTextureFile()
    {
        return "/gfx/forestry/items/items.png";
    }
}
