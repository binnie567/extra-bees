package binnie.extrabees.genetics;

import java.util.List;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class ItemTemplate extends ItemTemplateBlank {

	enum ObtainableSpecies {
		FOREST("forestry.speciesForest"), MEADOWS("forestry.speciesMeadows"), MODEST(
				"forestry.speciesModest"), TROPICAL("forestry.speciesTropical"), WINTRY(
				"forestry.speciesWintry"), COMMON("forestry.speciesCommon"), WATER(
				"extrabees.species.water"), ROCK("extrabees.species.rock"), ;

		String uid;

		ObtainableSpecies(String uid) {
			this.uid = uid;
		}
	}

	public ItemTemplate(int id) {
		super(id);
		setMaxStackSize(1);
		setIconIndex(33);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer,
			List list, boolean par4) {
		super.addInformation(itemStack, entityPlayer, list, par4);
		ModuleEngineering.getQualityInfo(itemStack, list);
	}

	@Override
	public int getColorFromItemStack(ItemStack itemstack, int j) {
		int i = itemstack.getItemDamage();
		if (j == 0) {
			return 0xFFFFFF;
		}

		IAlleleBeeSpecies species = (IAlleleBeeSpecies) AlleleManager.alleleRegistry
				.getFromMetaMap(i);

		if (species == null)
			return 0xFFFFFF;

		return species.getPrimaryColor();
	}

	// Image ID's
	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		if (j > 0) {
			return 34;
		}

		return 33;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		IAlleleBeeSpecies species = (IAlleleBeeSpecies) AlleleManager.alleleRegistry
				.getFromMetaMap(itemstack.getItemDamage());

		if (species == null)
			return "Corrupt Template";

		return species.getName() + " Template";

	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (ObtainableSpecies oSpecies : ObtainableSpecies.values()) {
			IAllele allele = AlleleManager.alleleRegistry.getAllele(oSpecies.uid);
			if (allele == null) {
				// System.out.println("Did not find species " + oSpecies.uid);
				continue;
			}
			ItemStack stack = new ItemStack(this, 1,
					AlleleManager.alleleRegistry.getFromUIDMap(allele.getUID()));
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setInteger("quality", 5);
			stack.setTagCompound(nbt);
			itemList.add(stack);
		}

	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	public static boolean canObtainSpecies(String uid) {
		for (ObtainableSpecies oSpecies : ObtainableSpecies.values()) {
			if (oSpecies.uid.equals(uid))
				return true;
		}
		return false;
	}

}
