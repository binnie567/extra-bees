package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerPowerMedium extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 12;
	}

	@Override
	public String getDescription() {
		return "Medium Energy";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			double percentage = machine.getEnergyStored()
					/ machine.getMaxEnergyStored();
			return percentage >= 0.35f && percentage <= 0.65f;
		}
		return false;
	}

	public TriggerPowerMedium() {
		super();
	}

}
