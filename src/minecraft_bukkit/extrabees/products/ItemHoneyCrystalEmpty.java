package extrabees.products;

import extrabees.core.ExtraBeeItem;
import java.util.ArrayList;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

public class ItemHoneyCrystalEmpty extends ItemHoneyCrystal
{
    public ItemHoneyCrystalEmpty(int i)
    {
        super(i);
        d(17);
        setMaxDurability(0);
        e(64);
    }

    public int getChargedItemId()
    {
        return ExtraBeeItem.honeyCrystal.id;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        arraylist.add(new ItemStack(this));
    }

    public int getIconFromDamage(int i)
    {
        return textureId;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return "Depleted Honey Crystal";
    }
}
