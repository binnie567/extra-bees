package binnie.extrabees.core;

import java.util.Random;

import binnie.core.BinnieCore;
import binnie.extrabees.apiary.EnumHiveFrame;
import binnie.extrabees.config.Config;
import binnie.extrabees.gen.ModuleGeneration;
import binnie.extrabees.genetics.EnumExtraBeeMutation;
import binnie.extrabees.genetics.EnumExtraBeeSpecies;
import binnie.extrabees.genetics.ModuleGenetics;
import binnie.extrabees.genetics.ModuleGeneticsXML;
import binnie.extrabees.liquids.ModuleLiquids;
import binnie.extrabees.machines.ModuleMachines;
import binnie.extrabees.nei.ModuleNEI;
import binnie.extrabees.pipes.ExtraBeePipesCore;
//import binnie.extrabees.pipes.ExtraBeePipesCore;
import binnie.extrabees.products.ItemHoneyComb;
import binnie.extrabees.products.ItemPropolis;
import binnie.extrabees.triggers.ExtraBeeAction;
import binnie.extrabees.triggers.ExtraBeeTrigger;

import net.minecraft.src.ExtraBees;
import net.minecraft.src.ICommand;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import forestry.api.core.IOreDictionaryHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ISaveEventHandler;

/// EXP

public class ExtraBeePlugin implements IPlugin {

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void preInit() {
		Config.load();
		ExtraBees.proxy.preInit();
	}

	@Override
	public void doInit() {
		ModLoader.getLogger().fine("Starting Initializing of Extra Bees:");

		ExtraBeeCore.checkExternalMods();
		ExtraBeeCore.loadForestryConfigs();

		ModuleGeneration.doInit();

		EnumHiveFrame.init();
		
		ModuleLiquids.doInit();

		ExtraBeeCore.setupBlocks();
		ExtraBeeCore.setupLiquids();
		ExtraBeeCore.setupItems();
		ExtraBeeCore.setupGenetics();

		ExtraBeeCore.addRecipes();

		ModLoader.getLogger().fine(
				EnumExtraBeeSpecies.values().length
						+ " species of bee added to Minecraft");
		ModLoader.getLogger().fine(
				ItemHoneyComb.EnumType.values().length
						+ " honey combs added to Minecraft");
		ModLoader.getLogger().fine(
				ItemPropolis.EnumType.values().length
						+ " propolises added to Minecraft");
		ModLoader.getLogger().fine(
				EnumExtraBeeMutation.values().length
						+ " bee mutations added to Minecraft");

		ModLoader.getLogger().fine("Finishing Initialization of Extra Bees");

		ModuleMachines.doInit();

		ExtraBeeAction.setup();
		ExtraBeeTrigger.setup();

		/*
		 * 
		 * if (ExtraBeeCore.modNEI) ExtraBeeNEIInputHandler.init();
		 */

		if (ExtraBeeCore.modBuildcraft)
			ExtraBeePipesCore.doInit();

		ExtraBees.proxy.doInit();

	}

	@Override
	public void postInit() {
		// ExtraBeeGeneticsCore.init();
		// ExtraBeeGeneticsCore.loadSpecies();
		// EnumBeeBranch.setup();
		// ExtraBeeGeneticsCore.checkSpecies();
		// ExtraBeeGeneticsCore.generateBeeBranchLog();

		ModuleGenetics.postInit();
		ModuleMachines.postInit();
		ExtraBeeCore.addHiveDrops();
		ExtraBees.proxy.postInit();

		if(ExtraBeeCore.modNEI&&BinnieCore.proxy.isClient())
			ModuleNEI.postInit();
		Config.save();
		
		
		
		
		ModuleGeneticsXML.writeToXML();		
		
		
	}

	@Override
	public String getDescription() {
		return "Extra Bees";
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
		ModuleGeneration.generateSurface(world, rand, chunkX, chunkZ);
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

	public static void modLoaded() {
	}

	@Override
	public ISaveEventHandler getSaveEventHandler() {
		return null;
	}

	@Override
	public cpw.mods.fml.common.network.IGuiHandler getGuiHandler() {
		return null;
	}

	@Override
	public forestry.api.core.IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public forestry.api.core.IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IOreDictionaryHandler getDictionaryHandler() {
		return null;
	}

	@Override
	public ICommand[] getConsoleCommands() {
		return null;
	}
}
