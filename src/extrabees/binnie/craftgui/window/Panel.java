package binnie.craftgui.window;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Widget;

public class Panel extends Widget {

	public Panel(IWidget parent, float x, float y, float width, float height,
			Type type) {
		super(parent);
		setPosition(x, y);
		setSize(width, height);
		this.type = type;
	}

	Type type;

	public enum Type {
		Black, Gray, Tinted
	}

	@Override
	public void onRenderBackground() {
		switch (type) {
		case Black:
			((IRendererWindow) getSuperParent().getRenderer())
					.renderPanelBlack(0, 0, (int) this.getSize().x,
							(int) this.getSize().y);
			break;

		case Gray:
			((IRendererWindow) getSuperParent().getRenderer()).renderPanelGray(
					0, 0, (int) this.getSize().x, (int) this.getSize().y);
			break;
			
		case Tinted:
			
			((IRendererWindow) getSuperParent().getRenderer()).renderPanelGray(
					0, 0, (int) this.getSize().x, (int) this.getSize().y);
			break;
		}

	}

	@Override
	public void onRenderForeground() {

	}

	@Override
	public void onRenderOverlay() {

	}

	@Override
	public void onUpdate() {

	}

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-panel.png";
	}
	
}
