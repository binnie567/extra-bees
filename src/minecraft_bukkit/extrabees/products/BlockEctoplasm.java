package extrabees.products;

import extrabees.core.ExtraBeeCore;
import forge.ITextureProvider;
import java.util.Random;
import net.minecraft.server.BlockWeb;
import net.minecraft.server.Item;

public class BlockEctoplasm extends BlockWeb implements ITextureProvider
{
    public BlockEctoplasm(int i)
    {
        super(i, 16);
        f(1);
        c(0.5F);
        a("ectoplasm");
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int a(Random random)
    {
        return random.nextInt(5) != 0 ? 0 : 1;
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int getDropType(int i, Random random, int j)
    {
        return Item.SLIME_BALL.id;
    }

    public String getTextureFile()
    {
        return ExtraBeeCore.getTextureFileMain();
    }
}
