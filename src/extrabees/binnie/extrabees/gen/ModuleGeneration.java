package binnie.extrabees.gen;

import java.util.Random;

import binnie.extrabees.config.Config;

import net.minecraft.src.World;

public class ModuleGeneration {

	static int waterRate = 1;
	static int rockRate = 2;
	static int netherRate = 2;
	static int marbleRate = 2;

	public static void doInit() {
		waterRate = Config.Main.hiveRate("Water", 1);
		rockRate = Config.Main.hiveRate("Rock", 2);
		netherRate = Config.Main.hiveRate("Nether", 2);
		marbleRate = Config.Main.hiveRate("Marble", 2);
	}

	public static void generateSurface(World world, Random rand, int chunkX,
			int chunkZ) {
		for (int i = 0; i < waterRate; i++) {
			int randPosX = chunkX + rand.nextInt(16);
			int randPosY = rand.nextInt(50) + 20;
			int randPosZ = chunkZ + rand.nextInt(16);
			new WorldGenHiveWater().generate(world, rand, randPosX, randPosY,
					randPosZ);
		}

		for (int i = 0; i < rockRate; i++) {
			int randPosX = chunkX + rand.nextInt(16);
			int randPosY = rand.nextInt(50) + 20;
			int randPosZ = chunkZ + rand.nextInt(16);
			new WorldGenHiveRock().generate(world, rand, randPosX, randPosY,
					randPosZ);
		}

		for (int i = 0; i < netherRate; i++) {
			int randPosX = chunkX + rand.nextInt(16);
			int randPosY = rand.nextInt(50) + 20;
			int randPosZ = chunkZ + rand.nextInt(16);
			new WorldGenHiveNether().generate(world, rand, randPosX, randPosY,
					randPosZ);
		}

		for (int i = 0; i < marbleRate; i++) {
			int randPosX = chunkX + rand.nextInt(16);
			int randPosY = rand.nextInt(50) + 20;
			int randPosZ = chunkZ + rand.nextInt(16);
			new WorldGenHiveMarble().generate(world, rand, randPosX, randPosY,
					randPosZ);
		}
	}
}
