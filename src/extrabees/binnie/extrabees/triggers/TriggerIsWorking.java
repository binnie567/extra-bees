package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerIsWorking extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 3;
	}

	@Override
	public String getDescription() {
		return "Is Working";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			return machine.doingWork;
		}
		return false;
	}

	public TriggerIsWorking() {
		super();
	}

}
