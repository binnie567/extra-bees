package extrabees.gen;

import forestry.api.apiculture.IHiveDrop;
import forestry.api.genetics.IAllele;
import net.minecraft.server.World;

public class HiveDrop implements IHiveDrop
{
    private IAllele template[];
    private int chance;

    public HiveDrop(IAllele aiallele[], int i)
    {
        template = aiallele;
        chance = i;
    }

    public IAllele[] getTemplate()
    {
        return template;
    }

    public int getChance(World world, int i, int j, int k)
    {
        return chance;
    }
}
