package binnie.extrabees.machines;

import java.util.List;
import java.util.Random;

import binnie.core.BinnieCore;

import net.minecraft.src.BlockContainer;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockGeneticMachine extends BlockContainer {

	enum Machine {
		Genepool, Sequencer, Splicer, @Deprecated
		AdvancedSequencer, @Deprecated
		AdvancedIndexer

		;

		boolean deprecated = false;

		public void deprecate() {
			deprecated = true;
		}
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (int i = 0; i < getNumberOfMachines(); i++) {
			if (i == 0 || !Machine.values()[i - 1].deprecated)
				itemList.add(new ItemStack(this, 1, i));
		}
	}

	public BlockGeneticMachine(int id) {
		super(id, 0, Material.iron);
		setBlockName("geneticMachine");
		setHardness(1.5f);
		setCreativeTab(CreativeTabs.tabRedstone);
		Machine.AdvancedSequencer.deprecate();
		Machine.AdvancedIndexer.deprecate();
	}

	@Override
	public void randomDisplayTick(World world, int x, int y, int z,
			Random random) {
		TileEntity entity = world.getBlockTileEntity(x, y, z);

		if (entity == null)
			return;

		if (entity instanceof TileEntityMachine) {
			if (!((TileEntityMachine) entity).isInProgress())
				return;
		} else {
			return;
		}

		if (!world.isAirBlock(x, y + 1, z))
			return;

		double xPos, yPos, zPos;

		xPos = x + 0.1 + world.rand.nextDouble() * 0.8;
		zPos = z + 0.1 + world.rand.nextDouble() * 0.8;

		/*
		 * if(entity instanceof TileEntityAcclimatiser) {
		 * TileEntityAcclimatiser.Mode mode = ((TileEntityAcclimatiser)
		 * entity).mode; switch(mode) { case WET: yPos = y + 1.0 +
		 * world.rand.nextDouble();
		 * 
		 * world.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * world.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * break; case DRY: yPos = y + 1.0 + world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("depthsuspend", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("suspended", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("depthsuspend", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); world.spawnParticle("suspended", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); break; case HOT: yPos = y + 1.0 +
		 * world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("flame", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * world.spawnParticle("smoke", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * break; case COLD: yPos = y + 1.0 + world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("snowshovel", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("snowshovel", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); break; default: break; } } /* WATER
		 * 
		 * double xPos = x + par1World.rand.nextDouble(); double xPos = x +
		 * par1World.rand.nextDouble(); double yPos = y + 1.0 +
		 * par1World.rand.nextDouble(); double zPos = z +
		 * par1World.rand.nextDouble();
		 * 
		 * par1World.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); par1World.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D);
		 */

	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return ModuleMachines.modelMachine;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer player, int par6, float par7, float par8, float par9) {

		int metadata = world.getBlockMetadata(x, y, z);

		if (metadata == 0)
			return true;
		;

		if (!BinnieCore.proxy.isSimulating(world)) {
			return true;
		} else {

			if (player.isSneaking())
				return true;

			TileEntity entity = world.getBlockTileEntity(x, y, z);

			if (entity instanceof TileEntityMachine) {
				ExtraBees.proxy.openGui(((TileEntityMachine) entity).getGUI(),
						player, x, y, z);
			}

			return true;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {
		switch (metadata) {
		case 1:
			return new TileEntityGenepool();
		case 2:
			return new TileEntitySequencer();
		case 3:
			return new TileEntitySplicer();
		case 4:
			return new TileEntityAdvancedSequencer();
		case 5:
			return new TileEntityAdvIndexer();
		default:
			return new TileEntityGeneticMachine();
		}
	}

	public String getMachineName(int meta) {
		switch (meta) {
		case 0:
			return "Genetic Machine";

		case 1:
			return "Genepool";

		case 2:
			return "Sequencer";

		case 3:
			return "Splicer";

		case 4:
			return "Genetic Sequencer";

		case 5:
			return "Advanced Indexer";

		default:
			return "??? Machine";
		}
	}

	public int getNumberOfMachines() {
		return Machine.values().length + 1;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, int par5, int par6) {
		TileEntity tileentity = world.getBlockTileEntity(x, y, z);

		if (!(tileentity instanceof TileEntityMachine))
			return;

		TileEntityMachine entity = (TileEntityMachine) tileentity;

		if (entity != null) {
			entity.onBlockDestroy(world, x, y, z);
		}

		super.breakBlock(world, x, y, z, par5, par6);
	}

	@Override
	public int damageDropped(int par1) {
		return par1;
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return createNewTileEntity(var1, 0);
	}

}
