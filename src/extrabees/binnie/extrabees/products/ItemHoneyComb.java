package binnie.extrabees.products;

import java.util.ArrayList;
import java.util.List;

import binnie.extrabees.core.ExtraBeeCore;
import binnie.extrabees.core.ExtraBeeItem;

import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraftforge.oredict.OreDictionary;
import ic2.api.Items;
//import net.minecraft.src.railcraft.common.api.ItemRegistry;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public class ItemHoneyComb extends Item {
	private int subID;

	public ItemHoneyComb(int i, int subID) {
		super(i);
		maxStackSize = 64;
		setMaxDamage(0);
		setHasSubtypes(true);
		setTextureFile("/gfx/forestry/items/items.png");
		setCreativeTab(CreativeTabs.tabMaterials);
		this.subID = subID;
	}

	public static ItemHoneyComb[] combs;

	public static ItemHoneyComb[] setupCombs(int ID) {
		int numberOfItems = 1 + ((EnumType.values().length - 1) / 16);
		combs = new ItemHoneyComb[numberOfItems];

		for (int i = 0; i < numberOfItems; i++) {
			combs[i] = new ItemHoneyComb(ID + i, i);
		}

		return combs;
	}

	public static ItemStack getItem(EnumType type) {
		return new ItemStack(combs[type.ordinal() / 16], 1, type.ordinal() % 16);
	}

	public static void addSubtypes() {

		if ((ItemInterface.getItem("beeswax") == null)
				|| (ItemInterface.getItem("honeyDrop") == null)) {
			ModLoader.throwException("Forstry beeswax or honey not found!",
					new Exception());
		}

		EnumType.BARREN.init("Barren Comb", 0x736C44, 0xC2BEA7);
		EnumType.BARREN.addProduct(ItemInterface.getItem("beeswax"), 100);
		EnumType.BARREN.addProduct(ItemInterface.getItem("honeyDrop"), 50);
		EnumType.ROTTEN.init("Rotten Comb", 0x3E5221, 0xB1CC89);
		EnumType.ROTTEN.addProduct(ItemInterface.getItem("beeswax"), 20);
		EnumType.ROTTEN.addProduct(ItemInterface.getItem("honeyDrop"), 20);
		EnumType.ROTTEN.addProduct(new ItemStack(Item.rottenFlesh, 1, 0), 80);
		EnumType.BONE.init("Skeletal Comb", 0xC4C4AF, 0xDEDEC1);
		EnumType.BONE.addProduct(ItemInterface.getItem("beeswax"), 20);
		EnumType.BONE.addProduct(ItemInterface.getItem("honeyDrop"), 20);
		EnumType.BONE.addProduct(new ItemStack(Item.dyePowder, 1, 15), 80);
		EnumType.OIL.init("Oily Comb", 0x060608, 0x2C2B36);
		EnumType.OIL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1,
				ItemPropolis.EnumType.OIL.ordinal()), 80);
		EnumType.OIL.addProduct(ItemInterface.getItem("honeyDrop"), 75);
		EnumType.COAL.init("Fossilised Comb", 0x9E9478, 0x38311E);
		EnumType.COAL.addProduct(ItemInterface.getItem("beeswax"), 80);
		EnumType.COAL.addProduct(ItemInterface.getItem("honeyDrop"), 75);

		if (ExtraBeeCore.modIC2 && (Items.getItem("coalDust") != null)) {
			EnumType.COAL.addProduct(Items.getItem("coalDust"), 100);
			EnumType.COAL.addProduct(Items.getItem("coalDust"), 50);
		} else {
			EnumType.COAL.addProduct(new ItemStack(Item.coal, 1, 0), 100);
			EnumType.COAL.addProduct(new ItemStack(Item.coal, 1, 0), 50);
		}

		EnumType.WATER.init("Wet Comb", 0x2732CF, 0x79A8C9);
		EnumType.WATER.addProduct(new ItemStack(ExtraBeeItem.propolis, 1,
				ItemPropolis.EnumType.WATER.ordinal()), 100);
		EnumType.WATER.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.STONE.init("Rocky Comb", 0x8C8C91, 0xC6C6CC);
		EnumType.STONE.addProduct(ItemInterface.getItem("beeswax"), 50);
		EnumType.STONE.addProduct(ItemInterface.getItem("honeyDrop"), 25);

		EnumType.MILK.init("Milky Comb", 0xD7D9C7, 0xFFFFFF);
		EnumType.MILK.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.MILK.ordinal()), 100);
		EnumType.MILK.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.FRUIT.init("Fruity Comb", 0x7D2934, 0xDB4F62);
		EnumType.FRUIT.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.APPLE.ordinal()), 100);
		EnumType.FRUIT.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.SEED.init("Seedy Comb", 0x344F33, 0x71CC6E);
		EnumType.SEED.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.SEED.ordinal()), 100);
		EnumType.SEED.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.ALCOHOL.init("Alcoholic Comb", 0x418521, 0xDED94E);
		EnumType.ALCOHOL.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.ALCOHOL.ordinal()), 100);
		EnumType.ALCOHOL.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.FUEL.init("Petroleum Comb", 0x9C6F40, 0xFFC400);
		EnumType.FUEL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1,
				ItemPropolis.EnumType.FUEL.ordinal()), 80);
		EnumType.FUEL.addProduct(ItemInterface.getItem("honeyDrop"), 50);

		EnumType.CREOSOTE.init("Tar Comb", 0x9C810C, 0xBDAA57);
		EnumType.CREOSOTE.addProduct(new ItemStack(ExtraBeeItem.propolis, 1,
				ItemPropolis.EnumType.CREOSOTE.ordinal()), 80);
		EnumType.CREOSOTE.addProduct(ItemInterface.getItem("honeyDrop"), 50);

		EnumType.LATEX.init("Latex Comb", 0x595541, 0xA8A285);
		EnumType.LATEX.addProduct(ItemInterface.getItem("honeyDrop"), 50);
		EnumType.LATEX.addProduct(ItemInterface.getItem("beeswax"), 85);

		if (!OreDictionary.getOres("itemRubber").isEmpty()) {
			EnumType.LATEX.addProduct(OreDictionary.getOres("itemRubber")
					.get(0), 100);
		}

		EnumType.REDSTONE.init("Glowing Comb", 0xFA9696, 0xE61010);
		EnumType.REDSTONE.addProduct(ItemInterface.getItem("beeswax"), 80);
		EnumType.REDSTONE.addProduct(new ItemStack(Item.redstone, 1, 0), 100);
		EnumType.REDSTONE.addProduct(ItemInterface.getItem("honeyDrop"), 50);
		EnumType.RESIN.init("Amber Comb", 0xFFC74F, 0xC98A00);
		EnumType.RESIN.addProduct(ItemInterface.getItem("beeswax"), 100);

		if (ExtraBeeCore.modIC2 && (Items.getItem("resin") != null)) {
			EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
			EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
			EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
			EnumType.RESIN.addProduct(Items.getItem("resin"), 50);
		}

		EnumType.IC2ENERGY.init("Static Comb", 0xE9F50F, 0x20B3C9);
		EnumType.IC2ENERGY.addProduct(ItemInterface.getItem("beeswax"), 80);
		EnumType.IC2ENERGY.addProduct(new ItemStack(Item.redstone, 1, 0), 75);
		EnumType.IC2ENERGY.addProduct(
				ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.ENERGY), 100);
		EnumType.IRON.init("Iron Comb", 0x363534, 0xA87058);
		EnumType.IRON.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("ironDust") != null)) {
			EnumType.IRON.addProduct(Items.getItem("ironDust"), 75);
		} else {
			EnumType.IRON.addProduct(new ItemStack(Item.ingotIron, 1, 0), 75);
		}

		EnumType.GOLD.init("Golden Comb", 0x363534, 0xE6CC0B);
		EnumType.GOLD.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("goldDust") != null)) {
			EnumType.GOLD.addProduct(Items.getItem("goldDust"), 75);
		} else {
			EnumType.GOLD.addProduct(new ItemStack(Item.ingotGold, 1, 0), 75);
		}

		EnumType.COPPER.init("Copper Comb", 0x363534, 0xD16308);
		EnumType.COPPER.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("copperDust") != null)) {
			EnumType.COPPER.addProduct(Items.getItem("copperDust"), 75);
		} else {
			if (!OreDictionary.getOres("ingotCopper").isEmpty()) {
				EnumType.COPPER.addProduct(OreDictionary.getOres("ingotCopper")
						.get(0), 75);
			}
		}

		EnumType.TIN.init("Tin Comb", 0x363534, 0xBDB1BD);
		EnumType.TIN.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("tinDust") != null)) {
			EnumType.TIN.addProduct(Items.getItem("tinDust"), 75);
		} else {
			if (!OreDictionary.getOres("ingotTin").isEmpty()) {
				EnumType.TIN.addProduct(OreDictionary.getOres("ingotTin")
						.get(0), 75);
			}
		}

		EnumType.SILVER.init("Silver Comb", 0x363534, 0xDBDBDB);
		EnumType.SILVER.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("silverDust") != null)) {
			EnumType.SILVER.addProduct(Items.getItem("silverDust"), 75);
		} else {
			if (!OreDictionary.getOres("ingotSilver").isEmpty()) {
				EnumType.SILVER.addProduct(OreDictionary.getOres("ingotSilver")
						.get(0), 75);
			}
		}

		EnumType.BRONZE.init("Bronze Comb", 0x363534, 0xD48115);
		EnumType.BRONZE.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("bronzeDust") != null)) {
			EnumType.BRONZE.addProduct(Items.getItem("bronzeDust"), 15);
		} else {
			if (!OreDictionary.getOres("ingotBronze").isEmpty()) {
				EnumType.BRONZE.addProduct(OreDictionary.getOres("ingotBronze")
						.get(0), 15);
			} else {
				EnumType.BRONZE.copyProducts(EnumType.STONE);
			}
		}

		EnumType.URANIUM.init("Radioactive Comb", 0x1EFF00, 0x41AB33);
		EnumType.URANIUM.copyProducts(EnumType.STONE);

		if (ExtraBeeCore.modIC2 && (Items.getItem("uraniumDrop") != null)) {
			EnumType.URANIUM.addProduct(Items.getItem("uraniumDrop"), 75);
		}

		EnumType.CLAY.init("Clay Comb", 0x6B563A, 0xB0C0D6);
		EnumType.CLAY.addProduct(ItemInterface.getItem("beeswax"), 25);
		EnumType.CLAY.addProduct(ItemInterface.getItem("honeyDrop"), 80);

		if (ExtraBeeCore.modIC2 && (Items.getItem("clayDust") != null)) {
			EnumType.CLAY.addProduct(Items.getItem("clayDust"), 75);
		} else {
			EnumType.CLAY.addProduct(new ItemStack(Item.clay, 1, 0), 75);
		}

		EnumType.OLD.init("Ancient Comb", 0x453314, 0xB39664);
		EnumType.OLD.addProduct(ItemInterface.getItem("beeswax"), 100);
		EnumType.OLD.addProduct(ItemInterface.getItem("honeyDrop"), 90);
		EnumType.FUNGAL.init("Fungal Comb", 0x6E654B, 0x2B9443);
		EnumType.FUNGAL.addProduct(ItemInterface.getItem("beeswax"), 90);
		EnumType.FUNGAL.addProduct(new ItemStack(Block.mushroomBrown, 1, 0),
				100);
		EnumType.FUNGAL.addProduct(new ItemStack(Block.mushroomRed, 1, 0), 75);
		EnumType.ACIDIC.init("Acidic Comb", 0x348543, 0x14F73E);
		EnumType.ACIDIC.addProduct(ItemInterface.getItem("beeswax"), 80);
		EnumType.ACIDIC.addProduct(
				ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.ACID), 80);
		EnumType.VENOMOUS.init("Venomous Comb", 0x7D187D, 0xFF33FF);
		EnumType.VENOMOUS.addProduct(ItemInterface.getItem("beeswax"), 80);
		EnumType.VENOMOUS.addProduct(
				ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.POISON), 80);
		EnumType.SLIME.init("Mucous Comb", 0x3B473C, 0x80D185);
		EnumType.SLIME.addProduct(ItemInterface.getItem("beeswax"), 100);
		EnumType.SLIME.addProduct(ItemInterface.getItem("honeyDrop"), 75);
		EnumType.SLIME.addProduct(new ItemStack(Item.slimeBall, 1, 0), 75);

		EnumType.BLAZE.init("Blazing Comb", 0xFF6A00, 0xFFCC00);
		EnumType.BLAZE.addProduct(ItemInterface.getItem("beeswax"), 75);
		EnumType.BLAZE.addProduct(new ItemStack(Item.blazePowder, 1, 0), 100);

		EnumType.COFFEE.init("Coffee Comb", 0x54381D, 0xB37F4B);
		EnumType.COFFEE.addProduct(ItemInterface.getItem("beeswax"), 90);
		EnumType.COFFEE.addProduct(ItemInterface.getItem("honeyDrop"), 75);
		if (ExtraBeeCore.modIC2 && (Items.getItem("coffeePowder") != null)) {
			EnumType.COFFEE.addProduct(Items.getItem("coffeePowder"), 75);
		}

		EnumType.GLACIAL.init("Glacial Comb", 0x4E8787, 0xCBF2F2);
		EnumType.GLACIAL.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.ICE.ordinal()), 80);
		EnumType.GLACIAL.addProduct(ItemInterface.getItem("honeyDrop"), 75);

		EnumType.MINT.init("Mint Comb", 0x4E664E, 0x84FF80);
		EnumType.MINT.addProduct(ItemInterface.getItem("beeswax"), 90);
		EnumType.MINT.addProduct(ItemInterface.getItem("honeyDrop"), 75);

		EnumType.CITRUS.init("Citrus Comb", 0xDE8209, 0xFFFF08);
		EnumType.CITRUS.addProduct(new ItemStack(ExtraBeeItem.honeyDrop, 1,
				ItemHoneyDrop.EnumType.CITRUS.ordinal()), 100);
		EnumType.CITRUS.addProduct(ItemInterface.getItem("honeyDrop"), 90);

		EnumType.PEAT.init("Peat Comb", 0x402318, 0x94320C);
		EnumType.PEAT.addProduct(new ItemStack(ExtraBeeItem.propolis, 1,
				ItemPropolis.EnumType.PEAT.ordinal()), 80);
		EnumType.PEAT.addProduct(ItemInterface.getItem("honeyDrop"), 50);

		/*
		 * EnumType.SHADOW.init("Shadow Comb", 0x000000, 0x361835);
		 * EnumType.SHADOW.addProduct(ItemInterface.getItem("honeyDrop"), 50);
		 * if (ExtraBeeCore.modRailcraft &&
		 * ItemRegistry.getItem("part.dust.obsidian", 1) != null) {
		 * EnumType.SHADOW.addProduct(
		 * ItemRegistry.getItem("part.dust.obsidian", 1), 80); }
		 */

	}

	@Override
	public boolean isDamageable() {
		return false;
	}

	@Override
	public boolean isRepairable() {
		return false;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return ItemHoneyComb.EnumType.getTypeFromID(itemstack.getItemDamage(),
				subID).name;
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	@Override
	public int getColorFromItemStack(ItemStack itemStack, int j) {
		int i = itemStack.getItemDamage();
		if (j == 0) {
			return EnumType.getTypeFromID(i, subID).primaryColor;
		}

		return EnumType.getTypeFromID(i, subID).secondaryColor;
	}

	// Image ID's

	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		if (j > 0) {
			return 108;
		}

		return 107;
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (EnumType type : EnumType.values()) {
			if (!type.isSecret && EnumType.isTypeInSubItem(type, subID)) {
				itemList.add(new ItemStack(this, 1, type.ordinal() % 16));
			}
		}
	}

	public static ItemStack getVanillaItem(EnumVanillaType type) {
		return new ItemStack(ItemInterface.getItem("beeComb").getItem(), 1,
				type.ordinal());
	}

	public static enum EnumVanillaType {
		HONEY, COCOA, SIMMERING, STRINGY, FROZEN, DRIPPING, SILKY, PARCHED, MYSTERIOUS, IRRADIATED, POWDERY, REDDENED, DARKENED, OMEGA, WHEATEN, MOSSY;
	}

	public static enum EnumType {
		BARREN,

		ROTTEN, BONE,

		OIL, COAL, FUEL,

		WATER, MILK, FRUIT, SEED, ALCOHOL,

		STONE, REDSTONE, RESIN, IC2ENERGY,

		IRON, GOLD, COPPER, TIN, SILVER, BRONZE, URANIUM, CLAY, OLD, FUNGAL,

		CREOSOTE, LATEX, ACIDIC, VENOMOUS, SLIME, BLAZE,

		COFFEE, GLACIAL,

		MINT, CITRUS, PEAT, SHADOW;

		public static EnumType getTypeFromID(int id, int subID) {
			return EnumType.class.getEnumConstants()[id + subID * 16];
		}

		public void copyProducts(EnumType stone2) {
			products.addAll(stone2.products);
			chances.addAll(stone2.chances);
		}

		public static boolean isTypeInSubItem(EnumType type, int subID) {
			return (type.ordinal() / 16) == subID;
		}

		public String name;
		public int primaryColor;
		public int secondaryColor;
		public boolean isSecret = false;
		public ArrayList<ItemStack> products;
		public ArrayList<Integer> chances;
		public int timeToCentifuge;

		EnumType() {
			this.name = "??? Comb";
			this.primaryColor = 0xFFFFFF;
			this.secondaryColor = 0xFFFFFF;
			this.isSecret = false;
			products = new ArrayList<ItemStack>();
			chances = new ArrayList<Integer>();
			this.timeToCentifuge = 20;
		}

		public void init(String name, int primaryColor, int secondaryColor) {
			this.name = name;
			this.primaryColor = primaryColor;
			this.secondaryColor = secondaryColor;
		}

		public EnumType setIsSecret() {
			this.isSecret = true;
			return this;
		}

		public EnumType addProduct(ItemStack item, int chance) {
			products.add(item);
			chances.add(chance);
			return this;
		}

		public void addRecipe() {
			int[] chancesI = new int[chances.size()];

			for (int i = 0; i < chances.size(); i++) {
				chancesI[i] = chances.get(i);
			}

			ItemStack[] productsI = new ItemStack[products.size()];

			for (int i = 0; i < products.size(); i++) {
				productsI[i] = products.get(i);
			}

			RecipeManagers.centrifugeManager.addRecipe(timeToCentifuge,
					ItemHoneyComb.getItem(this), productsI, chancesI);
		}
	}

}
