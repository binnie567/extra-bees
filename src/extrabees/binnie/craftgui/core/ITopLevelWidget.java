package binnie.craftgui.core;

public interface ITopLevelWidget extends IWidget {

	public void setMousePosition(int x, int y);

	public Vector2f getAbsoluteMousePosition();

	public IWidget getFocusedWidget();

	public IWidget getMousedOverWidget();

	public IWidget getDraggedWidget();

	public boolean isFocused(IWidget widget);

	public boolean isMouseOver(IWidget widget);

	public boolean isDragged(IWidget widget);

	public IRenderer getTopLevelRenderer();

	public void updateTopLevel();

	void setRenderer(IRenderer renderer);
}
