package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.ItemStack;

public class SlotValidatorAdvTemplate extends SlotValidator {

	public enum Mode {
		Blank, Written
	}

	public Mode mode;

	public SlotValidatorAdvTemplate(Mode mode) {
		this.mode = mode;
	}

	@Override
	public boolean isValid(ItemStack itemStack) {

		switch (mode) {
		case Blank:
			return itemStack.itemID == ExtraBeeItem.advancedTemplateBlank.shiftedIndex;
		case Written:
			return itemStack.itemID == ExtraBeeItem.advancedTemplate.shiftedIndex;
		default:
			return false;
		}

	}

}
