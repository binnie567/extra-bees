package binnie.botany.genetics;

import java.util.ArrayList;

public enum EnumAcidity {
	ACID("Acidic", 0), WEAK("Weakly Acidic", 0), NETURAL("Neutral", 0), Alkaline("Alkaline", 0);
	public final String name;
	public final int iconIndex;

	private EnumAcidity(String name, int iconIndex) {
		this.name = name;
		this.iconIndex = iconIndex;
	}

	public String getName() {
		return this.name;
	}

	public int getIconIndex() {
		return this.iconIndex;
	}
}
