package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeGUI;

public class TileEntityDatabank extends TileEntityMachine {

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/ApiaristDatabank.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Database;
	}

}
