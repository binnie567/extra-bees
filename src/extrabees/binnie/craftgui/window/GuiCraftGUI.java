package binnie.craftgui.window;

import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import binnie.core.BinnieCore;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventKeyTyped;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.events.EventMouseMoved;
import binnie.craftgui.events.EventMouseUp;

import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiContainer;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.OpenGlHelper;
import net.minecraft.src.RenderHelper;

public class GuiCraftGUI extends GuiContainer {

	@Override
	public void updateScreen() {
		int dx = Mouse.getDX();
		int dy = Mouse.getDY();
		
		if(dx != 0 || dy != 0) {
			IWidget origin = window;
			if (window.getDraggedWidget() != null) {
				origin = window.getDraggedWidget();
			}
			
			float scaleX = (float)this.width/(float)this.mc.displayWidth;
			float scaleY = (float)this.height/(float)this.mc.displayHeight;

			window.callEvent(new EventMouseMoved(origin, dx/scaleX, dy/scaleY));
		}
		
		
		window.update();
	}

	Window window;

	public Minecraft getMinecraft() {
		return mc;
	}

	public GuiCraftGUI(Window window, int width, int height) {
		super(window.getContainer());
		this.window = window;
		this.xSize = width;
		this.ySize = height;
	}

	// Does nothing

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2,
			int var3) {
	}

	@Override
	public void initGui() {
		super.initGui();
		this.mc.thePlayer.openContainer = this.inventorySlots;
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		window.setSize(xSize, ySize);
		window.setPosition(guiLeft, guiTop);
		window.initGui();
	}

	// Main Draw Function

	@Override
	public void drawScreen(int par1, int par2, float par3) {
		window.setMousePosition(par1 - (int) window.getPosition().x, par2
				- (int) window.getPosition().y);

		window.setMousedOverWidget(window.calculateMousedOverWidget());

		this.drawDefaultBackground();
		window.renderBackground();

		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
				240 / 1.0F, 240 / 1.0F);

		window.renderForeground();

		InventoryPlayer var21 = this.mc.thePlayer.inventory;
		if (var21.getItemStack() != null) {
			GL11.glTranslatef(0.0F, 0.0F, 32.0F);
			this.zLevel = 200.0F;
			itemRenderer.zLevel = 200.0F;
			itemRenderer.renderItemIntoGUI(this.fontRenderer,
					this.mc.renderEngine, var21.getItemStack(), par1 - 8,
					par2 - 8);
			itemRenderer.renderItemOverlayIntoGUI(this.fontRenderer,
					this.mc.renderEngine, var21.getItemStack(), par1 - 8,
					par2 - 8);
			this.zLevel = 0.0F;
			itemRenderer.zLevel = 0.0F;
		}

		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.disableStandardItemLighting();

		window.renderOverlay();

		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);

		if (window.getTooltip() != null) {
			List<String> var24 = window.getTooltip();

			if (var24.size() > 0) {
				int var10 = 0;
				int var11;
				int var12;

				for (var11 = 0; var11 < var24.size(); ++var11) {
					var12 = this.fontRenderer.getStringWidth(var24.get(var11));

					if (var12 > var10) {
						var10 = var12;
					}
				}

				var11 = par1 + 12;
				var12 = (par2);
				int var14 = 8;

				if (var24.size() > 1) {
					var14 += 2 + (var24.size() - 1) * 10;
				}

				this.zLevel = 300.0F;
				itemRenderer.zLevel = 300.0F;
				int var15 = -267386864;
				this.drawGradientRect(var11 - 3, var12 - 4, var11 + var10 + 3,
						var12 - 3, var15, var15);
				this.drawGradientRect(var11 - 3, var12 + var14 + 3, var11
						+ var10 + 3, var12 + var14 + 4, var15, var15);
				this.drawGradientRect(var11 - 3, var12 - 3, var11 + var10 + 3,
						var12 + var14 + 3, var15, var15);
				this.drawGradientRect(var11 - 4, var12 - 3, var11 - 3, var12
						+ var14 + 3, var15, var15);
				this.drawGradientRect(var11 + var10 + 3, var12 - 3, var11
						+ var10 + 4, var12 + var14 + 3, var15, var15);
				int var16 = 1347420415;
				int var17 = (var16 & 16711422) >> 1 | var16 & -16777216;
				this.drawGradientRect(var11 - 3, var12 - 3 + 1, var11 - 3 + 1,
						var12 + var14 + 3 - 1, var16, var17);
				this.drawGradientRect(var11 + var10 + 2, var12 - 3 + 1, var11
						+ var10 + 3, var12 + var14 + 3 - 1, var16, var17);
				this.drawGradientRect(var11 - 3, var12 - 3, var11 + var10 + 3,
						var12 - 3 + 1, var16, var16);
				this.drawGradientRect(var11 - 3, var12 + var14 + 2, var11
						+ var10 + 3, var12 + var14 + 3, var17, var17);

				for (int var18 = 0; var18 < var24.size(); ++var18) {
					String var19 = var24.get(var18);

					if (var18 == 0) {
						var19 = "\u00a7f" + var19;
					} else {
						var19 = "\u00a77" + var19;
					}

					this.fontRenderer.drawStringWithShadow(var19, var11, var12,
							-1);

					if (var18 == 0) {
						var12 += 2;
					}

					var12 += 10;
				}

				this.zLevel = 0.0F;
				itemRenderer.zLevel = 0.0F;
			}
		}

		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	public void renderRect(int x, int y, int width, int height, int u, int v) {
		this.drawTexturedModalRect(x, y, u, v, width, height);
	}

	public void renderRectTiled(int x, int y, int width, int height, int u,
			int v, int textWidth, int textHeight) {
		renderRectBordered(x, y, width, height, u, v, textWidth, textHeight, 0,
				0, 0, 0);
	}

	public void renderRectBordered(int posX, int posY, int width, int height,
			int u, int v, int textWidth, int textHeight, int borderLeft,
			int borderRight, int borderTop, int borderBottom) {

		if(width*height*textWidth*textHeight==0)
			return;
		
		this.drawTexturedModalRect(posX, posY, u, v, borderLeft, borderTop);

		// Top Right
		this.drawTexturedModalRect(posX + width - borderRight, posY, u
				+ textWidth - borderRight, v, borderRight, borderTop);

		// Bottom Left
		this.drawTexturedModalRect(posX, posY + height - borderBottom, u, v
				+ textHeight - borderBottom, borderLeft, borderBottom);

		// Bottom Right
		this.drawTexturedModalRect(posX + width - borderRight, posY + height
				- borderBottom, u + textWidth - borderRight, v + textHeight
				- borderBottom, borderRight, borderBottom);

		// Top and Bottom Border
		int currentXPos = borderLeft;
		while (currentXPos < (width - borderRight)) {

			int distanceXRemaining = width - borderRight - currentXPos;

			// Width of the texture that will be rendered
			int texturingWidth = textWidth - borderLeft - borderRight;

			if (texturingWidth > distanceXRemaining)
				texturingWidth = distanceXRemaining;

			// Render Top Border
			this.drawTexturedModalRect(posX + currentXPos, posY,
					u + borderLeft, v, texturingWidth, borderTop);

			this.drawTexturedModalRect(posX + currentXPos, posY + height
					- borderBottom, u + borderLeft, v + textHeight
					- borderBottom, texturingWidth, borderBottom);

			int currentYPos = borderTop;
			while (currentYPos < (height - borderBottom)) {
				int distanceYRemaining = height - borderBottom - currentYPos;

				// Width of the texture that will be rendered
				int texturingHeight = textHeight - borderTop - borderBottom;

				if (texturingHeight > distanceYRemaining)
					texturingHeight = distanceYRemaining;

				// Render Top Border
				this.drawTexturedModalRect(posX + currentXPos, posY
						+ currentYPos, u + borderLeft, v + borderTop,
						texturingWidth, texturingHeight);

				currentYPos += texturingHeight;
			}

			currentXPos += texturingWidth;
		}

		// Top and Bottom Border
		int currentYPos = borderTop;
		while (currentYPos < (height - borderBottom)) {
			int distanceYRemaining = height - borderBottom - currentYPos;

			// Width of the texture that will be rendered
			int texturingHeight = textHeight - borderTop - borderBottom;

			if (texturingHeight > distanceYRemaining)
				texturingHeight = distanceYRemaining;

			// Render Top Border
			this.drawTexturedModalRect(posX, posY + currentYPos, u, v
					+ borderTop, borderLeft, texturingHeight);

			this.drawTexturedModalRect(posX + width - borderRight, posY
					+ currentYPos, u + textWidth - borderRight, v + borderTop,
					borderRight, texturingHeight);
			currentYPos += texturingHeight;
		}

	}

	String currentTexture = "";

	public void setTexture(String texture) {
		if (texture != currentTexture && texture != "" && texture != null) {
			BinnieCore.proxy.bindTexture(texture);
		}
	}

	@Override
	protected void mouseClicked(int x, int y, int button) {
		IWidget origin = window;
		if (window.getMousedOverWidget() != null) {
			origin = window.getMousedOverWidget();
		}
		window.callEvent(new EventMouseClick(origin, x, y, button));
	}

	@Override
	protected void keyTyped(char c, int key) {

		if (key == 1
				|| (key == this.mc.gameSettings.keyBindInventory.keyCode && window
						.getFocusedWidget() == null)) {
			this.mc.thePlayer.closeScreen();
		}
		window.callEvent(new EventKeyTyped(window, c, key));
	}
	@Override
	protected void mouseMovedOrUp(int x, int y, int button) {
		IWidget origin = window;
		if (window.getDraggedWidget() != null) {
			origin = window.getDraggedWidget();
		}

		if (button == -1) {
			// int x =
			// par1-(int)window.getPosition().x-(int)window.getMousePosition().x;
			// int y =
			// par2-(int)window.getPosition().y-(int)window.getMousePosition().y;
			float dx = (float) Mouse.getEventDX() * (float) this.width
					/ this.mc.displayWidth;
			float dy = -(Mouse.getEventDY() * (float) this.height / this.mc.displayHeight);
			//window.callEvent(new EventMouseMoved(origin, dx, dy));
		} else {
			window.callEvent(new EventMouseUp(window, x, y, button));
		}
	}

	@Override
	public void onGuiClosed() {
		window.onClose();
	}

	public void renderPanelMain(int x, int y, int width, int height) {
		renderRectBordered(x, y, width, height, 0, 0, 128, 128, 5, 5, 5, 5);
	}

	public void renderPanelGray(int x, int y, int width, int height) {
		renderRectBordered(x, y, width, height, 128, 0, 32, 32, 1, 1, 1, 1);
	}

	public void renderPanelBlack(int x, int y, int width, int height) {
		renderRectBordered(x, y, width, height, 128, 32, 32, 32, 1, 1, 1, 1);
	}

	public int getTextWidth(String text) {
		return fontRenderer.getStringWidth(text);
	}

	public int getTextHeight() {
		return fontRenderer.FONT_HEIGHT;
	}

	public void renderText(int x, int y, String text, int colour) {
		fontRenderer.drawString(text, x, y, colour);
		GL11.glColor3f(1.0f, 1.0f, 1.0f);
	}

	public void renderOutline(int x, int y, int w, int h, int colour) {
		drawHorizontalLine(x, x + w, y, colour);
		drawHorizontalLine(x, x + w, y + h, colour);
		drawVerticalLine(x, y + h, y, colour);
		drawVerticalLine(x + w, y + h, y, colour);
	}

	public void renderSolidRect(int x, int y, int w, int h, int colour) {
		drawRect(x, y, w, h, colour);
	}

	public void renderItem(int x, int y, ItemStack item) {
		this.zLevel = 200.0F;
		itemRenderer.zLevel = 200.0F;
		itemRenderer.renderItemAndEffectIntoGUI(this.fontRenderer,
				this.mc.renderEngine, item, x, y);
		itemRenderer.renderItemOverlayIntoGUI(this.fontRenderer,
				this.mc.renderEngine, item, x, y);
		this.zLevel = 0.0F;
		itemRenderer.zLevel = 0.0F;

	}

	public void renderGradientRect(int x, int y, int w, int h, int c1, int c2) {
		this.drawGradientRect(x, y, x + w, y + h, c1, c2);

	}

	public void setColour(int hex) {

		int r = (hex & 0xFF0000) >> 16;
		int g = (hex & 0xFF00) >> 8;
		int b = (hex & 0xFF);

		GL11.glColor3f((r) / 255.0f, (g) / 255.0f, (b) / 255.0f);

	}

	public void limitArea(float x, float y, float w, float h) {
		//w *= 1.1f;
		//System.out.println(y);
		y = this.height - (y + h);
		float k = this.xSize;
		float scaleX = (float)this.width/(float)this.mc.displayWidth;
		float scaleY = (float)this.height/(float)this.mc.displayHeight;
		//GL11.glScissor((int)((x+guiLeft)/scaleX)+8, (int)((y+guiTop)/scaleY)-100, (int)(w/scaleX)+2, (int)(h/scaleY));
		GL11.glScissor((int)((x)/scaleX), (int)((y)/scaleY), (int)(w/scaleX)+2, (int)(h/scaleY));
		
	}
}
