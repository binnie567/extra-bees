package binnie.extrabees.liquids;

import java.util.List;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;
import binnie.core.BinnieCore;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.liquids.ItemCast.CastType;
import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.oredict.OreDictionary;

public class ItemMoltenMetal extends Item {
	
	public static int ITEM_ID;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List par3List) {
		for(int i = 0; i < MoltenMetal.values().length; i++)
			par3List.add(new ItemStack(this.shiftedIndex, 1, i));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getIconFromDamage(int par1) {
		return 16 + par1;
	}

	@Override
	public String getItemDisplayName(ItemStack itemStack) {
		if(itemStack==null) return "";
		return "Molten " + MoltenMetal.values()[itemStack.getItemDamage()].toString();
	}
	
	static int ingotAmount = 100;
	static int blockAmount = 400;
	static int nineblockAmount = 800;

	public ItemMoltenMetal(int id) {
		super(id);
		ITEM_ID = shiftedIndex;
		this.hasSubtypes = true;
		setCreativeTab(CreativeTabs.tabMisc);
		setIconIndex(16);
		setTextureFile(ExtraBeeTexture.Liquids.getTexture());
		
	}
	
	public static void setupMoltenLiquids() {
		for(MoltenMetal liquid : MoltenMetal.values()) {
			ExtraBees.proxy.addMoltenFX(liquid);
			
			ItemStack tinGear = ItemInterface.getItem("gearTin");
			ItemStack copperGear = ItemInterface.getItem("gearCopper");
			ItemStack bronzeGear = ItemInterface.getItem("gearBronze");
			ItemStack stoneGear = new ItemStack(Block.cobblestone);
			ItemStack ironGear = new ItemStack(Item.ingotIron);
			ItemStack goldGear = new ItemStack(Item.ingotGold);
			ItemStack diamondGear = new ItemStack(Item.diamond);
			
			try {
				stoneGear = new ItemStack((Item) Class.forName("buildcraft.BuildCraftCore").getField("stoneGearItem").get(null));
				ironGear = new ItemStack((Item) Class.forName("buildcraft.BuildCraftCore").getField("ironGearItem").get(null));
				goldGear = new ItemStack((Item) Class.forName("buildcraft.BuildCraftCore").getField("goldGearItem").get(null));
				diamondGear = new ItemStack((Item) Class.forName("buildcraft.BuildCraftCore").getField("diamondGearItem").get(null));
			} catch (Exception ex) {
				FMLLog.fine("No BuildCraft gears found.");
				return;
			}
			
			switch (liquid) {
			case Iron:
				liquid.addFabricatorRecipe(new ItemStack(Item.ingotIron), ingotAmount);
				liquid.addFabricatorRecipe(new ItemStack(Block.blockSteel), nineblockAmount);
				liquid.addCastRecipe(new ItemStack(Item.helmetSteel), CastType.Helmet);
				liquid.addCastRecipe(new ItemStack(Item.plateSteel), CastType.Chestplate);
				liquid.addCastRecipe(new ItemStack(Item.legsSteel), CastType.Legging);
				liquid.addCastRecipe(new ItemStack(Item.bootsSteel), CastType.Boots);
				liquid.addCastRecipe(new ItemStack(Item.axeSteel), CastType.Axe);
				liquid.addCastRecipe(new ItemStack(Item.pickaxeSteel), CastType.Pickaxe);
				liquid.addCastRecipe(new ItemStack(Item.shovelSteel), CastType.Shovel);
				liquid.addCastRecipe(new ItemStack(Item.hoeSteel), CastType.Hoe);
				liquid.addCastRecipe(new ItemStack(Block.blockSteel), CastType.Block);
				liquid.addGearRecipe(ironGear, stoneGear);
				break;
			case Bronze:
				for(ItemStack ingot : OreDictionary.getOres("ingotBronze"))
					liquid.addFabricatorRecipe(ingot, ingotAmount);
				
				liquid.addCastRecipe(ItemInterface.getItem("bronzePickaxe"), CastType.Pickaxe);
				liquid.addCastRecipe(ItemInterface.getItem("bronzeShovel"), CastType.Shovel);
				
				liquid.addGearRecipe(bronzeGear, stoneGear);
				break;
			case Copper:
				for(ItemStack ingot : OreDictionary.getOres("ingotCopper"))
					liquid.addFabricatorRecipe(ingot, ingotAmount);
				
				liquid.addGearRecipe(copperGear, stoneGear);
				break;
			case Diamond:
				liquid.addFabricatorRecipe(new ItemStack(Item.diamond), ingotAmount);
				liquid.addFabricatorRecipe(new ItemStack(Block.blockDiamond), nineblockAmount);
				
				liquid.addCastRecipe(new ItemStack(Item.helmetDiamond), CastType.Helmet);
				liquid.addCastRecipe(new ItemStack(Item.plateDiamond), CastType.Chestplate);
				liquid.addCastRecipe(new ItemStack(Item.legsDiamond), CastType.Legging);
				liquid.addCastRecipe(new ItemStack(Item.bootsDiamond), CastType.Boots);
				liquid.addCastRecipe(new ItemStack(Item.axeDiamond), CastType.Axe);
				liquid.addCastRecipe(new ItemStack(Item.pickaxeDiamond), CastType.Pickaxe);
				liquid.addCastRecipe(new ItemStack(Item.shovelDiamond), CastType.Shovel);
				liquid.addCastRecipe(new ItemStack(Item.hoeDiamond), CastType.Hoe);
				liquid.addCastRecipe(new ItemStack(Block.blockDiamond), CastType.Block);
				
				liquid.addGearRecipe(diamondGear, goldGear);
				break;
			case Emerald:
				liquid.addFabricatorRecipe(new ItemStack(Item.emerald), ingotAmount);
				liquid.addFabricatorRecipe(new ItemStack(Block.blockEmerald), nineblockAmount);
				
				break;
			case Gold:
				liquid.addFabricatorRecipe(new ItemStack(Item.ingotGold), ingotAmount);
				liquid.addFabricatorRecipe(new ItemStack(Block.blockGold), nineblockAmount);
				
				liquid.addCastRecipe(new ItemStack(Item.helmetGold), CastType.Helmet);
				liquid.addCastRecipe(new ItemStack(Item.plateGold), CastType.Chestplate);
				liquid.addCastRecipe(new ItemStack(Item.legsGold), CastType.Legging);
				liquid.addCastRecipe(new ItemStack(Item.bootsGold), CastType.Boots);
				liquid.addCastRecipe(new ItemStack(Item.axeGold), CastType.Axe);
				liquid.addCastRecipe(new ItemStack(Item.pickaxeGold), CastType.Pickaxe);
				liquid.addCastRecipe(new ItemStack(Item.shovelGold), CastType.Shovel);
				liquid.addCastRecipe(new ItemStack(Item.hoeGold), CastType.Hoe);
				liquid.addCastRecipe(new ItemStack(Block.blockGold), CastType.Block);
				
				liquid.addGearRecipe(goldGear, ironGear);
				
				break;
			case Obsidian:
				liquid.addFabricatorRecipe(new ItemStack(Block.obsidian), blockAmount);
				break;
			case Silver:
				for(ItemStack ingot : OreDictionary.getOres("ingotSilver"))
					liquid.addFabricatorRecipe(ingot, ingotAmount);
				break;
			case Tin:
				for(ItemStack ingot : OreDictionary.getOres("ingotTin"))
					liquid.addFabricatorRecipe(ingot, ingotAmount);
				
				liquid.addGearRecipe(tinGear, stoneGear);
				break;
			default:
				break;
			}
			
			
		}
	}

	public enum MoltenMetal {
		Iron(150, 150, 150, 2000, 0.7f),
		Copper(210, 141, 77, 1400, 0.8f),
		Tin(192, 221, 233, 250, 0.9f),
		Bronze(207, 167, 56, 1200, 0.8f),
		Silver(192, 204, 217, 1200, 0.8f),
		Gold(235, 235, 20, 1400, 0.8f), // 255
		Diamond(140, 235, 226, 5000, 0.6f), // 244
		Emerald(23, 221, 98, 2800, 0.7f),
		Obsidian(56, 48, 80, 1400, 0.8f),
		;
		public int red;
		public int blue;
		public int green;
		public int mp;
		public float mult;
		MoltenMetal(int r, int g, int b, int mp, float m) {
			red = r;
			green = g;
			blue = b;
			mult = m;
			this.mp = mp;
		}
		public void addGearRecipe(ItemStack gear, ItemStack gearBase) {
			RecipeManagers.fabricatorManager.addRecipe(
					new ItemStack(ExtraBeeItem.moltenCast.shiftedIndex, 1, CastType.Gear.ordinal()),  
					getLiquidStack((int)(ingotAmount*CastType.Gear.amount*mult)),
					gear,
					CastType.Gear.getGearRecipe(gearBase));
		}
		public void addCastRecipe(ItemStack item, CastType type) {
			RecipeManagers.fabricatorManager.addRecipe(
					new ItemStack(ExtraBeeItem.moltenCast.shiftedIndex, 1, type.ordinal()),  
					getLiquidStack((int)(ingotAmount*type.amount*mult)),
					item,
					type.getRecipe());
		}
		public LiquidStack getLiquidStack(int amount) {
			return LiquidDictionary.getOrCreateLiquid(getMoltenName(), 
					new LiquidStack(ITEM_ID,  1000, ordinal()));
		}
		public void addFabricatorRecipe(ItemStack item, int amount) {
			RecipeManagers.fabricatorManager.addSmelting(item, getLiquidStack(amount), mp);
		}
		String getMoltenName() {
			return "molten"+toString();
		}
	}

}
