package binnie.extrabees.genetics;

import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeTexture;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;

public class ItemDictionary extends Item {

	public ItemDictionary(int par1) {
		super(par1);
		setIconIndex(19);
		setTextureFile(ExtraBeeTexture.Main.getTexture());
		setCreativeTab(CreativeTabs.tabTools);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world,
			EntityPlayer player) {
		// ExtraBeePlatform.openDictionaryGui(player);
		ExtraBees.proxy.openGui(ExtraBeeGUI.Database, player,
				(int) player.posX, (int) player.posY, (int) player.posZ);
		return itemstack;
	}

	@Override
	public String getItemDisplayName(ItemStack i) {
		return "Apiarist Database";
	}
}