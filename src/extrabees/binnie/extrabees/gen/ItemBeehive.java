package binnie.extrabees.gen;

import java.util.List;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;

public class ItemBeehive extends ItemBlock {
	public ItemBeehive(int i) {
		super(i);
		setMaxDamage(0);
		setHasSubtypes(true);
		this.setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public int getMetadata(int i) {
		return i;
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return getItemNameIS(par1ItemStack);
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (int i = 0; i < 4; i++) {
			itemList.add(new ItemStack(this, 1, i));
		}
	}

	@Override
	public String getItemNameIS(ItemStack itemstack) {
		switch (itemstack.getItemDamage()) {
		case 0:
			return "Water Hive";

		case 1:
			return "Rock Hive";

		case 2:
			return "Nether Hive";

		case 3:
			return "Marble Hive";

		default:
			return "??? Hive";
		}
	}
}
