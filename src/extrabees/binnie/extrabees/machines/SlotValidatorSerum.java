package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.ItemStack;

public class SlotValidatorSerum extends SlotValidator {

	public enum Mode {
		Empty, Serum
	}

	public Mode mode;

	public SlotValidatorSerum(Mode mode) {
		this.mode = mode;
	}

	@Override
	public boolean isValid(ItemStack itemStack) {

		switch (mode) {
		case Empty:
			return itemStack.itemID == ExtraBeeItem.serumEmpty.shiftedIndex;
		case Serum:
			return itemStack.itemID == ExtraBeeItem.serum.shiftedIndex;

		default:
			return false;
		}
	}

}
