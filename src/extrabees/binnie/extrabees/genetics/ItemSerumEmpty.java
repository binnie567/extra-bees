package binnie.extrabees.genetics;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemSerumEmpty extends Item {

	public ItemSerumEmpty(int id) {
		super(id);
		setMaxStackSize(32);
		setIconIndex(52);
		setTextureFile(ExtraBeeTexture.Engineering.getTexture());
		setCreativeTab(CreativeTabs.tabMisc);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return "Empty Serum Vial";
	}

}
