package binnie.extrabees.core;

public enum ExtraBeeTexture {

	Main("/gfx/extrabees/extrabees.png"), Liquids(
			"/gfx/extrabees/extrabees_liquids.png"), Icons(
			"/gfx/extrabees/extrabees_icons.png"), Engineering(
			"/gfx/extrabees/extrabees_engineering.png"), FX(
			"/gfx/extrabees/FX.png"), Triggers(
			"/gfx/extrabees/extrabees_triggers.png"), Containers("/gfx/extrabees/extrabees_containers.png");

	String texture;

	ExtraBeeTexture(String texture) {
		this.texture = texture;
	}

	public String getTexture() {
		return texture;
	}

}
