package binnie.craftgui.window;

import forestry.api.apiculture.BeeManager;
import binnie.extrabees.machines.IInventorySlots;
import binnie.extrabees.machines.InventorySlot;
import binnie.extrabees.machines.TileEntityIndexer;
import net.minecraft.src.Container;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Slot;

public class ContainerCraftGUI extends Container {

	Window window;

	public ContainerCraftGUI(Window window) {
		super();
		this.window = window;
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return true;
	}

	public void newSlot(Slot slot) {
		this.addSlotToContainer(slot);
	}

	@Override
	public final ItemStack transferStackInSlot(EntityPlayer player, int slotID) {

		Slot slot = (Slot) inventorySlots.get(slotID);

		if (!slot.getHasStack())
			return null;

		ItemStack itemstack = slot.getStack().copy();

		IInventory machine = window.getInventory();

		if (slot.inventory == player.inventory) {
			if (machine instanceof IInventorySlots
					&& !(machine instanceof TileEntityIndexer)) {
				for (InventorySlot iSlot : ((IInventorySlots) machine)
						.getAllSlots()) {

					if (!iSlot.isItemValid(itemstack))
						continue;

					if (iSlot.getItemStack() == null) {
						iSlot.setItemStack(itemstack.copy());
						slot.putStack(null);
						return null;
					}

					if (itemstack.isStackable()) {
						ItemStack merged = iSlot.getItemStack().copy();
						ItemStack[] newStacks = mergeStacks(itemstack, merged);
						itemstack = newStacks[0];
						if (!newStacks[1].equals(merged))
							iSlot.setItemStack(newStacks[1]);
						if (itemstack == null) {
							slot.putStack(null);
							return null;
						}

					}

				}
			} else {

				if (machine instanceof TileEntityIndexer
						&& !BeeManager.beeInterface.isBee(itemstack))
					return null;

				for (int i = 0; i < machine.getSizeInventory(); i++) {

					if (machine.getStackInSlot(i) == null) {
						machine.setInventorySlotContents(i, itemstack.copy());
						slot.putStack(null);
						return null;
					}

					if (itemstack.isStackable()) {
						ItemStack merged = machine.getStackInSlot(i).copy();
						ItemStack[] newStacks = mergeStacks(itemstack, merged);
						itemstack = newStacks[0];
						machine.setInventorySlotContents(i, newStacks[1]);
						if (itemstack == null) {
							slot.putStack(null);
							return null;
						}

					}

				}
			}
		} else {
			// Machine to Player
			for (int i = 0; i < player.inventory.getSizeInventory(); i++) {

				if (player.inventory.getStackInSlot(i) == null) {
					player.inventory.setInventorySlotContents(i,
							itemstack.copy());
					slot.putStack(null);
					return null;
				}

				if (itemstack.isStackable()) {
					ItemStack merged = player.inventory.getStackInSlot(i)
							.copy();
					ItemStack[] newStacks = mergeStacks(itemstack, merged);
					itemstack = newStacks[0];
					player.inventory.setInventorySlotContents(i, newStacks[1]);
					if (itemstack == null) {
						slot.putStack(null);
						return null;
					}

				}

			}
		}

		slot.putStack(itemstack);

		return null;
	}

	public ItemStack[] mergeStacks(ItemStack itemstack, ItemStack merged) {
		if (ItemStack.areItemStacksEqual(itemstack, merged)
				&& itemstack.isItemEqual(merged)) {
			int space = merged.getMaxStackSize() - merged.stackSize;
			if (space > 0) {
				if (itemstack.stackSize > space) {
					itemstack.stackSize -= space;
					merged.stackSize += space;
				} else if (itemstack.stackSize <= space) {
					merged.stackSize += itemstack.stackSize;
					itemstack = null;
				}
			}
		}

		return new ItemStack[] { itemstack, merged };
	}
}
