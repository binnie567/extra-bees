package binnie.extrabees.machines;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import net.minecraft.src.EntityItem;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;
import binnie.core.BinnieCore;
import binnie.core.network.BinniePacket;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.PacketPayload;
import binnie.core.network.PacketUpdate;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.network.PacketID;
import binnie.extrabees.network.PacketITankContainer;
import binnie.extrabees.triggers.ActionCancelTask;
import binnie.extrabees.triggers.ActionPauseProcess;
import binnie.extrabees.triggers.ExtraBeeAction;
import binnie.extrabees.triggers.ExtraBeeTrigger;
import net.minecraftforge.common.ForgeDirection;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.inventory.ISelectiveInventory;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import buildcraft.api.power.IPowerProvider;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerFramework;

public class TileEntityMachine extends TileEntity implements IInventory,
		ISelectiveInventory, ICustomGates, IInventorySlots, INetworkedEntity,
		ITankContainer {

	public class ErrorState {
		String name;

		public ErrorState(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public TileEntityMachine() {
		super();
		IPowerProvider power = PowerFramework.currentFramework
				.createPowerProvider();
		power.configure(1, 5, 10, 25, 5000);
		setPowerProvider(power);

	}

	private float energyPerTick = 0.0f;
	private float progressPerTick = 0.0f;

	public TileEntityMachine(int energyUsed, int timeTaken, int maxEnergy) {
		this();
		energyPerTick = (float) energyUsed / (float) timeTaken;
		progressPerTick = 100.0f / timeTaken;
		int maxEnergyPerTick = (int) (energyPerTick * 1.5f);

		if (BinnieCore.proxy.isDebug()) {
			energyPerTick = 1.0f;
			progressPerTick = 1.0f;
			maxEnergyPerTick = 1000;
		}
		
		int minEnergyPerTick = 1;

		getPowerProvider().configure(1, minEnergyPerTick, maxEnergyPerTick, minEnergyPerTick,
				maxEnergy);
	}

	public ExtraBeeGUI getGUI() {
		return null;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {

		super.writeToNBT(nbttagcompound);

		NBTTagList inventoryNBT = new NBTTagList();

		for (Map.Entry<Integer, InventorySlot> entry : inventory.entrySet()) {
			NBTTagCompound slotNBT = new NBTTagCompound();
			slotNBT.setInteger("id", entry.getKey());
			entry.getValue().writeToNBT(slotNBT);
			inventoryNBT.appendTag(slotNBT);
		}

		nbttagcompound.setTag("inventory", inventoryNBT);

		NBTTagList tanksNBT = new NBTTagList();

		for (Map.Entry<Integer, ILiquidTank> entry : liquidTanks.entrySet()) {
			NBTTagCompound tankNBT = new NBTTagCompound();
			tankNBT.setInteger("index", entry.getKey());
			if (entry.getValue().getLiquid() != null)
				entry.getValue().getLiquid().writeToNBT(tankNBT);
			tanksNBT.appendTag(tankNBT);

		}

		nbttagcompound.setTag("liquidTanks", tanksNBT);

		if (getPowerProvider() != null) {
			NBTTagCompound powerNBT = new NBTTagCompound();
			getPowerProvider().writeToNBT(powerNBT);
			nbttagcompound.setCompoundTag("powerProvider", powerNBT);
		}

		nbttagcompound.setFloat("progressAmount", progressAmount);

		sendNetworkUpdate();
		sendITankContainerPacket();

	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {

		super.readFromNBT(nbttagcompound);

		if (nbttagcompound.hasKey("inventory")) {
			NBTTagList inventoryNBT = nbttagcompound.getTagList("inventory");
			for (int i = 0; i < inventoryNBT.tagCount(); i++) {
				NBTTagCompound slotNBT = (NBTTagCompound) inventoryNBT.tagAt(i);
				int index = slotNBT.getInteger("id");
				if (slotNBT.hasKey("Slot"))
					index = slotNBT.getByte("Slot") & 0xff;
				if (inventory.containsKey(index)) {
					inventory.get(index).readFromNBT(slotNBT);
				}
			}
		}

		if (nbttagcompound.hasKey("liquidTanks")) {
			NBTTagList tanksNBT = nbttagcompound.getTagList("liquidTanks");
			for (int i = 0; i < tanksNBT.tagCount(); i++) {
				NBTTagCompound tankNBT = (NBTTagCompound) tanksNBT.tagAt(i);
				int index = tankNBT.getInteger("index");
				if (liquidTanks.containsKey(index)) {
					LiquidStack liquid = LiquidStack
							.loadLiquidStackFromNBT(tankNBT);
					liquidTanks.get(i).setLiquid(liquid);
				}
			}
		}

		if (nbttagcompound.hasKey("powerProvider")
				&& getPowerProvider() != null) {
			NBTTagCompound powerNBT = new NBTTagCompound();
			NBTTagCompound powerNBTNew = nbttagcompound
					.getCompoundTag("powerProvider");
			getPowerProvider().writeToNBT(powerNBT);
			if (powerNBTNew.hasKey("storedEnergy"))
				powerNBT.setFloat("storedEnergy",
						powerNBTNew.getFloat("storedEnergy"));
			getPowerProvider().readFromNBT(powerNBT);
		}

		progressAmount = nbttagcompound.getFloat("progressAmount");
	}

	@Override
	public final void addSlot(int index) {
		inventory.put(index, new InventorySlot());
	}

	@Override
	public final void addSlotArray(int[] indexes) {
		for (int k : indexes) {
			addSlot(k);
		}
	}

	@Override
	public InventorySlot getSlot(int index) {
		if (inventory.containsKey(index))
			return inventory.get(index);
		return null;
	}

	@Override
	public InventorySlot[] getAllSlots() {
		return inventory.values().toArray(new InventorySlot[0]);
	}

	@Override
	public InventorySlot[] getSlots(int[] indexes) {
		List<InventorySlot> list = new ArrayList<InventorySlot>();
		for (int i : indexes) {
			if (getSlot(i) != null)
				list.add(getSlot(i));
		}
		return list.toArray(new InventorySlot[0]);
	}

	Map<Integer, InventorySlot> inventory = new LinkedHashMap<Integer, InventorySlot>();

	// IInventory

	@Override
	public int getSizeInventory() {
		return inventory.size();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if (inventory.containsKey(index))
			return inventory.get(index).getItemStack();
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int amount) {
		if (inventory.containsKey(index)) {
			onInventoryChanged();
			return inventory.get(index).decrStackSize(amount);
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack itemStack) {
		if (inventory.containsKey(index))
			inventory.get(index).setItemStack(itemStack);
		onInventoryChanged();
	}

	protected void transferItem(int indexFrom, int indexTo) {
		if (inventory.containsKey(indexFrom) && inventory.containsKey(indexTo)) {
			ItemStack newStack = inventory.get(indexFrom).getItemStack().copy();
			inventory.get(indexFrom).setItemStack(null);
			inventory.get(indexTo).setItemStack(newStack);
		}
		onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {

	}

	@Override
	public void closeChest() {

	}

	public boolean isInProgress() {
		return progressAmount > 0.0f;
	}

	@Override
	public void updateEntity() {

		if (!BinnieCore.proxy.isSimulating(worldObj)) {
			if(BinnieCore.proxy.isDebug())
				doFX(worldObj, xCoord, yCoord, zCoord, 1.0f);
			return;
		}

		if (actionPauseProcess > 0)
			actionPauseProcess--;
		if (actionCancelTask > 0)
			actionCancelTask--;

		if (this instanceof IPowerReceptor && getPowerProvider() != null) {
			getPowerProvider().update((IPowerReceptor) this);
		}
		if (canWork() == null) {
			if (!isInProgress() && getAvailableEnergy() > 0.0f) {
				onStartTask();
				progressAmount += 0.01f;
			}
		} else {
			if (isInProgress()) {
				onCancelTask();
				progressAmount = 0.0f;
			}

		}
		if (progressAmount >= 100.0f) {
			onFinishTask();
			progressAmount = 0.0f;
		}

		sendNetworkUpdate();
		if(this.syncLiquidTanks)
			sendITankContainerPacket();

	}

	public final void doFX(World world, int x, int y, int z, float efficiency) {
		onFX(world, x, y, z, efficiency);
	}

	@Override
	public boolean canUpdate() {
		return true;
	}

	private final List<ItemStack> getInventoryOnDestroy() {
		List<ItemStack> list = new ArrayList<ItemStack>();
		for (InventorySlot item : inventory.values()) {
			if (item.getItemStack() != null) {
				list.add(item.getItemStack());
			}
		}
		return list;
	}

	public void onBlockDestroy(World world, int x, int y, int z) {
		for (ItemStack stack : getInventoryOnDestroy()) {
			if (stack != null) {
				float var8 = world.rand.nextFloat() * 0.8F + 0.1F;
				float var9 = world.rand.nextFloat() * 0.8F + 0.1F;
				EntityItem var12;

				for (float var10 = world.rand.nextFloat() * 0.8F + 0.1F; stack.stackSize > 0; world
						.spawnEntityInWorld(var12)) {
					int var11 = world.rand.nextInt(21) + 10;

					if (var11 > stack.stackSize) {
						var11 = stack.stackSize;
					}

					stack.stackSize -= var11;
					var12 = new EntityItem(world, x + var8, y + var9,
							z + var10, new ItemStack(stack.itemID, var11,
									stack.getItemDamage()));
					float var13 = 0.05F;
					var12.motionX = (float) world.rand.nextGaussian() * var13;
					var12.motionY = (float) world.rand.nextGaussian() * var13
							+ 0.2F;
					var12.motionZ = (float) world.rand.nextGaussian() * var13;

					if (stack.hasTagCompound()) {
						var12.item.setTagCompound((NBTTagCompound) stack
								.getTagCompound().copy());
					}
				}
			}
		}
	}

	// IPowerProvider

	private IPowerProvider powerProvider = null;
	private float progressAmount = 0.0f;
	private int storedEnergy = 0;

	private float powerUsed = 0.0f;
	private float effieciency = 0.0f;

	public final void setPowerProvider(IPowerProvider provider) {
		powerProvider = provider;
	}

	public final IPowerProvider getPowerProvider() {
		return powerProvider;
	}

	public final void doWork() {
		doingWork = false;
		if (canWork() == null && canProgress() == null)
			progressTick();
	}

	public final int powerRequest() {
		float space = powerProvider.getMaxEnergyStored()
				- powerProvider.getEnergyStored();

		if (space < powerProvider.getMaxEnergyReceived())
			if (space > powerProvider.getMinEnergyReceived())
				return (int) space;

		return powerProvider.getMaxEnergyReceived();
	}

	public final float getAvailableEnergy() {
		if (this instanceof IPowerReceptor) {
			return powerProvider.useEnergy(0.0f, getPowerPerTick(), false);
		} else {
			return getPowerPerTick();
		}
	}

	public final void progressTick() {
		powerUsed = powerProvider.useEnergy(0.0f, getPowerPerTick(), false);
		if (powerUsed > 0.0f) {
			doingWork = true;
			effieciency = powerProvider
					.useEnergy(0.0f, getPowerPerTick(), true)
					/ getPowerPerTick();
			progressAmount += effieciency * getProgressPerTick();
			onProgressTask(effieciency);
			if (!BinnieCore.proxy.isSimulating(worldObj)) {
				doFX(this.worldObj, this.xCoord, this.yCoord, this.zCoord,
						effieciency);
			}
			
		}
	}

	public ErrorState canWork() {
		return actionCancelTask == 0 ? null : new ErrorState("Task Cancelled");
	}

	public ErrorState canProgress() {
		return actionPauseProcess == 0 ? null
				: new ErrorState("Process Paused");
	}

	public ErrorState powerErrorState() {
		if (powerUsed == 0.0f)
			return new ErrorState("Not Enough Power");
		return null;
	}

	public void onStartTask() {

	}

	public void onProgressTask(float effieciency) {

	}

	public void onFinishTask() {

	}

	public void onCancelTask() {

	}

	@SideOnly(Side.CLIENT)
	public void onFX(World world, int x, int y, int z, float efficiency) {

	}

	public float getProgressPerTick() {
		return progressPerTick;

	}

	public float getPowerPerTick() {
		return energyPerTick;
	}

	public float getCurrentProgress() {
		return this.progressAmount;
	}

	// ITankContainer

	Map<Integer, ILiquidTank> liquidTanks = new LinkedHashMap<Integer, ILiquidTank>();
	boolean syncLiquidTanks = true;

	public void setLiquidTanks(List<ILiquidTank> tanks) {
		int n = 0;
		for (int x : liquidTanks.keySet()) {
			liquidTanks.put(x, tanks.get(n));
			n++;
		}
	}

	public final void sendITankContainerPacket() {
		syncLiquidTanks = false;
		if (this.canContainLiquid()) {
			BinniePacket packet = new PacketITankContainer(this.xCoord,
					this.yCoord, this.zCoord, this);
			ExtraBees.proxy.sendNetworkPacket(packet, xCoord, yCoord, zCoord);
		}
	}

	@Override
	public final int fill(ForgeDirection from, LiquidStack resource,
			boolean doFill) {
		int index = getTankIndexToFill(from, resource);
		if (liquidTanks.containsKey(index)) {
			return fill(index, resource, doFill);
		} else
			return 0;
	}

	@Override
	public final LiquidStack drain(ForgeDirection from, int maxDrain,
			boolean doDrain) {
		int index = getTankIndexToDrain(from);
		if (liquidTanks.containsKey(index)) {
			return drain(index, maxDrain, doDrain);
		} else
			return null;
	}

	@Override
	public final int fill(int tankIndex, LiquidStack resource, boolean doFill) {
		if (!this.liquidTanks.containsKey(tankIndex))
			return 0;
		ILiquidTank tank = liquidTanks.get(tankIndex);
		if (doFill)
			syncLiquidTanks = true;
		return tank.fill(resource, doFill);
	}

	@Override
	public final LiquidStack drain(int tankIndex, int maxDrain, boolean doDrain) {
		if (!this.liquidTanks.containsKey(tankIndex))
			return null;
		ILiquidTank tank = liquidTanks.get(tankIndex);
		if (doDrain)
			syncLiquidTanks = true;
		return tank.drain(maxDrain, doDrain);
	}

	@Override
	public final ILiquidTank[] getTanks(ForgeDirection direction) {
		return liquidTanks.values().toArray(new ILiquidTank[0]);
	}

	// Custom Liquid

	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}

	public final void addTank(int index, ILiquidTank tank) {
		liquidTanks.put(index, tank);
	}

	public final ILiquidTank getTank(int index) {
		return liquidTanks.get(index);
	}

	@Override
	public ILiquidTank getTank(ForgeDirection direction, LiquidStack type) {
		return null;
	}

	// ISelectiveInventory

	@Override
	public final int addItem(ItemStack stack, boolean doAdd, ForgeDirection from) {
		List<InventorySlot> slots = getSlotsToFill(stack, from);
		for (InventorySlot slot : slots) {
			if (slot.getItemStack() == null && slot.isItemValid(stack)) {
				if (doAdd) {
					slot.setItemStack(stack);
					onInventoryChanged();
				}

				return stack.stackSize;
			}
		}
		return 0;
	}

	@Override
	public final ItemStack[] extractItem(boolean doRemove, ForgeDirection from,
			int maxItemCount) {
		List<InventorySlot> slots = getSlotsToExtract(from);
		for (InventorySlot slot : slots) {
			if (slot.getItemStack() != null) {
				ItemStack extracted = slot.getItemStack().copy();
				if (doRemove) {
					slot.setItemStack(null);
					onInventoryChanged();
				}

				return new ItemStack[] { extracted };
			}
		}
		return new ItemStack[0];
	}

	@Override
	public final ItemStack[] extractItem(Object[] desired, boolean exclusion,
			boolean doRemove, ForgeDirection from, int maxItemCount) {
		return extractItem(doRemove, from, maxItemCount);
	}

	public List<InventorySlot> getSlotsToFill(ItemStack stack,
			ForgeDirection from) {
		List<InventorySlot> list = new ArrayList<InventorySlot>();
		for (InventorySlot slot : this.inventory.values()) {
			if (!slot.readOnly && slot.isItemValid(stack))
				list.add(slot);
		}
		return list;
	}

	public List<InventorySlot> getSlotsToExtract(ForgeDirection from) {
		return new ArrayList<InventorySlot>(this.inventory.values());
	}

	// ICustomGates

	@Override
	public final void actionActivated(IAction action) {
		if (action instanceof ActionCancelTask) {
			this.actionCancelTask = 20;
			this.onCancelTask();
		} else if (action instanceof ActionPauseProcess) {
			this.actionPauseProcess = 20;
		}
		onAction(action);
	}

	@Override
	public final LinkedList<IAction> getCustomActions() {
		LinkedList<IAction> list = new LinkedList<IAction>();
		list.add(ExtraBeeAction.actionPauseProcess);
		list.add(ExtraBeeAction.actionCancelTask);
		getActions(list);
		return list;
	}

	@Override
	public final LinkedList<ITrigger> getCustomTriggers() {
		LinkedList<ITrigger> list = new LinkedList<ITrigger>();
		list.add(ExtraBeeTrigger.triggerCanWork);
		list.add(ExtraBeeTrigger.triggerCannotWork);
		list.add(ExtraBeeTrigger.triggerIsWorking);
		list.add(ExtraBeeTrigger.triggerIsNotWorking);
		list.add(ExtraBeeTrigger.triggerPowerNone);
		list.add(ExtraBeeTrigger.triggerPowerLow);
		list.add(ExtraBeeTrigger.triggerPowerMedium);
		list.add(ExtraBeeTrigger.triggerPowerHigh);
		list.add(ExtraBeeTrigger.triggerPowerFull);
		getTriggers(list);
		return list;
	}

	// Custom functions
	public void getActions(LinkedList<IAction> actions) {
	}

	public void getTriggers(LinkedList<ITrigger> actions) {
	}

	public void onAction(IAction action) {
	}

	// Queries

	public final boolean canBePowered() {
		return this instanceof IPowerReceptor;
	}

	public final boolean canContainLiquid() {
		return this instanceof ITankContainer;
	}

	public String getRenderTexture() {
		return "";
	}

	@Override
	public void sendNetworkUpdate() {
		PacketUpdate packet = new PacketUpdate(
				PacketID.NetworkEntityUpdate.ordinal(), this.xCoord,
				this.yCoord, this.zCoord, getPacketPayload());
		ExtraBees.proxy.sendNetworkPacket(packet, xCoord, yCoord, zCoord);
	}

	@Override
	public final PacketPayload getPacketPayload() {
		PacketPayload payload = new PacketPayload();
		if (this.canBePowered()) {
			payload.addFloat(getPowerProvider().getEnergyStored());
			payload.addInteger(getPowerProvider().getMaxEnergyReceived());
			payload.addInteger(getPowerProvider().getMaxEnergyStored());
			payload.addFloat(this.progressAmount);
		}
		writeToPacket(payload);
		return payload;
	}

	public void writeToPacket(PacketPayload data) {

	}

	public void readFromPacket(PacketPayload data) {

	}

	@Override
	public final void fromPacketPayload(PacketPayload payload) {
		if (this.canBePowered()) {
			clientEnergyStored = payload.getFloat();
			clientMaxEnergyRecieved = payload.getInteger();
			clientMaxEnergyStored = payload.getInteger();
			this.progressAmount = payload.getFloat();
		}
		readFromPacket(payload);
	}

	float clientEnergyStored = 0.0f;
	int clientMaxEnergyRecieved = 0;
	int clientMaxEnergyStored = 0;

	public boolean doingWork;
	public int actionCancelTask = 0;
	public int actionPauseProcess = 0;

	public float getEnergyStored() {
		if (this.canBePowered()) {
			if (BinnieCore.proxy.isSimulating(this.worldObj)) {
				return getPowerProvider().getEnergyStored();
			} else {
				return clientEnergyStored;
			}
		}
		return 0.0f;
	}

	public int getMaxEnergyReceived() {
		if (this.canBePowered()) {
			if (BinnieCore.proxy.isSimulating(this.worldObj)) {
				return getPowerProvider().getMaxEnergyReceived();
			} else {
				return clientMaxEnergyRecieved;
			}
		}
		return 0;
	}

	public int getMaxEnergyStored() {
		if (this.canBePowered()) {
			if (BinnieCore.proxy.isSimulating(this.worldObj)) {
				return getPowerProvider().getMaxEnergyStored();
			} else {
				return clientMaxEnergyStored;
			}
		}
		return 0;
	}

	@Override
	public void clientConnected() {
		// this.sendNetworkUpdate();
		// this.sendITankContainerPacket(true);
	}

	public ItemStack shiftClick(ItemStack stack) {
		return stack;
	}

}
