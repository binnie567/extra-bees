package extrabees.gen;

import extrabees.core.ExtraBeeBlock;
import forestry.api.core.GlobalManager;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.*;

public class WorldGenHiveMarble extends WorldGenerator
{
    public WorldGenHiveMarble()
    {
    }

    public boolean a(World world, Random random, int i, int j, int k)
    {
        BiomeBase biomebase = world.getWorldChunkManager().getBiome(i, k);
        int l = i;
        int i1 = j;
        int j1 = k;

        if (world.isEmpty(l, i1, j1) && world.isEmpty(l, i1 + 1, j1) && GlobalManager.dirtBlockIds.contains(Integer.valueOf(world.getTypeId(l, i1 - 1, j1))))
        {
            world.setRawTypeIdAndData(l, i1, j1, ExtraBeeBlock.hive.id, 3);
        }

        return true;
    }
}
