package binnie.botany.proxy;

import net.minecraft.src.EntityPlayer;
import binnie.core.network.BinniePacket;
import binnie.core.proxy.IProxyCore;

public interface IBotanyProxy extends IProxyCore {

	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z);

	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer);

	public void sendToServer(BinniePacket packet);

}
