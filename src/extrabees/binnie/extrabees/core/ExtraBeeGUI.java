package binnie.extrabees.core;

public enum ExtraBeeGUI {

	Database, DatabaseNEI, Acclimatiser, Indexer, Genepool, Sequencer, Splicer, IndustrialAlveary, AdvancedSequencer, LogicPipe, Isolator, Replicator, Purifier, Inoculator, AdvancedIndexer, Synthesizer

}
