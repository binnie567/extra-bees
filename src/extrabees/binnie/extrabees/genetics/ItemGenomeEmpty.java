package binnie.extrabees.genetics;

import java.util.List;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemGenomeEmpty extends Item {

	public ItemGenomeEmpty(int id) {
		super(id);
		setMaxStackSize(32);
		setIconIndex(128);
		setTextureFile(ExtraBeeTexture.Engineering.getTexture());
		setCreativeTab(CreativeTabs.tabMisc);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return "Empty Genome Vial";
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
	}

}
