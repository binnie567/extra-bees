package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.ItemStack;

public class SlotValidatorTemplate extends SlotValidator {

	public enum Mode {
		Blank, Written, Written2
	}

	public Mode mode;

	public SlotValidatorTemplate(Mode mode) {
		this.mode = mode;
	}

	@Override
	public boolean isValid(ItemStack itemStack) {

		switch (mode) {
		case Blank:
			return itemStack.itemID == ExtraBeeItem.templateBlank.shiftedIndex;
		case Written:
			return itemStack.itemID == ExtraBeeItem.template.shiftedIndex;

		case Written2:
			return itemStack.itemID == ExtraBeeItem.template.shiftedIndex
					|| itemStack.itemID == ExtraBeeItem.advancedTemplate.shiftedIndex;
		default:
			return false;
		}
	}

}
