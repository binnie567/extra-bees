package binnie.craftgui.window;

import java.util.List;

import cpw.mods.fml.common.Side;

import binnie.core.BinnieCore;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlText.Alignment;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.TopLevelWidget;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;

public abstract class Window extends TopLevelWidget {

	public List<String> getTooltip() {

		if (getMousedOverWidget() == null)
			return null;

		if (getMousedOverWidget() instanceof ITooltip)
			return ((ITooltip) getMousedOverWidget()).getTooltip();
		else
			return null;
	}

	private GuiCraftGUI gui;
	private ContainerCraftGUI container;
	private WindowInventory windowInventory;
	private ControlText title;

	public Window(int width, int height, EntityPlayer player,
			IInventory inventory, Side side) {
		super();
		this.side = side;
		setInventories(player, inventory);
		container = new ContainerCraftGUI(this);
		windowInventory = new WindowInventory();
		if (isServer())
			setRenderer(new Renderer(null));
		else {
			gui = new GuiCraftGUI(this, width, height);
			setRenderer(new Renderer(gui));
		}

	}

	// Window specific functions

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public final GuiCraftGUI getGui() {
		return gui;
	}

	public final ContainerCraftGUI getContainer() {
		return container;
	}

	public final IInventory getWindowInventory() {
		return windowInventory;
	}

	public final void initGui() {
		title = new ControlText(this, (int) (getSize().x / 2.0), 9, " ",
				Alignment.Center);
		title.setColour(4210752);
		initialize();
	}

	public void initialize() {

	}

	@Override
	public void onRenderBackground() {
		
		getRenderer().setColour(this.getColour());
		
		((IRendererWindow) getRenderer()).renderPanelMain(0, 0,
				(int) this.getSize().x, (int) this.getSize().y);
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {

	}

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-panel.png";
	}

	private EntityPlayer player;
	private IInventory entityInventory;

	public EntityPlayer getPlayer() {
		return player;
	}

	public ItemStack getHeldItemStack() {
		if (player != null)
			return player.inventory.getItemStack();
		return null;
	}

	public IInventory getInventory() {
		return entityInventory;
	}

	public void setInventories(EntityPlayer player2, IInventory inventory) {
		player = player2;
		entityInventory = inventory;
	}

	public void onClose() {

	}

	public void setHeldItemStack(ItemStack stack) {
		if (player != null)
			player.inventory.setItemStack(stack);

	}

	// Side code

	Side side = Side.CLIENT;

	public boolean isServer() {
		return !isClient();
	}

	public boolean isClient() {
		return side == Side.CLIENT;
	}

	public World getWorld() {
		if(getPlayer()!=null)
			return getPlayer().worldObj;
		return BinnieCore.proxy.getWorld();
	}

}
