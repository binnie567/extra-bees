package binnie.extrabees.proxy;

import net.minecraft.src.EntityPlayer;
import binnie.core.network.BinniePacket;
import binnie.core.proxy.IProxyCore;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.liquids.ItemMoltenMetal;

public interface IExtraBeesProxy extends IProxyCore {

	public void openGui(ExtraBeeGUI ID, EntityPlayer player, int x, int y, int z);

	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z);

	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer);

	public void sendToServer(BinniePacket packet);
	
	public void addMoltenFX(ItemMoltenMetal.MoltenMetal type);

}
