package binnie.extrabees.machines;

import binnie.extrabees.core.ExtraBeeBlock;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;

public class ItemApiaristMachine extends ItemBlock {

	BlockApiaristMachine associatedBlock;

	public ItemApiaristMachine(int i) {
		super(i);
		setMaxDamage(0);
		setHasSubtypes(true);
		this.associatedBlock = ExtraBeeBlock.apiaristMachine;
	}

	@Override
	public int getMetadata(int i) {
		return i;
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return getItemNameIS(par1ItemStack);
	}

	@Override
	public String getItemNameIS(ItemStack itemstack) {
		return associatedBlock.getMachineName(itemstack.getItemDamage());

	}
}
