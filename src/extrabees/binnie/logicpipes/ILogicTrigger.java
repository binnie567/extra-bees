package binnie.logicpipes;

import java.util.List;

public interface ILogicTrigger {

	/**
	 * Return the texture file for this trigger icon
	 */
	public abstract String getTextureFile();

	/**
	 * Return the icon id in the texture file
	 */
	public abstract int getIndexInTexture();

	/**
	 * Return the trigger description in the UI
	 */
	public abstract String getDescription();

	/**
	 * Return the trigger tooltip, not including description in the UI
	 */
	public abstract List<String> getTooltip();

}
