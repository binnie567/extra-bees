package binnie.extrabees.core;

import java.util.Arrays;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;

public class ExtraBeeUtil {

	public static boolean tryStack(IInventory inventory, int slotID,
			ItemStack added, boolean doAdd) {

		if (added == null)
			return false;

		if (inventory.getStackInSlot(slotID) == null) {
			if (doAdd) {
				inventory.setInventorySlotContents(slotID, added.copy());
				added.stackSize = 0;
			}

			return true;
		}

		ItemStack stack = inventory.getStackInSlot(slotID);

		if (stack.itemID != added.itemID)
			return false;

		if (stack.getItemDamage() != added.getItemDamage())
			return false;

		if ((stack.stackSize + added.stackSize) > stack.getMaxStackSize())
			return false;

		if (stack.getTagCompound() == null && added.getTagCompound() == null) {

		} else if (stack.getTagCompound() == null
				|| added.getTagCompound() == null) {
			return false;
		} else {
			if (!stack.getTagCompound().equals(added.getTagCompound())) {
				return false;
			}
		}

		if (doAdd) {
			stack.stackSize += added.stackSize;
			added.stackSize = 0;
			inventory.setInventorySlotContents(slotID, stack);

		}

		return true;

	};

	public static <T> T[] concat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

	public static int[] concat(int[] first, int[] second) {
		int[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

	public static float[] concat(float[] first, float[] second) {
		float[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

}
