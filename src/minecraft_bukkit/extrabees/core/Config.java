package extrabees.core;

import java.io.File;

import extrabees.platform.ExtraBeePlatform;
import forge.Configuration;
import forge.Property;

public class Config
{
    public static final String CATEGORY_GENERAL = "general";
    public static final String CATEGORY_BLOCK = "block";
    public static final String CATEGORY_ITEM = "item";
    public static Configuration config;

    public static int hiveID;
    public static int ectoplasmID;
    public static int combID;
    public static int propolisID;
    public static int speciesID;

    public static int honeyDropID;
    public static int honeyCrystalID;
    public static int honeyCrystalEmptyID;
    public static int liquidID;
    public static int containerID;
	//public static boolean debugMode;
	public static boolean canQuarryMineHives;

    public static void load()
    {
        config = new Configuration(new File(ExtraBeePlatform.getDirectory(),
                "config/forestry/extrabees.conf"));
        config.load();
        
        Property propHiveID = config.getOrCreateBlockIdProperty("hive", 4000);
        propHiveID.comment = "Block ID for Hives";
        hiveID = Integer.parseInt(propHiveID.value);
        
        Property propEctoplasmID = config.getOrCreateBlockIdProperty(
                "ectoplasm", 4001);
        propEctoplasmID.comment = "Block ID for ectoplasm";
        ectoplasmID = Integer.parseInt(propEctoplasmID.value);
        
        Property propLiquidID = config.getOrCreateIntProperty("liquidID",
                CATEGORY_ITEM, 8500);
        propLiquidID.comment = "Item ID for Liquids";
        liquidID = Integer.parseInt(propLiquidID.value);
        
        Property propContainerID = config.getOrCreateIntProperty("containerID",
                CATEGORY_ITEM, 8510);
        propContainerID.comment = "Item ID for Containers";
        containerID = Integer.parseInt(propContainerID.value);
        
        Property propCombID = config.getOrCreateIntProperty("combID",
                CATEGORY_ITEM, 8520);
        propCombID.comment = "Item ID for Combs, will use consecutive item ids if required";
        combID = Integer.parseInt(propCombID.value);
        
        Property propPropolisID = config.getOrCreateIntProperty("propolisID",
                CATEGORY_ITEM, 8525);
        propPropolisID.comment = "Item ID for Propolis, will use consecutive item ids if required";
        propolisID = Integer.parseInt(propPropolisID.value);
        
        Property propSpeciesID = config.getOrCreateIntProperty("speciesID",
                CATEGORY_ITEM, 65);
        propSpeciesID.comment = "The first ID used by Extra Bees Species, do not change unless you know what it does";
        speciesID = Integer.parseInt(propSpeciesID.value);
        
        Property propHoneyDropID = config.getOrCreateIntProperty("honeyDropID",
                CATEGORY_ITEM, 8530);
        propHoneyDropID.comment = "Item ID of Energy Drop";
        honeyDropID = Integer.parseInt(propHoneyDropID.value);
        
        Property propHoneyCrystalID = config.getOrCreateIntProperty(
                "honeyCrystalID", CATEGORY_ITEM, 8535);
        propHoneyCrystalID.comment = "Item ID of Honey Crystal";
        honeyCrystalID = Integer.parseInt(propHoneyCrystalID.value);
        
        Property propHoneyCrystalEmptyID = config.getOrCreateIntProperty(
                "honeyCrystalEmptyID", CATEGORY_ITEM, 8536);
        propHoneyCrystalEmptyID.comment = "Item ID of Honey Crystal (Empty)";
        honeyCrystalEmptyID = Integer.parseInt(propHoneyCrystalEmptyID.value);
        
        //Property propDebugMode = config.getOrCreateBooleanProperty(
        //        "debugMode", CATEGORY_GENERAL, false);
        //propDebugMode.comment = "Debug Mode - All extra bees in creative, useful for testing";
        //debugMode = Boolean.parseBoolean(propDebugMode.value);
        
        Property propQuarryMineHives = config.getOrCreateBooleanProperty(
                "canQuarryMineHives", CATEGORY_GENERAL, true);
        propQuarryMineHives.comment = "Set to false to stop BC quarry from mining hives. It will leave them instead";
        canQuarryMineHives = Boolean.parseBoolean(propQuarryMineHives.value);
        
        config.save();
    }
}
