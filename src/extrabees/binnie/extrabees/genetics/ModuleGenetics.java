package binnie.extrabees.genetics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import binnie.core.BinnieCore;
import binnie.extrabees.config.Config;
import binnie.extrabees.core.ExtraBeeItem;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBranch;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IMutation;
import forestry.api.recipes.RecipeManagers;
import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import net.minecraftforge.liquids.LiquidStack;

public class ModuleGenetics {

	public static void preInit() {
	}

	public static void doInit() {
		ExtraBeeItem.dictionary = new ItemDictionary(Config.Main.getItemID(
				"dictionary", ExtraBeeItem.dictionaryID));

		ExtraBeeItem.template = new ItemTemplate(Config.Main.getItemID(
				"template", ExtraBeeItem.templateID));
		ExtraBeeItem.templateBlank = new ItemTemplateBlank(
				Config.Main.getItemID("templateBlank",
						ExtraBeeItem.templateBlankID));

		ExtraBeeItem.advancedTemplate = new ItemAdvancedTemplate(
				Config.Main.getItemID("advancedTemplate",
						ExtraBeeItem.advancedTemplateID));
		ExtraBeeItem.advancedTemplateBlank = new ItemAdvancedTemplateBlank(
				Config.Main.getItemID("advancedTemplateBlank",
						ExtraBeeItem.advancedTemplateBlankID));

		ExtraBeeItem.serum = new ItemSerum(Config.Main.getItemID("serum",
				ExtraBeeItem.serumID));

		ExtraBeeItem.serumEmpty = new ItemSerumEmpty(Config.Main.getItemID(
				"serumEmpty", ExtraBeeItem.serumEmptyID));

		ExtraBeeItem.genome = new ItemGenome(Config.Main.getItemID("genome",
				ExtraBeeItem.genomeID));

		ExtraBeeItem.genomeEmpty = new ItemGenomeEmpty(Config.Main.getItemID(
				"genomeEmpty", ExtraBeeItem.genomeEmptyID));
	}

	public static void postInit() {
		EnumExtraBeeEffect.doInit();
		EnumExtraBeeFlowers.doInit();
		EnumExtraBeeSpecies.doInit();
		EnumExtraBeeMutation.doInit();
		EnumExtraBeeBranch.doInit();

		calculateArrays();

		ModLoader.addRecipe(new ItemStack(ExtraBeeItem.templateBlank),
				new Object[] { " G ", "GHG", " G ", 'G', Block.thinGlass, 'H',
						ItemInterface.getItem("honeydew") });

		ModLoader.addRecipe(
				new ItemStack(ExtraBeeItem.serumEmpty, 2),
				new Object[] { "GgG", "GHG", "GgG", 'g', Item.ingotGold, 'G',
						Block.thinGlass, 'H',
						ItemInterface.getItem("royalJelly") });

		RecipeManagers.carpenterManager.addRecipe(100, new LiquidStack(
				Block.waterStill.blockID, 2000), null, new ItemStack(
				ExtraBeeItem.dictionary), new Object[] { "X#X", "X#X", "RDR",
				Character.valueOf('#'), Block.thinGlass,
				Character.valueOf('X'), Item.ingotGold, Character.valueOf('R'),
				Item.redstone, Character.valueOf('D'), Item.emerald });

	}

	public static IGenome getGenome(IAlleleBeeSpecies allele0) {
		return BeeManager.beeInterface
				.templateAsGenome(BeeManager.breedingManager
						.getBeeTemplate(allele0.getUID()));
	}

	private static boolean hasCalculatedArrays = false;

	static List<IBeeBranch> beeBranches = null;
	static List<IAlleleBeeSpecies> beeSpecies = null;

	static Map<IAlleleBeeSpecies, List<IMutation>> resultantMutations = null;
	static Map<IAlleleBeeSpecies, List<IMutation>> furtherMutations = null;

	public static List<IBeeBranch> getAllBeeBranches() {
		if (!hasCalculatedArrays)
			calculateArrays();
		return beeBranches;
	}

	public static Collection<IAlleleBeeSpecies> getAllBeeSpecies() {
		if (!hasCalculatedArrays)
			calculateArrays();
		return beeSpecies;
	}

	public static void calculateArrays() {

		hasCalculatedArrays = true;

		// All Branches

		Collection<IBranch> allBranches = AlleleManager.alleleRegistry
				.getRegisteredBranches().values();
		beeBranches = new ArrayList<IBeeBranch>();
		for (IBranch branch : allBranches) {
			if (branch instanceof IBeeBranch)
				beeBranches.add((IBeeBranch) branch);
		}

		// All Species

		Collection<IAllele> allAlleles = AlleleManager.alleleRegistry
				.getRegisteredAlleles().values();
		beeSpecies = new ArrayList<IAlleleBeeSpecies>();
		for (IAllele species : allAlleles) {
			if (species instanceof IAlleleBeeSpecies)
				beeSpecies.add((IAlleleBeeSpecies) species);
		}

		resultantMutations = new HashMap<IAlleleBeeSpecies, List<IMutation>>();
		furtherMutations = new HashMap<IAlleleBeeSpecies, List<IMutation>>();

		for (IAlleleBeeSpecies species : beeSpecies) {
			resultantMutations.put(species, new ArrayList<IMutation>());
			furtherMutations.put(species, new ArrayList<IMutation>());
		}

		for (IMutation mutation : BeeManager.beeMutations) {

			Set<IAlleleBeeSpecies> participatingSpecies = new LinkedHashSet<IAlleleBeeSpecies>();
			if (mutation.getAllele0() instanceof IAlleleBeeSpecies
					&& beeSpecies.contains(mutation.getAllele0()))
				participatingSpecies.add((IAlleleBeeSpecies) mutation
						.getAllele0());
			if (mutation.getAllele1() instanceof IAlleleBeeSpecies
					&& beeSpecies.contains(mutation.getAllele1()))
				participatingSpecies.add((IAlleleBeeSpecies) mutation
						.getAllele1());
			for (IAlleleBeeSpecies species : participatingSpecies) {
				furtherMutations.get(species).add(mutation);
			}

			if (resultantMutations.containsKey(mutation.getTemplate()[0]))
				resultantMutations.get(mutation.getTemplate()[0]).add(mutation);
		}
	}

	public static ItemStack getBeeIcon(IAlleleBeeSpecies species) {
		if (species == null)
			return null;
		IAllele[] template = BeeManager.breedingManager.getBeeTemplate(species
				.getUID());
		if (template == null)
			return null;
		IBeeGenome genome = BeeManager.beeInterface.templateAsGenome(template);
		IBee bee = BeeManager.beeInterface.getBee(BinnieCore.proxy.getWorld(),
				genome);
		ItemStack item = BeeManager.beeInterface.getBeeStack(bee,
				forestry.api.apiculture.EnumBeeType.PRINCESS);
		return item;
	}

	public static List<IMutation> getResultantMutations(
			IAlleleBeeSpecies species) {
		return resultantMutations.get(species);
	}

	public static List<IMutation> getFurtherMutations(IAlleleBeeSpecies species) {
		return furtherMutations.get(species);
	}

	public static boolean isMutationDiscovered(IMutation mutation, World world, boolean isNEI) {
		return isNEI ? true : BeeManager.breedingManager.getApiaristTracker(
				world).isDiscovered(mutation);
	}

	public static boolean isSpeciesDiscovered(IAlleleSpecies species, World world, boolean isNEI) {
		if(BinnieCore.proxy.isDebug() || isNEI) return true;
		return BeeManager.breedingManager.getApiaristTracker(
				world).isDiscovered(species);
	}

	public static Collection<IBeeBranch> getDiscoveredBeeBranches(World world, boolean isNEI) {
		List<IBeeBranch> branches = new ArrayList<IBeeBranch>();
		for (IBeeBranch branch : getAllBeeBranches()) {
			boolean discovered = false;
			for (IAlleleSpecies species : branch.getMembers())
				if (ModuleGenetics.isSpeciesDiscovered(species, world, isNEI))
					discovered = true;
			if (discovered)
				branches.add(branch);
		}
		return branches;
	}

	public static Collection<IAlleleBeeSpecies> getDiscoveredBeeSpecies(World world, boolean isNEI) {
		List<IAlleleBeeSpecies> speciesList = new ArrayList<IAlleleBeeSpecies>();

		for (IAlleleBeeSpecies species : getAllBeeSpecies())
			if (ModuleGenetics.isSpeciesDiscovered(species, world, isNEI))
				speciesList.add(species);

		return speciesList;
	}

}
