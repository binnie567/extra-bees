package binnie.extrabees.nei;

import java.util.ArrayList;
import java.util.List;

import binnie.extrabees.core.ExtraBeeBlock;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.genetics.ModuleEngineering;
import binnie.extrabees.products.ItemHoneyComb;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import codechicken.nei.api.ItemInfo;

public class NEIExtraBeeConfig implements IConfigureNEI {

	@Override
	public void loadConfig() {
		try {
			MultiItemRange machines = new MultiItemRange();

			List<ItemStack> machineList = new ArrayList<ItemStack>();

			ExtraBeeBlock.apiaristMachine.getSubBlocks(0, null, machineList);
			ExtraBeeBlock.geneticMachine.getSubBlocks(0, null, machineList);
			ExtraBeeBlock.advGeneticMachine.getSubBlocks(0, null, machineList);

			for (ItemStack item : machineList)
				machines.add(item);

			API.addSetRange("Forestry.ExtraBees.Machines", machines);

			MultiItemRange products = new MultiItemRange();

			List<ItemStack> productList = new ArrayList<ItemStack>();

			for (Item item : ItemHoneyComb.combs)
				if (item != null)
					item.getSubItems(0, null, productList);

			ExtraBeeItem.honeyDrop.getSubItems(0, null, productList);
			ExtraBeeItem.propolis.getSubItems(0, null, productList);

			for (ItemStack item : productList)
				products.add(item);

			API.addSetRange("Forestry.ExtraBees.Products", products);

			neiSerums();

		} catch (Exception e) {

		}

	}

	public void neiSerums() {
		ArrayList<int[]> damageRange = new ArrayList<int[]>();
		damageRange.add(new int[] { 0, -1 });
		ItemInfo.damageVariants.put(ExtraBeeItem.serum.shiftedIndex,
				damageRange);

		ArrayList<ItemStack> nei = new ArrayList<ItemStack>();

		nei.addAll(ModuleEngineering.getAllSerums());

		ItemInfo.itemcompounds.put(ExtraBeeItem.serum.shiftedIndex, nei);

	}

	@Override
	public String getName() {
		return "Extra Bees";
	}

	@Override
	public String getVersion() {
		return "";
	}

}
