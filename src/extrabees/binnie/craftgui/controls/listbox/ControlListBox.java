package binnie.craftgui.controls.listbox;

import binnie.craftgui.controls.scroll.ControlScroll;
import binnie.craftgui.controls.scroll.IControlScrollable;
import binnie.craftgui.core.IWidget;

public class ControlListBox<T> extends ControlAbstractListBox<T> implements IControlScrollable {

	@Override
	public boolean isCroppedWidet() {
		return true;
	}

	public ControlListBox(IWidget parent, float x, float y, float w, float h) {
		super(parent, x, y, w, h);
		new ControlScroll(this, this.getSize().x-12, 0, 12, this.getSize().y, this);
	}

	@Override
	protected ControlAbstractOptionSet createOptionSet() {
		return new ControlOptionSet<T>(this, 0, 0, getSize().x-14);
	}
	
	@Override
	public IWidget createOption(int index, int height, T value) {
		return new ControlTextOption<T>(controlOptions, 0, height, controlOptions.getSize().x, 12, value);
	}

	@Override
	public float getPercentageShown() {
		float shown = this.getSize().y/this.controlOptions.getSize().y;
		return Math.min(shown, 1f);
	}
	
	float percentageIndex = 0.0f;

	@Override
	public float getPercentageIndex() {
		return percentageIndex;
	}
	
	int percentageIndexOffset = 0;

	@Override
	public void movePercentage(float percentage) {
		percentageIndex += percentage;
		percentageIndexOffset = 0;
		if(percentageIndex + getPercentageShown() > 1f) {
			percentageIndex = 1f - getPercentageShown();
			percentageIndexOffset= 1;
		}
		if(percentageIndex<0f)
			percentageIndex=0f;
		
		controlOptions.setOffset(0, -percentageIndex*this.controlOptions.getSize().y-percentageIndexOffset);
	}
	

}
