package binnie.extrabees.apiary;

import java.util.ArrayList;

import net.minecraft.src.World;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeekeepingMode;

public class ModeDebug implements IBeekeepingMode {

	@Override
	public float getTerritoryModifier(IBeeGenome genome) {
		return 1.0f;
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate) {
		return 1.0f;
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate) {
		return 1.0f;
	}

	@Override
	public float getProductionModifier(IBeeGenome genome) {
		return 1.0f;
	}

	@Override
	public String getName() {
		return "";
	}

	@Override
	public ArrayList<String> getDescription() {
		ArrayList<String> array = new ArrayList<String>();
		return array;
	}

	@Override
	public float getWearModifier() {
		return 1.0f;
	}

	@Override
	public int getFinalFertility(IBee queen, World world, int x, int y, int z) {
		return 1;
	}

	@Override
	public boolean isFatigued(IBee queen) {
		return false;
	}

	@Override
	public boolean isNaturalOffspring(IBee queen) {
		return true;
	}

	@Override
	public boolean mayMultiplyPrincess(IBee queen) {
		return false;
	}

}
