package binnie.extrabees.core;

import net.minecraft.src.NBTBase;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;

public class NBTLogger {

	public NBTLogger(NBTTagCompound nbt) {

		readNBTCompound(nbt, "");

	}

	public void readNBTCompound(NBTTagCompound compound, String offset) {
		for (Object obj : compound.getTags()) {
			analyzeTag((NBTBase) obj, offset);

		}
	}

	public void readNBTTagList(NBTTagList list, String offset) {
		for (int i = 0; i < list.tagCount(); i++) {
			analyzeTag(list.tagAt(i), offset);
		}
	}

	public void analyzeTag(NBTBase tag, String offset) {
		if (tag instanceof NBTTagList) {
			System.out.println(offset + tag.getName() + " : " + "  ["
					+ tag.getClass() + "]");
			readNBTTagList((NBTTagList) tag, offset + "   ");
		} else if (tag instanceof NBTTagCompound) {
			System.out.println(offset + tag.getName() + " : " + "  ["
					+ tag.getClass() + "]");
			readNBTCompound((NBTTagCompound) tag, offset + "   ");
		} else {
			System.out.println(offset + tag.getName() + " : " + tag + "  ["
					+ tag.getClass() + "]");
		}
	}

}
