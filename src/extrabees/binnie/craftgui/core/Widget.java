package binnie.craftgui.core;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventKeyTyped;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.events.EventMouseMoved;
import binnie.craftgui.events.EventMouseUp;

public abstract class Widget implements IWidget {

	public Widget(IWidget parent) {
		this.parent = parent;
		if (parent != null)
			parent.addWidget(this);
	}

	// Hierarchy Functions

	private IWidget parent = null;
	private List<IWidget> subWidgets = new ArrayList<IWidget>();

	@Override
	public final void deleteChild(IWidget child) {
		this.subWidgets.remove(child);
	}

	@Override
	public final IWidget getParent() {
		return parent;
	}

	@Override
	public final ITopLevelWidget getSuperParent() {
		return isTopLevel() ? (ITopLevelWidget) this : parent.getSuperParent();
	}

	@Override
	public final IWidget addWidget(IWidget widget) {
		subWidgets.add(widget);
		return widget;
	}

	@Override
	public final List<IWidget> getWidgets() {
		return subWidgets;
	}

	@Override
	public final boolean isTopLevel() {
		return this instanceof ITopLevelWidget;
	}

	// Dimension and Positioning Functions

	private Vector2f position = new Vector2f(0.0f, 0.0f);
	private Vector2f size = new Vector2f(0.0f, 0.0f);
	private Vector2f offset = new Vector2f(0.0f, 0.0f);

	@Override
	public final Vector2f getPosition() {
		return Vector2f.add(position, offset);
	}

	@Override
	public final Vector2f getOriginalPosition() {
		return position;
	}

	@Override
	public final Vector2f getAbsolutePosition() {
		return isTopLevel() ? this.getPosition() : Vector2f.add(getParent()
				.getAbsolutePosition(), getPosition());
	}

	@Override
	public final Vector2f getOriginalAbsolutePosition() {
		return isTopLevel() ? this.getOriginalPosition() : Vector2f.sub(
				getParent().getOriginalPosition(), getOriginalPosition());
	}

	@Override
	public final Vector2f getSize() {
		return size;
	}

	@Override
	public final Vector2f getOffset() {
		return offset;
	}

	@Override
	public final void setPosition(Vector2f vector) {
		if (vector != position) {
			this.position = new Vector2f(vector);
			onChangePosition();
		}
	}

	@Override
	public final void setSize(Vector2f vector) {
		if (vector != size) {
			this.size = new Vector2f(vector);
			onChangeSize();
		}
	}

	@Override
	public final void setOffset(Vector2f vector) {
		if (vector != offset) {
			this.offset = new Vector2f(vector);
			onChangeOffset();
		}
	}

	@Override
	public final void setPosition(float x, float y) {
		if (new Vector2f(x, y) != position) {
			this.position = new Vector2f(x, y);
			onChangePosition();
		}
	}

	@Override
	public final void setSize(float x, float y) {
		if (new Vector2f(x, y) != size) {
			this.size = new Vector2f(x, y);
			onChangeSize();
		}
	}

	@Override
	public final void setOffset(float x, float y) {
		if (new Vector2f(x, y) != offset) {
			this.offset = new Vector2f(x, y);
			onChangeOffset();
		}
	}

	// Colour related Functions

	int colour = 0xFFFFFF;

	@Override
	public final void setColour(int colour) {
		this.colour = colour;
		onChangeColour();
	}

	@Override
	public final int getColour() {
		return colour;
	}

	// Widget Functionality

	@Override
	public boolean canMouseOver() {
		return false;
	}

	@Override
	public boolean canFocus() {
		return false;
	}

	// Event related Functions

	@Override
	public final void callEvent(Event event) {
		getSuperParent().recieveEvent(event);
	}

	@Override
	public final void recieveEvent(Event event) {

		this.handleEvent(event);

		for (IWidget child : this.getWidgets())
			child.recieveEvent(event);
	}

	@Override
	public final void handleEvent(Event event) {

		if (event instanceof EventMouseClick)
			mouseClicked((EventMouseClick) event);
		else if (event instanceof EventMouseUp)
			mouseUp((EventMouseUp) event);
		else if (event instanceof EventMouseMoved)
			mouseMoved((EventMouseMoved) event);
		else if (event instanceof EventKeyTyped)
			keyTyped((EventKeyTyped) event);

		onHandleEvent(event);
	}

	@Override
	public void onHandleEvent(Event event) {
	}

	// Input Related Functions

	@Override
	public final void mouseClicked(EventMouseClick event) {
		if (isTopLevel() || event.isOrigin(this))
			onMouseClick(event);
	}

	@Override
	public final void keyTyped(EventKeyTyped event) {
		if (isVisible() && isEnabled())
			onKeyTyped(event);
	}

	@Override
	public final void mouseMoved(EventMouseMoved event) {
		if (isTopLevel() || event.isOrigin(this))
			onMouseMoved(event);
	}

	@Override
	public final void mouseUp(EventMouseUp event) {
		if (isTopLevel() || event.isOrigin(this))
			onMouseUp(event);
	}

	@Override
	public final Vector2f getMousePosition() {
		return getSuperParent().getAbsoluteMousePosition();
	}

	@Override
	public final Vector2f getRelativeMousePosition() {
		return isTopLevel() ? this.getMousePosition() : Vector2f.sub(
				getParent().getRelativeMousePosition(), getPosition());
	}

	// Render Functions

	@Override
	public final IRenderer getRenderer() {
		return getSuperParent().getTopLevelRenderer();
	}

	public boolean isCroppedWidet() {
		return false;
	}

	private final void preRender() {
		GL11.glPushMatrix();
		GL11.glTranslatef(this.getPosition().x, this.getPosition().y, 0.0F);

		this.getSuperParent().getRenderer().setTexture(this.getTextureFile());

		getRenderer().setColour(getColour());
		
		if (isCroppedWidet()) {
			Vector2f size = this.getSize();
			Vector2f pos = this.getAbsolutePosition();
			getRenderer().limitArea(pos.x, pos.y, size.x, size.y);
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
		}
	}

	private final void postRender() {
		if (isCroppedWidet()) {
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}
		GL11.glPopMatrix();
	}

	@Override
	public final void renderBackground() {
		if (this.isVisible()) {
		    preRender();
			onRenderBackground();
			for (IWidget widget : getWidgets()) {
				widget.renderBackground();
			}
			postRender();
		}	
	}

	@Override
	public final void renderForeground() {
		if (this.isVisible()) {
		    preRender();
			onRenderForeground();
			for (IWidget widget : getWidgets()) {
				widget.renderForeground();
			}
			postRender();
		}	
	}

	@Override
	public final void renderOverlay() {
		if (this.isVisible()) {
		    preRender();
			onRenderOverlay();
			for (IWidget widget : getWidgets()) {
				widget.renderOverlay();
			}
			postRender();
		}	
	}

	// Update functions

	@Override
	public final void update() {
		if (!this.isVisible())
			return;

		if (this.getSuperParent() == this)
			((ITopLevelWidget) this).updateTopLevel();

		this.onUpdate();

		for (IWidget widget : getWidgets()) {
			widget.update();
		}
	}

	@Override
	public final IWidget calculateMousedOverWidget() {

		boolean amIMousedOver = calculateIsMouseOver();

		if (!isVisible())
			return null;

		if (amIMousedOver || !isCroppedWidet()) {
			for (IWidget child : getWidgets()) {
				IWidget result = child.calculateMousedOverWidget();
				if (result != null)
					return result;
			}
		}

		if (canMouseOver() && amIMousedOver)
			return this;
		else
			return null;
	}

	@Override
	public final boolean calculateIsMouseOver() {
		Vector2f mouse = getRelativeMousePosition();
		return mouse.x > 0.0f && mouse.y > 0.0f && mouse.x < getSize().x
				&& mouse.y < getSize().y;
	}

	// State related Functions

	private boolean enabled = true;
	private boolean visible = true;

	@Override
	public final void enable() {
		enabled = true;
		onEnable();
	}

	@Override
	public final void disable() {
		enabled = false;
		onDisable();
	}

	@Override
	public final void show() {
		visible = true;
		onShow();
	}

	@Override
	public final void hide() {
		visible = false;
		onHide();
	}

	@Override
	public final boolean isEnabled() {
		return enabled
				&& (isTopLevel() ? true : getParent().isEnabled()
						&& getParent().isChildEnabled(this));
	}

	@Override
	public final boolean isVisible() {
		return visible
				&& (isTopLevel() ? true : getParent().isVisible()
						&& getParent().isChildVisible(this));
	}

	@Override
	public final boolean isFocused() {
		return getSuperParent().isFocused(this);
	}

	@Override
	public final boolean isDragged() {
		return getSuperParent().isDragged(this);
	}

	@Override
	public final boolean isMouseOver() {
		return getSuperParent().isMouseOver(this);
	}

	@Override
	public boolean isChildVisible(IWidget child) {
		return true;
	}

	@Override
	public boolean isChildEnabled(IWidget child) {
		return true;
	}

	// Methods to override

	@Override
	public void onRenderBackground() {
	};

	@Override
	public void onRenderForeground() {
	};

	@Override
	public void onRenderOverlay() {
	};

	@Override
	public void onUpdate() {
	};

	@Override
	@SideOnly(Side.CLIENT)
	public void onMouseClick(EventMouseClick event) {
	};

	@Override
	@SideOnly(Side.CLIENT)
	public void onKeyTyped(EventKeyTyped event) {
	};

	@Override
	@SideOnly(Side.CLIENT)
	public void onMouseMoved(EventMouseMoved event) {
	};

	@Override
	@SideOnly(Side.CLIENT)
	public void onMouseUp(EventMouseUp event) {
	};

	@Override
	public String getTextureFile() {
		return "";
	};

	@Override
	public void onEnable() {
	};

	@Override
	public void onDisable() {
	};

	@Override
	public void onShow() {
	};

	@Override
	public void onHide() {
	};

	@Override
	public void onGainFocus() {
	};

	@Override
	public void onLoseFocus() {
	};

	@Override
	public void onStartDrag() {
	};

	@Override
	public void onEndDrag() {
	};

	@Override
	public void onStartMouseOver() {
	};

	@Override
	public void onEndMouseOver() {
	};

	@Override
	public void onChangePosition() {
	};

	@Override
	public void onChangeSize() {
	};

	@Override
	public void onChangeOffset() {
	};

	@Override
	public void onChangeColour() {
	}

}
