package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Widget;

public class ControlScroll extends Control {

	public IControlScrollable scrollWidget;

	public ControlScroll(IWidget parent, float x, float y, float width, float height,
			IControlScrollable scrollWidget) {
		super(parent, x, y, width, height);
		this.scrollWidget = scrollWidget;
		new ControlScrollBar(this);
		new ControlScrollButton(this, ControlScrollButton.Position.Top);
		new ControlScrollButton(this, ControlScrollButton.Position.Bottom);
	}

}
