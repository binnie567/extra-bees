package extrabees.liquids;

import forestry.api.core.ItemInterface;
import forestry.api.liquids.LiquidContainer;
import forestry.api.liquids.LiquidManager;
import forestry.api.recipes.*;
import net.minecraft.server.ItemStack;

public class ExtraBeeLiquidsCore
{
    public ExtraBeeLiquidsCore()
    {
    }

    public static void injectLiquidContainer(LiquidContainer liquidcontainer)
    {
        injectLiquidContainer(liquidcontainer, null, 0);
    }

    public static void injectWaxContainer(LiquidContainer liquidcontainer)
    {
        injectLiquidContainer(liquidcontainer, ItemInterface.getItem("beeswax"), 10);
    }

    public static void injectRefractoryContainer(LiquidContainer liquidcontainer)
    {
        injectLiquidContainer(liquidcontainer, ItemInterface.getItem("refractoryWax"), 10);
    }

    public static void injectTinContainer(LiquidContainer liquidcontainer)
    {
        injectLiquidContainer(liquidcontainer, ItemInterface.getItem("ingotTin"), 5);
    }

    public static void injectLiquidContainer(LiquidContainer liquidcontainer, ItemStack itemstack, int i)
    {
        LiquidManager.registerLiquidContainer(liquidcontainer);

        if (RecipeManagers.squeezerManager != null && !liquidcontainer.isBucket)
        {
            if (itemstack != null)
            {
                RecipeManagers.squeezerManager.addRecipe(10, new ItemStack[]
                        {
                            liquidcontainer.filled
                        }, liquidcontainer.liquid, itemstack, i);
            }
            else
            {
                RecipeManagers.squeezerManager.addRecipe(10, new ItemStack[]
                        {
                            liquidcontainer.filled
                        }, liquidcontainer.liquid);
            }
        }

        if (RecipeManagers.bottlerManager != null)
        {
            RecipeManagers.bottlerManager.addRecipe(5, liquidcontainer.liquid, liquidcontainer.empty, liquidcontainer.filled);
        }
    }
}
