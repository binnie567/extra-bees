package binnie.extrabees.machines;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.genetics.AlleleManager;
import binnie.core.fx.EntityBinnieFX;
import binnie.core.network.PacketPayload;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.fx.EntitySequencerFX;
import binnie.extrabees.genetics.ItemTemplate;
import buildcraft.api.power.IPowerReceptor;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.World;

public class TileEntitySequencer extends TileEntityMachine implements
		IPowerReceptor {

	public static final int[] SlotReserves = new int[] { 5, 6, 7, 8, 9, 10 };
	public static final int SlotBee = 2;
	public static final int SlotTemplateBlank = 3;
	public static final int SlotTemplateWritten = 4;

	@SideOnly(Side.CLIENT)
	public void onFX(World world, int x, int y, int z, float efficiency) {
		if(worldObj.rand.nextInt(3)==0)
			EntityBinnieFX.createFX(new EntitySequencerFX(this));
	}
	
	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Sequencer.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Sequencer;
	}

	public TileEntitySequencer() {
		super(5000, 500, 10000);

		DNAProgress = 0.0f;
		SpeciesUID = "";

		addSlot(SlotBee);
		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		addSlotArray(SlotReserves);
		for (InventorySlot slot : getSlots(SlotReserves))
			slot.setValidator(new SlotValidatorBee(SlotValidatorBee.Mode.Bee));

		addSlot(SlotTemplateBlank);
		getSlot(SlotTemplateBlank).setValidator(
				new SlotValidatorTemplate(SlotValidatorTemplate.Mode.Blank));

		addSlot(SlotTemplateWritten);
		getSlot(SlotTemplateWritten).setValidator(
				new SlotValidatorTemplate(SlotValidatorTemplate.Mode.Written));
		getSlot(SlotTemplateWritten).setReadOnly();
	}

	public float DNAProgress;
	public String SpeciesUID;

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		if (getSlot(SlotBee).getItemStack() == null) {
			for (int i : SlotReserves) {
				if (getSlot(i).getItemStack() != null) {
					setInventorySlotContents(SlotBee, getSlot(i).getItemStack());
					setInventorySlotContents(i, null);
					break;
				}
			}
		}
	}

	@Override
	public void onFinishTask() {
		IBee bee = BeeManager.beeInterface.getBee(getStackInSlot(SlotBee));

		float amount = LogicMachine.getSequencerAmount(getStackInSlot(SlotBee));

		if (amount > 0.0f) {

			SpeciesUID = bee.getGenome().getPrimaryAsBee().getUID();

			setInventorySlotContents(SlotBee, null);

			DNAProgress += amount;

			if (DNAProgress >= 100.0f) {
				int species = AlleleManager.alleleRegistry
						.getFromUIDMap(SpeciesUID);

				ItemStack stack = new ItemStack(ExtraBeeItem.template, 1,
						species);

				NBTTagCompound nbt = new NBTTagCompound();
				nbt.setInteger("quality", this.worldObj.rand.nextInt(11));
				stack.setTagCompound(nbt);

				setInventorySlotContents(SlotTemplateWritten, stack);
				decrStackSize(SlotTemplateBlank, 1);
				SpeciesUID = "";
				DNAProgress = 0.0f;
			}
		}
	}

	@Override
	public ErrorState canWork() {

		if (this.getStackInSlot(SlotTemplateBlank) == null) {
			SpeciesUID = "";
			DNAProgress = 0.0f;
			return new ErrorState("No Blank Template");
		}

		if (this.getStackInSlot(SlotTemplateWritten) != null)
			return new ErrorState("No Free Template Slot");

		if (this.getStackInSlot(SlotBee) == null)
			return new ErrorState("No Bee");

		IAlleleBeeSpecies species = BeeManager.beeInterface
				.getBee(getStackInSlot(SlotBee)).getGenome().getPrimaryAsBee();

		if (!SpeciesUID.equals("") && !species.getUID().equals(SpeciesUID)) {
			return new ErrorState("Bee does not match current species");
		}

		if (SpeciesUID.equals("")
				&& !ItemTemplate.canObtainSpecies(species.getUID())) {
			return new ErrorState("Cannot create template for this species");

		}

		return super.canWork();

	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.setFloat("dnaProgress", DNAProgress);
		nbttagcompound.setString("speciesUID", SpeciesUID);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		DNAProgress = nbttagcompound.getFloat("dnaProgress");
		SpeciesUID = nbttagcompound.getString("speciesUID");
	}

	@Override
	public void writeToPacket(PacketPayload data) {
		data.addString(SpeciesUID);
		data.addFloat(DNAProgress);
	}

	@Override
	public void readFromPacket(PacketPayload data) {
		SpeciesUID = data.getString();
		DNAProgress = data.getFloat();
	}

}
