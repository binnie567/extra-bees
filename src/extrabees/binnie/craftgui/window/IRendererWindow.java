package binnie.craftgui.window;

import binnie.craftgui.core.IRenderer;

public interface IRendererWindow extends IRenderer {

	public void renderPanelMain(int x, int y, int width, int height);

	public void renderPanelGray(int x, int y, int width, int height);

	public void renderPanelBlack(int x, int y, int width, int height);

}
