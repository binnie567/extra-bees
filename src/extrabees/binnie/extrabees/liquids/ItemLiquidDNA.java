package binnie.extrabees.liquids;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemLiquidDNA extends Item {

	public ItemLiquidDNA(int id) {
		super(id);
		setMaxDamage(0);
		setIconIndex(255);
		setTextureFile(ExtraBeeTexture.Liquids.getTexture());
		setCreativeTab(CreativeTabs.tabMaterials);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return "Liquid DNA";
	}
}
