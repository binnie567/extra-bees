package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IBeeBranch;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

public class EventBranchChanged extends Event {

	IBeeBranch branch;

	public IBeeBranch getBranch() {
		return branch;
	}

	public EventBranchChanged(IWidget origin, IBeeBranch branch) {
		super(origin);
		this.branch = branch;
	}

}
