package binnie.botany.machines;

import net.minecraft.src.Container;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ICrafting;
import net.minecraft.src.IInventory;
import forestry.core.gadgets.Machine;
import forestry.core.gadgets.TileMachine;

public abstract class BotanyMachine extends Machine {

	public BotanyMachine(TileMachine tile) {
		super(tile);
	}

	@Override
	public void openGui(EntityPlayer player, IInventory tile) {
	}

	@Override
	public void getGUINetworkData(int i, int j) {
	}

	@Override
	public void sendGUINetworkData(Container container, ICrafting iCrafting) {
	}

}
