package binnie.extrabees.apiary;

import binnie.extrabees.core.ExtraBeeTexture;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IHiveFrame;

public class ItemHiveFrame extends Item implements IHiveFrame {

	@Override
	public String getItemName() {
		return frame.name;
	}

	@Override
	public String getItemNameIS(ItemStack par1ItemStack) {
		return frame.name;
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return frame.name;
	}

	public ItemHiveFrame(EnumHiveFrame frame) {
		super(frame.getItemID());
		this.setIconIndex(frame.getIconIndex());
		this.setTextureFile(ExtraBeeTexture.Main.getTexture());
		this.frame = frame;
		this.setMaxDamage(frame.maxDamage);
		this.setCreativeTab(CreativeTabs.tabMisc);
		this.setMaxStackSize(1);
	}

	EnumHiveFrame frame;

	@Override
	public float getTerritoryModifier(IBeeGenome genome) {
		return frame.getTerritoryModifier(genome);
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate) {
		return frame.getMutationModifier(genome, mate);
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate) {
		return frame.getLifespanModifier(genome, mate);
	}

	@Override
	public float getProductionModifier(IBeeGenome genome) {
		return frame.getProductionModifier(genome);
	}

	@Override
	public ItemStack frameUsed(World world, ItemStack frame, IBee queen,
			int wear) {
		frame.setItemDamage(frame.getItemDamage() + wear);
		if (frame.getItemDamage() >= frame.getMaxDamage())
			return null;
		else
			return frame;
	}

}
