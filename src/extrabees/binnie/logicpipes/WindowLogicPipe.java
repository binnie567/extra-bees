package binnie.logicpipes;

import cpw.mods.fml.common.Side;
import binnie.craftgui.window.Panel;
import binnie.craftgui.window.Panel.Type;
import binnie.craftgui.window.Window;
import buildcraft.transport.TileGenericPipe;
import net.minecraft.src.EntityPlayer;

public class WindowLogicPipe extends Window {

	public WindowLogicPipe(EntityPlayer player, Object inventory, Side side) {
		super(320, 180, player, null, side);
		if (inventory instanceof TileGenericPipe) {
			gate = (LogicGate) ((TileGenericPipe) inventory).pipe.gate;
		}
	}

	LogicGate gate;

	public static Window create(EntityPlayer player, Object inventory, Side side) {

		if (player == null || inventory == null)
			return null;
		return new WindowLogicPipe(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Logic Pipe");

		new Panel(this, 16, 32, 72, 130, Type.Black);
		
		new Panel(this, 110, 32, 190, 130, Type.Black);
		
		ControlStatement test = new ControlStatement(this, 24, 48, new LogicStatement(new ATriggerRedstone(), new AActionExtractItem()));
		
		
	}

};
