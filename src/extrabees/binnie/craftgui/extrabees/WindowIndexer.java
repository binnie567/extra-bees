package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlCycle;
import binnie.craftgui.controls.button.ControlEnumButton;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventCycleChanged;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityIndexer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowIndexer extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityIndexer machine;

	ControlSlotIndexerArray slotIndexer;

	ControlCycle pageCycle;

	ControlEnumButton<TileEntityIndexer.Mode> modeButton;

	public WindowIndexer(EntityPlayer player, IInventory inventory, Side side) {
		super(256, 198, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityIndexer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowIndexer(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Indexer");

		playerInventory = new ControlPlayerInventory(this);

		slotIndexer = new ControlSlotIndexerArray(this, 11, 24, 13, 3);

		modeButton = new ControlEnumButton<TileEntityIndexer.Mode>(this, 24,
				86, 64, 18, TileEntityIndexer.Mode.class);

		pageCycle = new ControlCycle(this, 136, 86, 96, 18, 24);

		slotIndexer.create(machine);

	}

	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if (event instanceof EventCycleChanged && event.isOrigin(pageCycle)) {
			int page = ((EventCycleChanged) event).getValue();
			slotIndexer.setPage(page);
		}
		if (event instanceof EventValueChanged && event.isOrigin(modeButton)) {
			machine.setMode(modeButton.getValue());
		}
	}

	@Override
	public void onUpdate() {
		modeButton.setValue(machine.getMode());
		pageCycle.setMax(machine.getNumberOfPages(39));
	}

};
