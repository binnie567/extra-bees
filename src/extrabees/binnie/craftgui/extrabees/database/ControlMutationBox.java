package binnie.craftgui.extrabees.database;

import binnie.craftgui.controls.listbox.ControlAbstractOption;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;
import binnie.extrabees.genetics.ModuleGenetics;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.genetics.IMutation;

public class ControlMutationBox extends ControlListBox<IMutation> {

	@Override
	public IWidget createOption(int index, int height, IMutation value) {
		return new ControlMutationItem(controlOptions, 0, height, this.controlOptions.getSize().x, 22, value);
	}

	enum Type {
		Resultant, Further
	}

	private int index;
	private Type type;

	public ControlMutationBox(IWidget parent, int x, int y, int width,
			int height, Type type) {
		super(parent, x, y, width, height);
		this.type = type;
	}

	IAlleleBeeSpecies species = null;

	public void setSpecies(IAlleleBeeSpecies species) {
		if (species != this.species) {
			this.species = species;
			this.index = 0;
			movePercentage(-100.0f);
			if (species != null) {
				if (this.type == Type.Resultant) {
					setOptions(ModuleGenetics.getResultantMutations(species));
				} else {
					setOptions(ModuleGenetics.getFurtherMutations(species));
				}
			}
		}

	}

}
