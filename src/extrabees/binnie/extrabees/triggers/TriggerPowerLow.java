package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerPowerLow extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 11;
	}

	@Override
	public String getDescription() {
		return "Low Energy";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			double percentage = machine.getEnergyStored()
					/ machine.getMaxEnergyStored();
			return percentage < 0.35f;
		}
		return false;
	}

	public TriggerPowerLow() {
		super();
	}

}
