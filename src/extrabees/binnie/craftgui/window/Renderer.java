package binnie.craftgui.window;

import net.minecraft.src.ItemStack;

public class Renderer implements IRendererWindow {

	GuiCraftGUI gui;

	public Renderer(GuiCraftGUI gui) {
		this.gui = gui;
	}

	@Override
	public void renderRect(int x, int y, int width, int height, int u, int v) {
		if (gui != null)
			gui.renderRect(x, y, width, height, u, v);
	}

	@Override
	public void renderRectTiled(int x, int y, int width, int height, int u,
			int v, int textWidth, int textHeight) {
		if (gui != null)
			gui.renderRectTiled(x, y, width, height, u, v, textWidth,
					textHeight);
	}

	@Override
	public void renderRectBordered(int x, int y, int width, int height, int u,
			int v, int textWidth, int textHeight, int borderLeft,
			int borderRight, int borderTop, int borderBottom) {
		if (gui != null)
			gui.renderRectBordered(x, y, width, height, u, v, textWidth,
					textHeight, borderLeft, borderRight, borderTop,
					borderBottom);
	}

	@Override
	public void setTexture(String texture) {
		if (gui != null)
			gui.setTexture(texture);
	}

	@Override
	public int getTextWidth(String text) {
		if (gui != null)
			return gui.getTextWidth(text);
		return 0;
	}

	@Override
	public int getTextHeight() {
		if (gui != null)
			return gui.getTextHeight();
		return 0;
	}

	@Override
	public void renderText(int x, int y, String text, int colour) {
		if (gui != null)
			gui.renderText(x, y, text, colour);
	}

	@Override
	public void renderOutline(int x, int y, int w, int h, int colour) {
		if (gui != null)
			gui.renderOutline(x, y, w, h, colour);
	}

	@Override
	public void renderSolidRect(int x, int y, int w, int h, int colour) {
		if (gui != null)
			gui.renderSolidRect(x, y, w, h, colour);
	}

	@Override
	public void renderItem(int x, int y, ItemStack item) {
		if (gui != null)
			gui.renderItem(x, y, item);
	}

	@Override
	public void renderGradientRect(int i, int j, int k, int l, int m, int n) {
		if (gui != null)
			gui.renderGradientRect(i, j, k, l, m, n);
	}

	@Override
	public void renderPanelMain(int x, int y, int width, int height) {
		if (gui != null)
			gui.renderPanelMain(x, y, width, height);
	}

	@Override
	public void renderPanelGray(int x, int y, int width, int height) {
		if (gui != null)
			gui.renderPanelGray(x, y, width, height);
	}

	@Override
	public void renderPanelBlack(int x, int y, int width, int height) {
		if (gui != null)
			gui.renderPanelBlack(x, y, width, height);
	}

	@Override
	public void setColour(int c) {
		if (gui != null)
			gui.setColour(c);
	}

	@Override
	public void limitArea(float x, float y, float x2, float y2) {
		if(gui != null)
			gui.limitArea(x, y, x2, y2);
	}

}
