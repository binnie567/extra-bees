package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlItemDisplay;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlText.Alignment;
import binnie.craftgui.controls.listbox.ControlAbstractOption;
import binnie.craftgui.controls.listbox.ControlAbstractOptionSet;
import binnie.craftgui.controls.listbox.ControlOption;

public class ControlSpeciexBoxOption extends ControlOption<IAlleleBeeSpecies> {

	ControlBeeDisplay controlBee;

	public ControlSpeciexBoxOption(ControlAbstractOptionSet<IAlleleBeeSpecies> controlOptions, float x,
			float y, float width, float height, IAlleleBeeSpecies value) {
		super(controlOptions, x, y, width, height, value);
		
		controlBee = new ControlBeeDisplay(this, 4, 4);
		controlBee.setSpecies(value, false);
		
		if(controlBee.discovered!=0)
			canMouseOver = false;
		
		ControlText textWidget = new ControlText(this, 12+getSize().x/2, getSize().y/2, 
				controlBee.discovered==0 ? value.getName() : "Undiscovered"
				, Alignment.Center);
	}

}
