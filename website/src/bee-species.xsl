<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>

				<xsl:for-each select="branches/branch">

					<div class="section section2 blue">

						<h2>
							<xsl:value-of select="name" />
							Branch -
							<i>
								Apidae
								<xsl:value-of select="scientific" />
							</i>
						</h2>
						<hr />

						<xsl:if test="description != '' and description != 'null'">
							<xsl:value-of select="description" />
							<hr />
						</xsl:if>


						<xsl:for-each select="species">

							<div class="section section3">

								<xsl:attribute name="class">
									<xsl:choose>
									  <xsl:when test="authority != 'Sengir'"> section section3 yellow expandable</xsl:when>
									  <xsl:otherwise> section section3 green expandable</xsl:otherwise>
									  </xsl:choose>
									</xsl:attribute>





								<h2>
									<xsl:value-of select="name" />
									Bee -
									<i>
										<xsl:value-of select="concat(../scientific, ' ', binomial)" />

									</i>
								</h2>
								<hr />

								<xsl:if test="description != '' and description != 'null'">
									<xsl:value-of select="description" />
									<hr />
								</xsl:if>

								<xsl:if test="description != '' and description != 'null'">
									<xsl:value-of select="description" />
									<hr />
								</xsl:if>

								<table border="1">
									<tr>
										<td>

											<div class="beeImage box">
												<img class="beeImage image glow" src="bee-glow.png"
													data-colour="16777215">
													<xsl:attribute name="data-colour">
            <xsl:value-of select="outlineColour" />
          </xsl:attribute>
												</img>



												<img class="beeImage image main" src="bee.png"
													data-colour="16777215" />

												<img class="beeImage image body" src="bee-body.png"
													data-colour="16777215">
													<xsl:attribute name="data-colour">
                                            <xsl:value-of
														select="bodyColour" />
                                        </xsl:attribute>
												</img>

											</div>

										</td>
										<td>
										
										
										
										
										Data
										</td>
									</tr>
								</table>



								<hr />

								<table style="text-align:center">
									<tr>
										<td style="width:348px">Tolerated Temperatures</td>
										<td style="width:186px">Tolerated Humidity</td>
									</tr>
									<tr style="font-size:18px;font-weight:bold;">
										<td>

											<table style="margin:auto">
												<tr style="height:24px;">
													<td style="background-color:#00FBFF;" title="Icy">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/temperature/Icy">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td style="background-color:#00C7E6;" title="Cold">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/temperature/Cold">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td style="background-color:#00D443;" title="Normal">
														<xsl:attribute name="class">
														  toleranceBox 
														  <xsl:if test="tolerance/temperature/Normal">selected </xsl:if>
														</xsl:attribute>
													</td>
													<td style="background-color:#F5ED00;" title="Warm">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/temperature/Warm">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td style="background-color:#F59F00;" title="Hot">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/temperature/Hot">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td style="background-color:#D60400;" title="Hellish">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/temperature/Hellish">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
												</tr>
											</table>

										</td>
										<td>

											<table style="margin:auto">
												<tr style="height:24px">
													<td class="toleranceBox" style="background-color:#E3DC91;"
														title="Arid">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/humidity/Arid">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td class="toleranceBox" style="background-color:#82D4F5;"
														title="Normal">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/humidity/Normal">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
													<td class="toleranceBox" style="background-color:#143BD9;"
														title="Damp">
														<xsl:attribute name="class">
                                                          toleranceBox 
                                                          <xsl:if
															test="tolerance/humidity/Damp">selected </xsl:if>
                                                        </xsl:attribute>
													</td>
												</tr>
											</table>

										</td>
									</tr>
								</table>

								<hr />
								<h4>Tolerated Biomes</h4>
								<div>
									<xsl:if test="tolerance/biomes/Plains">
										<img class="biomeIcon" src="gfx/habitat/plains.png" title="Plains" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Desert">
										<img class="biomeIcon" src="gfx/habitat/desert.png" title="Desert" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/IcePlains">
										<img class="biomeIcon" src="gfx/habitat/snow.png" title="Ice Plains" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/ExtremeHills">
										<img class="biomeIcon" src="gfx/habitat/hills.png" title="Extreme Hills" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Forest">
										<img class="biomeIcon" src="gfx/habitat/forest.png" title="Forest" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Jungle">
										<img class="biomeIcon" src="gfx/habitat/jungle.png" title="Jungle" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Taiga">
										<img class="biomeIcon" src="gfx/habitat/snow-forest.png"
											title="Taiga" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Swamplands">
										<img class="biomeIcon" src="gfx/habitat/swamp.png" title="Swamplands" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Ocean">
										<img class="biomeIcon" src="gfx/habitat/ocean.png" title="Ocean" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/MushroomIsland">
										<img class="biomeIcon" src="gfx/habitat/mushroom.png"
											title="Mushroom Island" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Hell">
										<img class="biomeIcon" src="gfx/habitat/nether.png" title="Nether" />
									</xsl:if>

									<xsl:if test="tolerance/biomes/Sky">
										<img class="biomeIcon" src="gfx/habitat/end.png" title="The End" />
									</xsl:if>
								</div>
								<hr />
								<h4>Further Mutations</h4>

								<hr />
								<h4>Resultant Mutations</h4>

							</div>

						</xsl:for-each>


					</div>

				</xsl:for-each>

			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>