package binnie.extrabees.core;

import binnie.extrabees.gen.BlockExtraBeeHive;
import binnie.extrabees.machines.BlockAdvancedGeneticMachine;
import binnie.extrabees.machines.BlockApiaristMachine;
import binnie.extrabees.machines.BlockGeneticMachine;
import net.minecraft.src.Block;
import net.minecraft.src.Material;
import net.minecraft.src.RenderBlocks;

public class ExtraBeeBlock {
	public static BlockExtraBeeHive hive;
	public static Material materialBeehive;
	public static Block ectoplasm;

	public static BlockApiaristMachine apiaristMachine;
	public static BlockGeneticMachine geneticMachine;
	public static BlockAdvancedGeneticMachine advGeneticMachine;

	public final static int apiaristMachineID = 4002;
	public final static int geneticMachineID = 4003;
	public final static int advGeneticMachineID = 4004;

	public final static int hiveID = 4000;
	public final static int ectoplasmID = 4001;

	public static void renderInvBlock(RenderBlocks renderblocks, Block block,
			int i, int j) {
	}
}
