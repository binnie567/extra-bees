package net.minecraft.src;

import binnie.extrabees.network.ExtraBeePacketHandler;
import binnie.extrabees.proxy.Proxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(modid = "ExtraBees", name = "Extra Bees", version = "1.4")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = { "EB" }, packetHandler = ExtraBeePacketHandler.class)
public class ExtraBees {

	@Instance("ExtraBees")
	public static ExtraBees instance;

	@SidedProxy(clientSide = "binnie.extrabees.proxy.ProxyClient", serverSide = "binnie.extrabees.proxy.ProxyServer")
	public static Proxy proxy;

	public static String channel = "EB";

	@Init
	public void load(FMLInitializationEvent evt) {
		ExtraBees.instance = this;
	}

}
