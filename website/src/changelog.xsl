<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>

				<h4>Current Issues and Warnings</h4>
				<div class="changelog-main">
					<xsl:for-each select="changelog/issues/*">

						<xsl:if test="name(.) = 'warning'">

							<div class="changelog-entry">
								<div class="changelog changelog-type warning">
									Warning
								</div>
								<div class="changelog changelog-message warning">
									<xsl:value-of select="@message" />
								</div>
							</div>

						</xsl:if>

						<xsl:if test="name(.) = 'issue'">

							<div class="changelog-entry">
								<div class="changelog changelog-type issue">
									Issue
								</div>
								<div class="changelog changelog-message issue">
									<xsl:value-of select="@message" />
								</div>
							</div>

						</xsl:if>


					</xsl:for-each>
				</div>

				<xsl:for-each select="changelog/version">
					<h4>
						Version
						<xsl:value-of select="@number" />
					</h4>
					<div class="changelog-main">
						<xsl:for-each select="*">

							<xsl:if test="name(.) = 'info'">

								<div class="changelog-entry">
									<div class="changelog changelog-type info">
										Info
									</div>
									<div class="changelog changelog-message info">
										<xsl:value-of select="@message" />
									</div>
								</div>

							</xsl:if>

							<xsl:if test="name(.) = 'new'">

								<div class="changelog-entry">
									<div class="changelog changelog-type new">
										New
									</div>
									<div class="changelog changelog-message new">
										<xsl:value-of select="@message" />
									</div>
								</div>

							</xsl:if>

							<xsl:if test="name(.) = 'change'">

								<div class="changelog-entry">
									<div class="changelog changelog-type change">
										Change
									</div>
									<div class="changelog changelog-message change">
										<xsl:value-of select="@message" />
									</div>
								</div>

							</xsl:if>

							<xsl:if test="name(.) = 'fix'">

								<div class="changelog-entry">
									<div class="changelog changelog-type fix">
										Fix
									</div>
									<div class="changelog changelog-message fix">
										<xsl:value-of select="@message" />
										<xsl:if test="@issue">
											&#160;(
											<a>
											<xsl:attribute name="href">https://bitbucket.org/binnie567/extra-bees/issue/<xsl:value-of select="@issue" />/</xsl:attribute>
											Issue #<xsl:value-of select="@issue" />
											</a>
											)
										</xsl:if>

									</div>
								</div>

							</xsl:if>

						</xsl:for-each>
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>