package binnie.extrabees.config;

import java.io.File;

import binnie.core.BinnieCore;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

public class ConfigBase {

	protected Configuration config;

	ConfigBase(String filename) {
		config = new Configuration(new File(BinnieCore.proxy.getDirectory(),
				"config/forestry/extrabees/" + filename + ".conf"));
		config.load();
	}

	public void save() {
		config.save();
	}

	public int getInt(String key, String category, int defaultValue) {
		Property property = config.get(category, key, defaultValue);
		property.comment = "Default value is " + defaultValue;
		return property.getInt(defaultValue);
	}

	public boolean getBoolean(String key, String category, boolean defaultValue) {
		Property property = config.get(category, key, defaultValue);
		property.comment = "Default value is " + defaultValue;
		return property.getBoolean(defaultValue);
	}

	public int getBlockId(String key, int defaultID) {
		Property property = config.get(Configuration.CATEGORY_BLOCK, key,
				defaultID);
		property.comment = "Default ID is " + defaultID;
		return property.getInt(defaultID);
	}

	public int getItemID(String key, int defaultID) {
		Property property = config.get(Configuration.CATEGORY_ITEM, key,
				defaultID);
		property.comment = "Default ID is " + defaultID;
		return property.getInt(defaultID);
	}

}
