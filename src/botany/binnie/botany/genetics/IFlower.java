package binnie.botany.genetics;

import java.util.ArrayList;

import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;
import forestry.api.genetics.IEffectData;
import forestry.api.genetics.IIndividual;

public interface IFlower extends IIndividual {

	IFlowerGenome getFlowerGenome();

	public void age();

	public IFlower die(World world);

	public void mate(IFlowerGenome genome);

	int getAge();

	public boolean isAlive();

	public boolean isPollinated();

	public IFlower getOffspring(World world);

	int getDamage();

	void setAge(int age);

}
