package binnie.extrabees.genetics;

import java.lang.reflect.Method;
import java.util.List;

import binnie.extrabees.core.ExtraBeeBlock;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.DamageSource;
import net.minecraft.src.EntityLightningBolt;
import net.minecraft.src.EntityList;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import forestry.api.apiculture.IAlleleBeeEffect;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IBeeHousing;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IEffectData;
import forestry.api.genetics.ILegacyHandler;

public enum EnumExtraBeeEffect implements IAlleleBeeEffect {
	ECTOPLASM, ACID, SPAWN_ZOMBIE, SPAWN_SKELETON, SPAWN_CREEPER, LIGHTNING, RADIOACTIVE

	;

	EnumExtraBeeEffect() {
		combinable = false;
		name = "??? Effect";
		dominant = true;
		id = 0;
	}

	public boolean combinable;
	public String name;
	public boolean dominant;
	public int id;
	private String uid;

	public static Method FX;

	public static void doInit() {
		int id = 1625;
		ECTOPLASM.init("Ectoplasm", id++);
		ACID.init("Acidic", id++);
		SPAWN_ZOMBIE.init("Zombies", id++);
		SPAWN_SKELETON.init("Skeletons", id++);
		SPAWN_CREEPER.init("Creepers", id++);
		LIGHTNING.init("Lightning", id++);
		RADIOACTIVE.init("Unstable", id++);
		ModLoader.getLogger().fine("Trying to implement forestry hive fx...");

		try {
			FX = Class.forName("forestry.apiculture.ProxyApiculture")
					.getMethod(
							"addBeeHiveFX",
							new Class[] { String.class, World.class,
									double.class, double.class, double.class,
									int.class });
			ModLoader.getLogger().fine("Succeeded.");
		} catch (Exception ex) {
			ModLoader.getLogger().fine("Failed: " + ex.getMessage());
		}

		for (EnumExtraBeeEffect effect : EnumExtraBeeEffect.values()) {
			effect.register();
		}

	}

	public void init(String name, int id) {
		this.name = name;
		this.id = id;
		this.uid = this.toString().toLowerCase();
		((ILegacyHandler) AlleleManager.alleleRegistry).registerLegacyMapping(
				id, uid);
	}

	public void register() {
		AlleleManager.alleleRegistry.registerAllele(this);
	}

	@Override
	public boolean isCombinable() {
		return combinable;
	}

	@Override
	public IEffectData validateStorage(IEffectData storedData) {
		return storedData;
	}

	@Override
	public String getIdentifier() {
		return name;
	}

	@Override
	public boolean isDominant() {
		return dominant;
	}

	public void spawnMob(World world, int x, int y, int z, String name) {
		if (this.anyPlayerInRange(world, x, y, z, 16)) {
			double var1 = x + world.rand.nextFloat();
			double var3 = y + world.rand.nextFloat();
			double var5 = z + world.rand.nextFloat();
			world.spawnParticle("smoke", var1, var3, var5, 0.0D, 0.0D, 0.0D);
			world.spawnParticle("flame", var1, var3, var5, 0.0D, 0.0D, 0.0D);
			EntityLiving var9 = ((EntityLiving) EntityList.createEntityByName(
					name, world));

			if (var9 == null) {
				return;
			}

			int var10 = world.getEntitiesWithinAABB(
					var9.getClass(),
					AxisAlignedBB.getBoundingBox(x, y, z, x + 1, y + 1, z + 1)
							.expand(8.0D, 4.0D, 8.0D)).size();

			if (var10 >= 6) {
				return;
			}

			if (var9 != null) {
				double var11 = x
						+ (world.rand.nextDouble() - world.rand.nextDouble())
						* 4.0D;
				double var13 = y + world.rand.nextInt(3) - 1;
				double var15 = z
						+ (world.rand.nextDouble() - world.rand.nextDouble())
						* 4.0D;
				var9.setLocationAndAngles(var11, var13, var15,
						world.rand.nextFloat() * 360.0F, 0.0F);

				if (var9.getCanSpawnHere()) {
					world.spawnEntityInWorld(var9);
					world.playAuxSFX(2004, x, y, z, 0);
					var9.spawnExplosionParticle();
				}
			}
		}
	}

	private boolean anyPlayerInRange(World world, int x, int y, int z,
			int distance) {
		return world.getClosestPlayer(x + 0.5D, y + 0.5D, z + 0.5D, distance) != null;
	}

	public static void doAcid(World world, int x, int y, int z) {
		int id = world.getBlockId(x, y, z);

		if ((id == Block.cobblestone.blockID) || (id == Block.stone.blockID)) {
			world.setBlockWithNotify(x, y, z, Block.gravel.blockID);
		} else if ((id == Block.dirt.blockID) | (id == Block.grass.blockID)) {
			world.setBlockWithNotify(x, y, z, Block.sand.blockID);
		}
	}

	@Override
	public String getUID() {
		return "extrabees.effect." + uid;
	}

	@Override
	public String getIconTextureFile() {
		return null;
	}

	@Override
	public int getIconIndex() {
		return -1;
	}

	@Override
	public IEffectData doEffect(IBeeGenome genome, IEffectData storedData,
			IBeeHousing housing) {

		World world = housing.getWorld();
		int x = housing.getXCoord();
		int y = housing.getYCoord();
		int z = housing.getZCoord();

		int[] area = genome.getTerritory();

		switch (this) {
		case ECTOPLASM: {
			if (world.rand.nextInt(100) < 4) {
				int xd = 1 + area[0] / 2;
				int yd = 1 + area[1] / 2;
				int zd = 1 + area[2] / 2;
				int x1 = x - xd + world.rand.nextInt(2 * xd + 1);
				int y1 = y - yd + world.rand.nextInt(2 * yd + 1);
				int z1 = z - zd + world.rand.nextInt(2 * zd + 1);

				if (world.isAirBlock(x1, y1, z1)
						&& (world.isBlockNormalCube(x1, y1 - 1, z1) || (world
								.getBlockId(x1, y1 - 1, z1) == ExtraBeeBlock.ectoplasm.blockID))) {
					world.setBlockWithNotify(x1, y1, z1,
							ExtraBeeBlock.ectoplasm.blockID);
				}

				return null;
			}
			break;
		}

		case ACID: {
			if (world.rand.nextInt(100) < 6) {
				int xd = 1 + area[0] / 2;
				int yd = 1 + area[1] / 2;
				int zd = 1 + area[2] / 2;
				int x1 = x - xd + world.rand.nextInt(2 * xd + 1);
				int y1 = y - yd + world.rand.nextInt(2 * yd + 1);
				int z1 = z - zd + world.rand.nextInt(2 * zd + 1);
				doAcid(world, x1, y1, z1);
			}
			break;
		}

		case SPAWN_ZOMBIE: {
			if (world.rand.nextInt(200) < 2) {
				spawnMob(world, x, y, z, "Zombie");
			}
			break;
		}

		case SPAWN_SKELETON: {
			if (world.rand.nextInt(200) < 2) {
				spawnMob(world, x, y, z, "Skeleton");
			}
			break;
		}

		case SPAWN_CREEPER: {
			if (world.rand.nextInt(200) < 2) {
				spawnMob(world, x, y, z, "Creeper");
			}
			break;
		}

		case LIGHTNING: {
			if (world.rand.nextInt(100) < 1) {
				int xd = 1 + area[0] / 2;
				int yd = 1 + area[1] / 2;
				int zd = 1 + area[2] / 2;
				int x1 = x - xd + world.rand.nextInt(2 * xd + 1);
				int y1 = y - yd + world.rand.nextInt(2 * yd + 1);
				int z1 = z - zd + world.rand.nextInt(2 * zd + 1);

				if (world.canBlockSeeTheSky(x1, y1, z1)) {
					world.spawnEntityInWorld(new EntityLightningBolt(world, x1,
							y1, z1));
				}
			}
			break;
		}

		case RADIOACTIVE: {
			int[] offset = new int[] { -Math.round(area[0] / 2),
					-Math.round(area[1] / 2), -Math.round(area[2] / 2) };

			int[] min = new int[] { housing.getXCoord() + offset[0],
					housing.getYCoord() + offset[1],
					housing.getZCoord() + offset[2] };
			int[] max = new int[] { housing.getXCoord() + offset[0] + area[0],
					housing.getYCoord() + offset[1] + area[1],
					housing.getZCoord() + offset[2] + area[2] };

			AxisAlignedBB hurtBox = AxisAlignedBB.getAABBPool()
					.addOrModifyAABBInPool(min[0], min[1], min[2], max[0],
							max[1], max[2]);
			List list = housing.getWorld().getEntitiesWithinAABB(
					EntityLiving.class, hurtBox);

			for (Object obj : list) {
				EntityLiving entity = (EntityLiving) obj;

				int damage = 4;
				/*
				 * // Players are not attacked if they wear a full set of
				 * apiarist's // armor. if (entity instanceof EntityPlayer) {
				 * int count = ItemArmorApiarist.wearsItems((EntityPlayer)
				 * entity); // Full set, no damage/effect if (count > 3)
				 * continue; else if (count > 2) damage = 1; else if (count > 1)
				 * damage = 2; else if (count > 0) damage = 3; }
				 */

				entity.attackEntityFrom(DamageSource.generic, damage);

			}
			break;
		}

		}

		return null;
	}

	@Override
	public IEffectData doFX(IBeeGenome genome, IEffectData storedData,
			IBeeHousing housing) {

		World world = housing.getWorld();
		int x = housing.getXCoord();
		int y = housing.getYCoord();
		int z = housing.getZCoord();

		try {
			FX.invoke(null, new Object[] {
					"/gfx/forestry/particles/swarm_bee.png", world, x, y, z,
					genome.getPrimaryAsBee().getPrimaryColor() });
		} catch (Exception e) {
		}

		return storedData;
	}
}
