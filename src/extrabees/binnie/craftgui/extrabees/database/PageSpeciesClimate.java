package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesClimate extends PageSpecies {

	public PageSpeciesClimate(IWidget parent) {
		super(parent);

		new ControlText(this, 72, 8, "Climate", ControlText.Alignment.Center);

		tempBar = new ControlClimateBar(this, 8, 24, 128, 12);
		humidBar = new ControlClimateBar(this, 8, 42, 128, 12, true);
		new ControlText(this, 72, 70, "Biomes", ControlText.Alignment.Center);

		biomes = new ControlBiomes(this, 8, 90, 8, 4);
	}

	ControlClimateBar tempBar;
	ControlClimateBar humidBar;
	ControlBiomes biomes;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		tempBar.setSpecies(species);
		humidBar.setSpecies(species);
		biomes.setSpecies(species);
	}

}
