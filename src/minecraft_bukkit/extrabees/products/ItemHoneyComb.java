package extrabees.products;

import extrabees.core.ExtraBeeCore;
import extrabees.core.ExtraBeeItem;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.ICentrifugeManager;
import forestry.api.recipes.RecipeManagers;
import forge.ITextureProvider;
import forge.oredict.OreDictionary;
import ic2.api.Items;
import java.util.ArrayList;
import net.minecraft.server.*;
import net.minecraft.server.ItemStack;
import extrabees.products.ItemHoneyComb;
import extrabees.products.ItemHoneyComb.EnumType;

public class ItemHoneyComb extends Item implements ITextureProvider
{
	public static enum EnumType
    {
        BARREN,

        ROTTEN, BONE,

        OIL, COAL, FUEL,

        WATER, MILK, FRUIT, SEED, ALCOHOL,

        STONE, REDSTONE, RESIN, IC2ENERGY,

        IRON, GOLD, COPPER, TIN, SILVER, BRONZE, URANIUM, CLAY, OLD, FUNGAL,

        CREOSOTE, LATEX, ACIDIC, VENOMOUS, SLIME, BLAZE,
        
        COFFEE, GLACIAL
        ;

        public static EnumType getTypeFromID(int id, int subID)
        {
            return EnumType.class.getEnumConstants()[id + subID * 16];
        }

        public void copyProducts(EnumType stone2)
        {
            products.addAll(stone2.products);
            chances.addAll(stone2.chances);
        }

        public static boolean isTypeInSubItem(EnumType type, int subID)
        {
            return (type.ordinal() / 16) == subID;
        }

        public String name;
        public int primaryColor;
        public int secondaryColor;
        public boolean isSecret = false;
        public ArrayList<ItemStack> products;
        public ArrayList<Integer> chances;
        public int timeToCentifuge;

        EnumType()
        {
            this.name = "??? Comb";
            this.primaryColor = 0xFFFFFF;
            this.secondaryColor = 0xFFFFFF;
            this.isSecret = false;
            products = new ArrayList<ItemStack>();
            chances = new ArrayList<Integer>();
            this.timeToCentifuge = 20;
        }

        public void init(String name, int primaryColor, int secondaryColor)
        {
            this.name = name;
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
        }

        public EnumType setIsSecret()
        {
            this.isSecret = true;
            return this;
        }

        public EnumType addProduct(ItemStack item, int chance)
        {
            products.add(item);
            chances.add(chance);
            return this;
        }

        public void addRecipe()
        {
            int[] chancesI = new int[chances.size()];

            for (int i = 0; i < chances.size(); i++)
            {
                chancesI[i] = chances.get(i);
            }

            ItemStack[] productsI = new ItemStack[products.size()];

            for (int i = 0; i < products.size(); i++)
            {
                productsI[i] = products.get(i);
            }

            RecipeManagers.centrifugeManager.addRecipe(timeToCentifuge,
                    ItemHoneyComb.getItem(this),
                    productsI, chancesI);
        }
    }

	public static enum EnumVanillaType
    {
        HONEY, COCOA, SIMMERING, STRINGY, FROZEN, DRIPPING, SILKY,
        PARCHED, MYSTERIOUS, IRRADIATED, POWDERY, REDDENED, DARKENED, OMEGA,
        WHEATEN, MOSSY;
    }

    private int subID;
    private static ItemHoneyComb combs[];

    public ItemHoneyComb(int i, int j)
    {
        super(i);
        maxStackSize = 64;
        setMaxDurability(0);
        a(true);
        subID = j;
    }

    public static ItemHoneyComb[] setupCombs(int i)
    {
        int j = 1 + (EnumType.values().length - 1) / 16;
        combs = new ItemHoneyComb[j];

        for (int k = 0; k < j; k++)
        {
            combs[k] = new ItemHoneyComb(i + k, k);
        }

        return combs;
    }

    public static ItemStack getItem(EnumType enumtype)
    {
        return new ItemStack(combs[enumtype.ordinal() / 16], 1, enumtype.ordinal() % 16);
    }

    public static void addSubtypes()
    {
        if (ItemInterface.getItem("beeswax") == null || ItemInterface.getItem("honeyDrop") == null)
        {
            ModLoader.throwException("Forstry beeswax or honey not found!", new Exception());
        }

        EnumType.BARREN.init("Barren Comb", 0x736c44, 0xc2bea7);
        EnumType.BARREN.addProduct(ItemInterface.getItem("beeswax"), 100);
        EnumType.BARREN.addProduct(ItemInterface.getItem("honeyDrop"), 50);
        EnumType.ROTTEN.init("Rotten Comb", 0x3e5221, 0xb1cc89);
        EnumType.ROTTEN.addProduct(ItemInterface.getItem("beeswax"), 20);
        EnumType.ROTTEN.addProduct(ItemInterface.getItem("honeyDrop"), 20);
        EnumType.ROTTEN.addProduct(new ItemStack(Item.ROTTEN_FLESH, 1, 0), 80);
        EnumType.BONE.init("Skeletal Comb", 0xc4c4af, 0xdedec1);
        EnumType.BONE.addProduct(ItemInterface.getItem("beeswax"), 20);
        EnumType.BONE.addProduct(ItemInterface.getItem("honeyDrop"), 20);
        EnumType.BONE.addProduct(new ItemStack(Item.INK_SACK, 1, 15), 80);
        EnumType.OIL.init("Oily Comb", 0x60608, 0x2c2b36);
        EnumType.OIL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.OIL.ordinal()), 80);
        EnumType.OIL.addProduct(ItemInterface.getItem("honeyDrop"), 75);
        EnumType.COAL.init("Fossilised Comb", 0x9e9478, 0x38311e);
        EnumType.COAL.addProduct(ItemInterface.getItem("beeswax"), 80);
        EnumType.COAL.addProduct(ItemInterface.getItem("honeyDrop"), 75);

        if (ExtraBeeCore.modIC2 && Items.getItem("coalDust") != null)
        {
            EnumType.COAL.addProduct(Items.getItem("coalDust"), 100);
            EnumType.COAL.addProduct(Items.getItem("coalDust"), 50);
        }
        else
        {
            EnumType.COAL.addProduct(new ItemStack(Item.COAL, 1, 0), 100);
            EnumType.COAL.addProduct(new ItemStack(Item.COAL, 1, 0), 50);
        }

        EnumType.WATER.init("Wet Comb", 0x2732cf, 0x79a8c9);
        EnumType.WATER.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.WATER.ordinal()), 100);
        EnumType.WATER.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.STONE.init("Rocky Comb", 0x8c8c91, 0xc6c6cc);
        EnumType.STONE.addProduct(ItemInterface.getItem("beeswax"), 50);
        EnumType.STONE.addProduct(ItemInterface.getItem("honeyDrop"), 25);
        EnumType.MILK.init("Milky Comb", 0xd7d9c7, 0xffffff);
        EnumType.MILK.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.MILK.ordinal()), 100);
        EnumType.MILK.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.FRUIT.init("Fruity Comb", 0x7d2934, 0xdb4f62);
        EnumType.FRUIT.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.FRUIT.ordinal()), 100);
        EnumType.FRUIT.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.SEED.init("Seedy Comb", 0x344f33, 0x71cc6e);
        EnumType.SEED.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.SEED.ordinal()), 100);
        EnumType.SEED.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.ALCOHOL.init("Alcoholic Comb", 0x418521, 0xded94e);
        EnumType.ALCOHOL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.ALCOHOL.ordinal()), 100);
        EnumType.ALCOHOL.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.FUEL.init("Petroleum Comb", 0x9c6f40, 0xffc400);
        EnumType.FUEL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.FUEL.ordinal()), 80);
        EnumType.FUEL.addProduct(ItemInterface.getItem("honeyDrop"), 50);
        EnumType.CREOSOTE.init("Tar Comb", 0x9c810c, 0xbdaa57);
        EnumType.CREOSOTE.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.CREOSOTE.ordinal()), 80);
        EnumType.CREOSOTE.addProduct(ItemInterface.getItem("honeyDrop"), 50);
        EnumType.LATEX.init("Latex Comb", 0x595541, 0xa8a285);
        EnumType.LATEX.addProduct(ItemInterface.getItem("honeyDrop"), 50);
        EnumType.LATEX.addProduct(ItemInterface.getItem("beeswax"), 85);

        if (ExtraBeeCore.modIC2 && Items.getItem("rubber") != null)
        {
            EnumType.LATEX.addProduct(Items.getItem("rubber"), 100);
        }
        else if (!OreDictionary.getOres("itemRubber").isEmpty())
        {
            EnumType.LATEX.addProduct((ItemStack)OreDictionary.getOres("itemRubber").get(0), 100);
        }

        EnumType.REDSTONE.init("Glowing Comb", 0xfa9696, 0xe61010);
        EnumType.REDSTONE.addProduct(ItemInterface.getItem("beeswax"), 80);
        EnumType.REDSTONE.addProduct(new ItemStack(Item.REDSTONE, 1, 0), 100);
        EnumType.REDSTONE.addProduct(ItemInterface.getItem("honeyDrop"), 50);
        EnumType.RESIN.init("Amber Comb", 0xffc74f, 0xc98a00);
        EnumType.RESIN.addProduct(ItemInterface.getItem("beeswax"), 100);

        if (ExtraBeeCore.modIC2 && Items.getItem("resin") != null)
        {
            EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
            EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
            EnumType.RESIN.addProduct(Items.getItem("resin"), 100);
            EnumType.RESIN.addProduct(Items.getItem("resin"), 50);
        }

        EnumType.IC2ENERGY.init("Static Comb", 0xe9f50f, 0x20b3c9);
        EnumType.IC2ENERGY.addProduct(ItemInterface.getItem("beeswax"), 80);
        EnumType.IC2ENERGY.addProduct(new ItemStack(Item.REDSTONE, 1, 0), 75);
        EnumType.IC2ENERGY.addProduct(ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.ENERGY), 100);
        EnumType.IRON.init("Iron Comb", 0x363534, 0xa87058);
        EnumType.IRON.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("ironDust") != null)
        {
            EnumType.IRON.addProduct(Items.getItem("ironDust"), 75);
        }
        else
        {
            EnumType.IRON.addProduct(new ItemStack(Item.IRON_INGOT, 1, 0), 75);
        }

        EnumType.GOLD.init("Golden Comb", 0x363534, 0xe6cc0b);
        EnumType.GOLD.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("goldDust") != null)
        {
            EnumType.GOLD.addProduct(Items.getItem("goldDust"), 75);
        }
        else
        {
            EnumType.GOLD.addProduct(new ItemStack(Item.GOLD_INGOT, 1, 0), 75);
        }

        EnumType.COPPER.init("Copper Comb", 0x363534, 0xd16308);
        EnumType.COPPER.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("copperDust") != null)
        {
            EnumType.COPPER.addProduct(Items.getItem("copperDust"), 75);
        }
        else if (!OreDictionary.getOres("ingotCopper").isEmpty())
        {
            EnumType.COPPER.addProduct((ItemStack)OreDictionary.getOres("ingotCopper").get(0), 75);
        }

        EnumType.TIN.init("Tin Comb", 0x363534, 0xbdb1bd);
        EnumType.TIN.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("tinDust") != null)
        {
            EnumType.TIN.addProduct(Items.getItem("tinDust"), 75);
        }
        else if (!OreDictionary.getOres("ingotTin").isEmpty())
        {
            EnumType.TIN.addProduct((ItemStack)OreDictionary.getOres("ingotTin").get(0), 75);
        }

        EnumType.SILVER.init("Silver Comb", 0x363534, 0xdbdbdb);
        EnumType.SILVER.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("silverDust") != null)
        {
            EnumType.SILVER.addProduct(Items.getItem("silverDust"), 75);
        }
        else if (!OreDictionary.getOres("ingotSilver").isEmpty())
        {
            EnumType.SILVER.addProduct((ItemStack)OreDictionary.getOres("ingotSilver").get(0), 75);
        }

        EnumType.BRONZE.init("Bronze Comb", 0x363534, 0xd48115);
        EnumType.BRONZE.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("bronzeDust") != null)
        {
            EnumType.BRONZE.addProduct(Items.getItem("bronzeDust"), 15);
        }
        else if (!OreDictionary.getOres("ingotBronze").isEmpty())
        {
            EnumType.BRONZE.addProduct((ItemStack)OreDictionary.getOres("ingotBronze").get(0), 15);
        }
        else
        {
            EnumType.BRONZE.copyProducts(EnumType.STONE);
        }

        EnumType.URANIUM.init("Radioactive Comb", 0x1eff00, 0x41ab33);
        EnumType.URANIUM.copyProducts(EnumType.STONE);

        if (ExtraBeeCore.modIC2 && Items.getItem("uraniumDrop") != null)
        {
            EnumType.URANIUM.addProduct(Items.getItem("uraniumDrop"), 75);
        }

        EnumType.CLAY.init("Clay Comb", 0x6b563a, 0xb0c0d6);
        EnumType.CLAY.addProduct(ItemInterface.getItem("beeswax"), 25);
        EnumType.CLAY.addProduct(ItemInterface.getItem("honeyDrop"), 80);

        if (ExtraBeeCore.modIC2 && Items.getItem("clayDust") != null)
        {
            EnumType.CLAY.addProduct(Items.getItem("clayDust"), 75);
        }
        else
        {
            EnumType.CLAY.addProduct(new ItemStack(Item.CLAY_BALL, 1, 0), 75);
        }

        EnumType.OLD.init("Ancient Comb", 0x453314, 0xb39664);
        EnumType.OLD.addProduct(ItemInterface.getItem("beeswax"), 100);
        EnumType.OLD.addProduct(ItemInterface.getItem("honeyDrop"), 90);
        EnumType.FUNGAL.init("Fungal Comb", 0x6e654b, 0x2b9443);
        EnumType.FUNGAL.addProduct(ItemInterface.getItem("beeswax"), 90);
        EnumType.FUNGAL.addProduct(new ItemStack(Block.BROWN_MUSHROOM, 1, 0), 100);
        EnumType.FUNGAL.addProduct(new ItemStack(Block.RED_MUSHROOM, 1, 0), 75);
        EnumType.ACIDIC.init("Acidic Comb", 0x348543, 0x14f73e);
        EnumType.ACIDIC.addProduct(ItemInterface.getItem("beeswax"), 80);
        EnumType.ACIDIC.addProduct(ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.ACID), 80);
        EnumType.VENOMOUS.init("Venomous Comb", 0x7d187d, 0xff33ff);
        EnumType.VENOMOUS.addProduct(ItemInterface.getItem("beeswax"), 80);
        EnumType.VENOMOUS.addProduct(ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.POISON), 80);
        EnumType.SLIME.init("Mucous Comb", 0x3b473c, 0x80d185);
        EnumType.SLIME.addProduct(ItemInterface.getItem("beeswax"), 100);
        EnumType.SLIME.addProduct(ItemInterface.getItem("honeyDrop"), 75);
        EnumType.SLIME.addProduct(new ItemStack(Item.SLIME_BALL, 1, 0), 75);
        EnumType.BLAZE.init("Blazing Comb", 0xff6a00, 0xffcc00);
        EnumType.BLAZE.addProduct(ItemInterface.getItem("beeswax"), 75);
        EnumType.BLAZE.addProduct(new ItemStack(Item.BLAZE_POWDER, 1, 0), 100);
        EnumType.COFFEE.init("Coffee Comb", 0x54381d, 0xb37f4b);
        EnumType.COFFEE.addProduct(ItemInterface.getItem("beeswax"), 90);
        EnumType.COFFEE.addProduct(ItemInterface.getItem("honeyDrop"), 75);

        if (ExtraBeeCore.modIC2 && Items.getItem("coffeePowder") != null)
        {
            EnumType.COFFEE.addProduct(Items.getItem("coffeePowder"), 75);
        }

        EnumType.GLACIAL.init("Glacial Comb", 0x4e8787, 0xcbf2f2);
        EnumType.GLACIAL.addProduct(new ItemStack(ExtraBeeItem.propolis, 1, ItemPropolis.EnumType.GLACIAL.ordinal()), 80);
        EnumType.GLACIAL.addProduct(ItemInterface.getItem("honeyDrop"), 75);
    }

    public boolean g()
    {
        return false;
    }

    public boolean isRepairable()
    {
        return false;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return EnumType.getTypeFromID(itemstack.getData(), subID).name;
    }

    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }

    public int getColorFromDamage(int i, int j)
    {
        if (j == 0)
        {
            return EnumType.getTypeFromID(i, subID).primaryColor;
        }
        else
        {
            return EnumType.getTypeFromID(i, subID).secondaryColor;
        }
    }

    public int func_46057_a(int i, int j)
    {
        return j <= 0 ? 107 : 108;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        EnumType aenumtype[] = EnumType.values();
        int i = aenumtype.length;

        for (int j = 0; j < i; j++)
        {
            EnumType enumtype = aenumtype[j];

            if (!enumtype.isSecret && EnumType.isTypeInSubItem(enumtype, subID))
            {
                arraylist.add(new ItemStack(this, 1, enumtype.ordinal() % 16));
            }
        }
    }

    public static ItemStack getVanillaItem(EnumVanillaType enumvanillatype)
    {
        return new ItemStack(ItemInterface.getItem("beeComb").getItem(), 1, enumvanillatype.ordinal());
    }

    public String getTextureFile()
    {
        return "/gfx/forestry/items/items.png";
    }
}
