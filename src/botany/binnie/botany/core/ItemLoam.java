package binnie.botany.core;

import java.util.List;

import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.FlowerSpecies;
import binnie.botany.genetics.IAlleleFlowerSpecies;
import binnie.botany.genetics.IAlleleFlowerStem;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Item;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;

public class ItemLoam extends ItemBlock {

	public ItemLoam(int i) {
		super(i);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return BlockLoam.getName(par1ItemStack.getItemDamage());
	}

	@Override
	public int getMetadata(int i) {
		return i;
	}

}
