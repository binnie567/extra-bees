package binnie.craftgui.extrabees.database;

import java.text.DecimalFormat;

import binnie.craftgui.controls.ControlItemDisplay;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.ControlText.Alignment;
import binnie.craftgui.controls.listbox.ControlAbstractOption;
import binnie.craftgui.controls.listbox.ControlAbstractOptionSet;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.extrabees.database.ControlProductsBox.Product;

public class ControlProductsItem extends
		ControlAbstractOption<ControlProductsBox.Product> {

	public ControlProductsItem(ControlAbstractOptionSet<Product> controlOptions, float x, float y,
			float width, float height, Product value) {
		super(controlOptions, x, y, width, height, value);
		item = new ControlItemDisplay(this, 4, 4);
		item.setTooltip();
		
		ControlText textWidget = new ControlText(this, getSize().x/2+12, getSize().y/2, "", Alignment.Center);

		if (value != null) {
			this.item.setItemStack(value.item);
			float time = (int) (550.0 * 100.0 / (value.chance));

			float seconds = time / 20.0f;
			float minutes = seconds / 60.0f;
			float hours = minutes / 60.0f;

			DecimalFormat df = new DecimalFormat("#.0");

			if (hours > 1.0f) {
				textWidget.setText("Every " + df.format(hours) + " hours");
			} else if (minutes > 1.0f) {
				textWidget.setText("Every " + df.format(minutes)
						+ " min.");
			} else {
				textWidget.setText("Every " + df.format(seconds)
						+ " sec.");
			}
		}
	}

	ControlItemDisplay item;

}
