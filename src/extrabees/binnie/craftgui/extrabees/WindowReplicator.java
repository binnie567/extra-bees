package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlLiquidTank;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.controls.ControlProgress.Direction;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityReplicator;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowReplicator extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityReplicator machine;

	ControlSlot slotSerum;
	ControlSlot slotSerumEmpty;
	ControlSlotArray slotSerumsDone;

	ControlLiquidTank tankDNA;

	ControlExtraBeeProgress progessWidget;

	public WindowReplicator(EntityPlayer player, IInventory inventory, Side side) {
		super(260, 212, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityReplicator) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowReplicator(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Replicator");

		playerInventory = new ControlPlayerInventory(this);

		progessWidget = new ControlExtraBeeProgress(this, 48, 24, 160, 79, 0,
				79, 0, 0, Direction.Right,
				"/gfx/extrabees/extrabees-processes2.png");

		slotSerum = new ControlSlot(this, 48 - 18, 28 + 30 - 9);
		slotSerum.create(machine, TileEntityReplicator.SlotSerum);

		slotSerumEmpty = new ControlSlot(this, 208, 60 - 26);
		slotSerumEmpty.create(machine, TileEntityReplicator.SlotSerumEmpty);

		slotSerumsDone = new ControlSlotArray(this, 208 - 36, 68, 3, 2);
		slotSerumsDone.create(machine, TileEntityReplicator.SlotSerumsDone[0]);

		new ControlErrorState(this, 70, 90);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
