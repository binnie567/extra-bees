package binnie.extrabees.apiary;

//package binnie.extrabees.alveary;
//
//import binnie.extrabees.core.ExtraBeeTexture;
//import net.minecraft.src.CreativeTabs;
//import net.minecraft.src.TileEntity;
//import net.minecraft.src.World;
//import forestry.apiculture.MaterialBeehive;
//import forestry.apiculture.gadgets.TileAlvearyFan;
//import forestry.apiculture.gadgets.TileAlvearyHeater;
//import forestry.apiculture.gadgets.TileAlvearySwarmer;
//import forestry.apiculture.gadgets.BlockAlvearyComponent.AlvearyComponent;
//import forestry.core.gadgets.BlockStructure;
//
//public class BlockAlvearyComponent extends BlockStructure {
//
//	public static enum AlvearyComponent {
//		Mutator(80, 96),
//		
//		;
//		
//		public int textureOff;
//		public int textureOn;
//		
//		private AlvearyComponent(int textureOff, int textureOn) {
//			this.textureOff = textureOff;
//			this.textureOn = textureOn;
//		}
//	}
//
//	private AlvearyComponent type;
//	
//	public BlockAlvearyComponent(int i) {
//		super(i, new MaterialBeehive(false));
//		setHardness(1.0f);
//		this.type = AlvearyComponent.Mutator;
//		setCreativeTab(CreativeTabs.tabRedstone);
//		setTextureFile(ExtraBeeTexture.Engineering.getTexture());
//	}
//
//	@Override
//    public int getRenderType() {
//        return 0;
//    }
//
//	@Override
//	public boolean renderAsNormalBlock() {
//		return true;
//	}
//
//	@Override
//	public TileEntity createNewTileEntity(World world) {
//		switch(type) {
//		case Mutator:
//			return new TileAlvearyMutator();
//		default:
//			return null;
//		}
//	}
//
//	@Override
//	public int getBlockTextureFromSideAndMetadata(int i, int j) {
//		if(j > 0)
//			return type.textureOn;
//		else
//			return type.textureOff;
//	}
//}
//
