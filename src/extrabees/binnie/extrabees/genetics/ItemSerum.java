package binnie.extrabees.genetics;

import java.util.List;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class ItemSerum extends ItemSerumEmpty {

	@Override
	@SideOnly(Side.CLIENT)
	public int getIconFromDamageForRenderPass(int par1, int par2) {
		return iconIndex + 1 + par2;
	}

	@Override
	public int getRenderPasses(int metadata) {
		return 4;
	}

	public ItemSerum(int id) {
		super(id);
		setMaxStackSize(1);
		setIconIndex(112);
		setMaxDamage(16);
	}

	@Override
	public int getColorFromItemStack(ItemStack itemstack, int j) {
		int i = itemstack.getItemDamage();

		IAllele allele = AlleleManager.alleleRegistry
				.getAllele(getUID(itemstack));
		EnumBeeChromosome chromosome = getChromosome(itemstack);

		if (allele == null || chromosome == null) {
			return 0x444444;
		}

		int colour = ModuleEngineering.getColour(allele, chromosome, j);

		if (colour >= 0)
			return colour;

		if (j == 3)
			return 0xD6A9A9;

		return 0xFFFFFF;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer,
			List list, boolean par4) {
		super.addInformation(itemStack, entityPlayer, list, par4);
		int damage = this.getMaxDamage() - itemStack.getItemDamage();

		if (damage == 0)
			list.add("Empty");
		else if (damage == 1)
			list.add("1 Charge");
		else
			list.add(damage + " Charges");

		ModuleEngineering.getQualityInfo(itemStack, list);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		String UID = getUID(itemstack);
		if (UID == null)
			return "Corrupted Serum";

		IAllele allele = AlleleManager.alleleRegistry.getAllele(UID);
		EnumBeeChromosome chromo = getChromosome(itemstack);

		if (allele == null || chromo == null)
			return "Corrupted Serum";

		return ModuleEngineering.getName(allele, chromo) + " Serum";

	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	public static String getUID(ItemStack item) {
		if (!item.hasTagCompound())
			return null;
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("uid"))
			return nbt.getString("uid");
		else
			return null;
	}

	public static EnumBeeChromosome getChromosome(ItemStack item) {
		if (!item.hasTagCompound())
			return null;
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("chromosome")) {
			int i = nbt.getInteger("chromosome");
			if (i >= EnumBeeChromosome.HUMIDITY.ordinal())
				i++;
			return EnumBeeChromosome.values()[i];
		}

		else
			return null;
	}
}
