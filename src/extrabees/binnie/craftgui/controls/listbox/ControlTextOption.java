package binnie.craftgui.controls.listbox;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlIndexed;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouseClick;

public class ControlTextOption<T> extends ControlOption<T> {

	public ControlTextOption(IWidget parent, float x, float y, float w, float h, T value) {
		super(parent, x, y, w, h, value);
		textWidget = new ControlText(this, w / 2, 1+(h - getRenderer().getTextHeight())/2, "",
				ControlText.Alignment.Center);
		if (value != null) {
			textWidget.setText(value.toString());
		} else {
			textWidget.setText("");
		}
	}

	protected ControlText textWidget = null;

	@Override
	public void onRenderBackground() {
		if(getValue()==null) return;
		int colour = 0xA0A0A0;
		if(getValue().equals(((IControlValue<T>)getParent()).getValue())) {
			colour = 0xFFFFFF;
			getRenderer().renderOutline(0, 0, (int) getSize().x, (int) getSize().y, 0xFFFFFFFF);
		}

		textWidget.setColour(colour);
	}

}
