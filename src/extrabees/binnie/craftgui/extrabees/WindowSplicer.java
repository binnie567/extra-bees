package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlLiquidTank;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntitySplicer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowSplicer extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntitySplicer machine;

	ControlSlot slotBee;
	ControlSlotArray slotReserve;

	ControlSlot slotBeeFinished;
	ControlSlotArray slotFinished;

	ControlSlot slotTemplate;

	ControlExtraBeeProgress splicerProgress;

	public WindowSplicer(EntityPlayer player, IInventory inventory, Side side) {
		super(264, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntitySplicer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowSplicer(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Splicer");

		playerInventory = new ControlPlayerInventory(this);

		splicerProgress = new ControlExtraBeeProgress(this, 77, 37, 74, 25, 0,
				132, 0, 157, ControlExtraBeeProgress.Direction.Right);

		slotBee = new ControlSlot(this, 63, 41);

		slotReserve = new ControlSlotArray(this, 11, 32, 2, 3);

		slotTemplate = new ControlSlot(this, 106, 72);

		slotBeeFinished = new ControlSlot(this, 147, 41);

		slotFinished = new ControlSlotArray(this, 173, 32, 2, 3);
		
		ControlLiquidTank tank = new ControlLiquidTank(this, 226, 29);
		tank.setTankID(0);
		
		slotBee.create(machine, TileEntitySplicer.SlotBee);
		slotReserve.create(machine, TileEntitySplicer.SlotReserve[0]);
		slotTemplate.create(machine, TileEntitySplicer.SlotTemplate);
		slotBeeFinished.create(machine, TileEntitySplicer.SlotBeeAfter);
		slotFinished.create(machine, TileEntitySplicer.SlotFinished[0]);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 17, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		new ControlErrorState(this, 64, 74);

	}

	@Override
	public void onUpdate() {
		splicerProgress.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
