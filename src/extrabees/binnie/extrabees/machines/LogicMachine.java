package binnie.extrabees.machines;

import java.util.Random;

import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;

public class LogicMachine {

	public static int getDNAAmount(ItemStack beeStack) {

		if (!BeeManager.beeInterface.isBee(beeStack))
			return 0;

		Random rand = new Random();

		int typeWorth = 0;
		if (BeeManager.beeInterface.isDrone(beeStack)) {
			typeWorth = 50;
		} else if (BeeManager.beeInterface.isMated(beeStack)) {
			typeWorth = 500;
		} else {
			typeWorth = 250;
		}

		if (BeeManager.beeInterface.getBee(beeStack).isAnalyzed()) {
			typeWorth *= 2;
		}

		return typeWorth;

	}

	public static float getSequencerAmount(ItemStack beeStack) {
		if (!BeeManager.beeInterface.isBee(beeStack))
			return 0.0f;

		IBee bee = BeeManager.beeInterface.getBee(beeStack);
		IBeeGenome genome = bee.getGenome();

		float typeWorth = 5.0f;
		if (BeeManager.beeInterface.isDrone(beeStack)) {
			typeWorth = 5.0f;
		} else if (BeeManager.beeInterface.isMated(beeStack)) {
			typeWorth = 25.0f;
		} else {
			typeWorth = 20.0f;
		}

		if (bee.isPureBred(EnumBeeChromosome.SPECIES)) {
			typeWorth *= 2.0f;
		}

		return typeWorth;

	}

	public static void spliceBee(ItemStack beeStack, IAlleleBeeSpecies species) {

		Random rand = new Random();

		IBee bee = BeeManager.beeInterface.getBee(beeStack);

		IBeeGenome genome = bee.getGenome();

		IChromosome[] chromosomes = genome.getChromosomes();

		IAllele[] template = BeeManager.breedingManager.getBeeTemplate(species
				.getUID());

		IBeeGenome newGenome = BeeManager.beeInterface.templateAsGenome(
				template, template);

		if (rand.nextInt(8) == 0)
			bee.setIsNatural(false);

		NBTTagCompound genomeNBT = new NBTTagCompound();
		newGenome.writeToNBT(genomeNBT);
		genome.readFromNBT(genomeNBT);

		NBTTagCompound beeNBT = new NBTTagCompound();
		bee.writeToNBT(beeNBT);
		beeStack.setTagCompound(beeNBT);

	}

}
