// Date: 7/16/2012 12:13:23 PM
// Template version 1.1
// Java generated by Techne
// Keep in mind that you still need to fill in some blanks
// - ZeuX

package binnie.extrabees.machines;

import net.minecraft.src.Entity;
import net.minecraft.src.ModelBase;
import net.minecraft.src.ModelRenderer;

public class ModelMachine extends ModelBase {
	// fields
	ModelRenderer Shape1;
	ModelRenderer Shape2;
	ModelRenderer Shape3;
	ModelRenderer Shape4;
	ModelRenderer Shape5;

	public ModelMachine() {
		textureWidth = 64;
		textureHeight = 64;

		Shape1 = new ModelRenderer(this, 0, 0);
		Shape1.addBox(0F, 0F, 0F, 2, 16, 2);
		Shape1.setRotationPoint(-8F, 8F, -8F);
		Shape1.setTextureSize(64, 64);
		Shape1.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		Shape2 = new ModelRenderer(this, 0, 0);
		Shape2.addBox(0F, 0F, 0F, 2, 16, 2);
		Shape2.setRotationPoint(-8F, 8F, 6F);
		Shape2.setTextureSize(64, 64);
		Shape2.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		Shape3 = new ModelRenderer(this, 0, 0);
		Shape3.addBox(0F, 0F, 0F, 2, 16, 2);
		Shape3.setRotationPoint(6F, 8F, -8F);
		Shape3.setTextureSize(64, 64);
		Shape3.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		Shape4 = new ModelRenderer(this, 0, 0);
		Shape4.addBox(0F, 0F, 0F, 2, 16, 2);
		Shape4.setRotationPoint(6F, 8F, 6F);
		Shape4.setTextureSize(64, 64);
		Shape4.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		Shape5 = new ModelRenderer(this, 8, 0);
		Shape5.addBox(0F, 0F, 0F, 14, 14, 14);
		Shape5.setRotationPoint(-7F, 9F, -7F);
		Shape5.setTextureSize(64, 64);
		Shape5.mirror = true;
		setRotation(Shape2, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3,
			float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Shape1.render(f5);
		Shape2.render(f5);
		Shape3.render(f5);
		Shape4.render(f5);
		Shape5.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void renderModel(float f1) {
		Shape1.render(f1);
		Shape2.render(f1);
		Shape3.render(f1);
		Shape4.render(f1);
		Shape5.render(f1);
	}

}
