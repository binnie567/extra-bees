package binnie.craftgui.extrabees.database;

import java.lang.reflect.Method;
import java.util.ArrayList;
import cpw.mods.fml.common.Side;

import binnie.craftgui.controls.ControlPages;
import binnie.craftgui.controls.ControlTabBar;
import binnie.craftgui.controls.button.ControlEnumButton;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.window.Panel;
import binnie.craftgui.window.Window;
import binnie.extrabees.genetics.ModuleGenetics;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IApiaristTracker;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.World;

public class WindowDictionary extends Window {

	public void changeMode(Mode mode) {
		if (mode == Mode.Branches) {
			speciesPages.hide();
			speciesTabs.hide();
			selectionBoxSpecies.hide();
			branchPages.show();
			branchTabs.show();
			selectionBoxBranches.show();
		} else {
			speciesPages.show();
			speciesTabs.show();
			selectionBoxSpecies.show();
			branchPages.hide();
			branchTabs.hide();
			selectionBoxBranches.hide();
		}
	}
	
	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if (event instanceof EventValueChanged && event.isOrigin(modeButton)) {

			Mode mode = ((EventValueChanged<Mode>) event).getValue();
			changeMode(mode);
			
		}

		if (event instanceof EventValueChanged
				&& event.isOrigin(selectionBoxSpecies)) {
			Species selection = ((EventValueChanged<Species>) event)
					.getValue();
			
			if (selection == null)
				speciesPages.hide();
			else
				this.callEvent(new EventSpeciesChanged(this, selection.getValue()));
		}

		if (event instanceof EventValueChanged
				&& event.isOrigin(selectionBoxBranches)) {
			Branch selection = ((EventValueChanged<Branch>) event)
					.getValue();
			if (selection == null)
				branchPages.hide();
			else
				this.callEvent(new EventBranchChanged(this, selection.getValue()));
		}

	}

	enum SpeciesTab {
		Overview(0x0000FF), Genome(0xFFFF00), Productivity(0x00FFFF), Climate(
				0xFF0000), ResultantMutations(0xFF00FF), FurtherMutations(
				0x00FF00), ;

		public int colour;

		SpeciesTab(int colour) {
			this.colour = colour;
		}
	}

	enum BranchesTab {
		Overview(0x0000FF), Species(0xFF0000),
		
		;
		
		public int colour;

		BranchesTab(int colour) {
			this.colour = colour;
		}
	}

	enum Mode {
		Species, Branches
	}

	// Wrapper class for IBranches and IAlleleBeeSpecies
	static class Species {
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof Species) {
				return ((Species) obj).value.getUID().equals(this.value.getUID());
			}
			return false;
		}

		IAlleleBeeSpecies value;

		Species(IAlleleBeeSpecies value) {
			this.value = value;
		}

		@Override
		public String toString() {
			if (value instanceof IAlleleSpecies)
				return value.getName();
			return super.toString();
		}

		public IAlleleBeeSpecies getValue() {
			return value;
		}
	}

	static class Branch {
		IBeeBranch value;

		Branch(IBeeBranch value) {
			this.value = value;
		}

		@Override
		public String toString() {
			if (value instanceof IBeeBranch)
				return value.getName();
			return super.toString();
		}

		public IBeeBranch getValue() {
			return value;
		}
	}

	boolean isNEI;
	
	public boolean isNEI() {
		return isNEI;
	}
	
	public WindowDictionary(EntityPlayer player, Side side, boolean nei) {
		super(280, 192, player, null, side);
		setColour(0xFFD900);
		isNEI = nei;
	}

	public static Window create(EntityPlayer player, Side side, boolean nei) {
		return new WindowDictionary(player, side, nei);
	}

	Panel panelInformation = null;
	Panel panelSearch = null;

	ControlPages<BranchesTab> branchPages = null;
	ControlPages<SpeciesTab> speciesPages = null;

	ControlTabBar<BranchesTab> branchTabs;
	ControlTabBar<SpeciesTab> speciesTabs;

	ControlEnumButton<Mode> modeButton = null;

	ControlListBox<Branch> selectionBoxBranches;
	ControlListBox<Species> selectionBoxSpecies;

	IApiaristTracker breedingManager;
	
	@Override
	public void initialize() {

		panelInformation = new Panel(this, 24, 8, 144, 176, Panel.Type.Tinted);
		panelInformation.setColour(0x0D2100);

		panelSearch = new Panel(this, 172, 32, 100, 151, Panel.Type.Tinted);
		panelSearch.setColour(0x0D2100);

		modeButton = new ControlEnumButton<Mode>(this, 172, 8, 100, 20,
				Mode.class);
		modeButton.setColour(0xFFD900);
		setupSpeciesView();
		setupBranchView();
		
		branchPages.hide();
		branchTabs.hide();
		selectionBoxBranches.hide();
		
		breedingManager = BeeManager.breedingManager.getApiaristTracker(getPlayer().worldObj);
		
		if(this.isServer()) {
			try {
				Class guiHandler = Class.forName("forestry.apiculture.GuiHandlerApiculture");
				Method method = guiHandler.getMethod("getServerGuiElement", int.class, EntityPlayer.class, 
						World.class, int.class, int.class, int.class);
				EntityPlayer player = getPlayer();
				method.invoke(guiHandler.newInstance(), 3, player, player.worldObj, (int)player.posX, (int)player.posY, (int)player.posZ);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void setupSpeciesView() {
		ArrayList<Species> species = new ArrayList<Species>();

			for (IAlleleBeeSpecies disc : ModuleGenetics.getDiscoveredBeeSpecies(getWorld(), isNEI())) {
				species.add(new Species(disc));
			}
		
		selectionBoxSpecies = new ControlListBox<Species>(panelSearch, 2,
				2, 95, 147);
		selectionBoxSpecies.setOptions(species);

		speciesTabs = new ControlTabBar<SpeciesTab>(this, 8, 8, 16, 176,
				SpeciesTab.class);
		speciesPages = new ControlPages<SpeciesTab>(panelInformation, 0, 0,
				160, 176, speciesTabs);

		for (SpeciesTab tab : SpeciesTab.values()) {
			speciesTabs.getTab(tab).setColour(tab.colour);
		}

		speciesPages.addPage(new PageSpeciesOverview(speciesPages),
				SpeciesTab.Overview);
		speciesPages.addPage(new PageSpeciesGenome(speciesPages),
				SpeciesTab.Genome);
		speciesPages.addPage(new PageSpeciesProducts(speciesPages),
				SpeciesTab.Productivity);
		speciesPages.addPage(new PageSpeciesClimate(speciesPages),
				SpeciesTab.Climate);
		speciesPages.addPage(new PageSpeciesResultant(speciesPages),
				SpeciesTab.ResultantMutations);
		speciesPages.addPage(new PageSpeciesMutations(speciesPages),
				SpeciesTab.FurtherMutations);
	}

	public void setupBranchView() {
		ArrayList<Branch> branches = new ArrayList<Branch>();

			for (IBeeBranch disc : ModuleGenetics.getDiscoveredBeeBranches(getWorld(), isNEI())) {
				branches.add(new Branch(disc));
			}
		
		
		selectionBoxBranches = new ControlListBox<Branch>(panelSearch, 2,
				2, 95, 147);
		selectionBoxBranches.setOptions(branches);

		branchTabs = new ControlTabBar<BranchesTab>(this, 8, 8, 16, 176,
				BranchesTab.class);
		branchPages = new ControlPages<BranchesTab>(panelInformation, 0, 0,
				160, 176, branchTabs);

		for (BranchesTab tab : BranchesTab.values()) {
			branchTabs.getTab(tab).setColour(tab.colour);
		}
		
		branchPages.addPage(new PageBranchOverview(branchPages),
				BranchesTab.Overview);
		branchPages.addPage(new PageBranchSpecies(branchPages),
				BranchesTab.Species);
	}

	public void gotoSpecies(IAlleleBeeSpecies value) {
		if(value!=null) {
			this.modeButton.setValue(Mode.Species);
			changeMode(Mode.Species);
			this.selectionBoxSpecies.setValue(new Species(value));
		}
		
	}

}
