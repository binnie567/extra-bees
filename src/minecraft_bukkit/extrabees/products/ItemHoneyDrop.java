package extrabees.products;

import extrabees.core.ExtraBeeItem;
import extrabees.liquids.ItemLiquid;
import forestry.api.liquids.LiquidStack;
import forestry.api.recipes.ISqueezerManager;
import forestry.api.recipes.RecipeManagers;
import forge.ITextureProvider;
import java.util.ArrayList;
import net.minecraft.server.*;
import net.minecraft.server.Block;
import net.minecraft.server.ItemStack;
import extrabees.products.ItemHoneyDrop.EnumType;

public class ItemHoneyDrop extends Item implements ITextureProvider
{
	public static enum EnumType
    {
        ENERGY,
        ACID,
        POISON,

        ;

        public static EnumType getTypeFromID(int id)
        {
            return EnumType.class.getEnumConstants()[id];
        }

        public String name;
        public int primaryColor;
        public int secondaryColor;
        public boolean isSecret = false;
        int time;
        LiquidStack liquid;
        ItemStack remenants;
        int chance;

        EnumType()
        {
            this.name = "??? Drop";
            this.primaryColor = 0xFFFFFF;
            this.secondaryColor = 0xFFFFFF;
            this.isSecret = false;
            this.time = 10;
            this.chance = 0;
            this.remenants = new ItemStack(Block.DIRT, 1, 0);
            liquid = null;
        }

        public void init(String name, int primaryColor, int secondaryColor)
        {
            this.name = name;
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
        }

        public EnumType setIsSecret()
        {
            this.isSecret = true;
            return this;
        }

        public void addLiquid(LiquidStack item)
        {
            liquid = item;
        }

        public void addRemenants(ItemStack item, int chance)
        {
            this.remenants = item;
            this.chance = chance;
        }

        public void addRecipe()
        {
            if (liquid != null)
            {
                RecipeManagers.squeezerManager.addRecipe(time,
                new ItemStack[] { new ItemStack(ExtraBeeItem.honeyDrop, 1, this.ordinal()) },
                liquid, remenants, this.chance);
            }
        }
    }

    public ItemHoneyDrop(int i)
    {
        super(i);
        maxStackSize = 64;
        setMaxDurability(0);
        a(true);
    }

    public static void addSubtypes()
    {
        EnumType.ENERGY.init("Energy Drop", 0x9c4972, 0xe37171);
        EnumType.ACID.init("Acid Drop", 0x4bb541, 0x49de3c);
        EnumType.ACID.addLiquid(new LiquidStack(ItemLiquid.liquids[extrabees.liquids.ItemLiquid.EnumType.ACID.ordinal()].id, 200, 0));
        EnumType.POISON.init("Venom Drop", 0xd106b9, 0xff03e2);
        EnumType.POISON.addLiquid(new LiquidStack(ItemLiquid.liquids[extrabees.liquids.ItemLiquid.EnumType.POISON.ordinal()].id, 200, 0));
    }

    public boolean g()
    {
        return false;
    }

    public boolean isRepairable()
    {
        return false;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return EnumType.getTypeFromID(itemstack.getData()).name;
    }

    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }

    public int getColorFromDamage(int i, int j)
    {
        if (j == 0)
        {
            return EnumType.getTypeFromID(i).primaryColor;
        }
        else
        {
            return EnumType.getTypeFromID(i).secondaryColor;
        }
    }

    public int func_46057_a(int i, int j)
    {
        return j <= 0 ? 109 : 110;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        EnumType aenumtype[] = EnumType.values();
        int i = aenumtype.length;

        for (int j = 0; j < i; j++)
        {
            EnumType enumtype = aenumtype[j];

            if (!enumtype.isSecret)
            {
                arraylist.add(new ItemStack(this, 1, enumtype.ordinal()));
            }
        }
    }

    public String getTextureFile()
    {
        return "/gfx/forestry/items/items.png";
    }

    public static ItemStack getItem(EnumType enumtype)
    {
        return new ItemStack(ExtraBeeItem.honeyDrop, 1, enumtype.ordinal());
    }
}
