package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.window.Window;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowIndustrialAlveary extends Window {

	public WindowIndustrialAlveary(EntityPlayer player, IInventory inventory,
			Side side) {
		super(320, 240, player, inventory, side);
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowIndustrialAlveary(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Automatic Breeder");
	}

};
