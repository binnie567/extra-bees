package binnie.extrabees.gen;

import net.minecraft.src.MapColor;
import net.minecraft.src.Material;

public class MaterialBeehive extends Material {
	public MaterialBeehive() {
		super(MapColor.stoneColor);
		setRequiresTool();
		setImmovableMobility();
	}

	@Override
	public boolean isOpaque() {
		return true;
	}
}