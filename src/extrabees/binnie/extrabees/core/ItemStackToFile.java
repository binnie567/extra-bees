package binnie.extrabees.core;

/********************************************************
 *														*
 *    Overlords Tutorial - 01							*
 *     http://www.flashbang.se							*
 *             2006                  					*
 *                                   					*
 *    ported by kappaOne 								*
 *    from http://www.flashbang.se/archives/48			*                           
 *********************************************************/

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.RenderHelper;
import net.minecraft.src.RenderItem;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.GL14;
import org.lwjgl.util.glu.GLU;

import binnie.core.BinnieCore;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

public class ItemStackToFile {

	static float angle;

	static int colorTextureID;
	static int framebufferID;
	static int depthRenderBufferID;

	/** time at last frame */
	static long lastFrame;

	/** frames per second */
	static int fps;
	/** last fps time */
	static long lastFPS;

	/** is VSync Enabled */
	static boolean vsyncEnabled;

	public static void start() {

		
	}
	
	public static void renderToFile(ItemStack stack) {
		initGL(); // init OpenGL

		renderGL(stack);

		save(stack);
	}

	public static void initGL() {

		glViewport(0, 0, 512, 512); // Reset The Current Viewport
		glMatrixMode(GL_PROJECTION); // Select The Projection Matrix
		glLoadIdentity(); // Reset The Projection Matrix
		GLU.gluPerspective(45.0f, 512f / 512f, 1.0f, 100.0f); // Calculate The
																// Aspect Ratio
																// Of The Window
		glMatrixMode(GL_MODELVIEW); // Select The Modelview Matrix
		glLoadIdentity(); // Reset The Modelview Matrix

		// Start Of User Initialization
		angle = 0.0f; // Set Starting Angle To Zero

		//glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
		glClearDepth(1.0f); // Depth Buffer Setup
		glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing (Less Or Equal)
		glEnable(GL_DEPTH_TEST); // Enable Depth Testing
		glShadeModel(GL_SMOOTH); // Select Smooth Shading
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Set Perspective
															// Calculations To
															// Most Accurate

		// check if GL_EXT_framebuffer_object can be use on this system
		if (!GLContext.getCapabilities().GL_EXT_framebuffer_object) {
			System.exit(0);
		} else {

			// init our fbo
			
			glEnable(GL_BLEND);             
		    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			framebufferID = glGenFramebuffersEXT(); // create a new framebuffer
			colorTextureID = glGenTextures(); // and a new texture used as a
												// color buffer
			depthRenderBufferID = glGenRenderbuffersEXT(); // And finally a new
															// depthbuffer

			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID); // switch
																		// to
																		// the
																		// new
																		// framebuffer

			// initialize color texture
			glBindTexture(GL_TEXTURE_2D, colorTextureID); // Bind the
															// colorbuffer
															// texture
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // make
																				// it
																				// linear
																				// filterd
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 512, 512, 0, GL_RGBA,
					GL_INT, (java.nio.ByteBuffer) null); // Create the texture
															// data
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
					GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, colorTextureID, 0); // attach
																					// it
																					// to
																					// the
																					// framebuffer

			// initialize depth renderbuffer
			glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthRenderBufferID); // bind
																				// the
																				// depth
																				// renderbuffer
			glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
					GL14.GL_DEPTH_COMPONENT32, 512, 512); // get the data space
															// for it
			glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
					GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT,
					depthRenderBufferID); // bind it to the renderbuffer

			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); // Swithch back to
															// normal
															// framebuffer
															// rendering

		}

	}
	
	static int transparentColour = 0xDD11DD;
	static int transparentColourAlpha = 0xFFDC11DC;
	public static void renderGL(ItemStack stack) {

		// FBO render pass

		glViewport(0, 0, 512, 512); // set The Current Viewport to the fbo size

		glBindTexture(GL_TEXTURE_2D, 0); // unlink textures because if we dont
											// it all is gonna fail
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID); // switch to
																	// rendering
																	// on our
																	// FBO

		// Normal render pass, draw cube with texture

		glEnable(GL_TEXTURE_2D); // enable texturing
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); // switch to rendering on
														// the framebuffer

		int r = (transparentColour & 0xFF0000) >> 16;
		int g = (transparentColour & 0xFF00) >> 8;
		int b = (transparentColour & 0xFF);
		
		glClearColor(((float)r)/256f, ((float)g)/256f, ((float)b)/256f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear Screen And
															// Depth Buffer on
															// the framebuffer
															// to black

		glEnable(GL_BLEND);             
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glBindTexture(GL_TEXTURE_2D, colorTextureID); // bind our FBO texture

		glViewport(0, 0, 512, 512); // set The Current Viewport

		glLoadIdentity(); // Reset The Modelview Matrix
		glTranslatef(0.0f, 0.0f, -6.0f); // Translate 6 Units Into The Screen
											// and then rotate
		glRotatef(angle, 0.0f, 1.0f, 0.0f);
		glRotatef(angle, 1.0f, 0.0f, 0.0f);
		glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glColor3f(1, 1, 1); // set the color to white
		// draw the box

		

		Minecraft mc = BinnieCore.proxy.getMinecraftInstance();

		mc.renderEngine.bindTexture(mc.renderEngine.getTexture(stack.getItem()
				.getTextureFile()));
		
		
		/*
		 * glBindTexture(GL_TEXTURE_2D, colorTextureID);
		 * 
		 * RenderItem itemRenderer = new RenderItem();
		 * 
		 * RenderHelper.enableGUIStandardItemLighting();
		 * GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		 * 
		 * 
		 * itemRenderer.renderItemAndEffectIntoGUI(null, mc.renderEngine, item,
		 * 25, 24); itemRenderer.renderItemOverlayIntoGUI(null, mc.renderEngine,
		 * item, 0, 0);
		 */

		// drawBox();

		

		glBegin(GL_QUADS);
		// Front Face

		int passes = stack.getItem().getRenderPasses(stack.getItemDamage());
		
		for(int p = 0; p < passes; p++)
			renderLayer(stack, p);
		
		// Back Face
		glEnd();

		glDisable(GL_TEXTURE_2D);
		glFlush();

	}
	
	public static void setColour(int hex) {

		int r = (hex & 0xFF0000) >> 16;
		int g = (hex & 0xFF00) >> 8;
		int b = (hex & 0xFF);

		GL11.glColor3f((r) / 255.0f, (g) / 255.0f, (b) / 255.0f);

	}
	
	public static void renderLayer(ItemStack stack, int renderPass) {
		
		int index = stack.getItem().getIconIndex(stack, renderPass, null, null, 0);
		int colour = stack.getItem().getColorFromItemStack(stack, renderPass);
		
		setColour(colour);
		
		float w = 2.0645f;
		float h = 2.0645f;
		float oy = 0f;

		float x = ((float) (index % 16)) / 16.0f;
		float y = ((float) (index / 16)) / 16.0f;

		glTexCoord2f(x, y);
		glVertex3f(-w / 2, oy -h / 2, 1.0f); // Bottom Left Of The Texture and Quad
		glTexCoord2f(x + 0.0625f, y);
		glVertex3f(w / 2, oy -h / 2, 1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(x + 0.0625f, y + 0.0625f);
		glVertex3f(w / 2, oy + h / 2, 1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(x, y + 0.0625f);
		glVertex3f(-w / 2, oy + h / 2, 1.0f); // Top Left Of The Texture and Quad
	}

	public static void drawBox() {
		// this func just draws a perfectly normal box with some texture
		// coordinates
		glBegin(GL_QUADS);
		// Front Face
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); // Top Left Of The Texture and Quad
		// Back Face
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f); // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Left Of The Texture and Quad
		// Top Face
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Texture and Quad
		// Bottom Face
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f); // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Right Of The Texture and Quad
		// Right face
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Left Of The Texture and Quad
		// Left Face
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Texture and Quad
		glEnd();
	}

	private static void save(ItemStack stack) {
		glReadBuffer(framebufferID);
		int width = 256;
		int height = 256;
		int bpp = 4; // Assuming a 32-bit display with a byte each for red,
						// green, blue, and alpha.
		ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
		GL11.glReadPixels(128, 128, width, height, GL11.GL_RGBA,
				GL11.GL_UNSIGNED_BYTE, buffer);

		String filename = stack.getDisplayName();
		filename.replaceAll(" ", "");
		
		File file = new File("items/"+filename+".png"); // The file to save to.
		String format = "PNG"; // Example: "PNG" or "JPG"
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int i = (x + (width * y)) * bpp;
				int r = buffer.get(i) & 0xFF;
				int g = buffer.get(i + 1) & 0xFF;
				int b = buffer.get(i + 2) & 0xFF;
				int a = buffer.get(i + 3) & 0xFF;
				// image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16)
				//		| (g << 8) | b);
				int colour = (a << 24) | (r << 16)
						| (g << 8) | b;
				
				if(colour==transparentColourAlpha)
					image.setRGB(x, y, 0x00000000);
				else
					image.setRGB(x, y, colour);
			}
		}

		try {
			ImageIO.write(image, format, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}