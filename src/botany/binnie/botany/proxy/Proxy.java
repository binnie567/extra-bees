package binnie.botany.proxy;

import binnie.core.BinnieCore;
import binnie.core.network.BinniePacket;
import binnie.core.proxy.BinnieProxy;
import net.minecraft.src.Botany;
import net.minecraft.src.EntityPlayer;

public class Proxy implements IBotanyProxy {

	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z) {
		BinnieCore.proxy.sendNetworkPacket(packet, Botany.channel, x, y, z);
	}

	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer) {
		BinnieCore.proxy.sendToPlayer(packet, Botany.channel, entityplayer);
	}

	public void sendToServer(BinniePacket packet) {
		BinnieCore.proxy.sendToServer(packet, Botany.channel);
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
	}

}
