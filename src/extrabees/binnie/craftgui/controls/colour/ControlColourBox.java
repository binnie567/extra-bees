package binnie.craftgui.controls.colour;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.window.Panel;

public class ControlColourBox extends Control {

	@Override
	public void onRenderForeground() {
		super.onRenderForeground();
		getRenderer().renderSolidRect(1, 1, (int) getSize().x - 2,
				(int) getSize().x - 2, colour);
	}

	protected ControlColourBox(IWidget parent, int x, int y, int width,
			int height) {
		super(parent, x, y, width, height);
		Panel panel = new Panel(this, 0, 0, width, height, Panel.Type.Gray);
	}

	int colour = 0xFFFFFF;

}
