package binnie.extrabees.genetics;

import java.util.List;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemAdvancedTemplateBlank extends Item {

	public ItemAdvancedTemplateBlank(int id) {
		super(id);
		setMaxStackSize(16);
		setIconIndex(35);
		setMaxDamage(1);
		setTextureFile(ExtraBeeTexture.Engineering.getTexture());
		setCreativeTab(CreativeTabs.tabMisc);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return "Blank Template";
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
	}

}
