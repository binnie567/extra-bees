<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  
  <xsl:for-each select="mutations/mutation">
  
  <p>

    <xsl:value-of select="species-0"/>
     + 
    <xsl:value-of select="species-1"/>
     -- 
     <xsl:value-of select="basechance"/>%
     --> 
    <xsl:value-of select="species-result"/>
    </p>
  
  
  
  </xsl:for-each>
  
  
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>