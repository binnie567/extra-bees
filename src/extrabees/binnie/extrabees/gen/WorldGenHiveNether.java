package binnie.extrabees.gen;

import java.util.Random;

import binnie.extrabees.core.ExtraBeeBlock;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.Block;
import net.minecraft.src.World;
import net.minecraft.src.WorldGenerator;

public class WorldGenHiveNether extends WorldGenerator {
	@Override
	public boolean generate(World world, Random random, int i, int j, int k) {
		BiomeGenBase biome = world.getWorldChunkManager().getBiomeGenAt(i, k);

		if (biome.biomeID != BiomeGenBase.hell.biomeID) {
			return true;
		}

		int i1 = i;
		int j1 = j;
		int k1 = k;

		if (embedInWall(world, Block.netherrack.blockID, i1, j1, k1)) {
			world.setBlockAndMetadata(i1, j1, k1, ExtraBeeBlock.hive.blockID, 2);
		}

		return true;
	}

	public boolean embedInWall(World world, int blockID, int i, int j, int k) {
		return ((world.getBlockId(i, j, k) == blockID)
				&& (world.getBlockId(i, j + 1, k) == blockID)
				&& (world.getBlockId(i, j - 1, k) == blockID) && ((world
				.isAirBlock(i + 1, j, k))
				|| (world.isAirBlock(i - 1, j, k))
				|| (world.isAirBlock(i, j, k + 1)) || (world.isAirBlock(i, j,
				k - 1))));
	}
}
