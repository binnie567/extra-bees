package binnie.extrabees.gen;

import java.util.Random;

import binnie.extrabees.core.ExtraBeeBlock;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.Block;
import net.minecraft.src.Material;
import net.minecraft.src.World;
import net.minecraft.src.WorldGenerator;

public class WorldGenHiveWater extends WorldGenerator {
	@Override
	public boolean generate(World world, Random random, int i, int j, int k) {
		BiomeGenBase biome = world.getWorldChunkManager().getBiomeGenAt(i, k);
		int i1 = i;
		int j1 = j;
		int k1 = k;

		if ((world.getBlockId(i1, j1, k1) == Block.waterStill.blockID)
				&& (world.getBlockId(i1, j1, k1) == Block.waterStill.blockID)
				&& ((world.getBlockMaterial(i1, j1 - 1, k1) == Material.sand)
						|| (world.getBlockMaterial(i1, j1 - 1, k1) == Material.clay)
						|| (world.getBlockMaterial(i1, j1 - 1, k1) == Material.ground) || (world
						.getBlockMaterial(i1, j1 - 1, k1) == Material.rock))) {
			world.setBlockAndMetadata(i1, j1, k1, ExtraBeeBlock.hive.blockID, 0);
		}

		return true;
	}
}
