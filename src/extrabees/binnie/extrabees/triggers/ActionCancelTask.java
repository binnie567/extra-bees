package binnie.extrabees.triggers;

public class ActionCancelTask extends ExtraBeeAction {

	@Override
	public int getIndexInTexture() {
		return 49;
	}

	@Override
	public String getDescription() {
		return "Cancel Task";
	}

}