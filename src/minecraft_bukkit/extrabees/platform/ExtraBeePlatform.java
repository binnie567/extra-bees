package extrabees.platform;

import extrabees.liquids.ItemLiquid;
import java.io.File;
import net.minecraft.server.*;

public class ExtraBeePlatform
{
    public ExtraBeePlatform()
    {
    }

    public static boolean isClient()
    {
        return false;
    }

    public static boolean isServer()
    {
        return false;
    }

    public static boolean isBukkit()
    {
        return true;
    }

    public static File getDirectory()
    {
        return new File("./");
    }

    public static void preInit()
    {
    }

    public static void doInit()
    {
    }

    public static void postInit()
    {
    }

    public static void addLiquidFX(extrabees.liquids.ItemLiquid.EnumType enumtype)
    {
    }

    public static void openBeeDictionary(ItemStack itemstack, World world, EntityHuman entityhuman)
    {
    }
}
