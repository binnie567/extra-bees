package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventValueChanged<T> extends Event {

	public EventValueChanged(IWidget origin, T value) {
		super(origin);
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public T value;

}
