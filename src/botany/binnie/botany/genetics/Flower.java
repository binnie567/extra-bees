package binnie.botany.genetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import binnie.botany.core.BotanyCore;

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.World;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IIndividual;
import forestry.apiculture.genetics.Bee;
import forestry.apiculture.genetics.BeeGenome;
import forestry.apiculture.genetics.BeeTemplates;
import forestry.core.genetics.Chromosome;
import forestry.core.genetics.Individual;

public class Flower extends Individual implements IFlower {

	public IFlowerGenome genome;
	public IFlowerGenome mate;

	public Flower(NBTTagCompound nbttagcompound) {
		readFromNBT(nbttagcompound);
	}

	public Flower(IFlowerGenome genome, int age) {

		this.genome = genome;

		this.age = age;

		this.isNatural = true;
		this.generation = 0;
	}

	@Override
	public String getDisplayName() {
		IAlleleFlowerSpecies species = getFlowerGenome().getPrimaryAsFlower();
		IAlleleFlowerStem stem = getFlowerGenome().getStem();

		String name = "";

		if (species != null)
			name += species.getName();

		if (age == 0)
			name += " Seed";

		return name;
	}

	@Override
	public void addTooltip(List<String> list) {
	}

	@Override
	public IGenome getGenome() {
		return genome;
	}

	@Override
	public String getIdent() {
		return null;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {

		super.readFromNBT(nbttagcompound);

		if (nbttagcompound == null) {
			this.genome = BotanyCore.flowerInterface.getDefaultFlowerGenome();
			return;
		}
		age = nbttagcompound.getInteger("Age");

		if (nbttagcompound.hasKey("Genome")) {
			genome = new FlowerGenome(nbttagcompound.getCompoundTag("Genome"));
		}

		if (nbttagcompound.hasKey("Mate"))
			mate = new FlowerGenome(nbttagcompound.getCompoundTag("Mate"));

	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {

		super.writeToNBT(nbttagcompound);

		nbttagcompound.setInteger("Age", age);

		if (genome != null) {
			NBTTagCompound NBTmachine = new NBTTagCompound();
			genome.writeToNBT(NBTmachine);
			nbttagcompound.setTag("Genome", NBTmachine);
		}
		if (mate != null) {
			NBTTagCompound NBTmachine = new NBTTagCompound();
			mate.writeToNBT(NBTmachine);
			nbttagcompound.setTag("Mate", NBTmachine);
		}

	}

	@Override
	public int getDamage() {

		return getAge() + getFlowerGenome().getStem().getIconIndex() * 1024
				+ getFlowerGenome().getPrimaryAsFlower().getPrimaryIconIndex()
				* 16;
	}

	@Override
	public IFlowerGenome getFlowerGenome() {
		return (IFlowerGenome) getGenome();
	}

	int age = 1;

	final int MAX_AGE = 6;

	@Override
	public void age() {
		age += 1;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public boolean isAlive() {
		return age < MAX_AGE;
	}

	@Override
	public IFlower getOffspring(World world) {
		return this;
		/*
		 * if(this.mate==null) return null; else { IChromosome[] chromosomes =
		 * new IChromosome[genome.getChromosomes().length]; IChromosome[]
		 * parent1 = genome.getChromosomes(); IChromosome[] parent2 =
		 * mate.getChromosomes();
		 * 
		 * for (int i = 0; i < parent1.length; i++) if (parent1[i] != null &&
		 * parent2[i] != null) chromosomes[i] = inheritChromosome(world.rand,
		 * parent1[i], parent2[i]);
		 * 
		 * mate = null;
		 * 
		 * return new Flower(new FlowerGenome(chromosomes), 0); }
		 */
	}

	private IChromosome inheritChromosome(Random rand, IChromosome parent1,
			IChromosome parent2) {

		IAllele choice1;
		if (rand.nextBoolean())
			choice1 = parent1.getPrimaryAllele();
		else
			choice1 = parent1.getSecondaryAllele();

		IAllele choice2;
		if (rand.nextBoolean())
			choice2 = parent2.getPrimaryAllele();
		else
			choice2 = parent2.getSecondaryAllele();

		if (rand.nextBoolean())
			return new Chromosome(choice1, choice2);
		else
			return new Chromosome(choice2, choice1);
	}

	@Override
	public void mate(IFlowerGenome genome) {
		this.mate = genome;
	}

	@Override
	public boolean isPollinated() {
		return mate != null;
	}

	@Override
	public IFlower die(World world) {

		if (mate == null && world.rand.nextBoolean())
			mate = genome;

		return getOffspring(world);

	}

	@Override
	public void setAge(int age) {
		this.age = age;
	}

}
