package binnie.logicpipes;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;

public class ControlTrigger extends Control implements ITooltip {

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/gui-logicpipes.png";
	}

	ILogicTrigger trigger;

	public ILogicTrigger getTrigger() {
		return trigger;
	}

	public void setTrigger(ILogicTrigger trigger) {
		this.trigger = trigger;
	}

	@Override
	public void onRenderBackground() {
		int offset = 0;
		if(trigger instanceof ILogicTrigger)
			offset = 18;
		getRenderer().renderRect(0, 0, 18, 18, offset, 0);

		if (getSuperParent().getMousedOverWidget() == this) {

			//getRenderer().renderGradientRect(1, 1, 16, 16, -2130706433,
			//		-2130706433);
		}

	}

	@Override
	public void onRenderForeground() {

		if (trigger != null) {
			getRenderer().setTexture(trigger.getTextureFile());
			int index = trigger.getIndexInTexture();
			getRenderer().renderRect(1, 1, 16, 16, index % 16, index / 16);
		}

	}

	protected ControlTrigger(IWidget parent, float x, float y, ILogicTrigger trigger) {
		super(parent, x, y, 18, 18);
		this.canMouseOver = true;
		this.trigger = trigger;
	}

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();
		if (trigger != null) {
			tooltip.add(trigger.getDescription());
			tooltip.addAll(trigger.getTooltip());
		}

		return tooltip;
	}

}
