package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityIsolator;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowIsolator extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityIsolator machine;

	ControlSlot slotBee;

	ControlSlot slotVial;
	ControlSlotArray slotVials;

	ControlExtraBeeProgress progessWidget;

	public WindowIsolator(EntityPlayer player, IInventory inventory, Side side) {
		super(320, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityIsolator) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowIsolator(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Isolator");

		playerInventory = new ControlPlayerInventory(this);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		slotBee = new ControlSlot(this, 62, 58);

		slotVial = new ControlSlot(this, 248 - 26, 58);

		slotVials = new ControlSlotArray(this, 248, 40, 3, 3);

		progessWidget = new ControlExtraBeeProgress(this, 80, 58, 142, 17, 0,
				218, 0, 201, ControlProgress.Direction.Right);

		slotBee.create(machine, TileEntityIsolator.SlotBee);

		slotVial.create(machine, TileEntityIsolator.SlotVials);
		slotVials.create(machine, TileEntityIsolator.SlotVialsDone[0]);

		new ControlErrorState(this, 144, 82);
	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
