package binnie.craftgui.controls.listbox;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlIndexed;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventValueChanged;

public abstract class ControlAbstractOptionSet<T> extends Control implements IControlValue<T>, IControlIndexed {

	Map<T, IWidget> options = new LinkedHashMap<T, IWidget>();
	
	int index = -1;
	T value = null;
	
	public final int getIndex() {
		return index;
	}

	public final void setIndex(int index) {
		if(this.index != index) {
			this.index = index;
			if(getIndex()>=0&&getIndex()<=options.size()) {
				int i = 0;
				for(T value : options.keySet()) {
					if(i==getIndex()) {
						this.value = value;
					}
					i++;
				}
			}
			else {
				value = null;
				index = -1;
			}
			callEvent(new EventValueChanged<T>(getParent(), value));
		}
	}
	
	public final void setValue(T value) {
		if(value==null) {
			setIndex(-1);
			return;
		}
		Iterator<T> iter = options.keySet().iterator();
		int i = 0;
		while(iter.hasNext()) {
			if(value.equals(iter.next())) {
				setIndex(i);
				return;
			}
			i++;
		}
		setIndex(-1);
	}
	
	public final T getValue() {
		return value;
	}
	
	public final void setOptions(List<T> options) {
		int width = 0;
		int height = 0;
		for(IWidget option : this.options.values()) {
			this.deleteChild(option);
		}
		this.options.clear();
		int i = 0;
		for(T option : options) {
			IWidget optionWidget = controlListBox.createOption(i, height, option);
			if(optionWidget != null) {
				height += optionWidget.getSize().y;
				width = Math.max((int)optionWidget.getSize().y, width);
				this.options.put(option, optionWidget);
			}
			i++;
		}
		setValue(getValue());
		setSize(width, height);
	}
	
	public Set<T> getOptions() {
		return options.keySet();
	}

	
	ControlAbstractListBox<T> controlListBox;

	public ControlAbstractOptionSet(ControlAbstractListBox parent, float x, float y, float w) {
		super(parent, x, y, w, 0);
		controlListBox = parent;
	}

	

}
