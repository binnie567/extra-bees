package binnie.extrabees.machines;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.fx.EntityGenepoolFX;
import binnie.extrabees.fx.EntitySplicerFX;
import net.minecraft.src.World;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityGenepool extends TileEntityMachine implements
		IPowerReceptor, ITankContainer {

	public static final int[] SlotReserve = new int[] { 4, 5, 6, 7, 8, 9 };
	public static final int SlotBee = 2;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Genepool.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Genepool;
	}

	public TileEntityGenepool() {

		super(1000, 200, 2000);

		addSlot(SlotBee);
		addSlotArray(SlotReserve);

		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		for (int slot : SlotReserve) {
			getSlot(slot).setValidator(
					new SlotValidatorBee(SlotValidatorBee.Mode.Bee));
		}

		this.addTank(0, new LiquidTank(2000));
	}

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		if (getSlot(SlotBee).getItemStack() == null) {
			for (int i : SlotReserve) {
				if (getSlot(i).getItemStack() != null) {
					setInventorySlotContents(SlotBee, getSlot(i).getItemStack());
					setInventorySlotContents(i, null);
					break;
				}
			}
		}
	}

	@Override
	public void onFinishTask() {
		if (LogicMachine.getDNAAmount(getSlot(SlotBee).getItemStack()) > 0.0) {
			this.fill(
					0,
					new LiquidStack(ExtraBeeItem.liquidDNA, LogicMachine
							.getDNAAmount(getSlot(SlotBee).getItemStack())),
					true);
		}

		setInventorySlotContents(SlotBee, null);
	}

	@Override
	public ErrorState canWork() {
		if (this.getStackInSlot(SlotBee) == null)
			return new ErrorState("No Bee");

		return super.canWork();
	}

}
