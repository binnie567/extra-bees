package binnie.extrabees.liquids;

import binnie.extrabees.config.Config;
import binnie.extrabees.config.ConfigMain;
import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.ItemStack;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public class ModuleLiquids {

	public static void injectLiquidContainer(LiquidContainerData container) {
		injectLiquidContainer(container, null, 0);
	}

	public static void injectWaxContainer(LiquidContainerData container) {
		injectLiquidContainer(container, ItemInterface.getItem("beeswax"), 10);
	}

	public static void injectRefractoryContainer(LiquidContainerData container) {
		injectLiquidContainer(container,
				ItemInterface.getItem("refractoryWax"), 10);
	}

	public static void injectTinContainer(LiquidContainerData container) {
		injectLiquidContainer(container, ItemInterface.getItem("ingotTin"), 5);
	}

	public static void injectLiquidContainer(LiquidContainerData container,
			ItemStack remnant, int chance) {
		LiquidContainerRegistry.registerLiquid(container);

		if (RecipeManagers.squeezerManager != null) {
			if (!container.container.getItem().hasContainerItem())
				if (remnant != null)
					RecipeManagers.squeezerManager.addRecipe(10,
							new ItemStack[] { container.filled },
							container.stillLiquid, remnant, chance);
				else
					RecipeManagers.squeezerManager.addRecipe(10,
							new ItemStack[] { container.filled },
							container.stillLiquid);
		}

		if (RecipeManagers.bottlerManager != null)
			RecipeManagers.bottlerManager.addRecipe(5, container.stillLiquid,
					container.container, container.filled);
	}
	
	public static void doInit() {
		ExtraBeeItem.moltenLiquid = new ItemMoltenMetal(Config.Main.getItemID("moltenLiquid", ExtraBeeItem.moltenLiquidID));
		ExtraBeeItem.moltenCast = new ItemCast(Config.Main.getItemID("moltenCast", ExtraBeeItem.moltenCastID));
		ItemMoltenMetal.setupMoltenLiquids();
		ItemCast.setupRecipes();
	}

}
