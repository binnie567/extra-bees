package binnie.extrabees.machines;

import net.minecraft.src.ItemStack;

public abstract class SlotValidator {

	public abstract boolean isValid(ItemStack itemStack);

}
