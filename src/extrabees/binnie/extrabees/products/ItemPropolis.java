package binnie.extrabees.products;

import java.util.List;

import denoflionsx.API.PfFEvents;
import denoflionsx.API.Annotations.PfFEventTypes;
import denoflionsx.API.Annotations.PfFSubscribe;
import denoflionsx.API.Events.EventFuelCreated;

import railcraft.common.api.core.items.ItemRegistry;

import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraftforge.liquids.LiquidStack;
import binnie.extrabees.core.ExtraBeeCore;
import binnie.extrabees.core.ExtraBeeExtMod;
import binnie.extrabees.core.ExtraBeeItem;
//import net.minecraft.src.railcraft.common.api.ItemRegistry;
import forestry.api.core.ItemInterface;
import forestry.api.recipes.RecipeManagers;

public class ItemPropolis extends Item {
	public ItemPropolis(int i) {
		super(i);
		maxStackSize = 64;
		setMaxDamage(0);
		setHasSubtypes(true);
		setIconIndex(98);
		setTextureFile("/gfx/forestry/items/items.png");
		setCreativeTab(CreativeTabs.tabMaterials);
		if (PfFEvents.fuelEvent != null)
			PfFEvents.fuelEvent.register(this);
	}

	public static void addSubtypes() {
		EnumType.WATER.init("Watery Propolis", 0x24B3C9, 0xC2BEA7);
		EnumType.WATER.addLiquid(new LiquidStack(Block.waterStill, 500));
		EnumType.OIL.init("Oily Propolis", 0x172F33, 0xC2BEA7);

		if (ExtraBeeCore.modBuildcraft && (ExtraBeeExtMod.bcOil != null)) {
			EnumType.OIL.addLiquid(new LiquidStack((new ItemStack(
					ExtraBeeExtMod.bcOil, 1, 0)).getItem(), 500));
		}

		EnumType.FUEL.init("Petroleum Propolis", 0xA38D12, 0xC2BEA7);
		EnumType.FUEL.addLiquid(new LiquidStack(ItemInterface.getItem(
				"liquidBiofuel").getItem(), 500));

		EnumType.MILK.init("Milky Propolis", 0xFFFFFF, 0xC2BEA7);
		EnumType.MILK.deprecate();
		// EnumType.MILK.addLiquid(new LiquidStack(ItemInterface.getItem(
		// "liquidMilk").getItem(), 500));

		EnumType.FRUIT.init("Fruity Propolis", 0xDE2A2A, 0xC2BEA7);
		EnumType.FRUIT.deprecate();
		// EnumType.FRUIT.addLiquid(new LiquidStack(ItemInterface.getItem(
		// "liquidJuice").getItem(), 500));
		// EnumType.FRUIT.addRemenants(ItemInterface.getItem("mulch"), 25);

		EnumType.SEED.init("Seedy Propolis", 0x6AD660, 0xC2BEA7);
		EnumType.SEED.deprecate();
		// EnumType.SEED.addLiquid(new LiquidStack(ItemInterface.getItem(
		// "liquidSeedOil").getItem(), 500));
		// EnumType.SEED.addRemenants(new ItemStack(Item.seeds, 1, 0), 25);

		EnumType.ALCOHOL.init("Alcoholic Propolis", 0x418521, 0xC2BEA7);
		EnumType.ALCOHOL.deprecate();
		// EnumType.ALCOHOL.addLiquid(new LiquidStack(ItemInterface.getItem(
		// "liquidMead").getItem(), 500));

		EnumType.CREOSOTE.init("Wood Tar Propolis", 0x877501, 0xBDA613);

		if (ExtraBeeCore.modRailcraft
				&& ItemRegistry.getItem("item.creosote.liquid", 1) != null) {
			EnumType.CREOSOTE.addLiquid(new LiquidStack(ItemRegistry.getItem(
					"item.creosote.liquid", 1).getItem(), 500));
		}

		EnumType.GLACIAL.init("Icy Propolis", 0xDFEBEB, 0x9DEBEB);
		EnumType.GLACIAL.deprecate();
		// EnumType.GLACIAL
		// .addLiquid(new LiquidStack(
		// ItemLiquid.liquids[ItemLiquid.EnumType.GLACIAL
		// .ordinal()].shiftedIndex, 500, 0));
		// EnumType.GLACIAL.addRemenants(new ItemStack(Item.snowball, 1, 0),
		// 25);

		EnumType.PEAT.init("Peat Propolis", 0x402318, 0x94320C);
		// EnumType.PEAT
		// .addLiquid(new LiquidStack(
		// ItemLiquid.liquids[ItemLiquid.EnumType.CREOSOTE
		// .ordinal()].shiftedIndex, 500, 0));
	}

	@PfFSubscribe(Event = PfFEventTypes.FUEL)
	public void PfFInit(EventFuelCreated event) {
		String name = event.getFuel().getLiquid().getItemName();
		ItemStack liquid = event.getFuel().getLiquid();

		if (name.equals("item.liquidPeat"))
			EnumType.PEAT
					.addLiquid(new LiquidStack(liquid.getItem().shiftedIndex,
							500, liquid.getItemDamage()));

	}

	@Override
	public boolean isDamageable() {
		return false;
	}

	@Override
	public boolean isRepairable() {
		return false;
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return ItemPropolis.EnumType.getTypeFromID(itemstack.getItemDamage()).name;
	}

	@Override
	public int getColorFromItemStack(ItemStack itemStack, int j) {
		int i = itemStack.getItemDamage();
		if (j == 0) {
			return EnumType.getTypeFromID(i).primaryColor;
		}

		return EnumType.getTypeFromID(i).secondaryColor;
	}

	// Image ID's

	@Override
	public int getIconFromDamageForRenderPass(int i, int j) {
		return 98;
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (EnumType type : EnumType.values())
			if (!type.isSecret && !type.deprecated) {
				itemList.add(new ItemStack(this, 1, type.ordinal()));
			}
	}

	public static enum EnumType {
		WATER, OIL, FUEL, @Deprecated
		MILK, @Deprecated
		FRUIT, @Deprecated
		SEED, @Deprecated
		ALCOHOL, CREOSOTE, @Deprecated
		GLACIAL, PEAT;

		public static EnumType getTypeFromID(int id) {
			return EnumType.class.getEnumConstants()[id];
		}

		public boolean deprecated = false;

		public void deprecate() {
			deprecated = true;
		}

		public String name;
		public int primaryColor;
		public int secondaryColor;
		public boolean isSecret = false;
		int time;
		LiquidStack liquid = null;
		ItemStack remenants = null;
		int chance;

		EnumType() {
			this.time = 20;
			this.chance = 0;
			this.remenants = new ItemStack(Block.dirt, 1, 0);
			liquid = null;
			name = "??? Propolis";
			primaryColor = 0xFFFFFF;
			secondaryColor = 0xFFFFFF;
		}

		public void init(String name, int primaryColor, int secondaryColor) {
			this.name = name;
			this.primaryColor = primaryColor;
			this.secondaryColor = secondaryColor;
		}

		public void setIsSecret() {
			this.isSecret = true;
		}

		public void addLiquid(LiquidStack item) {
			liquid = item;
			addRecipe();
		}

		public void addRemenants(ItemStack item, int chance) {
			this.remenants = item;
			this.chance = chance;
		}

		public void addRecipe() {
			if (liquid != null) {
				RecipeManagers.squeezerManager.addRecipe(time,
						new ItemStack[] { new ItemStack(ExtraBeeItem.propolis,
								1, this.ordinal()) }, liquid, ItemInterface
								.getItem("propolis"), 50);
			}
		}
	}

}
