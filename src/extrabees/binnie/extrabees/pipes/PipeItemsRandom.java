package binnie.extrabees.pipes;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import binnie.extrabees.core.ExtraBeeTexture;
import buildcraft.transport.pipes.PipeItemsWood;

public class PipeItemsRandom extends PipeItemsWood {

	int baseTexture = 64;
	int plainTexture = 65;

	public PipeItemsRandom(int itemID) {
		super(itemID);

		/*
		 * try { ModLoader.setPrivateValue(Pipe.class, this, "logic", new
		 * PipeLogicRandom()); } catch (Exception e) { e.printStackTrace(); }
		 */
	}

	@Override
	public String getTextureFile() {
		return ExtraBeeTexture.Main.getTexture();
	}

	@Override
	public int getTextureIndex(ForgeDirection direction) {
		if (direction == ForgeDirection.UNKNOWN)
			return baseTexture;
		else {
			int metadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);

			if (metadata == direction.ordinal())
				return plainTexture;
			else
				return baseTexture;
		}
	}

	@Override
	public ItemStack checkExtractGeneric(IInventory inventory,
			boolean doRemove, ForgeDirection from, int start, int stop) {

		class ItemStackInv {
			public ItemStack item;
			public int slot;

			public ItemStackInv(ItemStack item, int slot) {
				this.item = item;
				this.slot = slot;
			}
		}

		int m = 0;
		Queue<ItemStackInv> items = new LinkedBlockingQueue<ItemStackInv>();

		for (int k = start; k <= stop; ++k)
			if (inventory.getStackInSlot(k) != null
					&& inventory.getStackInSlot(k).stackSize > 0) {

				ItemStack slot = inventory.getStackInSlot(k);

				if (slot != null && slot.stackSize > 0) {
					items.add(new ItemStackInv(slot, k));
					m += slot.stackSize;
				}

			}

		if (m <= 0)
			return null;

		Random rand = new Random();

		int i = rand.nextInt(m);

		ItemStackInv item = null;

		while (i > 0) {
			item = items.poll();
			i -= item.item.stackSize;
		}

		if (item != null) {
			if (doRemove)
				inventory.setInventorySlotContents(item.slot, null);
			return item.item;
		}

		return null;

	}

}
