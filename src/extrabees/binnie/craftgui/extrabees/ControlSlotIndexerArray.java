package binnie.craftgui.extrabees;

import net.minecraft.src.IInventory;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.core.IWidget;

public class ControlSlotIndexerArray extends ControlSlotArray {

	@Override
	public ControlSlot createSlot(int x, int y) {
		return new ControlSlotIndexer(this, x, y, pageLength);
	}

	public void create(IInventory inventory) {
		super.create(inventory, 0);
	}

	public ControlSlotIndexerArray(IWidget parent, int x, int y, int columns,
			int rows) {
		super(parent, x, y, columns, rows);
		this.pageLength = columns * rows;
		page = 0;
	}

	int page;

	int pageLength;

	public void setPage(int page) {
		if (this.page == page)
			return;

		this.page = page;
		for (ControlSlot slot : this.slots) {
			((ControlSlotIndexer) slot).setPage(page);
		}
	}

}
