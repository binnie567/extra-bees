package extrabees.gen;

import java.util.Random;
import net.minecraft.server.World;

public class ExtraBeeGenCore
{
    public ExtraBeeGenCore()
    {
    }

    public static void generateSurface(World world, Random random, int i, int j)
    {
        int k = i + random.nextInt(16);
        int k1 = random.nextInt(50) + 20;
        int k2 = j + random.nextInt(16);
        (new WorldGenHiveWater()).a(world, random, k, k1, k2);

        for (int l = 0; l < 2; l++)
        {
            int l1 = i + random.nextInt(16);
            int l2 = random.nextInt(50) + 20;
            int k3 = j + random.nextInt(16);
            (new WorldGenHiveRock()).a(world, random, l1, l2, k3);
        }

        for (int i1 = 0; i1 < 2; i1++)
        {
            int i2 = i + random.nextInt(16);
            int i3 = random.nextInt(50) + 20;
            int l3 = j + random.nextInt(16);
            (new WorldGenHiveNether()).a(world, random, i2, i3, l3);
        }

        for (int j1 = 0; j1 < 2; j1++)
        {
            int j2 = i + random.nextInt(16);
            int j3 = random.nextInt(50) + 20;
            int i4 = j + random.nextInt(16);
            (new WorldGenHiveMarble()).a(world, random, j2, j3, i4);
        }
    }
}
