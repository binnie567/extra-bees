package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesProducts extends PageSpecies {

	public PageSpeciesProducts(IWidget parent) {
		super(parent);

		pageSpeciesProducts_Title = new ControlText(this, 72, 8, "Products",
				ControlText.Alignment.Center);

		pageSpeciesProducts_Products = new ControlProductsBox(this, 4, 20, 136,
				48, ControlProductsBox.Type.Products);

		pageSpeciesProducts_Title2 = new ControlText(this, 72, 68,
				"Specialties", ControlText.Alignment.Center);

		pageSpeciesProducts_Specialties = new ControlProductsBox(this, 4, 80,
				136, 48, ControlProductsBox.Type.Specialties);
	}

	ControlText pageSpeciesProducts_Title;
	ControlProductsBox pageSpeciesProducts_Products;

	ControlText pageSpeciesProducts_Title2;
	ControlProductsBox pageSpeciesProducts_Specialties;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		pageSpeciesProducts_Products.setSpecies(species);
		pageSpeciesProducts_Specialties.setSpecies(species);
	}

}
