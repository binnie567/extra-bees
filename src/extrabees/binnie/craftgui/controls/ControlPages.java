package binnie.craftgui.controls;

import java.util.HashMap;
import java.util.Map;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Widget;

public class ControlPages<T extends Enum<T>> extends Widget {

	@Override
	public boolean isChildVisible(IWidget child) {

		if (child == null)
			return false;

		// System.out.println("Selected - "+pages.get(linkedTabBar.selectedTab));
		// System.out.println("Child - "+child);
		// System.out.println(pages.get(linkedTabBar.selectedTab) == child);
		// System.out.println("");

		return pages.get(linkedTabBar.selectedTab) == child;

	}

	public ControlTabBar<T> linkedTabBar;

	Map<T, ControlPage> pages = new HashMap<T, ControlPage>();

	public ControlPages(IWidget parent, int x, int y, int width, int height,
			ControlTabBar<T> linkedTabBar) {
		super(parent);
		this.linkedTabBar = linkedTabBar;
	}

	public void addPage(ControlPage page, T value) {
		pages.put(value, page);
	}

	@Override
	public void onRenderBackground() {
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
	}

	@Override
	public String getTextureFile() {
		return "";
	}

}
