package binnie.craftgui.controls;

public interface IControlSelection<T> {

	public T getSelectedValue();

	public void setSelectedValue(T value);

	public boolean isSelected(IControlSelectionOption<T> control);

}
