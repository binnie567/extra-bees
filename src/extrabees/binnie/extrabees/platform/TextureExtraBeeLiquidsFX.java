package binnie.extrabees.platform;

import binnie.extrabees.core.ExtraBeeTexture;

public class TextureExtraBeeLiquidsFX extends TextureLiquidsFX {
	public TextureExtraBeeLiquidsFX(int iconID, int r, int g, int b) {
		super(r - 20, r + 20, g - 20, g + 20, b - 20, b + 20, iconID,
				ExtraBeeTexture.Liquids.getTexture());
	}
}
