package binnie.extrabees.products;

import java.util.Random;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.BlockWeb;
import net.minecraft.src.Item;

public class BlockEctoplasm extends BlockWeb {
	public BlockEctoplasm(int par1) {
		super(par1, 16);
		setLightOpacity(1);
		setHardness(0.5F);
		setBlockName("ectoplasm");
		setTextureFile(ExtraBeeTexture.Main.getTexture());
	}

	@Override
	public int quantityDropped(Random rand) {
		return rand.nextInt(5) == 0 ? 1 : 0;
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	@Override
	public int idDropped(int par1, Random par2Random, int par3) {
		return Item.slimeBall.shiftedIndex;
	}
}