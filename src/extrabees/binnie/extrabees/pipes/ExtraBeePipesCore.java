package binnie.extrabees.pipes;

import net.minecraft.src.Item;

public class ExtraBeePipesCore {

	public static void doInit() {
		ExtraBeePipes.initPipes();
	}

	public static Item pipeItemsRandom;
	public static Item pipeItemsRedstoneExtraction;
	public static Item itemGateEternal;

}
