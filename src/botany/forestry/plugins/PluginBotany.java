package forestry.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import binnie.botany.core.BlockBotany;
import binnie.botany.core.BlockLoam;
import binnie.botany.core.ItemBotany;
import binnie.botany.core.ItemLoam;
import binnie.botany.core.ItemRendererBotany;
import binnie.botany.core.TileEntityPlant;
import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.FlowerSpecies;
import binnie.botany.machines.PackagesBotany;
import binnie.core.BinnieCore;

import net.minecraft.src.Block;
import net.minecraft.src.ICommand;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import cpw.mods.fml.common.network.IGuiHandler;
import forestry.api.core.IOreDictionaryHandler;
import forestry.api.core.IPacketHandler;
import forestry.api.core.IPickupHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ISaveEventHandler;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;

public class PluginBotany implements IPlugin {

	public static Block blockBotany;
	public static Block blockLoam;
	public static Item itemBotany;

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {

		for (IAllele allele : FlowerSpecies.values()) {
			AlleleManager.alleleRegistry.registerAllele(allele);
		}

		for (IAllele allele : AlleleFlowerStem.values()) {
			AlleleManager.alleleRegistry.registerAllele(allele);
		}

		// Block bb = new BlockBotany(2291);
		// ModLoader.registerBlock(bb);
		blockBotany = new BlockBotany(2290);
		blockLoam = new BlockLoam(2291);
		itemBotany = new ItemBotany(2292);

		ModLoader.registerBlock(blockLoam, ItemLoam.class);

		ModLoader.registerBlock(blockBotany);

		/*if (BinnieCore.proxy.isClient()) {
			List<ItemStack> loamBlocks = new ArrayList<ItemStack>();
			blockLoam.getSubBlocks(0, null, loamBlocks);
			for (ItemStack loam : loamBlocks)
				ModLoader
						.addName(loam, BlockLoam.getName(loam.getItemDamage()));
		}*/

		BinnieCore.proxy.registerTileEntity(TileEntityPlant.class,
				"botany.plant", 
				BinnieCore.proxy.createTileEntityRenderer("binnie.botany.core.RendererPlant"));

		BinnieCore.proxy.registerCustomItemRenderer(itemBotany.shiftedIndex,
				new ItemRendererBotany());

	}

	@Override
	public void postInit() {
	}

	@Override
	public String getDescription() {
		return "Flower and Herb breeding";
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
	}

	@Override
	public IGuiHandler getGuiHandler() {
		return null;
	}

	@Override
	public IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

	@Override
	public ISaveEventHandler getSaveEventHandler() {
		return null;
	}

	@Override
	public IOreDictionaryHandler getDictionaryHandler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ICommand[] getConsoleCommands() {
		// TODO Auto-generated method stub
		return null;
	}

}
