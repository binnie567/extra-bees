package binnie.extrabees.proxy;

import java.util.ArrayList;
import codechicken.nei.api.ItemInfo;

import binnie.core.BinnieCore;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.liquids.ItemLiquid;
import binnie.extrabees.liquids.ItemMoltenMetal.MoltenMetal;
import binnie.extrabees.platform.TextureExtraBeeLiquidsFX;
import binnie.extrabees.platform.TextureLiquidDNAFX;

public class ProxyClient extends Proxy implements IExtraBeesProxy {

	@Override
	public void doInit() {
		for (ExtraBeeTexture texture : ExtraBeeTexture.values()) {
			BinnieCore.proxy.preloadTexture(texture.getTexture());
		}
		BinnieCore.proxy.addTexture(new TextureLiquidDNAFX());
	}

	@Override
	public void postInit() {

		ArrayList<int[]> damageRange = new ArrayList<int[]>();
		damageRange.add(new int[] { 0, -1 });
		ItemInfo.damageVariants.put(ExtraBeeItem.genome.shiftedIndex,
				damageRange);
		ItemInfo.damageVariants.put(ExtraBeeItem.genomeEmpty.shiftedIndex,
				damageRange);
		ItemInfo.damageVariants.put(ExtraBeeItem.advancedTemplate.shiftedIndex,
				damageRange);
		ItemInfo.damageVariants.put(
				ExtraBeeItem.advancedTemplateBlank.shiftedIndex, damageRange);
	}

	@Override
	public void addLiquidFX(ItemLiquid.EnumType liquid) {
		BinnieCore.proxy.addTexture(new TextureExtraBeeLiquidsFX(liquid
				.ordinal(), liquid.red, liquid.green, liquid.blue));
	}
	
	@Override
	public void addMoltenFX(MoltenMetal type) {
		BinnieCore.proxy.addTexture(new TextureExtraBeeLiquidsFX(type
				.ordinal()+16, type.red, type.green, type.blue));
	}

}
