package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesOverview extends PageSpecies {

	public PageSpeciesOverview(IWidget parent) {
		super(parent);

		controlName = new ControlText(this, 72, 8, "",
				ControlText.Alignment.Center);

		controlScientific = new ControlText(this, 72, 32, "",
				ControlText.Alignment.Center);
		controlAuthority = new ControlText(this, 72, 44, "",
				ControlText.Alignment.Center);

		controlDescription = new ControlText(this, 72, 84, "",
				ControlText.Alignment.Center);
		
		controlBranch = new ControlText(this, 72, 156, "",
				ControlText.Alignment.Center);
	}

	ControlText controlName;
	ControlText controlScientific;
	ControlText controlAuthority;
	
	ControlText controlBranch;
	
	ControlText controlDescription;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		
		String branchBinomial = (species.getBranch()!=null) ? species.getBranch().getScientific() : "<Unknown>";
		String branchName = (species.getBranch()!=null) ? species.getBranch().getName() : "Unknown";
		
		controlName.setText("\u00a7n"+species.getName()+" Bees\u00a7r");
		controlScientific.setText("\u00a7o" + branchBinomial + " " + species.getBinomial()+"\u00a7r");
		controlAuthority.setText("Discovered by \u00a7l"+species.getAuthority()+"\u00a7r");
		controlBranch.setText("<< \u00a7n" + branchName + " Branch\u00a7r >>");
		String desc = species.getDescription();
		if(desc == null || desc == "")
			desc = "\u00a7oNo Description Provided.";
		controlDescription.setText(desc+"\u00a7r");
	}

}
