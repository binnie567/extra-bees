package binnie.extrabees.triggers;

public class ActionPauseProcess extends ExtraBeeAction {

	@Override
	public int getIndexInTexture() {
		return 48;
	}

	@Override
	public String getDescription() {
		return "Pause Process";
	}

}
