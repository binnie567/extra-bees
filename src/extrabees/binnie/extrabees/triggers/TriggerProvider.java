package binnie.extrabees.triggers;

import java.util.LinkedList;

import binnie.extrabees.machines.ICustomGates;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.gates.ITriggerProvider;
import buildcraft.api.transport.IPipe;

import net.minecraft.src.Block;
import net.minecraft.src.TileEntity;

public class TriggerProvider implements ITriggerProvider {

	@Override
	public LinkedList<ITrigger> getPipeTriggers(IPipe pipe) {
		return null;
	}

	@Override
	public LinkedList<ITrigger> getNeighborTriggers(Block block, TileEntity tile) {
		if (tile instanceof ICustomGates)
			return ((ICustomGates) tile).getCustomTriggers();
		return null;
	}

}
