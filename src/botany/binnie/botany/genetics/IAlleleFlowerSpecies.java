package binnie.botany.genetics;

import forestry.api.genetics.IAlleleSpecies;

public interface IAlleleFlowerSpecies extends IAlleleSpecies {

	public String getTextureFile();

	public int getPrimaryIconIndex();

	public int getSecondaryIconIndex();
}
