package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlPage;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;

public abstract class PageSpecies extends ControlPage {

	public PageSpecies(IWidget parent) {
		super(parent, parent.getPosition().x, parent.getPosition().y, parent
				.getSize().x, parent.getSize().y);
	}

	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if (event instanceof EventSpeciesChanged) {
			onSpeciesChanged(((EventSpeciesChanged) event).getSpecies());
		}
	}

	public abstract void onSpeciesChanged(IAlleleBeeSpecies species);

}
