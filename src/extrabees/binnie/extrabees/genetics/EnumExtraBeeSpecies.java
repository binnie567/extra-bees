package binnie.extrabees.genetics;

import java.util.HashMap;
import java.util.LinkedHashMap;

import binnie.extrabees.products.ItemHoneyComb;
import binnie.extrabees.products.ItemHoneyComb.EnumType;
import binnie.extrabees.products.ItemHoneyComb.EnumVanillaType;

import net.minecraft.src.Achievement;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBranch;
import forestry.api.genetics.ILegacyHandler;

public enum EnumExtraBeeSpecies implements IAlleleBeeSpecies {

	ARID, BARREN, DESOLATE,

	ROTTEN, BONE, CREEPER,

	ROCK, STONE, GRANITE, MINERAL,

	IRON, GOLD, COPPER, TIN, SILVER, BRONZE,

	UNSTABLE, NUCLEAR, RADIOACTIVE,

	ANCIENT, PRIMEVAL, PREHISTORIC, RELIC,

	COAL, RESIN, OIL,

	DISTILLED, FUEL, /* CREOSOTE *//* LATeX */

	WATER, RIVER, OCEAN, INK,

	SWEET, SUGAR, FRUIT,

	ALCOHOL, FARM, MILK,

	SWAMP, BOGGY, FUNGAL,

	CREOSOTE, LATEX,

	MARBLE, ROMAN, GREEK, CLASSICAL,

	BASALT, TEMPERED, ANGRY, VOLCANIC,

	MALICIOUS, INFECTIOUS, VIRULENT,

	VISCOUS, GLUTINOUS, STICKY,

	CORROSIVE, CAUSTIC, ACIDIC,

	EXCITED, ENERGETIC, ECSTATIC,

	COFFEE,

	ARTIC, FREEZING,

	CITRUS, PEAT, MINT,

	SHADOW, DARKENED, ABYSS

	;

	private String name = "";
	private String description = "";
	private int primaryColor = 0xFFFFFF;
	private int secondaryColor = 0xFFDC16;
	private EnumTemperature temperature = EnumTemperature.NORMAL;
	private EnumHumidity humidity = EnumHumidity.NORMAL;
	private boolean hasEffect = false;
	private boolean isSecret = true;
	private boolean isCounted = true;
	private String binomial = "";
	private IBranch branch = null;
	private String uid = "";
	private Achievement achievement = null;
	private boolean dominant = true;
	private HashMap<ItemStack, Integer> products = new LinkedHashMap<ItemStack, Integer>();
	private HashMap<ItemStack, Integer> specialties = new LinkedHashMap<ItemStack, Integer>();

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public int getPrimaryColor() {
		return primaryColor;
	}

	@Override
	public int getSecondaryColor() {
		return secondaryColor;
	}

	@Override
	public EnumTemperature getTemperature() {
		return temperature;
	}

	@Override
	public EnumHumidity getHumidity() {
		return humidity;
	}

	@Override
	public boolean hasEffect() {
		return hasEffect;
	}

	@Override
	public boolean isSecret() {
		return isSecret;
	}

	@Override
	public boolean isCounted() {
		return isCounted;
	}

	@Override
	public String getBinomial() {
		return binomial;
	}

	@Override
	public String getAuthority() {
		return "Binnie";
	}

	@Override
	public IBranch getBranch() {
		return branch;
	}

	@Override
	public Achievement getAchievement() {
		return achievement;
	}

	@Override
	public String getUID() {
		return "extrabees.species." + uid;
	}

	@Override
	public boolean isDominant() {
		return dominant;
	}

	@Override
	public HashMap<ItemStack, Integer> getProducts() {
		return products;
	}

	@Override
	public HashMap<ItemStack, Integer> getSpecialty() {
		return specialties;
	}

	@Override
	public boolean isJubilant(World world, int biomeid, int x, int y, int z) {
		return true;
	}

	public void init(String name, String binomial, int colour) {
		this.template = getDefaultTemplate();
		this.name = name;
		this.uid = this.toString().toLowerCase();
		this.binomial = binomial;
		this.primaryColor = colour;
		((ILegacyHandler) AlleleManager.alleleRegistry).registerLegacyMapping(
				65 + this.ordinal(), getUID());
	}

	public void register() {
		BeeManager.breedingManager.registerBeeTemplate(getTemplate());
		AlleleManager.alleleRegistry.registerAllele(this);
	}

	public void addProduct(ItemStack product, int chance) {
		this.products.put(product, chance);
	}

	public void addSpecialty(ItemStack product, int chance) {
		this.specialties.put(product, chance);
	}

	public void setHumidity(EnumHumidity humidity) {
		this.humidity = humidity;
	}

	public void setTemperature(EnumTemperature temperature) {
		this.temperature = temperature;
	}

	private IAllele[] template;

	public IAllele[] getDefaultTemplate() {
		return BeeManager.breedingManager.getDefaultBeeTemplate();
	}

	public IAllele[] getTemplate() {
		this.template[EnumBeeChromosome.SPECIES.ordinal()] = this;
		return template;
	};

	public void importVanillaTemplate(String uid) {
		String text = uid.substring(0, 1).toUpperCase()
				+ uid.substring(1).toLowerCase();
		IAllele[] vanilla = BeeManager.breedingManager
				.getBeeTemplate("forestry.species" + text);

		if (vanilla != null) {
			this.template = vanilla.clone();
			this.template[EnumBeeChromosome.SPECIES.ordinal()] = this;
		} else
			ModLoader.getLogger().warning(
					"Couldn't find vanilla template - " + "forestry.species"
							+ text);
	}

	public void importTemplate(EnumExtraBeeSpecies species) {
		this.template = species.getTemplate().clone();
		this.template[EnumBeeChromosome.SPECIES.ordinal()] = this;
	}

	public void recessive() {
		this.dominant = false;
	}

	public void setIsSecret(boolean secret) {
		this.isSecret = secret;
	}

	public void setHasEffect(boolean effect) {
		this.hasEffect = effect;
	}

	public void setSecondaryColor(int colour) {
		this.secondaryColor = colour;
	}

	public static void doInit() {
		ARID.init("Arid", "aridus", 0xBEE854);
		ARID.setHumidity(EnumHumidity.ARID);
		ARID.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		ARID.importVanillaTemplate("modest");
		ARID.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.NONE;

		BARREN.init("Barren", "infelix", 0xE0D263);
		BARREN.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		BARREN.importTemplate(ARID);

		DESOLATE.init("Desolate", "desolo", 0xD1B890);
		DESOLATE.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		DESOLATE.importTemplate(BARREN);
		DESOLATE.recessive();

		ROTTEN.init("Decaying", "caries", 0xBFE0B6);
		ROTTEN.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		ROTTEN.addSpecialty(ItemHoneyComb.getItem(EnumType.ROTTEN), 10);
		ROTTEN.importTemplate(DESOLATE);
		ROTTEN.template[EnumBeeChromosome.NOCTURNAL.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROTTEN.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROTTEN.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROTTEN.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_ZOMBIE;
		ROTTEN.template[EnumBeeChromosome.FERTILITY.ordinal()] = AlleleManager
				.getAllele("fertilityLow");

		BONE.init("Skeletal", "os", 0xE9EDE8);
		BONE.importTemplate(ROTTEN);
		BONE.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		BONE.addSpecialty(ItemHoneyComb.getItem(EnumType.BONE), 10);
		BONE.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_SKELETON;

		CREEPER.init("Creepy", "erepo", 0x2CE615);
		CREEPER.importTemplate(ROTTEN);
		CREEPER.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 30);
		CREEPER.addSpecialty(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.POWDERY), 10);
		CREEPER.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.SPAWN_CREEPER;
		// CREEPER.template[EnumBeeChromosome.EFFECT.ordinal()] =
		// PluginForestryExtraBees
		// .getAllele("effectCreeper");

		ROCK.init("Rocky", "saxum", 0xA8A8A8);
		ROCK.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 30);
		ROCK.setIsSecret(false);
		ROCK.template[EnumBeeChromosome.NOCTURNAL.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROCK.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROCK.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		ROCK.template[EnumBeeChromosome.TEMPERATURE_TOLERANCE.ordinal()] = AlleleManager
				.getAllele("toleranceBoth2");
		ROCK.template[EnumBeeChromosome.HUMIDITY_TOLERANCE.ordinal()] = AlleleManager
				.getAllele("toleranceBoth2");
		ROCK.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.ROCK;
		ROCK.template[EnumBeeChromosome.FERTILITY.ordinal()] = AlleleManager
				.getAllele("fertilityLow");
		ROCK.setSecondaryColor(10066329);

		STONE.init("Stone", "lapis", 0x757575);
		STONE.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 30);
		STONE.importTemplate(ROCK);
		STONE.recessive();
		STONE.setSecondaryColor(10066329);

		GRANITE.init("Granite", "granum", 0x695555);
		GRANITE.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 30);
		GRANITE.importTemplate(STONE);
		GRANITE.setSecondaryColor(10066329);

		MINERAL.init("Mineral", "minerale", 0x6E757D);
		MINERAL.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 30);
		MINERAL.importTemplate(GRANITE);
		MINERAL.setSecondaryColor(10066329);

		IRON.init("Ferrous", "ferrous", 0xA87058);
		IRON.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		IRON.addSpecialty(ItemHoneyComb.getItem(EnumType.IRON), 10);
		IRON.importTemplate(MINERAL);
		IRON.recessive();
		IRON.setSecondaryColor(10066329);

		GOLD.init("Aureus", "aureus", 0xE6CC0B);
		GOLD.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		GOLD.addSpecialty(ItemHoneyComb.getItem(EnumType.GOLD), 10);
		GOLD.importTemplate(MINERAL);
		GOLD.setHasEffect(true);
		GOLD.setSecondaryColor(10066329);

		COPPER.init("Cuprous", "cuprous", 0xD16308);
		COPPER.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		COPPER.addSpecialty(ItemHoneyComb.getItem(EnumType.COPPER), 10);
		COPPER.importTemplate(MINERAL);
		COPPER.setSecondaryColor(10066329);

		TIN.init("Stannic", "stannus", 0xBDB1BD);
		TIN.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		TIN.addSpecialty(ItemHoneyComb.getItem(EnumType.TIN), 10);
		TIN.importTemplate(MINERAL);
		TIN.setSecondaryColor(10066329);

		SILVER.init("Argenti", "argentus", 0xDBDBDB);
		SILVER.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		SILVER.addSpecialty(ItemHoneyComb.getItem(EnumType.SILVER), 10);
		SILVER.importTemplate(MINERAL);
		SILVER.recessive();
		SILVER.recessive();
		SILVER.setSecondaryColor(10066329);

		BRONZE.init("Aeneus", "pyropus", 0xD48115);
		BRONZE.addProduct(ItemHoneyComb.getItem(EnumType.STONE), 20);
		BRONZE.addSpecialty(ItemHoneyComb.getItem(EnumType.BRONZE), 10);
		BRONZE.importTemplate(MINERAL);
		BRONZE.recessive();
		BRONZE.setSecondaryColor(10066329);

		UNSTABLE.init("Unstable", "levis", 0x3E8C34);
		UNSTABLE.importTemplate(ROCK);
		UNSTABLE.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 20);
		UNSTABLE.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.RADIOACTIVE;
		UNSTABLE.recessive();
		UNSTABLE.setSecondaryColor(10066329);

		NUCLEAR.init("Nuclear", "nucleus", 0x41CC2F);
		NUCLEAR.importTemplate(UNSTABLE);
		NUCLEAR.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 20);
		NUCLEAR.recessive();
		NUCLEAR.setSecondaryColor(10066329);

		RADIOACTIVE.init("Radioactive", "fervens", 0x1EFF00);
		RADIOACTIVE.importTemplate(NUCLEAR);
		RADIOACTIVE.addProduct(ItemHoneyComb.getItem(EnumType.BARREN), 20);
		RADIOACTIVE.addSpecialty(ItemHoneyComb.getItem(EnumType.URANIUM), 5);
		RADIOACTIVE.setHasEffect(true);
		RADIOACTIVE.recessive();
		RADIOACTIVE.setSecondaryColor(10066329);

		ANCIENT.init("Ancient", "antiquus", 0xF2DB8F);
		ANCIENT.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 30);
		ANCIENT.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager
				.getAllele("lifespanElongated");
		PRIMEVAL.init("Primeval", "priscus", 0xB3A67B);
		PRIMEVAL.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 30);
		PRIMEVAL.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager
				.getAllele("lifespanLong");
		PREHISTORIC.init("Prehistoric", "pristinus", 0x6E5A40);
		PREHISTORIC.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 30);
		PREHISTORIC.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager
				.getAllele("lifespanLonger");
		PREHISTORIC.recessive();
		RELIC.init("Relic", "sapiens", 0x4D3E16);
		RELIC.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 30);
		RELIC.setHasEffect(true);
		RELIC.template[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager
				.getAllele("lifespanLongest");

		COAL.init("Fossil", "carbo", 0x7A7648);
		COAL.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 20);
		COAL.addSpecialty(ItemHoneyComb.getItem(EnumType.COAL), 10);
		RESIN.init("Preserved", "lacrima", 0xA6731B);
		RESIN.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 20);
		RESIN.addSpecialty(ItemHoneyComb.getItem(EnumType.RESIN), 10);
		RESIN.recessive();
		OIL.init("Oil", "lubricus", 0x574770);
		OIL.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 20);
		OIL.addSpecialty(ItemHoneyComb.getItem(EnumType.OIL), 10);
		PEAT.init("Peat", "peatus", 0x8C2A0A);
		PEAT.addProduct(ItemHoneyComb.getItem(EnumType.OLD), 20);
		PEAT.addSpecialty(ItemHoneyComb.getItem(EnumType.PEAT), 10);

		DISTILLED.init("Distilled", "distilli", 0x356356);
		DISTILLED.addProduct(ItemHoneyComb.getItem(EnumType.OIL), 30);
		DISTILLED.recessive();
		FUEL.init("Refined", "refina", 0xFFC003);
		FUEL.addProduct(ItemHoneyComb.getItem(EnumType.OIL), 25);
		FUEL.addSpecialty(ItemHoneyComb.getItem(EnumType.FUEL), 10);
		FUEL.setHasEffect(true);
		CREOSOTE.init("Tarry", "creosota", 0x979E13);
		CREOSOTE.addProduct(ItemHoneyComb.getItem(EnumType.OIL), 25);
		CREOSOTE.addSpecialty(ItemHoneyComb.getItem(EnumType.CREOSOTE), 10);
		CREOSOTE.setHasEffect(true);
		LATEX.init("Latex", "latex", 0x494A3E);
		LATEX.addProduct(ItemHoneyComb.getItem(EnumType.OIL), 25);
		LATEX.addSpecialty(ItemHoneyComb.getItem(EnumType.LATEX), 10);
		LATEX.setHasEffect(true);

		WATER.init("Water", "aqua", 0x94A2FF);
		WATER.addProduct(ItemHoneyComb.getItem(EnumType.WATER), 30);
		WATER.setIsSecret(false);
		WATER.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		WATER.template[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		WATER.template[EnumBeeChromosome.HUMIDITY_TOLERANCE.ordinal()] = AlleleManager
				.getAllele("toleranceBoth1");
		WATER.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.WATER;
		WATER.setHumidity(EnumHumidity.DAMP);

		RIVER.init("River", "flumen", 0x83B3D4);
		RIVER.addProduct(ItemHoneyComb.getItem(EnumType.WATER), 30);
		RIVER.addSpecialty(ItemHoneyComb.getItem(EnumType.CLAY), 15);
		RIVER.importTemplate(WATER);

		OCEAN.init("Ocean", "mare", 0x1D2EAD);
		OCEAN.addProduct(ItemHoneyComb.getItem(EnumType.WATER), 30);
		OCEAN.importTemplate(WATER);
		OCEAN.recessive();

		INK.init("Inkling", "atramentum", 0x0E1447);
		INK.addProduct(ItemHoneyComb.getItem(EnumType.WATER), 30);
		INK.addSpecialty(new ItemStack(Item.dyePowder, 1, 0), 15);
		INK.importTemplate(WATER);

		SWEET.init("Sweet", "mellitus", 0xFC51F1);
		SWEET.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				40);
		SWEET.addProduct(new ItemStack(Item.sugar, 1, 0), 10);
		SWEET.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.SUGAR;

		SUGAR.init("Sugary", "dulcis", 0xE6D3E0);
		SUGAR.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				40);
		SUGAR.addProduct(new ItemStack(Item.sugar, 1, 0), 20);
		SUGAR.importTemplate(SWEET);

		FRUIT.init("Fruity", "pomum", 0xDB5876);
		FRUIT.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				30);
		FRUIT.addProduct(new ItemStack(Item.sugar, 1, 0), 10);
		FRUIT.addSpecialty(ItemHoneyComb.getItem(EnumType.FRUIT), 15);
		FRUIT.setHasEffect(true);
		FRUIT.importTemplate(SUGAR);

		ALCOHOL.init("Fermented", "vinum", 0xE88A61);
		ALCOHOL.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN), 30);
		ALCOHOL.addSpecialty(ItemHoneyComb.getItem(EnumType.ALCOHOL), 15);
		ALCOHOL.importVanillaTemplate("rural");
		ALCOHOL.recessive();

		FARM.init("Farmed", "ager", 0x75DB60);
		FARM.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN),
				30);
		FARM.addSpecialty(ItemHoneyComb.getItem(EnumType.SEED), 15);
		FARM.importVanillaTemplate("rural");

		MILK.init("Bovine", "lacteus", 0xE3E8E8);
		MILK.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN),
				30);
		MILK.addSpecialty(ItemHoneyComb.getItem(EnumType.MILK), 15);
		MILK.importVanillaTemplate("rural");

		COFFEE.init("Coffee", "arabica", 0x8C5E30);
		COFFEE.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN), 30);
		COFFEE.addSpecialty(ItemHoneyComb.getItem(EnumType.COFFEE), 15);
		COFFEE.importVanillaTemplate("rural");

		CITRUS.init("Citrus", "citrus", 0xFAEC55);
		CITRUS.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN), 30);
		CITRUS.addSpecialty(ItemHoneyComb.getItem(EnumType.CITRUS), 15);
		CITRUS.importVanillaTemplate("rural");

		MINT.init("Mint", "minty", 0x6DF768);
		MINT.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.WHEATEN),
				30);
		MINT.addSpecialty(ItemHoneyComb.getItem(EnumType.MINT), 15);
		MINT.importVanillaTemplate("rural");

		SWAMP.init("Swamp", "paludis", 0x356933);
		SWAMP.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.MOSSY),
				30);
		SWAMP.importVanillaTemplate("marshy");
		SWAMP.setHumidity(EnumHumidity.DAMP);

		BOGGY.init("Boggy", "lama", 0x785C29);
		BOGGY.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.MOSSY),
				30);
		BOGGY.importTemplate(SWAMP);
		BOGGY.recessive();

		FUNGAL.init("Fungal", "boletus", 0xD16200);
		FUNGAL.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.MOSSY),
				30);
		FUNGAL.addSpecialty(ItemHoneyComb.getItem(EnumType.FUNGAL), 15);
		FUNGAL.importTemplate(BOGGY);
		FUNGAL.setHasEffect(true);

		MARBLE.init("Marbled", "marbla", 0xD6C9CF);

		MARBLE.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				30);
		MARBLE.importVanillaTemplate("noble");

		ROMAN.init("Roman", "roman", 0xAD8BB0);
		ROMAN.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				30);
		ROMAN.importTemplate(MARBLE);

		GREEK.init("Greek", "greco", 0x854C8A);
		GREEK.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY),
				30);
		GREEK.recessive();
		GREEK.importTemplate(ROMAN);

		CLASSICAL.init("Classical", "classica", 0x831D8C);
		CLASSICAL.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.HONEY), 30);
		CLASSICAL.addSpecialty(ItemInterface.getItem("royalJelly"), 25);
		CLASSICAL.setHasEffect(true);
		CLASSICAL.importTemplate(GREEK);

		BASALT.init("Embittered", "aceri", 0x8C6969);
		BASALT.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SIMMERING), 25);
		BASALT.importVanillaTemplate("sinister");
		BASALT.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager
				.getAllele("effectAggressive");
		BASALT.setSecondaryColor(10101539);
		BASALT.setHumidity(EnumHumidity.ARID);
		BASALT.setTemperature(EnumTemperature.HELLISH);
		TEMPERED.init("Angry", "turbo", 0x8A4848);
		TEMPERED.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SIMMERING), 25);
		TEMPERED.importTemplate(BASALT);
		TEMPERED.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager
				.getAllele("effectIgnition");
		TEMPERED.recessive();
		TEMPERED.setSecondaryColor(10101539);
		ANGRY.init("Furious", "iratus", 0x801F1F);
		ANGRY.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SIMMERING), 25);
		ANGRY.importTemplate(TEMPERED);
		ANGRY.setSecondaryColor(10101539);
		VOLCANIC.init("Volcanic", "volcano", 0x4D0C0C);
		VOLCANIC.importTemplate(ANGRY);
		VOLCANIC.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SIMMERING), 25);
		VOLCANIC.addSpecialty(ItemHoneyComb.getItem(EnumType.BLAZE), 25);
		VOLCANIC.setHasEffect(true);
		VOLCANIC.setSecondaryColor(10101539);

		MALICIOUS.init("Malicious", "acerbus", 0x782A77);
		MALICIOUS.importVanillaTemplate("tropical");
		MALICIOUS.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY), 25);
		MALICIOUS.setSecondaryColor(431972);
		MALICIOUS.setHumidity(EnumHumidity.DAMP);
		MALICIOUS.setTemperature(EnumTemperature.WARM);
		INFECTIOUS.init("Infectious", "contagio", 0xB82EB5);
		INFECTIOUS.importTemplate(MALICIOUS);
		INFECTIOUS.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY), 25);
		INFECTIOUS.setSecondaryColor(431972);
		VIRULENT.init("Virulent", "morbus", 0xF013EC);
		VIRULENT.importTemplate(INFECTIOUS);
		VIRULENT.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY), 25);
		VIRULENT.addSpecialty(ItemHoneyComb.getItem(EnumType.VENOMOUS), 15);
		VIRULENT.recessive();
		VIRULENT.setHasEffect(true);
		VIRULENT.setSecondaryColor(431972);

		VISCOUS.init("Viscous", "liquidus", 0x09470E);
		VISCOUS.importVanillaTemplate("tropical");
		VISCOUS.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.ECTOPLASM;
		VISCOUS.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY),
				25);
		VISCOUS.setSecondaryColor(431972);
		VISCOUS.setHumidity(EnumHumidity.DAMP);
		VISCOUS.setTemperature(EnumTemperature.WARM);
		GLUTINOUS.init("Glutinous", "glutina", 0x1D8C27);
		GLUTINOUS.importTemplate(VISCOUS);
		GLUTINOUS.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY), 25);
		GLUTINOUS.setSecondaryColor(431972);
		STICKY.init("Sticky", "lentesco", 0x17E328);
		STICKY.importTemplate(GLUTINOUS);
		STICKY.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY),
				25);
		STICKY.addSpecialty(ItemHoneyComb.getItem(EnumType.SLIME), 15);
		STICKY.setHasEffect(true);
		STICKY.setSecondaryColor(431972);

		CORROSIVE.init("Corrosive", "corrumpo", 0x4A5C0B);
		CORROSIVE.importVanillaTemplate("tropical");
		CORROSIVE.setHumidity(EnumHumidity.DAMP);
		CORROSIVE.setTemperature(EnumTemperature.WARM);
		CORROSIVE.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.ACID;
		CORROSIVE.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY), 20);
		CORROSIVE.recessive();
		CORROSIVE.setSecondaryColor(431972);
		CAUSTIC.init("Caustic", "torrens", 0x84A11D);
		CAUSTIC.importTemplate(CORROSIVE);
		CAUSTIC.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY),
				20);
		CAUSTIC.setSecondaryColor(431972);

		ACIDIC.init("Acidic", "acidus", 0xC0F016);
		ACIDIC.importTemplate(CAUSTIC);
		ACIDIC.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.SILKY),
				20);
		ACIDIC.addSpecialty(ItemHoneyComb.getItem(EnumType.ACIDIC), 15);
		ACIDIC.setHasEffect(true);
		ACIDIC.setSecondaryColor(431972);

		EXCITED.init("Excited", "excita", 0xFF4545);
		EXCITED.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
		EXCITED.addProduct(ItemHoneyComb.getItem(EnumType.REDSTONE), 25);
		EXCITED.template[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager
				.getAllele("boolTrue");
		EXCITED.template[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = EnumExtraBeeFlowers.REDSTONE;

		ENERGETIC.init("Energetic", "energia", 0xE835C7);
		ENERGETIC.importTemplate(EXCITED);
		ENERGETIC.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
		ENERGETIC.addProduct(ItemHoneyComb.getItem(EnumType.REDSTONE), 25);
		ENERGETIC.recessive();

		ECSTATIC.init("Ecstatic", "ecstatica", 0xAF35E8);
		ECSTATIC.importTemplate(ENERGETIC);
		ECSTATIC.template[EnumBeeChromosome.EFFECT.ordinal()] = EnumExtraBeeEffect.LIGHTNING;
		ECSTATIC.addProduct(ItemHoneyComb.getItem(EnumType.REDSTONE), 25);
		ECSTATIC.addSpecialty(ItemHoneyComb.getItem(EnumType.IC2ENERGY), 10);
		ECSTATIC.setHasEffect(true);
		ECSTATIC.setIsSecret(true);

		ARTIC.init("Artic", "artica", 0xADE0E0);
		ARTIC.importVanillaTemplate("wintry");
		ARTIC.addProduct(ItemHoneyComb.getVanillaItem(EnumVanillaType.FROZEN),
				25);
		ARTIC.setTemperature(EnumTemperature.ICY);

		FREEZING.init("Glacial", "glacia", 0x7BE3E3);
		FREEZING.importTemplate(ARTIC);
		FREEZING.addProduct(
				ItemHoneyComb.getVanillaItem(EnumVanillaType.FROZEN), 20);
		FREEZING.addSpecialty(ItemHoneyComb.getItem(EnumType.GLACIAL), 15);

		SHADOW.init("Shadowy", "shadowa", 0x595959);
		SHADOW.addProduct(ItemHoneyComb.getItem(EnumType.SHADOW), 25);
		SHADOW.importTemplate(BASALT);
		SHADOW.template[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager
				.getAllele("effectIgnition");
		SHADOW.recessive();

		DARKENED.init("Darkened", "darka", 0x332E33);
		DARKENED.addProduct(ItemHoneyComb.getItem(EnumType.SHADOW), 25);
		DARKENED.importTemplate(SHADOW);

		ABYSS.init("Abyss", "abyssba", 0x210821);
		ABYSS.importTemplate(DARKENED);
		ABYSS.addProduct(ItemHoneyComb.getItem(EnumType.SHADOW), 25);
		ABYSS.setHasEffect(true);

		for (EnumExtraBeeSpecies species : EnumExtraBeeSpecies.values()) {
			species.register();
		}

	}

	public static IAlleleBeeSpecies getVanilla(String name) {
		return (IAlleleBeeSpecies) AlleleManager.alleleRegistry
				.getAllele("forestry.species" + name);
	}

	public static IAllele[] getVanillaTemplate(String uid) {
		String text = uid.substring(0, 1).toUpperCase()
				+ uid.substring(1).toLowerCase();
		return BeeManager.breedingManager.getBeeTemplate("forestry.species"
				+ text);
	}

	@Override
	public int getBodyType() {
		return 0;
	}

	void setBranch(IBranch branch) {
		this.branch = branch;
	}

}
