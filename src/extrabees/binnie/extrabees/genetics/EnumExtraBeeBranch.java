package binnie.extrabees.genetics;

import java.util.LinkedHashSet;
import java.util.Set;

import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeBranch;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IBranch;

public enum EnumExtraBeeBranch implements IBeeBranch {
	BARREN, HOSTILE, ROCKY, METALLIC, NUCLEAR, HISTORIC, FOSSILIZED, REFINED, AQUATIC, SACCHARINE, CLASSICAL, VOLCANIC, VIRULENT, VISCOUS, CAUSTIC, ENERGETIC, FARMING, SHADOW, ;

	private String uid = "";
	private String name = "";
	private String scientific = "";
	private String description = "";

	public void setDescription(String description) {
		this.description = description;
	}

	private Set<IAlleleBeeSpecies> speciesSet = new LinkedHashSet<IAlleleBeeSpecies>();

	@Override
	public String getUID() {
		return "extrabees.branch." + uid;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getScientific() {
		return scientific;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public IAlleleSpecies[] getMembers() {
		return speciesSet.toArray(new IAlleleSpecies[0]);
	}

	@Override
	public void addMember(IAlleleSpecies species) {
		speciesSet.add((IAlleleBeeSpecies) species);
		if (species instanceof EnumExtraBeeSpecies)
			((EnumExtraBeeSpecies) species).setBranch(this);
	}

	public void init(String name, String scientific) {
		this.name = name;
		this.scientific = scientific;
		this.uid = this.toString().toLowerCase();
	}

	public void register() {
		AlleleManager.alleleRegistry.registerBranch(this);
	}

	public static void doInit() {

		IBranch frozenBranch = AlleleManager.alleleRegistry
				.getBranch("forestry.frozen");
		if (frozenBranch != null) {
			frozenBranch.addMember(EnumExtraBeeSpecies.ARTIC);
			EnumExtraBeeSpecies.ARTIC.setBranch(frozenBranch);
			frozenBranch.addMember(EnumExtraBeeSpecies.FREEZING);
			EnumExtraBeeSpecies.FREEZING.setBranch(frozenBranch);
		}

		IBranch agrarianBranch = AlleleManager.alleleRegistry
				.getBranch("forestry.agrarian");
		if (agrarianBranch != null) {
			agrarianBranch.addMember(EnumExtraBeeSpecies.FARM);
			EnumExtraBeeSpecies.FARM.setBranch(agrarianBranch);
		}

		IBranch boggyBranch = AlleleManager.alleleRegistry
				.getBranch("forestry.boggy");
		if (boggyBranch != null) {
			boggyBranch.addMember(EnumExtraBeeSpecies.SWAMP);
			boggyBranch.addMember(EnumExtraBeeSpecies.BOGGY);
			boggyBranch.addMember(EnumExtraBeeSpecies.FUNGAL);
			EnumExtraBeeSpecies.SWAMP.setBranch(boggyBranch);
			EnumExtraBeeSpecies.BOGGY.setBranch(boggyBranch);
			EnumExtraBeeSpecies.FUNGAL.setBranch(boggyBranch);
		}

		FARMING.init("Agricultural", "Agriapis");
		FARMING.addMember(EnumExtraBeeSpecies.ALCOHOL);
		FARMING.addMember(EnumExtraBeeSpecies.MILK);
		FARMING.addMember(EnumExtraBeeSpecies.COFFEE);
		FARMING.addMember(EnumExtraBeeSpecies.CITRUS);
		FARMING.addMember(EnumExtraBeeSpecies.MINT);
		FARMING.register();

		BARREN.init("Barren", "Vacapis");
		BARREN.addMember(EnumExtraBeeSpecies.ARID);
		BARREN.addMember(EnumExtraBeeSpecies.BARREN);
		BARREN.addMember(EnumExtraBeeSpecies.DESOLATE);
		BARREN.setDescription("Althought initially these bees are considerably useless due to their meagre combs, they can lead onto greater things.");
		BARREN.register();

		HOSTILE.init("Hostile", "Infenapis");
		HOSTILE.addMember(EnumExtraBeeSpecies.ROTTEN);
		HOSTILE.addMember(EnumExtraBeeSpecies.BONE);
		HOSTILE.addMember(EnumExtraBeeSpecies.CREEPER);
		HOSTILE.setDescription("Prone to summoning monsters out of the blue, these bees nonetheless make useful if distasteful products.");
		HOSTILE.register();

		ROCKY.init("Rocky", "Monapis");
		ROCKY.addMember(EnumExtraBeeSpecies.ROCK);
		ROCKY.addMember(EnumExtraBeeSpecies.STONE);
		ROCKY.addMember(EnumExtraBeeSpecies.GRANITE);
		ROCKY.addMember(EnumExtraBeeSpecies.MINERAL);
		ROCKY.setDescription("Found amoungst the rocks, these bees have very high tolarance to adverse conditions.");
		ROCKY.register();

		METALLIC.init("Metallic", "Lamminapis");
		METALLIC.addMember(EnumExtraBeeSpecies.IRON);
		METALLIC.addMember(EnumExtraBeeSpecies.COPPER);
		METALLIC.addMember(EnumExtraBeeSpecies.TIN);
		METALLIC.addMember(EnumExtraBeeSpecies.BRONZE);
		METALLIC.addMember(EnumExtraBeeSpecies.SILVER);
		METALLIC.addMember(EnumExtraBeeSpecies.GOLD);
		METALLIC.register();

		NUCLEAR.init("Nuclear", "Levapis");
		NUCLEAR.addMember(EnumExtraBeeSpecies.UNSTABLE);
		NUCLEAR.addMember(EnumExtraBeeSpecies.NUCLEAR);
		NUCLEAR.addMember(EnumExtraBeeSpecies.RADIOACTIVE);
		NUCLEAR.register();

		HISTORIC.init("Historic", "Priscapis");
		HISTORIC.addMember(EnumExtraBeeSpecies.ANCIENT);
		HISTORIC.addMember(EnumExtraBeeSpecies.PRIMEVAL);
		HISTORIC.addMember(EnumExtraBeeSpecies.PREHISTORIC);
		HISTORIC.addMember(EnumExtraBeeSpecies.RELIC);
		HISTORIC.register();

		FOSSILIZED.init("Fossilized", "Fosiapis");
		FOSSILIZED.addMember(EnumExtraBeeSpecies.COAL);
		FOSSILIZED.addMember(EnumExtraBeeSpecies.RESIN);
		FOSSILIZED.addMember(EnumExtraBeeSpecies.OIL);
		FOSSILIZED.addMember(EnumExtraBeeSpecies.PEAT);
		FOSSILIZED.register();

		REFINED.init("Refined", "Petrapis");
		REFINED.addMember(EnumExtraBeeSpecies.DISTILLED);
		REFINED.addMember(EnumExtraBeeSpecies.FUEL);
		REFINED.addMember(EnumExtraBeeSpecies.CREOSOTE);
		REFINED.addMember(EnumExtraBeeSpecies.LATEX);
		REFINED.register();

		AQUATIC.init("Aquatic", "Aquapis");
		AQUATIC.addMember(EnumExtraBeeSpecies.WATER);
		AQUATIC.addMember(EnumExtraBeeSpecies.RIVER);
		AQUATIC.addMember(EnumExtraBeeSpecies.OCEAN);
		AQUATIC.addMember(EnumExtraBeeSpecies.INK);
		AQUATIC.register();

		SACCHARINE.init("Saccharine", "Sacchapis");
		SACCHARINE.addMember(EnumExtraBeeSpecies.SWEET);
		SACCHARINE.addMember(EnumExtraBeeSpecies.SUGAR);
		SACCHARINE.addMember(EnumExtraBeeSpecies.FRUIT);
		SACCHARINE.register();

		CLASSICAL.init("Classical", "Grecapis");
		CLASSICAL.addMember(EnumExtraBeeSpecies.MARBLE);
		CLASSICAL.addMember(EnumExtraBeeSpecies.ROMAN);
		CLASSICAL.addMember(EnumExtraBeeSpecies.GREEK);
		CLASSICAL.addMember(EnumExtraBeeSpecies.CLASSICAL);
		CLASSICAL.register();

		VOLCANIC.init("Volcanic", "Irrapis");
		VOLCANIC.addMember(EnumExtraBeeSpecies.BASALT);
		VOLCANIC.addMember(EnumExtraBeeSpecies.TEMPERED);
		VOLCANIC.addMember(EnumExtraBeeSpecies.ANGRY);
		VOLCANIC.addMember(EnumExtraBeeSpecies.VOLCANIC);
		VOLCANIC.register();

		VISCOUS.init("Viscous", "Viscapis");
		VISCOUS.addMember(EnumExtraBeeSpecies.VISCOUS);
		VISCOUS.addMember(EnumExtraBeeSpecies.GLUTINOUS);
		VISCOUS.addMember(EnumExtraBeeSpecies.STICKY);
		VISCOUS.register();

		VIRULENT.init("Virulent", "extrabees.virulent");
		VIRULENT.addMember(EnumExtraBeeSpecies.MALICIOUS);
		VIRULENT.addMember(EnumExtraBeeSpecies.INFECTIOUS);
		VIRULENT.addMember(EnumExtraBeeSpecies.VIRULENT);
		VIRULENT.register();

		CAUSTIC.init("Caustic", "Morbapis");
		CAUSTIC.addMember(EnumExtraBeeSpecies.CORROSIVE);
		CAUSTIC.addMember(EnumExtraBeeSpecies.CAUSTIC);
		CAUSTIC.addMember(EnumExtraBeeSpecies.ACIDIC);
		CAUSTIC.register();

		ENERGETIC.init("Energetic", "Incitapis");
		ENERGETIC.addMember(EnumExtraBeeSpecies.EXCITED);
		ENERGETIC.addMember(EnumExtraBeeSpecies.ENERGETIC);
		ENERGETIC.addMember(EnumExtraBeeSpecies.ECSTATIC);
		ENERGETIC.register();

		SHADOW.init("Shadow", "Pullapis");
		SHADOW.addMember(EnumExtraBeeSpecies.SHADOW);
		SHADOW.addMember(EnumExtraBeeSpecies.DARKENED);
		SHADOW.addMember(EnumExtraBeeSpecies.ABYSS);
		SHADOW.register();
	}
}