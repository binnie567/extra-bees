package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntitySequencer;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerNoBlankTemplate extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 0;
	}

	@Override
	public String getDescription() {
		return "No Blank Templates";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntitySequencer) {
			TileEntitySequencer machine = (TileEntitySequencer) tile;
			return machine.getSlot(TileEntitySequencer.SlotTemplateBlank)
					.isEmpty();
		}
		return false;
	}

	public TriggerNoBlankTemplate() {
		super();
	}

}
