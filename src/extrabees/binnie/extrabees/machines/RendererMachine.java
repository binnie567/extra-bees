package binnie.extrabees.machines;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

import net.minecraft.src.Block;
import net.minecraft.src.IBlockAccess;
import net.minecraft.src.RenderBlocks;
import net.minecraft.src.TileEntity;
import net.minecraft.src.TileEntitySpecialRenderer;

public class RendererMachine extends TileEntitySpecialRenderer implements
		ISimpleBlockRenderingHandler {

	public RendererMachine() {
		model = new ModelMachine();
	}

	private ModelMachine model;

	@Override
	public void renderTileEntityAt(TileEntity entity, double x, double y,
			double z, float var8) {
		renderModel((TileEntityMachine) entity, x, y, z, var8);
	}

	public void renderModel(TileEntityMachine entity, double x, double y,
			double z, float var8) {

		GL11.glPushMatrix();

		GL11.glTranslated(x + 0.5, y + 1.5, z + 0.5);
		GL11.glRotatef(180, 0f, 0f, 1f);

		bindTextureByName(entity.getRenderTexture());

		GL11.glPushMatrix();

		model.renderModel(0.0625f);

		GL11.glPopMatrix();

		GL11.glPopMatrix();
	}

	public void renderInvBlock(RenderBlocks renderblocks, Block block, int i,
			int j) {

		TileEntity entity = block.createTileEntity(null, i);

		renderModel((TileEntityMachine) entity, 0.0, -0.1, 0.0, 0.0625F);

	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		if (modelID == ModuleMachines.modelMachine) {
			this.renderInvBlock(renderer, block, metadata, modelID);
		}
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
			Block block, int modelId, RenderBlocks renderer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean shouldRender3DInInventory() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getRenderId() {
		return ModuleMachines.modelMachine;
	}

}
