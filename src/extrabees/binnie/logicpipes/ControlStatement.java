package binnie.logicpipes;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlStatement extends Control {

	@Override
	public void onRenderBackground() {
		getRenderer().renderRect(9, 7, 32, 4, 0, 18);
		//getRenderer().renderRect(9, 7, 32, 4, 0, 22);
		//getRenderer().renderRect(9, 0, 32, 18, 0, 26);
	}

	ILogicStatement statement;
	
	protected ControlStatement(IWidget parent, float x, float y, ILogicStatement statement) {
		super(parent, x, y, 54, 18);
		this.statement = statement;
		new ControlTrigger(this, 0, 0, statement.getTrigger());
		new ControlAction(this, 36, 0, statement.getAction());
		texture = "/gfx/extrabees/gui-logicpipes.png";
	}

}
