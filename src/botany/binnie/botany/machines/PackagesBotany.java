package binnie.botany.machines;

import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;
import forestry.core.config.Config;
import forestry.core.config.Defaults;
import forestry.core.config.ForestryItem;
import forestry.core.gadgets.MachinePackage;
import forestry.core.proxy.Proxies;
import forestry.core.utils.CraftingIngredients;
import forestry.core.utils.EnergyConfiguration;
import forestry.core.utils.StructureBlueprint;
import forestry.core.utils.TextureDescription;
import forestry.cultivation.PackagesCultivation;
import forestry.cultivation.gadgets.MillTreetap;
import forestry.cultivation.harvesters.HarvesterSeeds;
import forestry.cultivation.planters.PlanterSeeds;
import forestry.factory.gadgets.MillRainmaker;
import forestry.plugins.PluginForestryCultivation;
import forestry.plugins.PluginIC2;

public class PackagesBotany {

	public static MachinePackage getFlowerHarvestPackage() {
		MachinePackage pack;

		CraftingIngredients recipe = null;
		if (Config.craftingFarmsEnabled)
			recipe = new CraftingIngredients(1, new Object[] { "#X#", "XYX",
					"#Z#", Character.valueOf('#'), Block.glass,
					Character.valueOf('X'),
					new ItemStack(ForestryItem.tubes, 1, 2),
					Character.valueOf('Y'), ForestryItem.sturdyCasing,
					Character.valueOf('Z'),
					new ItemStack(ForestryItem.circuitboards, 1, 0) });
		pack = new MachinePackage(new HarvesterSeeds.Factory(),
				"Flower Harvester", new TextureDescription(192, 192, 193, 193,
						193, 193), recipe);

		pack.energyConfig = PackagesCultivation.energyConfigDefaultHarvester;
		return pack;
	}

	public static MachinePackage getFlowerFarmPackage() {
		MachinePackage pack;

		CraftingIngredients recipe = null;
		if (Config.craftingFarmsEnabled)
			recipe = new CraftingIngredients(1, new Object[] { "#X#", "XYX",
					"#Z#", Character.valueOf('#'), Block.glass,
					Character.valueOf('X'),
					new ItemStack(ForestryItem.tubes, 1, 2),
					Character.valueOf('Y'), ForestryItem.sturdyCasing,
					Character.valueOf('Z'),
					new ItemStack(ForestryItem.circuitboards, 1, 0) });
		pack = new MachinePackage(new PlanterFlower.Factory(), "Flower Farm",
				PluginForestryCultivation.proxy
						.getRenderDefaultPlanter("/gfx/botany/gardener_"),
				recipe);
		pack.energyConfig = PackagesCultivation.energyConfigDefaultPlanter;

		return pack;
	}

	public static MachinePackage getPollinatorPackage() {
		MachinePackage pack;

		CraftingIngredients recipe = null;
		if (PluginIC2.instance.isAvailable())
			recipe = new CraftingIngredients(1, new Object[] { "X#X", "#Y#",
					"X#X", Character.valueOf('#'), Block.glass,
					Character.valueOf('X'), PluginIC2.treetap,
					Character.valueOf('Y'), ForestryItem.sturdyCasing });

		byte charges = 8;
		pack = new MachinePackage(new MillTreetap.Factory(), "Pollinator",
				Proxies.render
						.getRenderMill("/gfx/botany/pollinator_", charges),
				recipe);

		// Configure energy
		pack.energyConfig = PackagesCultivation.energyConfigDefaultGrower;

		return pack;
	}

}
