package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventMouseMoved extends Event {

	public float getDx() {
		return dx;
	}

	public float getDy() {
		return dy;
	}

	public EventMouseMoved(IWidget origin, float dx, float dy) {
		super(origin);
		this.dx = dx;
		this.dy = dy;
	}

	float dx;
	float dy;

}
