package binnie.extrabees.fx;

import net.minecraft.src.EntityFX;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.machines.TileEntitySequencer;
import binnie.extrabees.machines.TileEntitySplicer;

public class EntityDNAFX extends EntityBinnieFX {

	public int blendmode = 1;
	private String texture = ExtraBeeTexture.FX.getTexture();
	
	public EntityDNAFX(TileEntity entity) {

		super(entity.worldObj, new Vector3f((float) entity.xCoord + 0.2f + 0.6f
				* entity.worldObj.rand.nextFloat(), (float) entity.yCoord + 1f,
				(float) entity.zCoord + 0.2f + 0.6f
						* entity.worldObj.rand.nextFloat()), new Vector3f(0f, 0f,
				0f));
		
		this.setSize(0.1F, 0.1F);
		this.particleScale *= 0.6F;
		this.particleMaxAge = (int) (20.0D / (Math.random() * 0.8D + 0.2D));
		this.noClip = true;

		this.motionX *= 0.0;
		this.motionY *= 0.119999999552965164D;
		this.motionZ *= 0.0;

		this.motionY = 0.01f;

		this.particleMaxAge = 20;
		
		setParticleTextureIndex(4);
		
		redAngle = (float) (worldObj.rand.nextFloat() * Math.PI * 2f);
		blueAngle = (float) (worldObj.rand.nextFloat() * Math.PI * 2f);
		greenAngle = (float) (worldObj.rand.nextFloat() * Math.PI * 2f);
	}
	
	float redAngle = 0f;
	float blueAngle = 0f;
	float greenAngle = 0f;
	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate() {

		particleRed = (float) (0.5 + 0.5*Math.sin(redAngle));
		particleBlue = (float) (0.5 + 0.5*Math.sin(blueAngle));
		particleGreen = (float) (0.5 + 0.5*Math.sin(greenAngle));
		
		redAngle += 0.1f;
		blueAngle += 0.1f;
		greenAngle += 0.1f;
		
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		this.moveEntity(this.motionX, this.motionY, this.motionZ);
		
		if (this.particleMaxAge-- <= 0)
			this.setDead();
		
	}
	
//	public void onUpdate() {
//
//		this.prevPosX = this.posX;
//		this.prevPosY = this.posY;
//		this.prevPosZ = this.posZ;
//		this.moveEntity(this.motionX, this.motionY, this.motionZ);
//
//		if(this.particleMaxAge < 20) {
//			this.setSize(0.005f*particleMaxAge, 0.005f*particleMaxAge);
//		}
//		
//		if (this.particleMaxAge-- <= 0)
//			this.setDead();
//		
//
//	}

}
