package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventKeyTyped extends Event {

	char character;
	int key;

	public EventKeyTyped(IWidget origin, char character, int key) {
		super(origin);
		this.character = character;
		this.key = key;
	}

	public char getCharacter() {
		return character;
	}

	public int getKey() {
		return key;
	}

}
