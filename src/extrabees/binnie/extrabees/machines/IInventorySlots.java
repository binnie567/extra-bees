package binnie.extrabees.machines;

public interface IInventorySlots {

	void addSlot(int index);

	void addSlotArray(int[] indexes);

	InventorySlot getSlot(int index);

	InventorySlot[] getSlots(int[] indexes);

	InventorySlot[] getAllSlots();

}
