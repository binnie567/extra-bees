package binnie.craftgui.controls;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlText extends Control {

	private String text;
	private Alignment align;

	public ControlText(IWidget parent, float x, float y, String text,
			Alignment align) {
		super(parent, x, y, 0, 0);
		setPosition(x, y);
		setSize(getSuperParent().getRenderer().getTextWidth(text),
				getSuperParent().getRenderer().getTextHeight());
		this.text = text;
		this.align = align;
	}

	public enum Alignment {
		Left, Center, Right
	}

	@Override
	public void onRenderBackground() {

		int width = getSuperParent().getRenderer().getTextWidth(text);

		int offset;
		if (align == Alignment.Left)
			offset = 0;
		else if (align == Alignment.Right)
			offset = -width;
		else
			offset = -width / 2;

		getSuperParent().getRenderer().renderText(offset, 0, text, getColour());

	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}
