package binnie.extrabees.machines;

import forestry.api.apiculture.BeeManager;
import net.minecraft.src.ItemStack;

public class SlotValidatorBee extends SlotValidator {

	public enum Mode {
		Bee, Princess, Drone, Queen
	}

	public Mode mode;

	public SlotValidatorBee(Mode mode) {
		this.mode = mode;
	}

	@Override
	public boolean isValid(ItemStack itemStack) {

		if (!BeeManager.beeInterface.isBee(itemStack))
			return false;

		switch (mode) {
		case Bee:
			return true;

		case Princess:
			return !BeeManager.beeInterface.isDrone(itemStack)
					&& !BeeManager.beeInterface.isMated(itemStack);

		case Queen:
			return !BeeManager.beeInterface.isDrone(itemStack)
					&& BeeManager.beeInterface.isMated(itemStack);

		case Drone:
			return BeeManager.beeInterface.isDrone(itemStack);

		default:
			break;
		}
		return false;
	}

}
