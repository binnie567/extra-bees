package binnie.extrabees.apiary;

import binnie.extrabees.core.ExtraBeeItem;
import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IHiveFrame;
import forestry.api.core.ItemInterface;

public enum EnumHiveFrame implements IHiveFrame {

	Cocoa("Chocolate Frame"), Cage("Restraint Frame"), Soul("Soul Frame"), Clay(
			"Healing Frame"), Debug("Nova Frame")

	;

	Item item;

	public static void init() {
		Cocoa.setLifespan(0.5f);
		Cocoa.setProduction(1.5f);

		Cage.setTerritory(0.5f);
		Cage.setLifespan(0.75f);
		Cage.setProduction(0.75f);

		Soul.setMutation(1.5f);
		Soul.setLifespan(0.75f);
		Soul.setProduction(0.25f);
		Soul.setMaxDamage(80);

		Clay.setLifespan(1.5f);
		Clay.setMutation(0.5f);
		Clay.setProduction(0.75f);

		Debug.setLifespan(0.0001f);

		for (EnumHiveFrame frame : EnumHiveFrame.values()) {
			frame.item = new ItemHiveFrame(frame);
		}

		ModLoader.addRecipe(new ItemStack(Cocoa.item), new Object[] { " c ",
				"cFc", " c ", 'F', ItemInterface.getItem("frameImpregnated"),
				'c', new ItemStack(Item.dyePowder, 1, 3) });

		ModLoader.addShapelessRecipe(new ItemStack(Cage.item), new Object[] {
				ItemInterface.getItem("frameImpregnated"), Block.fenceIron });

		ModLoader.addShapelessRecipe(new ItemStack(Soul.item), new Object[] {
				ItemInterface.getItem("frameImpregnated"), Block.slowSand });

		ModLoader.addRecipe(new ItemStack(Clay.item), new Object[] { " c ",
				"cFc", " c ", 'F', ItemInterface.getItem("frameImpregnated"),
				'c', Item.clay });
	}

	public int getIconIndex() {
		return 55 + ordinal();
	}

	public int getItemID() {
		return ExtraBeeItem.hiveFrameID + ordinal();
	}

	public void setMutation(float mutation) {
		this.mutation = mutation;
	}

	public void setTerritory(float territory) {
		this.territory = territory;
	}

	public void setLifespan(float lifespan) {
		this.lifespan = lifespan;
	}

	public void setProduction(float production) {
		this.production = production;
	}

	public void setMaxDamage(int damage) {
		this.maxDamage = damage;
	}

	float mutation = 1.0f;
	float territory = 1.0f;
	float lifespan = 1.0f;
	float production = 1.0f;
	int maxDamage = 240;

	String name;

	EnumHiveFrame(String name) {
		this.name = name;
	}

	@Override
	public float getTerritoryModifier(IBeeGenome genome) {
		return territory;
	}

	@Override
	public float getMutationModifier(IBeeGenome genome, IBeeGenome mate) {
		return mutation;
	}

	@Override
	public float getLifespanModifier(IBeeGenome genome, IBeeGenome mate) {
		return lifespan;
	}

	@Override
	public float getProductionModifier(IBeeGenome genome) {
		return production;
	}

	@Override
	public ItemStack frameUsed(World world, ItemStack frame, IBee queen,
			int wear) {
		frame.setItemDamage(frame.getItemDamage() + wear);
		if (frame.getItemDamage() >= frame.getMaxDamage())
			return null;
		else
			return frame;
	}

}
