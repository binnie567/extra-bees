package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import binnie.core.BinnieCore;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesGenome extends PageSpecies {

	public PageSpeciesGenome(IWidget parent) {
		super(parent);

		pageSpeciesGenome_Title = new ControlText(this, 72, 8, "Genome",
				ControlText.Alignment.Center);

		new ControlText(this, 64, 32, "Speed:", ControlText.Alignment.Right);
		new ControlText(this, 64, 44, "Lifespan:", ControlText.Alignment.Right);
		new ControlText(this, 64, 56, "Fertility:", ControlText.Alignment.Right);
		new ControlText(this, 64, 68, "Flowering:", ControlText.Alignment.Right);
		new ControlText(this, 64, 80, "Territory:", ControlText.Alignment.Right);
		new ControlText(this, 64, 97, "Night:", ControlText.Alignment.Right);
		new ControlText(this, 64, 109, "Darkness:", ControlText.Alignment.Right);
		new ControlText(this, 64, 121, "Rain:", ControlText.Alignment.Right);
		new ControlText(this, 64, 138, "Flower:", ControlText.Alignment.Right);
		new ControlText(this, 64, 155, "Effect:", ControlText.Alignment.Right);

		pageSpeciesGenome_SpeedText = new ControlText(this, 72, 32, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_LifespanText = new ControlText(this, 72, 44, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_FertilityText = new ControlText(this, 72, 56, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_FloweringText = new ControlText(this, 72, 68, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_TerritoryText = new ControlText(this, 72, 80, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_NocturnalText = new ControlText(this, 72, 97, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_CaveDwellingText = new ControlText(this, 72, 109, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_TolerantFlyerText = new ControlText(this, 72, 121,
				"", ControlText.Alignment.Left);
		pageSpeciesGenome_FlowerText = new ControlText(this, 72, 138, "",
				ControlText.Alignment.Left);
		pageSpeciesGenome_EffectText = new ControlText(this, 72, 155, "",
				ControlText.Alignment.Left);
	}

	ControlText pageSpeciesGenome_Title;
	ControlText pageSpeciesGenome_SpeedText;
	ControlText pageSpeciesGenome_LifespanText;
	ControlText pageSpeciesGenome_FertilityText;
	ControlText pageSpeciesGenome_FloweringText;
	ControlText pageSpeciesGenome_TerritoryText;

	ControlText pageSpeciesGenome_NocturnalText;
	ControlText pageSpeciesGenome_CaveDwellingText;
	ControlText pageSpeciesGenome_TolerantFlyerText;

	ControlText pageSpeciesGenome_FlowerText;

	ControlText pageSpeciesGenome_EffectText;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		IAllele[] template = BeeManager.breedingManager.getBeeTemplate(species
				.getUID());

		if (template != null) {
			IBeeGenome genome = BeeManager.beeInterface
					.templateAsGenome(template);
			IBee bee = BeeManager.beeInterface.getBee(
					BinnieCore.proxy.getWorld(), genome);

			pageSpeciesGenome_SpeedText.setText(rateSpeed(genome.getSpeed()));
			pageSpeciesGenome_LifespanText.setText(rateLifespan(genome
					.getLifespan()));
			pageSpeciesGenome_FertilityText.setText(genome.getFertility()
					+ " children");
			ControlText pageSpeciesGenome_FloweringText;
			int[] area = genome.getTerritory();
			pageSpeciesGenome_TerritoryText.setText(area[0] + "x" + area[1]
					+ "x" + area[2]);

			pageSpeciesGenome_NocturnalText.setText(tolerated(genome
					.getNocturnal()));
			pageSpeciesGenome_CaveDwellingText.setText(tolerated(genome
					.getCaveDwelling()));
			pageSpeciesGenome_TolerantFlyerText.setText(tolerated(genome
					.getTolerantFlyer()));

			if (genome.getFlowerProvider() != null)
				pageSpeciesGenome_FlowerText.setText(genome.getFlowerProvider()
						.getDescription());
			else
				pageSpeciesGenome_FlowerText.setText("None");
			;

			pageSpeciesGenome_EffectText.setText(genome.getEffect()
					.getIdentifier());
			;

		}
	}

	public static String rateSpeed(float speed) {
		if (speed >= 1.7f)
			return "Fastest";
		else if (speed >= 1.4f)
			return "Faster";
		else if (speed >= 1.2f)
			return "Fast";
		else if (speed >= 1.0f)
			return "Normal";
		else if (speed >= 0.8f)
			return "Slow";
		else if (speed >= 0.6f)
			return "Slower";
		else
			return "Slowest";
	}

	public static String rateLifespan(int life) {
		if (life >= 70)
			return "Longest";
		else if (life >= 60)
			return "Longer";
		else if (life >= 50)
			return "Long";
		else if (life >= 45)
			return "Elongated";
		else if (life >= 40)
			return "Normal";
		else if (life >= 35)
			return "Shortened";
		else if (life >= 30)
			return "Short";
		else if (life >= 20)
			return "Shortest";
		else
			return "Shorted";
	}

	public static String tolerated(boolean t) {
		if (t)
			return "Tolerated";
		else
			return "Not Tolerated";

	}

}
