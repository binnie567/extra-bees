package binnie.extrabees.network;

import java.io.DataInputStream;
import java.io.IOException;

import binnie.core.BinnieCore;
import binnie.core.network.INetworkedEntity;
import binnie.core.network.PacketCoordinates;
import binnie.core.network.PacketUpdate;
import binnie.extrabees.machines.TileEntityIndexer;
import binnie.extrabees.machines.TileEntityMachine;

import net.minecraft.src.INetworkManager;
import net.minecraft.src.Packet250CustomPayload;
import net.minecraft.src.TileEntity;
import cpw.mods.fml.common.network.Player;

public enum PacketID {

	NetworkEntityUpdate, ITankContainerUpdate, ClientConnected, // Used to
																// request to
																// send all data
																// to new
																// clients
	IndexerChanged

	;

	public void onPacketData(INetworkManager network,
			Packet250CustomPayload packet250, Player player,
			DataInputStream data) {
		if (this == NetworkEntityUpdate) {
			try {
				PacketUpdate packet = new PacketUpdate();
				packet.readData(data);
				TileEntity tile = packet.getTileEntity(BinnieCore.proxy
						.getWorld());
				if (tile instanceof INetworkedEntity)
					((INetworkedEntity) tile).fromPacketPayload(packet.payload);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == ITankContainerUpdate) {
			try {
				PacketITankContainer packet = new PacketITankContainer();
				packet.readData(data);
				TileEntity tile = packet.getTileEntity(BinnieCore.proxy
						.getWorld());
				if (tile instanceof TileEntityMachine)
					((TileEntityMachine) tile).setLiquidTanks(packet.tanks);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == ClientConnected) {
			try {
				PacketCoordinates packet = new PacketCoordinates();
				packet.readData(data);
				TileEntity tile = packet.getTileEntity(BinnieCore.proxy
						.getWorld());
				if (tile instanceof INetworkedEntity)
					((INetworkedEntity) tile).clientConnected();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		else if (this == IndexerChanged) {
			try {
				PacketIndexerChanged packet = new PacketIndexerChanged();
				packet.readData(data);
				TileEntity tile = packet.getTileEntity(BinnieCore.proxy
						.getWorld());
				if (tile instanceof TileEntityIndexer)
					((TileEntityIndexer) tile).loadFromPacket(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
