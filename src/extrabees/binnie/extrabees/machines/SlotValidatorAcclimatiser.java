package binnie.extrabees.machines;

import net.minecraft.src.ItemStack;

public class SlotValidatorAcclimatiser extends SlotValidator {

	@Override
	public boolean isValid(ItemStack itemStack) {

		return LogicAcclimatiser.isAcclimatiserItem(itemStack);
	}

}
