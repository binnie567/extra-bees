package binnie.logicpipes;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;

public class ControlAction extends Control implements ITooltip {

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/gui-logicpipes.png";
	}

	ILogicAction action;

	public ILogicAction getAction() {
		return action;
	}

	public void setAction(ILogicAction action) {
		this.action = action;
	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderRect(0, 0, 18, 18, 0, 0);

		if (getSuperParent().getMousedOverWidget() == this) {

			//getRenderer().renderGradientRect(1, 1, 16, 16, -2130706433,
			//		-2130706433);
		}

	}

	@Override
	public void onRenderForeground() {

		if (action != null) {
			getRenderer().setTexture(action.getTextureFile());
			int index = action.getIndexInTexture();
			getRenderer().renderRect(1, 1, 16, 16, index % 16, index / 16);
		}

	}

	protected ControlAction(IWidget parent, float x, float y, ILogicAction action) {
		super(parent, x, y, 18, 18);
		this.canMouseOver = true;
		this.action = action;
	}

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();
		if (action != null) {
			tooltip.add(action.getDescription());
			tooltip.addAll(action.getTooltip());
		}

		return tooltip;
	}

}
