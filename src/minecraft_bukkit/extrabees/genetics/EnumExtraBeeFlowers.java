package extrabees.genetics;

import forestry.api.apiculture.IAlleleFlowers;
import forestry.api.apiculture.IFlowerProvider;
import forestry.api.genetics.AlleleManager;
import net.minecraft.server.*;

public enum EnumExtraBeeFlowers implements IFlowerProvider, IAlleleFlowers
{
    WATER,
    SUGAR,
    ROCK,
    BOOK,
    NONE,
    REDSTONE;

    public int id;
    public String name;
    public boolean dominant;

    public static void setup(int i)
    {
        WATER.init("Lily Pad", i++);
        SUGAR.init("Reeds", i++);
        ROCK.init("Rock", i++);
        BOOK.init("Books", i++);
        NONE.init("None", i++);
        REDSTONE.init("Redstone", i++);
    }

    public void init(String s, int i)
    {
        id = i;
        name = s;
        AlleleManager.alleleList[i] = this;
    }

    private EnumExtraBeeFlowers()
    {
        id = 0;
        name = "??? Flowers";
        dominant = true;
    }

    public int getId()
    {
        return id;
    }

    public boolean isDominant()
    {
        return dominant;
    }

    public IFlowerProvider getProvider()
    {
        return this;
    }

    public boolean isAcceptedFlower(World world, int i, int j, int k, int l)
    {
   
        switch (this)
        {
            case WATER:
                return world.getTypeId(j, k, l) == Block.WATER_LILY.id;

            case ROCK:
                return world.getMaterial(j, k, l) == Material.STONE;

            case SUGAR:
                return world.getTypeId(j, k, l) == Block.SUGAR_CANE_BLOCK.id;

            case BOOK:
                return world.getTypeId(j, k, l) == Block.BOOKSHELF.id;

            case REDSTONE:
                return world.getTypeId(j, k, l) == Block.REDSTONE_TORCH_ON.id;

            case NONE:
                return true;
        }

        return false;
    }

    public boolean growFlower(World world, int i, int j, int k, int l)
    {
        switch (this)
        {
            case WATER:
                if (world.isEmpty(j, k, l) && world.getTypeId(j, k - 1, l) == Block.STATIONARY_WATER.id)
                {
                    return world.setTypeId(j, k, l, Block.WATER_LILY.id);
                }
                else
                {
                    return false;
                }

            case SUGAR:
                if (world.getTypeId(j, k, l) == Block.SUGAR_CANE_BLOCK.id && world.isEmpty(j, k + 1, l))
                {
                    return world.setTypeId(j, k + 1, l, Block.SUGAR_CANE_BLOCK.id);
                }
                else
                {
                    return false;
                }
		default:
			break;
        }

        return true;
    }

    public String getDescription()
    {
        return name;
    }
}
