package extrabees.genetics;

import extrabees.core.ExtraBeeBlock;
import forestry.api.apiculture.*;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IEffectData;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import net.minecraft.server.*;

public enum EnumExtraBeeEffect implements IAlleleBeeEffect {
	ECTOPLASM, ACID, SPAWN_ZOMBIE, SPAWN_SKELETON, SPAWN_CREEPER, LIGHTNING;

	public boolean combinable;
	public String name;
	public boolean dominant;
	public int id;
	public static Method FX;

	private EnumExtraBeeEffect() {
		combinable = false;
		name = "??? Effect";
		dominant = true;
		id = 0;
	}

	public static void setup(int i) {
		ECTOPLASM.init("Ectoplasm", i++);
		ACID.init("Acidic", i++);
		SPAWN_ZOMBIE.init("Zombies", i++);
		SPAWN_SKELETON.init("Skeletons", i++);
		SPAWN_CREEPER.init("Creepers", i++);
		LIGHTNING.init("Lightning", i++);
		ModLoader.getLogger().fine("Trying to implement forestry hive fx...");

		try {
			FX = Class.forName("forestry.apiculture.ProxyApiculture")
					.getMethod(
							"addBeeHiveFX",
							new Class[] { java.lang.String.class,
									net.minecraft.server.World.class,
									Double.TYPE, Double.TYPE, Double.TYPE,
									Integer.TYPE });
			ModLoader.getLogger().fine("Succeeded.");
		} catch (Exception exception) {
			ModLoader.getLogger().fine(
					(new StringBuilder()).append("Failed: ")
							.append(exception.getMessage()).toString());
		}
	}

	public void init(String s, int i) {
		name = s;
		id = i;
		AlleleManager.alleleList[i] = this;
	}

	public boolean isCombinable() {
		return combinable;
	}

	public IEffectData validateStorage(IEffectData ieffectdata) {
		return ieffectdata;
	}

	public String getIdentifier() {
		return name;
	}

	public int getId() {
		return id;
	}

	public boolean isDominant() {
		return dominant;
	}

	public IEffectData doEffect(IBeeGenome ibeegenome, IEffectData ieffectdata,
			World world, int i, int j, int k, int l) {
		int[] ai = ibeegenome.getTerritory();

		switch (this) {
		case ECTOPLASM:
			if (world.random.nextInt(100) >= 4) {
				break;
			}

			int i1 = 1 + ai[0] / 2;
			int l1 = 1 + ai[1] / 2;
			int k2 = 1 + ai[2] / 2;
			int j3 = (j - i1) + world.random.nextInt(2 * i1 + 1);
			int i4 = (k - l1) + world.random.nextInt(2 * l1 + 1);
			int l4 = (l - k2) + world.random.nextInt(2 * k2 + 1);

			if (world.isEmpty(j3, i4, l4)
					&& (world.e(j3, i4 - 1, l4) || world.getTypeId(j3, i4 - 1,
							l4) == ExtraBeeBlock.ectoplasm.id)) {
				world.setTypeId(j3, i4, l4, ExtraBeeBlock.ectoplasm.id);
			}

			return null;

		case ACID: {
			if (world.random.nextInt(100) < 6) {
				int j1 = 1 + ai[0] / 2;
				int i2 = 1 + ai[1] / 2;
				int l2 = 1 + ai[2] / 2;
				int k3 = (j - j1) + world.random.nextInt(2 * j1 + 1);
				int j4 = (k - i2) + world.random.nextInt(2 * i2 + 1);
				int i5 = (l - l2) + world.random.nextInt(2 * l2 + 1);
				doAcid(world, k3, j4, i5);
			}

			break;
		}

		case SPAWN_ZOMBIE: {
			if (world.random.nextInt(200) < 2) {
				spawnMob(world, j, k, l, "Zombie");
			}
			break;

		}

		case SPAWN_SKELETON: {
			if (world.random.nextInt(200) < 2) {
				spawnMob(world, j, k, l, "Skeleton");
			}
			break;
		}

		case SPAWN_CREEPER: {
			if (world.random.nextInt(200) < 2) {
				spawnMob(world, j, k, l, "Creeper");
			}

			break;
		}

		case LIGHTNING: {
			if (world.random.nextInt(100) >= 1) {
				break;
			}

			int k1 = 1 + ai[0] / 2;
			int j2 = 1 + ai[1] / 2;
			int i3 = 1 + ai[2] / 2;
			int l3 = (j - k1) + world.random.nextInt(2 * k1 + 1);
			int k4 = (k - j2) + world.random.nextInt(2 * j2 + 1);
			int j5 = (l - i3) + world.random.nextInt(2 * i3 + 1);

			if (world.isChunkLoaded(l3, k4, j5)) {
				world.addEntity(new EntityWeatherLighting(world, l3, k4, j5));
			}

			break;
		}

		}

		return null;
	}

	public void spawnMob(World world, int i, int j, int k, String s) {
		if (anyPlayerInRange(world, i, j, k, 16)) {
			double d = (float) i + world.random.nextFloat();
			double d1 = (float) j + world.random.nextFloat();
			double d2 = (float) k + world.random.nextFloat();
			world.a("smoke", d, d1, d2, 0.0D, 0.0D, 0.0D);
			world.a("flame", d, d1, d2, 0.0D, 0.0D, 0.0D);
			EntityLiving entityliving = (EntityLiving) EntityTypes
					.createEntityByName(s, world);

			if (entityliving == null) {
				return;
			}

			int l = world.a(
					entityliving.getClass(),
					AxisAlignedBB.b(i, j, k, i + 1, j + 1, k + 1).grow(8D, 4D,
							8D)).size();

			if (l >= 6) {
				return;
			}

			if (entityliving != null) {
				double d3 = (double) i
						+ (world.random.nextDouble() - world.random
								.nextDouble()) * 4D;
				double d4 = (j + world.random.nextInt(3)) - 1;
				double d5 = (double) k
						+ (world.random.nextDouble() - world.random
								.nextDouble()) * 4D;
				entityliving.setPositionRotation(d3, d4, d5,
						world.random.nextFloat() * 360F, 0.0F);

				if (entityliving.canSpawn()) {
					world.addEntity(entityliving);
					world.triggerEffect(2004, i, j, k, 0);
					entityliving.aC();
				}
			}
		}
	}

	private boolean anyPlayerInRange(World world, int i, int j, int k, int l) {
		return world.findNearbyPlayer((double) i + 0.5D, (double) j + 0.5D,
				(double) k + 0.5D, l) != null;
	}

	public static void doAcid(World world, int i, int j, int k) {
		int l = world.getTypeId(i, j, k);

		if (l == Block.COBBLESTONE.id || l == Block.STONE.id) {
			world.setTypeId(i, j, k, Block.GRAVEL.id);
		} else if ((l == Block.DIRT.id) | (l == Block.GRASS.id)) {
			world.setTypeId(i, j, k, Block.SAND.id);
		}
	}

	public IEffectData doFX(IBeeGenome ibeegenome, IEffectData ieffectdata,
			World world, int i, int j, int k, int l) {
		try {
			FX.invoke(
					null,
					new Object[] {
							"/gfx/forestry/particles/swarm_bee.png",
							world,
							Integer.valueOf(j),
							Integer.valueOf(k),
							Integer.valueOf(l),
							Integer.valueOf(ibeegenome.getPrimaryAsBee()
									.getPrimaryColor()) });
		} catch (Exception exception) {
		}

		return ieffectdata;
	}
}
