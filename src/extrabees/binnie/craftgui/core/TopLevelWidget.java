package binnie.craftgui.core;

import org.lwjgl.input.Mouse;

import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.events.EventMouseUp;

public class TopLevelWidget extends Widget implements ITopLevelWidget {

	public TopLevelWidget() {
		super(null);
	}

	IWidget mousedOverWidget = null;
	IWidget draggedWidget = null;
	IWidget focusedWidget = null;

	public void setMousedOverWidget(IWidget widget) {
		if (mousedOverWidget == widget)
			return;
		if (mousedOverWidget != null)
			mousedOverWidget.onEndMouseOver();
		mousedOverWidget = widget;
		if (mousedOverWidget != null)
			mousedOverWidget.onStartMouseOver();
	}

	public void setDraggedWidget(IWidget widget) {
		if (draggedWidget == widget)
			return;
		if (draggedWidget != null)
			draggedWidget.onEndDrag();
		draggedWidget = widget;
		if (draggedWidget != null)
			draggedWidget.onStartDrag();
	}

	public void setFocusedWidget(IWidget widget) {
		IWidget newWidget = widget;
		if (focusedWidget == newWidget)
			return;
		if (newWidget != null && !newWidget.canFocus())
			newWidget = null;
		if (focusedWidget != null)
			focusedWidget.onLoseFocus();
		focusedWidget = newWidget;
		if (focusedWidget != null)
			focusedWidget.onGainFocus();
	}

	@Override
	public IWidget getMousedOverWidget() {
		return mousedOverWidget;
	}

	@Override
	public IWidget getDraggedWidget() {
		return draggedWidget;
	}

	@Override
	public IWidget getFocusedWidget() {
		return focusedWidget;
	}

	@Override
	public boolean isMouseOver(IWidget widget) {
		return getMousedOverWidget() == widget;
	}

	@Override
	public boolean isDragged(IWidget widget) {
		return getDraggedWidget() == widget;
	}

	@Override
	public boolean isFocused(IWidget widget) {
		return getFocusedWidget() == widget;
	}

	@Override
	public void updateTopLevel() {

		setMousedOverWidget(super.calculateMousedOverWidget());

		if (Mouse.isButtonDown(0)) {
		} else {
			if (draggedWidget != null)
				setDraggedWidget(null);
		}
	}

	@Override
	public void onMouseClick(EventMouseClick event) {
		setDraggedWidget(mousedOverWidget);
		setFocusedWidget(mousedOverWidget);
	}

	@Override
	public void onMouseUp(EventMouseUp event) {
		setDraggedWidget(null);
	}

	protected Vector2f mousePosition = new Vector2f(0.0f, 0.0f);

	@Override
	public void setMousePosition(int x, int y) {
		mousePosition = new Vector2f(x, y);
	}

	@Override
	public Vector2f getAbsoluteMousePosition() {
		return mousePosition;
	}

	IRenderer renderer;

	@Override
	public void setRenderer(IRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public IRenderer getTopLevelRenderer() {
		return renderer;
	}

}
