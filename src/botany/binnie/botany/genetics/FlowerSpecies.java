package binnie.botany.genetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.src.Achievement;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBranch;

public enum FlowerSpecies implements IAlleleFlowerSpecies, IColour {

	Default("Default Flower", "", "Defaulta defaulta", 0x000000, 1),

	Rose("Rose", "", "Rosa indica", RED, 1), DogRose("Dog Rose", "",
			"Rosa canina", PINK, 1), FrenchRose("French Rose", "",
			"Rosa gallica", MAGENTA, 1), WhiteRose("Lady Banks' Rose", "",
			"Rosa banksianae", WHITE, 1), AustrianBriar("Austrian Briar", "",
			"Rosa foetida", YELLOW, 1),

	Dandelion("Dandelion", "", "Taraxacum officinale", YELLOW, 3), Chicory(
			"Common Chicory", "", "Cichorium intybus", LBLUE, 3), Salsify(
			"Common Salsify", "", "Tragopogon porrifolius", PURPLE, 3), DesertDandelion(
			"Cliff Desertdandelion", "", "Malacothrix saxatilis", WHITE, 3),

	AfricanMarigold("African Marigold", "", "Tagetes erecta", ORANGE, ORANGE,
			5, 7), FrenchMarigold("French Marigold", "", "Tagetes erecta",
			ORANGE, RED, 5, 7), CornMarigold("Corn Marigold", "",
			"Tagetes erecta", YELLOW, WHITE, 5, 7), DesertMarigold(
			"Desert Marigold", "", "Tagetes erecta", YELLOW, YELLOW, 5, 7),

	Cornflower("Cornflower", "", "Centaurea cyanus", BLUE, LBLUE, 9, 11), PerennialCornflower(
			"Perennial Cornflower", "", "Centaurea montana", MAGENTA, PURPLE,
			9, 11), AmericanStarthistle("American Starthistle", "",
			"Centaurea americana", WHITE, PINK, 9, 11), PersianCornflower(
			"Persian Cornflower", "", "Centaurea Dealbata", PINK, PURPLE, 9, 11),

	FieldPansy("Field Pansy", "", "Viola arvensis", YELLOW, WHITE, 13, 15), NativeViolet(
			"Native Violet", "", "Viola banksii", PURPLE, WHITE, 13, 15), YellowPansy(
			"Yellow Pansy", "", "Viola pedunculata", YELLOW, YELLOW, 13, 15), FenViolet(
			"Fen Violet", "", "Viola persicifolia", WHITE, WHITE, 13, 15), BlueViolet(
			"Blue Violet", "", "Viola sororia", PURPLE, PURPLE, 13, 15),

	BloodIris("Blood Iris", "", "Iris sanguinea", PURPLE, 17), MunzIris(
			"Munz's Iris", "", "Iris munzii", WHITE, 17), YellowIris(
			"Yellow Iris", "", "Iris pseudacorus", YELLOW, 17), EnglishIris(
			"English Iris", "", "Iris latifolia", BLUE, 17), ;

	private FlowerSpecies(String name, String description, String binomial,
			int colour, int iconIndex) {
		this(name, description, binomial, colour, colour, iconIndex, iconIndex);
	}

	private FlowerSpecies(String name, String description, String binomial,
			int primaryColor, int secondaryColor, int primaryIconIndex,
			int secondaryIconIndex) {
		this.name = name;
		this.description = description;
		this.binomial = binomial;
		this.primaryColor = primaryColor;
		this.secondaryColor = secondaryColor;
		this.primaryIconIndex = primaryIconIndex;
		this.secondaryIconIndex = secondaryIconIndex;
	}

	String name;
	String description;
	String binomial;
	int primaryColor;;
	int secondaryColor;
	int primaryIconIndex;
	int secondaryIconIndex;
	String texture = "/gfx/botany/botany.png";

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public int getBodyType() {
		return 0;
	}

	@Override
	public int getPrimaryColor() {
		return primaryColor;
	}

	@Override
	public int getSecondaryColor() {
		return secondaryColor;
	}

	@Override
	public EnumTemperature getTemperature() {
		return null;
	}

	@Override
	public EnumHumidity getHumidity() {
		return null;
	}

	@Override
	public boolean hasEffect() {
		return false;
	}

	@Override
	public boolean isSecret() {
		return false;
	}

	@Override
	public boolean isCounted() {
		return false;
	}

	@Override
	public String getBinomial() {
		return null;
	}

	@Override
	public String getAuthority() {
		return "Binnie";
	}

	@Override
	public IBranch getBranch() {
		return null;
	}

	@Override
	public String getUID() {
		return "botany.flowers.species." + this.toString();
	}

	@Override
	public boolean isDominant() {
		return false;
	}

	@Override
	public String getTextureFile() {
		return texture;
	}

	@Override
	@Deprecated
	public Achievement getAchievement() {
		return null;
	}

	@Override
	public int getPrimaryIconIndex() {
		return primaryIconIndex;
	}

	public IAllele[] getTemplate() {
		Random rand = new Random();

		IAllele[] template = new IAllele[2];
		template[0] = this;
		template[1] = AlleleFlowerStem.values()[rand.nextInt(AlleleFlowerStem
				.values().length)];
		return template;
	}

	public static IAllele[][] getTemplates() {

		List<IAllele[]> templates = new ArrayList<IAllele[]>();

		for (FlowerSpecies species : FlowerSpecies.values()) {
			templates.add(species.getTemplate());
		}

		return templates.toArray(new IAllele[][] {});

	}

	@Override
	public int getSecondaryIconIndex() {
		return secondaryIconIndex;
	}

}
