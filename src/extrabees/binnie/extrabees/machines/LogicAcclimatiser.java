package binnie.extrabees.machines;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import binnie.core.BinnieCore;
import binnie.extrabees.liquids.ItemLiquid;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.apiculture.IAlleleFlowers;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.apiculture.IFlowerProvider;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.EnumTolerance;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;

public class LogicAcclimatiser {

	static Set<ItemStack> acclimatiserItems = new HashSet<ItemStack>();

	static Map<ItemStack, Integer> temperatureItems = new HashMap<ItemStack, Integer>();
	static Map<ItemStack, Integer> humidityItems = new HashMap<ItemStack, Integer>();
	static Map<ItemStack, IAlleleFlowers> flowerItems = new HashMap<ItemStack, IAlleleFlowers>();

	public static void addTemperatureItem(ItemStack itemstack, int amount) {
		if (itemstack == null)
			return;
		if (acclimatiserItems.add(itemstack))
			;
		temperatureItems.put(itemstack, amount);
	}

	public static void addFlowerItem(ItemStack itemstack, IAlleleFlowers allele) {
		if (itemstack == null)
			return;
		if (acclimatiserItems.add(itemstack))
			;
		flowerItems.put(itemstack, allele);
	}

	public static void addHumidityItem(ItemStack itemstack, int amount) {
		if (itemstack == null)
			return;
		if (acclimatiserItems.add(itemstack))
			;
		humidityItems.put(itemstack, amount);
	}

	public static boolean isAcclimatiserItem(ItemStack item) {

		if (item == null)
			return false;

		for (ItemStack stack : acclimatiserItems)
			if (stack.isItemEqual(item))
				return true;
		return false;
	}

	public static int getTemperatureEffect(ItemStack item) {
		for (ItemStack stack : temperatureItems.keySet())
			if (stack.isItemEqual(item))
				return temperatureItems.get(stack);
		return 0;
	}

	public static int getHumidityEffect(ItemStack item) {
		for (ItemStack stack : humidityItems.keySet())
			if (stack.isItemEqual(item))
				return humidityItems.get(stack);
		return 0;
	}

	public static IAlleleFlowers getFlowerAllele(ItemStack item) {
		for (ItemStack stack : flowerItems.keySet())
			if (stack.isItemEqual(item))
				return flowerItems.get(stack);
		return null;
	}

	public static void postInit() {
		addTemperatureItem(new ItemStack(Item.blazePowder), 50);
		addTemperatureItem(new ItemStack(Item.blazeRod), 75);
		addTemperatureItem(new ItemStack(Item.bucketLava), 75);
		addTemperatureItem(new ItemStack(Item.snowball), -15);
		addTemperatureItem(new ItemStack(Block.ice), -75);

		addHumidityItem(new ItemStack(Item.bucketWater), 75);
		addHumidityItem(new ItemStack(Block.sand), -15);

		addTemperatureItem(ItemInterface.getItem("canLava"), 75);
		addTemperatureItem(ItemInterface.getItem("refractoryLava"), 75);

		addHumidityItem(ItemInterface.getItem("canWater"), 75);
		addHumidityItem(ItemInterface.getItem("refractoryWater"), 75);
		addHumidityItem(ItemInterface.getItem("waxCapsuleWater"), 75);

		addTemperatureItem(new ItemStack(ItemLiquid.waxCapsule, 1,
				ItemLiquid.EnumType.GLACIAL.ordinal()), -75);
		addTemperatureItem(new ItemStack(ItemLiquid.refactoryCapsule, 1,
				ItemLiquid.EnumType.GLACIAL.ordinal()), -75);
		addTemperatureItem(new ItemStack(ItemLiquid.can, 1,
				ItemLiquid.EnumType.GLACIAL.ordinal()), -75);
		addTemperatureItem(new ItemStack(ItemLiquid.bucket, 1,
				ItemLiquid.EnumType.GLACIAL.ordinal()), -75);

		for (IAllele allele : AlleleManager.alleleRegistry
				.getRegisteredAlleles().values()) {
			if (allele instanceof IAlleleFlowers) {
				ItemStack[] flowers = ((IAlleleFlowers) allele).getProvider()
						.getItemStacks();
				if (flowers != null) {
					for (ItemStack item : flowers)
						addFlowerItem(item, (IAlleleFlowers) allele);
				}
			}
		}
	}

	enum Type {
		Temperature, Humidity, Flower
	}

	public static Type getType(ItemStack itemStack) {
		if (getFlowerAllele(itemStack) != null)
			return Type.Flower;
		else if (getTemperatureEffect(itemStack) != 0)
			return Type.Temperature;
		else
			return Type.Humidity;
	}

	public static boolean canAcclimatise(ItemStack beeStack, ItemStack itemStack) {

		if (beeStack == null || itemStack == null)
			return false;

		if (!isAcclimatiserItem(itemStack))
			return false;

		Type type = getType(itemStack);

		IBee bee = BeeManager.beeInterface.getBee(beeStack);

		IBeeGenome genome = bee.getGenome();

		int temperatureTolerance = toleranceToInt(genome.getToleranceHumid());
		int humidityTolerance = toleranceToInt(genome.getToleranceHumid());

		switch (type) {
		case Temperature:
			int effect = getTemperatureEffect(itemStack);
			if (effect > 0 && temperatureTolerance < 5)
				return true;
			else if (effect < 0 && temperatureTolerance > -5)
				return true;
			else
				return false;

		case Humidity:
			effect = getHumidityEffect(itemStack);
			if (effect > 0 && humidityTolerance < 5)
				return true;
			else if (effect < 0 && humidityTolerance > -5)
				return true;
			else
				return false;

		case Flower:
			if (getFlowerAllele(itemStack) != null) {
				IFlowerProvider a = getFlowerAllele(itemStack).getProvider();
				IFlowerProvider b = genome.getFlowerProvider();
				return a != b;
			}
		}

		return false;
	}
	
	
	
	public static void alterAllele(ItemStack beeStack, EnumBeeChromosome chromosome, int allele, String UID) {
		
		if (beeStack == null)
			return;

		int chromsomeID = chromosome.ordinal()>=EnumBeeChromosome.FLOWER_PROVIDER.ordinal() ?
				chromosome.ordinal()-1 : chromosome.ordinal();

		NBTTagCompound beeNBT = beeStack.getTagCompound();

		NBTTagCompound genomeNBT = beeNBT.getCompoundTag("Genome");

		NBTTagList chromosomes = genomeNBT.getTagList("Chromosomes");

		NBTTagCompound chromosomeNBT = (NBTTagCompound) chromosomes
				.tagAt(chromsomeID);

		chromosomeNBT.setString("UID" + allele, UID);

		beeStack.setTagCompound(beeNBT);
		
	}
	
	

	public static void acclimatise(ItemStack beeStack, ItemStack itemStack) {

		Random rand = new Random();

		if (beeStack == null || itemStack == null)
			return;

		if (!isAcclimatiserItem(itemStack))
			return;

		Type type = getType(itemStack);

		IBee bee = BeeManager.beeInterface.getBee(beeStack);

		IBeeGenome genome = bee.getGenome();

		int temperatureTolerance = toleranceToInt(genome.getToleranceHumid());
		int humidityTolerance = toleranceToInt(genome.getToleranceHumid());

		switch (type) {
		case Temperature:

		case Humidity:

		case Flower:
			if (getFlowerAllele(itemStack) == null)
				return;

			IAllele primary = genome.getChromosomes()[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()].getPrimaryAllele();
			IAllele secondary = genome.getChromosomes()[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()].getPrimaryAllele();

			IAlleleFlowers flowerPrimary = (IAlleleFlowers) primary;
			IAlleleFlowers flowerSecondary = (IAlleleFlowers) secondary;

			IAlleleFlowers itemFlower = getFlowerAllele(itemStack);

			if (flowerPrimary != itemFlower) {
				if (rand.nextDouble() < getFlowerChance()) {
					flowerPrimary = itemFlower;
				}
			}

			if (flowerSecondary != itemFlower) {
				if (rand.nextDouble() < getFlowerChance()) {
					flowerSecondary = itemFlower;
				}
			}
			
			alterAllele(beeStack, EnumBeeChromosome.FLOWER_PROVIDER, 0, flowerPrimary.getUID());
			alterAllele(beeStack, EnumBeeChromosome.FLOWER_PROVIDER, 1, flowerSecondary.getUID());
			
			
		}

	}

	private static double getFlowerChance() {
		if(BinnieCore.proxy.isDebug())
			return 2.0f;
		return 0.5f;
	}

	public static IAllele[] getAlleles(IChromosome chromosome) {
		return new IAllele[] { chromosome.getPrimaryAllele(),
				chromosome.getSecondaryAllele() };
	}

	public static int toleranceToInt(EnumTolerance tolerance) {
		switch (tolerance) {
		case DOWN_5:
			return -5;

		case DOWN_4:
			return -4;

		case DOWN_3:
			return -3;

		case DOWN_2:
			return -2;

		case DOWN_1:
			return -1;

		case UP_1:
			return 1;

		case UP_2:
			return 2;

		case UP_3:
			return 3;

		case UP_4:
			return 4;

		case UP_5:
			return 5;

		default:
			return 0;
		}
	}

	public static EnumTolerance intToTolerance(Integer tolerance) {
		switch (tolerance) {
		case -5:
			return EnumTolerance.DOWN_5;

		case -4:
			return EnumTolerance.DOWN_4;

		case -3:
			return EnumTolerance.DOWN_3;

		case -2:
			return EnumTolerance.DOWN_2;

		case -1:
			return EnumTolerance.DOWN_1;

		case 1:
			return EnumTolerance.UP_1;

		case 2:
			return EnumTolerance.UP_2;

		case 3:
			return EnumTolerance.UP_3;

		case 4:
			return EnumTolerance.UP_4;

		case 5:
			return EnumTolerance.UP_5;

		default:
			return EnumTolerance.NONE;
		}
	}

}
