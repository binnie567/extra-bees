package binnie.extrabees.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import binnie.core.network.PacketCoordinates;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;

import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;

public class PacketITankContainer extends PacketCoordinates {

	public List<ILiquidTank> tanks;

	public PacketITankContainer() {
	}

	public PacketITankContainer(ITankContainer container) {
		this(0, 0, 0, container);
	}

	public PacketITankContainer(int posX, int posY, int posZ,
			ITankContainer container) {
		super(PacketID.ITankContainerUpdate.ordinal(), posX, posY, posZ);

		ILiquidTank[] containerTanks = container
				.getTanks(ForgeDirection.UNKNOWN);

		tanks = new ArrayList<ILiquidTank>();
		for (ILiquidTank tank : containerTanks)
			tanks.add(tank);
	}

	@Override
	public void writeData(DataOutputStream data) throws IOException {

		super.writeData(data);

		if (tanks == null) {
			data.writeInt(0);
			return;
		}

		data.writeInt(tanks.size());

		for (ILiquidTank tank : tanks) {
			data.writeInt(tank.getCapacity());
			if (tank.getLiquid() == null) {
				data.writeInt(0);
				data.writeInt(0);
				data.writeInt(0);
			} else {
				data.writeInt(tank.getLiquid().itemID);
				data.writeInt(tank.getLiquid().itemMeta);
				data.writeInt(tank.getLiquid().amount);
			}
		}

	}

	@Override
	public void readData(DataInputStream data) throws IOException {

		super.readData(data);

		int length = data.readInt();

		tanks = new ArrayList<ILiquidTank>();

		for (int i = 0; i < length; i++) {
			ILiquidTank tank = new LiquidTank(data.readInt());
			int id = data.readInt();
			int meta = data.readInt();
			int size = data.readInt();
			if (size == 0) {
				tank.setLiquid(null);
			} else {
				tank.setLiquid(new LiquidStack(id, size, meta));
			}
			tanks.add(tank);

		}

	}
}
