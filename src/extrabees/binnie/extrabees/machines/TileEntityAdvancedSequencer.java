package binnie.extrabees.machines;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IBee;
import binnie.core.network.PacketPayload;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.core.ExtraBeeItem;
import buildcraft.api.power.IPowerReceptor;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class TileEntityAdvancedSequencer extends TileEntityMachine implements
		IPowerReceptor {

	public static final int SlotBee = 2;
	public static final int SlotTemplateBlank = 3;
	public static final int SlotTemplateWritten = 4;

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Sequencer.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.AdvancedSequencer;
	}

	public TileEntityAdvancedSequencer() {
		super(30000, 4000, 50000);

		addSlot(SlotBee);
		getSlot(SlotBee).setValidator(
				new SlotValidatorBee(SlotValidatorBee.Mode.Bee));

		addSlot(SlotTemplateBlank);
		getSlot(SlotTemplateBlank).setValidator(
				new SlotValidatorAdvTemplate(
						SlotValidatorAdvTemplate.Mode.Blank));

		addSlot(SlotTemplateWritten);
		getSlot(SlotTemplateWritten).setValidator(
				new SlotValidatorAdvTemplate(
						SlotValidatorAdvTemplate.Mode.Written));
		getSlot(SlotTemplateWritten).setReadOnly();
	}

	public String name = " ";

	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
	}

	@Override
	public void onFinishTask() {
		IBee bee = BeeManager.beeInterface.getBee(getStackInSlot(SlotBee));

		float amount = LogicMachine.getSequencerAmount(getStackInSlot(SlotBee));

		if (amount > 0.0f) {

			setInventorySlotContents(SlotBee, null);

			ItemStack stack = new ItemStack(ExtraBeeItem.advancedTemplate, 1, 0);

			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setInteger("quality", this.worldObj.rand.nextInt(11));
			nbt.setString("name", name);
			nbt.setString("desc", "");
			nbt.setInteger("color", 0xFFFFFF);
			NBTTagCompound genome = new NBTTagCompound();
			bee.getGenome().writeToNBT(genome);
			nbt.setCompoundTag("genome", genome);
			stack.setTagCompound(nbt);

			setInventorySlotContents(SlotTemplateWritten, stack);
			decrStackSize(SlotTemplateBlank, 1);
		}
	}

	@Override
	public ErrorState canWork() {

		if (this.getStackInSlot(SlotTemplateBlank) == null)
			return new ErrorState("No Blank Templates");

		if (this.getStackInSlot(SlotTemplateWritten) != null)
			return new ErrorState("Written Template In Slot");

		if (this.getStackInSlot(SlotBee) == null)
			return new ErrorState("No Bee");

		return super.canWork();
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.setString("name", name);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		name = nbttagcompound.getString("name");

	}

	@Override
	public void writeToPacket(PacketPayload data) {
		data.addString(name);
	}

	@Override
	public void readFromPacket(PacketPayload data) {
		name = data.getString();
	}

}
