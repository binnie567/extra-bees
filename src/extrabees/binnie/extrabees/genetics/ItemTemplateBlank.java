package binnie.extrabees.genetics;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemTemplateBlank extends Item {

	public ItemTemplateBlank(int id) {
		super(id);
		setMaxStackSize(16);
		setIconIndex(32);
		setTextureFile(ExtraBeeTexture.Engineering.getTexture());
		setCreativeTab(CreativeTabs.tabMisc);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return "Blank Template";
	}

}
