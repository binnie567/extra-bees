package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlLiquidTank;
import binnie.craftgui.controls.ControlProgress.Direction;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntitySynthesizer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowSynthesizer extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntitySynthesizer machine;

	ControlSlot slotSerum;

	ControlLiquidTank tankDNA;

	ControlExtraBeeProgress progessWidget;

	public WindowSynthesizer(EntityPlayer player, IInventory inventory,
			Side side) {
		super(234, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntitySynthesizer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowSynthesizer(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Synthesizer");

		playerInventory = new ControlPlayerInventory(this);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		progessWidget = new ControlExtraBeeProgress(this, 133, 27, 30, 60, 30,
				158, 0, 158, Direction.Up,
				"/gfx/extrabees/extrabees-processes2.png");

		slotSerum = new ControlSlot(this, 140, 28 + 30 - 9);
		slotSerum.create(machine, TileEntitySynthesizer.SlotSerum);

		tankDNA = new ControlLiquidTank(this, 60, 28);
		tankDNA.setTankID(0);

		new ControlErrorState(this, 102, 50);

	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
