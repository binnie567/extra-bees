package binnie.craftgui.controls;

import binnie.craftgui.controls.button.ControlButton;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventCycleChanged;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.window.Panel;

public class ControlCycle extends Control {

	public static final String eventCycleIndexChange = "cycleIndexChange";

	public ControlCycle(IWidget parent, int x, int y, int width, int height,
			int buttonWidth) {
		super(parent, x, y, width, height);
		background = new Panel(this, 0, 0, width, height, Panel.Type.Black);
		buttonLeft = new ControlButton(this, 0, 0, buttonWidth, height, "<");
		buttonRight = new ControlButton(this, width - buttonWidth, 0,
				buttonWidth, height, ">");
		text = new ControlText(this, 0 + width / 2, 5, "",
				ControlText.Alignment.Center);
		setText();
	}

	public void setText() {
		text.setText(index + 1 + " / " + max);
	}

	public void setMax(int max) {
		this.max = max;
		if (index >= max)
			setIndex(index);
		setText();

	}

	public void setIndex(int index) {
		this.index = index;
		this.callEvent(new EventCycleChanged(this, index));
		setText();
	}

	@Override
	public void onHandleEvent(Event event) {
		if (event instanceof EventMouseClick) {
			if (event.isOrigin(buttonLeft)) {
				if (index > 0)
					setIndex(index - 1);
			} else if (event.isOrigin(buttonRight)) {
				if (index < max - 1)
					setIndex(index + 1);
			}
		}
	}

	int index = 0;
	int max = 5;

	ControlButton buttonLeft;
	ControlButton buttonRight;
	ControlText text;
	Panel background;

	public int getIndex() {
		return index;
	}

}
