package binnie.extrabees.proxy;

import binnie.core.BinnieCore;
import binnie.core.network.BinniePacket;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.liquids.ItemLiquid;
import binnie.extrabees.liquids.ItemMoltenMetal.MoltenMetal;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ExtraBees;

public class Proxy implements IExtraBeesProxy {

	@Override
	public void openGui(ExtraBeeGUI ID, EntityPlayer player, int x, int y, int z) {
		BinnieCore.proxy.openGui(ExtraBees.instance, ID.ordinal(), player, x,
				y, z);
	}

	@Override
	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z) {
		BinnieCore.proxy.sendNetworkPacket(packet, ExtraBees.channel, x, y, z);
	}

	@Override
	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer) {
		BinnieCore.proxy.sendToPlayer(packet, ExtraBees.channel, entityplayer);
	}

	@Override
	public void sendToServer(BinniePacket packet) {
		BinnieCore.proxy.sendToServer(packet, ExtraBees.channel);
	}

	public void addLiquidFX(ItemLiquid.EnumType liquid) {
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
	}

	@Override
	public void addMoltenFX(MoltenMetal type) {
		// TODO Auto-generated method stub
		
	}

}
