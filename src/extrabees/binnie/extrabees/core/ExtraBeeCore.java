package binnie.extrabees.core;

import java.util.ArrayList;
import binnie.extrabees.config.Config;
import binnie.extrabees.gen.BlockExtraBeeHive;
import binnie.extrabees.gen.HiveDrop;
import binnie.extrabees.gen.ItemBeehive;
import binnie.extrabees.gen.MaterialBeehive;
import binnie.extrabees.genetics.EnumExtraBeeSpecies;
import binnie.extrabees.genetics.ModuleGenetics;
import binnie.extrabees.liquids.ItemLiquid;
import binnie.extrabees.products.BlockEctoplasm;
import binnie.extrabees.products.ExtraBeeProductsCore;
import binnie.extrabees.products.ItemHoneyComb;
import binnie.extrabees.products.ItemHoneyDrop;
import cpw.mods.fml.common.network.NetworkRegistry;

import net.minecraft.src.Block;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraftforge.common.MinecraftForge;
import ic2.api.Ic2Recipes;
import ic2.api.Items;
import forestry.api.apiculture.IHiveDrop;
import forestry.api.core.BlockInterface;
import forestry.api.core.ItemInterface;
import forestry.api.fuels.EngineBronzeFuel;
import forestry.api.fuels.FuelManager;

public class ExtraBeeCore {

	public static boolean modIC2;
	public static boolean modRailcraft;
	public static boolean modBuildcraft;
	public static boolean modRedpower2;
	public static boolean modBetterFarming;
	public static boolean modPluginsforForestry;
	public static boolean modNEI;

	public static ArrayList<IHiveDrop>[] hiveDrops;
	public static String beeBreedingMode;

	public static boolean isAvailable() {
		return true;
	}

	public static void preInit() {

	}

	public static void loadConfig() {
		ModLoader.getLogger().fine("Loading Extra Bees Config");
		Config.load();
	}

	public static void checkExternalMods() {

		modIC2 = ModLoader.isModLoaded("IC2");
		modNEI = ModLoader.isModLoaded("mod_NotEnoughItems");
		modRailcraft = ModLoader.isModLoaded("Railcraft");
		modBuildcraft = ModLoader.isModLoaded("BuildCraft|Core");
		modRedpower2 = ModLoader.isModLoaded("RedPower2");

		modBetterFarming = ModLoader.isModLoaded("mod_BetterFarming");

		modPluginsforForestry = ModLoader.isModLoaded("mod_PluginsforForestry");

		if (modIC2) {
			ModLoader.getLogger().fine("Found IndustrialCraft 2");
		}

		if (modRailcraft) {
			ModLoader.getLogger().fine("Found Railcraft");
		}

		if (modBuildcraft) {
			ModLoader.getLogger().fine("Found Buildcraft");

			if (ModLoader.isModLoaded("BuildCraft|Energy")) {
				try {
					ExtraBeeExtMod.bcOil = (Block) Class
							.forName("buildcraft.BuildCraftEnergy")
							.getField("oilStill").get(null);
				} catch (Exception e) {
				}

				try {
					ExtraBeeExtMod.bcFuel = (Item) Class
							.forName("buildcraft.BuildCraftEnergy")
							.getField("fuel").get(null);
				} catch (Exception e) {
				}
			}
		}

		if (modRedpower2) {
			ModLoader.getLogger().fine("Found RedPower 2");
		}

		if (modBetterFarming) {
			ModLoader.getLogger().fine("Found Better Farming");
		}

		if (modPluginsforForestry) {
			ModLoader.getLogger().fine("Found Plugins for Forestry");
		}
	}

	public static void loadForestryConfigs() {
		try {
			Object option = Class.forName("forestry.core.config.Config")
					.getField("beeBreedingMode").get(null);
			Class enumB = Class.forName("forestry.core.EnumBreedingMode");
			beeBreedingMode = (String) enumB.getMethod("toString",
					new Class[] {}).invoke(option, new Object[] {});
			ModLoader.getLogger().fine(
					"Config option beeBreedingMode loaded from Forestry - "
							+ beeBreedingMode);
		} catch (Exception e) {
			ModLoader.getLogger().warning(
					"Could not load forestry config, due to java reflection failing!\n"
							+ e.getMessage());
			ModLoader.getLogger().fine(
					"Config option beeBreedingMode defaulting to NORMAL");
			beeBreedingMode = "NORMAL";
		}

	}

	public static void setupBlocks() {

		ExtraBeeBlock.materialBeehive = new MaterialBeehive();
		ExtraBeeBlock.hive = new BlockExtraBeeHive(Config.Main.getBlockId(
				"hive", ExtraBeeBlock.hiveID));
		ModLoader.registerBlock(ExtraBeeBlock.hive, ItemBeehive.class);

		ExtraBeeBlock.ectoplasm = new BlockEctoplasm(Config.Main.getBlockId(
				"ectoplasm", ExtraBeeBlock.ectoplasmID));
		ModLoader.registerBlock(ExtraBeeBlock.ectoplasm);
		ModLoader.addName(ExtraBeeBlock.ectoplasm, "Ectoplasm");

		MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 0, "scoop", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 1, "scoop", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 2, "scoop", 0);
		MinecraftForge.setBlockHarvestLevel(ExtraBeeBlock.hive, 3, "scoop", 0);

		ModLoader
				.addName(new ItemStack(ExtraBeeBlock.hive, 1, 0), "Water Hive");
		ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 1), "Rock Hive");
		ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 2),
				"Nether Hive");
		ModLoader.addName(new ItemStack(ExtraBeeBlock.hive, 1, 3),
				"Marble Hive");

		if (!Config.Main.getBoolean("general", "canQuarryMineHives", true)) {
			try {
				boolean[] blocks = (boolean[]) Class
						.forName("buildcraft.api.BuildCraftAPI")
						.getField("softBlocks").get(null);

				blocks[ExtraBeeBlock.hive.blockID] = true;
				blocks[BlockInterface.getBlock("beehives").getItem().shiftedIndex] = true;
			} catch (Exception e) {

			}
			try {
				boolean[] blocks = (boolean[]) Class
						.forName("buildcraft.api.API").getField("softBlocks")
						.get(null);

				blocks[ExtraBeeBlock.hive.blockID] = true;
				blocks[BlockInterface.getBlock("beehives").getItem().shiftedIndex] = true;
			} catch (Exception e) {

			}
		}

		NetworkRegistry.instance().registerGuiHandler(ExtraBees.instance,
				new ExtraBeeGUIHandler());

	}

	public static void setupLiquids() {
		ItemLiquid.setup(Config.Main
				.getBlockId("liquid", ExtraBeeItem.liquidID), Config.Main
				.getBlockId("liquidContainer", ExtraBeeItem.liquidContainerID));
	}

	public static void setupItems() {
		ExtraBeeProductsCore.doInit();

	}

	public static void setupGenetics() {
		ModuleGenetics.doInit();
		/*
		 * File file = new File(ExtraBeePlatform.getDirectory(),
		 * "ExtraBees.csv"); try { BufferedWriter out = new BufferedWriter(new
		 * FileWriter(file.getAbsolutePath())); for(EnumExtraBeeSpecies species
		 * : EnumExtraBeeSpecies.values()) {
		 * out.write(species.outputSpeciesData()); } out.close(); } catch
		 * (IOException e) { }
		 */

	}

	public static void addHiveDrops() {
		hiveDrops = new ArrayList[4];
		hiveDrops[0] = new ArrayList<IHiveDrop>();
		hiveDrops[0].add(new HiveDrop(EnumExtraBeeSpecies.WATER, 80));
		hiveDrops[0].add(new HiveDrop(
				EnumExtraBeeSpecies.getVanilla("Valiant"), 3));
		hiveDrops[1] = new ArrayList<IHiveDrop>();
		hiveDrops[1].add(new HiveDrop(EnumExtraBeeSpecies.ROCK, 80));
		hiveDrops[1].add(new HiveDrop(
				EnumExtraBeeSpecies.getVanilla("Valiant"), 3));
		hiveDrops[2] = new ArrayList<IHiveDrop>();
		hiveDrops[2].add(new HiveDrop(EnumExtraBeeSpecies.BASALT, 80));
		hiveDrops[2].add(new HiveDrop(
				EnumExtraBeeSpecies.getVanilla("Valiant"), 3));
		hiveDrops[3] = new ArrayList<IHiveDrop>();
		hiveDrops[3].add(new HiveDrop(EnumExtraBeeSpecies.MARBLE, 80));
		hiveDrops[3].add(new HiveDrop(
				EnumExtraBeeSpecies.getVanilla("Valiant"), 3));
	}

	public static void addRecipes() {
/*
		FuelManager.bronzeEngineFuel.put(
				32,
				new EngineBronzeFuel(
						new ItemStack(
								ItemLiquid.liquids[ItemLiquid.EnumType.ACID
										.ordinal()]), 4, 10000, 1));
										*/

		ModLoader.addRecipe(
				new ItemStack(ExtraBeeItem.honeyCrystalEmpty),
				new Object[] { "#@#", "@#@", "#@#", '@',
						ItemInterface.getItem("honeyDrop"), '#',
						ItemHoneyDrop.getItem(ItemHoneyDrop.EnumType.ENERGY) });

		for (ItemHoneyComb.EnumType info : ItemHoneyComb.EnumType.values()) {
			info.addRecipe();
		}

		if (modIC2) {
			Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canWater"),
					Items.getItem("waterCell"));
			Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canLava"),
					Items.getItem("lavaCell"));
			Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canBiomass"),
					Items.getItem("bioCell"));
			Ic2Recipes.addExtractorRecipe(ItemInterface.getItem("canBiofuel"),
					Items.getItem("biofuelCell"));
			Ic2Recipes.addExtractorRecipe(new ItemStack(ItemLiquid.can, 1,
					ItemLiquid.EnumType.GLACIAL.ordinal()), Items
					.getItem("coolingCell"));

			Ic2Recipes.addCompressorRecipe(Items.getItem("clayDust"),
					new ItemStack(Item.clay, 1, 0));

			Ic2Recipes.addCompressorRecipe(Items.getItem("coalDust"),
					new ItemStack(Item.coal, 1, 0));
		}

	}

	public static boolean areItemStacksEqual(ItemStack a, ItemStack b) {
		if (a == null || b == null)
			return false;
		return (a.itemID == b.itemID)
				&& (a.getItemDamage() == b.getItemDamage());
	}

}
