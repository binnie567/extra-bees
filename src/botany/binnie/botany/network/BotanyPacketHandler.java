package binnie.botany.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import binnie.core.network.BinniePacketHandler;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.INetworkManager;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet250CustomPayload;
import net.minecraft.src.TileEntity;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public class BotanyPacketHandler extends BinniePacketHandler {

	@Override
	public void onPacketData(INetworkManager manager,
			Packet250CustomPayload packet, Player player) {

		DataInputStream data = new DataInputStream(new ByteArrayInputStream(
				packet.data));

		try {
			int packetId = data.readByte();
			if (packetId < 0 || packetId >= PacketID.values().length)
				return;
			PacketID id = PacketID.values()[packetId];
			id.onPacketData(manager, packet, player, data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
