package binnie.craftgui.controls.scroll;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.events.EventMouseMoved;
import binnie.craftgui.window.IRendererWindow;

public class ControlScrollBar extends Control {

	@Override
	public void onMouseMoved(EventMouseMoved event) {
		parent.scrollWidget.movePercentage((-event.getDy()) / maxHeight);
	}

	ControlScroll parent;

	float maxHeight = 0.0f;

	public ControlScrollBar(ControlScroll parent) {
		super(parent, 0, parent.getSize().x, parent.getSize().x, parent
				.getSize().y - parent.getSize().x * 2);
		maxHeight = parent.getSize().y - parent.getSize().x * 2;
		this.parent = parent;
		this.canMouseOver = true;
	}

	@Override
	public void onRenderBackground() {

		setSize(getSize().x,
				maxHeight * parent.scrollWidget.getPercentageShown());

		int v = 0;
		if (this.isMouseOver())
			v += 40;
		else if (this.isEnabled())
			v += 20;

		((IRendererWindow) getSuperParent().getRenderer()).renderRectBordered(
				0, 0, (int) this.getSize().x, (int) this.getSize().y, 0, v,
				128, 20, 2, 2, 2, 3);

	}

	@Override
	public void onUpdate() {
		float yOffset = maxHeight * parent.scrollWidget.getPercentageIndex();
		setOffset(0, yOffset);
	}

}
