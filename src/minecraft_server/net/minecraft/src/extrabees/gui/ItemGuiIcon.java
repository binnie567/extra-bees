package net.minecraft.src.extrabees.gui;

import java.util.List;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.forge.ITextureProvider;

public class ItemGuiIcon extends Item implements ITextureProvider {

	
	public static ItemGuiIcon mainItem;
	
	public static ItemStack UndiscoveredProduct;
	public static ItemStack UndiscoveredFlower;
	public static ItemStack UndiscoveredEffect;
	public static ItemStack UndiscoveredSpecies;
	public static ItemStack UnalignedSpecies;
	public static ItemStack MutationBranch;
	public static ItemStack PlaceholderSpecies;
	public static ItemStack UndiscoveredMutation;
	
	public ItemGuiIcon() {
		super(11000);
		mainItem = this;
		this.setMaxStackSize(1);
		this.setHasSubtypes(true);
		
		UndiscoveredProduct = createItem("Undiscovered", 1);
		UndiscoveredFlower = createItem("Undiscovered", 2);
		UndiscoveredEffect = createItem("Undiscovered", 3);
		UndiscoveredSpecies = createItem("Undiscovered", 4);
		UnalignedSpecies = createItem("Unaligned Species", 20);
		MutationBranch = createItem("Branch X", 20);
		PlaceholderSpecies = createItem("PlaceholderSpecies", 36);
		UndiscoveredMutation = createItem("Undiscovered", 33);
	}


	public static ItemStack createItem(String name, int iconIndex) {
		return createItem(name, iconIndex, new String[] {});
	}
	
	public static ItemStack createItem(String name, int iconIndex, String[] strings) {
		ItemStack itemstack = new ItemStack(mainItem);
		itemstack.setItemDamage(iconIndex);
	   if (itemstack.stackTagCompound == null)
       {
               itemstack.setTagCompound(new NBTTagCompound());
       }
       itemstack.stackTagCompound.setString("name", name);

       int i = 0;
       for(String string : strings) {
    	   itemstack.stackTagCompound.setString("info"+i++, string);
       }
       
		return itemstack;
	}
	
	public String getTextureFile() {
		return "/gfx/extrabees/extrabees_icons.png";
	}
	
}
