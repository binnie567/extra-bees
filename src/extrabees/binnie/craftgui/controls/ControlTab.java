package binnie.craftgui.controls;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.Widget;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.window.IRendererWindow;

public class ControlTab<T extends Enum<T>> extends Widget implements ITooltip {

	@Override
	public void onMouseClick(EventMouseClick event) {
		callEvent(new EventValueChanged(this, this.value));
	}

	private ControlTabBar<T> tabBar;
	private T value;

	public ControlTab(ControlTabBar<T> parent, T value) {
		super(parent);
		this.value = value;
		this.tabBar = parent;
	}

	@Override
	public boolean canMouseOver() {
		return true;
	}

	@Override
	public void onRenderBackground() {

		int v = 0;
		if (this.isMouseOver())
			v += 40;
		else if (tabBar.selectedTab == this.value)
			v += 20;

		if (getColour() != 0xFFFFFF) {
			getRenderer().setColour(getColour());
		}

		((IRendererWindow) getSuperParent().getRenderer()).renderRectBordered(
				0, 0, (int) this.getSize().x, (int) this.getSize().y, 0, v, 20,
				20, 2, 2, 2, 3);

		if (getColour() != 0xFFFFFF) {
			getRenderer().setColour(0xFFFFFF);
		}
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
	}

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-controls.png";
	}

	@Override
	public List<String> getTooltip() {
		List<String> tooltip = new ArrayList<String>();
		tooltip.add(value.toString());
		return tooltip;
	}

	public T getValue() {
		return value;
	}

}
