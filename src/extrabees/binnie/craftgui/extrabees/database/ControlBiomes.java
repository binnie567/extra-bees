package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.src.BiomeGenBase;
import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.EnumTolerance;
import binnie.core.BinnieCore;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.window.IRendererWindow;

public class ControlBiomes extends Control implements ITooltip {

	public ControlBiomes(IWidget parent, int x, int y, int width, int height) {
		super(parent, x, y, width * 16, height * 16);
		canMouseOver = true;
	}

	List<Integer> tolerated = new ArrayList<Integer>();

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();

		if (tolerated.isEmpty())
			return list;

		int x = (int) (getRelativeMousePosition().x / 16);
		int y = (int) (getRelativeMousePosition().y / 16);

		int i = x + y * 8;

		if (i < tolerated.size()) {
			list.add(BiomeGenBase.biomeList[tolerated.get(i)].biomeName);
		}

		return list;
	}
	
	@Override
	public void onRenderForeground() {
		for (int i = 0; i < tolerated.size(); i++) {
			int x = (i % 8) * 16;
			int y = (i / 8) * 16;
			
			getRenderer().setColour(BiomeGenBase.biomeList[i].color);
			
			((IRendererWindow) getSuperParent().getRenderer()).renderRectBordered(
					x, y, 16, 16, 0, 20,
					128, 20, 2, 2, 2, 3);
		}
	}
	
	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-controls.png";
	}


	public void setSpecies(IAlleleBeeSpecies species) {
		tolerated.clear();

		if (species == null)
			return;

		int main;
		EnumTolerance tolerance;

		IBeeGenome genome = BeeManager.beeInterface
				.templateAsGenome(BeeManager.breedingManager
						.getBeeTemplate(species.getUID()));
		IBee bee = BeeManager.beeInterface.getBee(BinnieCore.proxy.getWorld(),
				genome);

		tolerated.addAll(bee.getSuitableBiomeIds());

	}

}
