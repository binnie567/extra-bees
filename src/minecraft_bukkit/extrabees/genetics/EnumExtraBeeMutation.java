package extrabees.genetics;

import extrabees.core.ExtraBeeCore;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.genetics.*;
import net.minecraft.server.World;

public enum EnumExtraBeeMutation implements IMutation
{
    ARID_A(EnumExtraBeeSpecies.getVanilla("Meadows"), EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.ARID, 10),
    BARREN_A(EnumExtraBeeSpecies.ARID, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.BARREN, 8),
    DESOLATE_A(EnumExtraBeeSpecies.ARID, EnumExtraBeeSpecies.BARREN, EnumExtraBeeSpecies.DESOLATE, 8),
    ROTTEN_A(EnumExtraBeeSpecies.DESOLATE, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.ROTTEN, 15),
    BONE_A(EnumExtraBeeSpecies.DESOLATE, EnumExtraBeeSpecies.getVanilla("Frugal"), EnumExtraBeeSpecies.BONE, 15),
    CREEPER_A(EnumExtraBeeSpecies.DESOLATE, EnumExtraBeeSpecies.getVanilla("Austere"), EnumExtraBeeSpecies.CREEPER, 15),
    STONE_A(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.STONE, 15),
    GRANITE_A(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.STONE, EnumExtraBeeSpecies.GRANITE, 15),
    MINERAL_A(EnumExtraBeeSpecies.STONE, EnumExtraBeeSpecies.GRANITE, EnumExtraBeeSpecies.MINERAL, 15),
    IRON_A(EnumExtraBeeSpecies.MINERAL, EnumExtraBeeSpecies.getVanilla("Cultivated"), EnumExtraBeeSpecies.IRON, 5),
    COPPER_A(EnumExtraBeeSpecies.MINERAL, EnumExtraBeeSpecies.getVanilla("Cultivated"), EnumExtraBeeSpecies.COPPER, 5),
    TIN_A(EnumExtraBeeSpecies.MINERAL, EnumExtraBeeSpecies.getVanilla("Cultivated"), EnumExtraBeeSpecies.TIN, 5),
    BRONZE_A(EnumExtraBeeSpecies.COPPER, EnumExtraBeeSpecies.TIN, EnumExtraBeeSpecies.BRONZE, 5),
    SILVER_A(EnumExtraBeeSpecies.IRON, EnumExtraBeeSpecies.IRON, EnumExtraBeeSpecies.SILVER, 5),
    GOLD_A(EnumExtraBeeSpecies.BRONZE, EnumExtraBeeSpecies.SILVER, EnumExtraBeeSpecies.GOLD, 5),
    UNSTABLE_A(EnumExtraBeeSpecies.getVanilla("Austere"), EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.UNSTABLE, 5),
    NUCLEUR_A(EnumExtraBeeSpecies.UNSTABLE, EnumExtraBeeSpecies.IRON, EnumExtraBeeSpecies.NUCLEAR, 5),
    RADIOACTIVE_A(EnumExtraBeeSpecies.NUCLEAR, EnumExtraBeeSpecies.GOLD, EnumExtraBeeSpecies.RADIOACTIVE, 5),
    ANCIENT_A(EnumExtraBeeSpecies.getVanilla("Noble"), EnumExtraBeeSpecies.getVanilla("Diligent"), EnumExtraBeeSpecies.ANCIENT, 10),
    PRIMEVAL_A(EnumExtraBeeSpecies.ANCIENT, EnumExtraBeeSpecies.getVanilla("Noble"), EnumExtraBeeSpecies.PRIMEVAL, 8),
    PREHISTORIC_A(EnumExtraBeeSpecies.PRIMEVAL, EnumExtraBeeSpecies.getVanilla("Majestic"), EnumExtraBeeSpecies.PREHISTORIC, 8),
    RELIC_A(EnumExtraBeeSpecies.PREHISTORIC, EnumExtraBeeSpecies.getVanilla("Imperial"), EnumExtraBeeSpecies.RELIC, 8),
    COAL_A(EnumExtraBeeSpecies.PRIMEVAL, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.COAL, 8),
    RESIN_A(EnumExtraBeeSpecies.PRIMEVAL, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.RESIN, 8),
    OIL_A(EnumExtraBeeSpecies.PRIMEVAL, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.OIL, 8),
    DISTILLED_A(EnumExtraBeeSpecies.OIL, EnumExtraBeeSpecies.getVanilla("Industrious"), EnumExtraBeeSpecies.DISTILLED, 8),
    FUEL_A(EnumExtraBeeSpecies.OIL, EnumExtraBeeSpecies.DISTILLED, EnumExtraBeeSpecies.FUEL, 8),
    CREOSOTE_A(EnumExtraBeeSpecies.FUEL, EnumExtraBeeSpecies.COAL, EnumExtraBeeSpecies.CREOSOTE, 8),
    LATEX_A(EnumExtraBeeSpecies.FUEL, EnumExtraBeeSpecies.RESIN, EnumExtraBeeSpecies.LATEX, 8),
    RIVER_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.RIVER, 10),
    OCEAN_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.OCEAN, 10),
    INK_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.OCEAN, EnumExtraBeeSpecies.INK, 8),
    SWEET_A(EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanilla("Diligent"), EnumExtraBeeSpecies.SWEET, 15),
    SUGAR_A(EnumExtraBeeSpecies.SWEET, EnumExtraBeeSpecies.getVanilla("Diligent"), EnumExtraBeeSpecies.SUGAR, 15),
    FRUIT_A(EnumExtraBeeSpecies.SUGAR, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.FRUIT, 5),
    ALCOHOL_A(EnumExtraBeeSpecies.FRUIT, EnumExtraBeeSpecies.getVanilla("Rural"), EnumExtraBeeSpecies.ALCOHOL, 10),
    FARM_A(EnumExtraBeeSpecies.getVanilla("Cultivated"), EnumExtraBeeSpecies.getVanilla("Rural"), EnumExtraBeeSpecies.FARM, 10),
    MILK_A(EnumExtraBeeSpecies.getVanilla("Rural"), EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.MILK, 10),
    COFFEE_A(EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanilla("Rural"), EnumExtraBeeSpecies.COFFEE, 10),
    SWAMP_A(EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.SWAMP, 10),
    BOGGY_A(EnumExtraBeeSpecies.SWAMP, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.BOGGY, 8),
    FUNGAL_A(EnumExtraBeeSpecies.BOGGY, EnumExtraBeeSpecies.SWAMP, EnumExtraBeeSpecies.FUNGAL, 8),
    COMMON_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_B(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Meadows"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_C(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_D(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_E(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_F(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_M(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_N(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_O(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_G(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_H(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Meadows"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_I(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_J(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_K(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_L(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_P(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_Q(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_R(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_S(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Meadows"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_T(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_U(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_V(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_W(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_X(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_Y(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Forest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_Z(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Meadows"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_AA(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Modest"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_AB(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_AC(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    COMMON_AD(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Marshy"), EnumExtraBeeSpecies.getVanillaTemplate("Common"), 15),
    CULTIVATED_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12),
    CULTIVATED_B(EnumExtraBeeSpecies.ROCK, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12),
    CULTIVATED_C(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12),
    CULTIVATED_D(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Common"), EnumExtraBeeSpecies.getVanillaTemplate("Cultivated"), 12),
    ROMAN_A(EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.getVanilla("Heroic"), EnumExtraBeeSpecies.ROMAN, 10),
    GREEK_A(EnumExtraBeeSpecies.ROMAN, EnumExtraBeeSpecies.MARBLE, EnumExtraBeeSpecies.GREEK, 8),
    CLASSICAL_A(EnumExtraBeeSpecies.GREEK, EnumExtraBeeSpecies.ROMAN, EnumExtraBeeSpecies.CLASSICAL, 8),
    TEMPERED_A(EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.getVanilla("Sinister"), EnumExtraBeeSpecies.TEMPERED, 10),
    ANGRY_A(EnumExtraBeeSpecies.TEMPERED, EnumExtraBeeSpecies.BASALT, EnumExtraBeeSpecies.ANGRY, 8),
    VOLCANIC_A(EnumExtraBeeSpecies.ANGRY, EnumExtraBeeSpecies.TEMPERED, EnumExtraBeeSpecies.VOLCANIC, 8),
    MALICIOUS_A(EnumExtraBeeSpecies.getVanilla("Sinister"), EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.MALICIOUS, 10),
    INFECTIOUS_A(EnumExtraBeeSpecies.MALICIOUS, EnumExtraBeeSpecies.getVanilla("Tropical"), EnumExtraBeeSpecies.INFECTIOUS, 8),
    VIRULENT_A(EnumExtraBeeSpecies.MALICIOUS, EnumExtraBeeSpecies.INFECTIOUS, EnumExtraBeeSpecies.VIRULENT, 8),
    VISCOUS_A(EnumExtraBeeSpecies.WATER, EnumExtraBeeSpecies.getVanilla("Exotic"), EnumExtraBeeSpecies.VISCOUS, 10),
    GLUTINOUS_A(EnumExtraBeeSpecies.VISCOUS, EnumExtraBeeSpecies.getVanilla("Exotic"), EnumExtraBeeSpecies.GLUTINOUS, 8),
    STICKY_A(EnumExtraBeeSpecies.VISCOUS, EnumExtraBeeSpecies.GLUTINOUS, EnumExtraBeeSpecies.STICKY, 8),
    CORROSIVE_A(EnumExtraBeeSpecies.VIRULENT, EnumExtraBeeSpecies.STICKY, EnumExtraBeeSpecies.CORROSIVE, 10),
    CAUSTIC_A(EnumExtraBeeSpecies.CORROSIVE, EnumExtraBeeSpecies.getVanilla("Fiendish"), EnumExtraBeeSpecies.CAUSTIC, 8),
    ACIDIC_A(EnumExtraBeeSpecies.CORROSIVE, EnumExtraBeeSpecies.CAUSTIC, EnumExtraBeeSpecies.ACIDIC, 8),
    EXCITED_A(EnumExtraBeeSpecies.getVanilla("Cultivated"), EnumExtraBeeSpecies.getVanilla("Valiant"), EnumExtraBeeSpecies.EXCITED, 10),
    ENERGETIC_A(EnumExtraBeeSpecies.EXCITED, EnumExtraBeeSpecies.getVanilla("Valiant"), EnumExtraBeeSpecies.ENERGETIC, 8),
    ECSTATIC_A(EnumExtraBeeSpecies.EXCITED, EnumExtraBeeSpecies.CAUSTIC, EnumExtraBeeSpecies.ECSTATIC, 8),
    ARTIC_A(EnumExtraBeeSpecies.getVanilla("Wintry"), EnumExtraBeeSpecies.getVanilla("Diligent"), EnumExtraBeeSpecies.ARTIC, 10),
    FREEZING_A(EnumExtraBeeSpecies.OCEAN, EnumExtraBeeSpecies.ARTIC, EnumExtraBeeSpecies.FREEZING, 10);

    private IAlleleBeeSpecies Species0;
    private IAlleleBeeSpecies Species1;
    private IAllele Mutant[];
    private int chance;

    private EnumExtraBeeMutation(IAlleleBeeSpecies iallelebeespecies, IAlleleBeeSpecies iallelebeespecies1, EnumExtraBeeSpecies enumextrabeespecies, int j)
    {
        Species0 = iallelebeespecies;
        Species1 = iallelebeespecies1;
        Mutant = enumextrabeespecies.getTemplate();
        chance = j;
    }

    private EnumExtraBeeMutation(IAlleleBeeSpecies iallelebeespecies, IAlleleBeeSpecies iallelebeespecies1, IAllele aiallele[], int j)
    {
        Species0 = iallelebeespecies;
        Species1 = iallelebeespecies1;
        Mutant = aiallele;
        chance = j;
    }

    public IAllele getAllele0()
    {
        return Species0;
    }

    public IAllele getAllele1()
    {
        return Species1;
    }

    public IAllele[] getTemplate()
    {
        return Mutant;
    }

    public int getBaseChance()
    {
        return chance;
    }

    public boolean isPartner(IAllele iallele)
    {
        return iallele.getId() == Species0.getId() || iallele.getId() == Species1.getId();
    }

    public IAllele getPartner(IAllele iallele)
    {
        return iallele.getId() != Species0.getId() ? Species0 : Species1;
    }

    public boolean isSecret()
    {
        return false;
    }

    public int getChance(World world, int i, int j, int k, int l, IAllele iallele, IAllele iallele1, IGenome igenome, IGenome igenome1)
    {
        int i1 = chance;

        if (ExtraBeeCore.beeBreedingMode == "HARDCORE")
        {
            i1 = Math.round(chance / 2);
        }
        else if (ExtraBeeCore.beeBreedingMode == "EASY")
        {
            i1 *= 2;
        }
        else if (ExtraBeeCore.beeBreedingMode == "FTB")
        {
            i1 = 100;
        }

        if (Species0.getId() == iallele.getId() && Species1.getId() == iallele1.getId())
        {
            return i1;
        }

        if (Species0.getId() == iallele1.getId() && Species1.getId() == iallele.getId())
        {
            return i1;
        }
        else
        {
            return 0;
        }
    }
}
