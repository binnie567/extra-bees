package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.core.EnumHumidity;
import forestry.api.core.EnumTemperature;
import forestry.api.genetics.EnumTolerance;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;

public class ControlClimateBar extends Control implements ITooltip {

	public ControlClimateBar(IWidget parent, int x, int y, int width, int height) {
		super(parent, x, y, width, height);
		canMouseOver = true;
	}

	public ControlClimateBar(IWidget parent, int x, int y, int width,
			int height, boolean humidity) {
		super(parent, x, y, width, height);
		canMouseOver = true;
		isHumidity = true;
	}

	boolean isHumidity = false;
	List<Integer> tolerated = new ArrayList<Integer>();

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();

		if (tolerated.isEmpty())
			return list;

		int types = isHumidity ? 3 : 6;

		int type = (int) ((int) (this.getRelativeMousePosition().x - 1) / ((this
				.getSize().x - 2) / types));

		if (!tolerated.contains(type))
			return list;

		if (type < types) {
			if (isHumidity)
				list.add(EnumHumidity.values()[type].name);
			else
				list.add(EnumTemperature.values()[type + 1].name);
		}

		return list;
	}

	int[] tempColours = new int[] { 0x00FFFB, 0x78BBFF, 0x4FFF30, 0xFFFF00,
			0xFFA200, 0xFF0000 };
	int[] humidColours = new int[] { 0xFFE7A3, 0x1AFF00, 0x307CFF };

	@Override
	public void onRenderBackground() {
		getRenderer().renderRectBordered(0, 0, (int) getSize().x,
				(int) getSize().y, 116, 60, 12, 14, 1, 1, 1, 1);

		int totalWidth = (int) (getSize().x - 2);
		int totalHeight = (int) (getSize().y - 2);

		int types = isHumidity ? 3 : 6;
		int w = (int) ((this.getSize().x - 2) / types);

		for (int i = 0; i < types; i++) {
			int x = i * w;
			if (tolerated.contains(i))
				if (isHumidity)
					getRenderer().setColour(humidColours[i]);
				else
					getRenderer().setColour(tempColours[i]);
			else
				getRenderer().setColour(0x000000);

			getRenderer().renderRectTiled(1 + x, 1, w, totalHeight, 93, 61, 10,
					12);

			getRenderer().setColour(0xFFFFFF);
		}

		getRenderer().renderRectBordered(0, 0, (int) getSize().x,
				(int) getSize().y, 104, 60, 12, 14, 1, 1, 1, 1);
	}

	public void setSpecies(IAlleleBeeSpecies species) {
		tolerated.clear();

		if (species == null)
			return;

		int main;
		EnumTolerance tolerance;

		if (!isHumidity) {
			main = species.getTemperature().ordinal() - 1;
			IBeeGenome genome = BeeManager.beeInterface
					.templateAsGenome(BeeManager.breedingManager
							.getBeeTemplate(species.getUID()));
			tolerance = genome.getToleranceTemp();
		} else {
			main = species.getHumidity().ordinal();
			IBeeGenome genome = BeeManager.beeInterface
					.templateAsGenome(BeeManager.breedingManager
							.getBeeTemplate(species.getUID()));
			tolerance = genome.getToleranceHumid();
		}

		tolerated.add(main);

		switch (tolerance) {
		case BOTH_5:
		case UP_5:
			tolerated.add(main + 5);
		case BOTH_4:
		case UP_4:
			tolerated.add(main + 4);
		case BOTH_3:
		case UP_3:
			tolerated.add(main + 3);
		case BOTH_2:
		case UP_2:
			tolerated.add(main + 2);
		case BOTH_1:
		case UP_1:
			tolerated.add(main + 1);
		default:
			break;
		}
		;

		switch (tolerance) {
		case BOTH_5:
		case DOWN_5:
			tolerated.add(main - 5);
		case BOTH_4:
		case DOWN_4:
			tolerated.add(main - 4);
		case BOTH_3:
		case DOWN_3:
			tolerated.add(main - 3);
		case BOTH_2:
		case DOWN_2:
			tolerated.add(main - 2);
		case BOTH_1:
		case DOWN_1:
			tolerated.add(main - 1);
		default:
			break;
		}
		;
	}

}
