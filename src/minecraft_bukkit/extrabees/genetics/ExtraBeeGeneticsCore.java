package extrabees.genetics;

import forestry.api.apiculture.EnumBeeChromosome;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;

public class ExtraBeeGeneticsCore
{
    public ExtraBeeGeneticsCore()
    {
    }

    public static IAllele[] getDefaultTemplate()
    {
        IAllele aiallele[] = new IAllele[EnumBeeChromosome.values().length];
        aiallele[EnumBeeChromosome.SPECIES.ordinal()] = AlleleManager.getAllele("speciesForest");
        aiallele[EnumBeeChromosome.SPEED.ordinal()] = AlleleManager.getAllele("speedSlowest");
        aiallele[EnumBeeChromosome.LIFESPAN.ordinal()] = AlleleManager.getAllele("lifespanShorter");
        aiallele[EnumBeeChromosome.FERTILITY.ordinal()] = AlleleManager.getAllele("fertilityNormal");
        aiallele[EnumBeeChromosome.TEMPERATURE_TOLERANCE.ordinal()] = AlleleManager.getAllele("toleranceNone");
        aiallele[EnumBeeChromosome.NOCTURNAL.ordinal()] = AlleleManager.getAllele("boolFalse");
        aiallele[EnumBeeChromosome.HUMIDITY_TOLERANCE.ordinal()] = AlleleManager.getAllele("toleranceNone");
        aiallele[EnumBeeChromosome.TOLERANT_FLYER.ordinal()] = AlleleManager.getAllele("boolFalse");
        aiallele[EnumBeeChromosome.CAVE_DWELLING.ordinal()] = AlleleManager.getAllele("boolFalse");
        aiallele[EnumBeeChromosome.FLOWER_PROVIDER.ordinal()] = AlleleManager.getAllele("flowersVanilla");
        aiallele[EnumBeeChromosome.FLOWERING.ordinal()] = AlleleManager.getAllele("floweringSlowest");
        aiallele[EnumBeeChromosome.TERRITORY.ordinal()] = AlleleManager.getAllele("territoryDefault");
        aiallele[EnumBeeChromosome.EFFECT.ordinal()] = AlleleManager.getAllele("effectNone");
        return aiallele;
    }
}
