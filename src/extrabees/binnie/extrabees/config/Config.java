package binnie.extrabees.config;

public class Config {

	public static ConfigMain Main;
	// public static ConfigBees Bees;
	public static ConfigMachines Machines;

	public static void load() {
		Main = new ConfigMain();
		Machines = new ConfigMachines();
	}

	public static void save() {
		Main.save();
		Machines.save();
	}

}
