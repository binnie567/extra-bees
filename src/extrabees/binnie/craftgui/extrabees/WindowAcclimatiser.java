package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityAcclimatiser;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowAcclimatiser extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	ControlSlot slotBee;
	ControlSlot slotItem;
	ControlExtraBeeProgress processWidget;

	TileEntityAcclimatiser machine;

	public WindowAcclimatiser(EntityPlayer player, IInventory inventory,
			Side side) {
		super(234, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityAcclimatiser) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowAcclimatiser(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Acclimatiser");

		playerInventory = new ControlPlayerInventory(this);

		processWidget = new ControlExtraBeeProgress(this, 96, 28, 64, 64, 0,
				64, 0, 0, ControlProgress.Direction.Right);

		slotBee = new ControlSlot(this, 119, 51);

		slotItem = new ControlSlot(this, 40, 40);

		slotBee.create(machine, TileEntityAcclimatiser.SlotBee);
		slotItem.create(machine, TileEntityAcclimatiser.SlotItem);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		new ControlErrorState(this, 180, 52);

	}

	@Override
	public void onUpdate() {
		processWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
