package binnie.craftgui.extrabees.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import binnie.core.BinnieCore;
import binnie.craftgui.controls.listbox.ControlAbstractOption;
import binnie.craftgui.controls.listbox.ControlListBox;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.extrabees.database.ControlProductsBox.Product;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IAlleleBeeSpecies;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import net.minecraft.src.ItemStack;

public class ControlProductsBox extends
		ControlListBox<ControlProductsBox.Product> {

	@Override
	public IWidget createOption(int index, int height, Product value) {
		return new ControlProductsItem(controlOptions, 0, height, getSize().x-14, 22, value);
	}

	enum Type {
		Products, Specialties
	}

	class Product {
		ItemStack item;
		float chance;

		public Product(ItemStack item, float chance) {
			this.item = item;
			this.chance = chance;
		}
	}

	private int index;
	private Type type;

	public ControlProductsBox(IWidget parent, int x, int y, int width,
			int height, Type type) {
		super(parent, x, y, width, height);
		this.type = type;
	}

	IAlleleBeeSpecies species = null;

	public void setSpecies(IAlleleBeeSpecies species) {
		if (species != this.species) {
			this.species = species;
			if (species != null) {

				IAllele[] template = BeeManager.breedingManager
						.getBeeTemplate(species.getUID());

				if (template == null)
					return;

				IBeeGenome genome = BeeManager.beeInterface
						.templateAsGenome(template);
				float speed = genome.getSpeed();

				float modeSpeed = BeeManager.breedingManager.getBeekeepingMode(
						BinnieCore.proxy.getWorld()).getProductionModifier(
						genome);

				List<Product> strings = new ArrayList<Product>();

				if (this.type == Type.Products) {
					for (Map.Entry<ItemStack, Integer> entry : species
							.getProducts().entrySet()) {
						strings.add(new Product(entry.getKey(), speed
								* modeSpeed * ((float) entry.getValue())));
					}
				} else {
					for (Map.Entry<ItemStack, Integer> entry : species
							.getSpecialty().entrySet()) {
						strings.add(new Product(entry.getKey(), speed
								* modeSpeed * ((float) entry.getValue())));
					}
				}

				setOptions(strings);

			}
		}

	}

}
