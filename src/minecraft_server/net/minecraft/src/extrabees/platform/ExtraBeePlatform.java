package net.minecraft.src.extrabees.platform;

import java.io.File;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;
import net.minecraft.src.extrabees.liquids.ItemLiquid;

public class ExtraBeePlatform {
	
	public static boolean isClient() {
		return false;
	}
	
	public static boolean isServer() {
		return true;
	}
	
	public static boolean isBukkit() {
		return false;
	}

	public static File getDirectory() {
		return new File("./");
	}

	public static void preInit() {
	}
	
	public static void doInit() {
	}
	
	public static void postInit() {
	}

	public static void addLiquidFX(ItemLiquid.EnumType liquid) {
	}

	public static void openBeeDictionary(ItemStack item, World world,
			EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	public static Object openGui(int ID, EntityPlayer player, IInventory object, World world, int x, int y, int z) {
		return null;
	}

	public static World getWorld() {
		return null;
	}
}
