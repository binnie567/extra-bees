package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlLiquidTank;
import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityPurifier;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowPurifier extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	ControlExtraBeeProgress progessWidget;

	TileEntityPurifier machine;

	ControlLiquidTank tankDNA;

	ControlSlot slotSerum;

	public WindowPurifier(EntityPlayer player, IInventory inventory, Side side) {
		super(234, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityPurifier) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowPurifier(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Purifier");

		playerInventory = new ControlPlayerInventory(this);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		tankDNA = new ControlLiquidTank(this, 70, 28);
		tankDNA.setTankID(0);

		progessWidget = new ControlExtraBeeProgress(this, 120, 28, 36, 60, 194,
				60, 194, 0, ControlProgress.Direction.Up);

		slotSerum = new ControlSlot(this, 120 + 9, 28 + 30 - 9);
		slotSerum.create(machine, TileEntityPurifier.SlotSerum);

		new ControlErrorState(this, 176, 50);
	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
