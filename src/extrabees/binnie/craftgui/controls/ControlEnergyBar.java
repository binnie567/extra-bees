package binnie.craftgui.controls;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.ITooltip;
import binnie.craftgui.core.IWidget;
import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.power.IPowerReceptor;

public class ControlEnergyBar extends Control implements ITooltip {

	public enum Direction {
		Up, Down, Left, Right
	}

	protected Direction direction;

	public ControlEnergyBar(IWidget parent, int x, int y, int width,
			int height, Direction direction) {
		super(parent, x, y, width, height);
		this.direction = direction;
		canMouseOver = true;
	}

	IPowerReceptor machine = null;

	public void setMachine(IPowerReceptor machine) {
		this.machine = machine;
	}

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();
		if (machine == null) {
			list.add("Unpowered Machine");
		} else {
			if (machine instanceof TileEntityMachine
					&& machine instanceof IPowerReceptor) {
				int percentage = (int) (100.0 * ((TileEntityMachine) machine)
						.getEnergyStored() / ((TileEntityMachine) machine)
						.getMaxEnergyStored());

				list.add(percentage + "% charged");
				list.add((int) ((TileEntityMachine) machine).getEnergyStored()
						+ "/"
						+ ((TileEntityMachine) machine).getMaxEnergyStored()
						+ " MJ");
				list.add("Max Input : "
						+ ((TileEntityMachine) machine).getMaxEnergyReceived()
						+ " MJ/t");
				list.add("Usage : "
						+ ((TileEntityMachine) machine).getPowerPerTick()
						+ " MJ/t");
			}

		}

		return list;
	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderRectBordered(0, 0, (int) getSize().x,
				(int) getSize().y, 116, 60, 12, 14, 1, 1, 1, 1);

		if (machine != null && machine instanceof TileEntityMachine
				&& machine instanceof IPowerReceptor) {

			double percentage = (((TileEntityMachine) machine)
					.getEnergyStored() / ((TileEntityMachine) machine)
					.getMaxEnergyStored());

			int height = (int) ((getSize().y - 2.0) * percentage);

			setColourFromPercentage((float) percentage);

			if (height > 0) {
				switch (direction) {
				case Up:
					getRenderer().renderRectTiled(1,
							(int) getSize().y - 1 - height,
							(int) this.getSize().x - 2, height, 93, 61, 10, 12);
					break;
				default:
					break;
				}
			}

			GL11.glColor3d(1.0, 1.0, 1.0);

			getRenderer().renderRectBordered(0, 0, (int) getSize().x,
					(int) getSize().y, 104, 60, 12, 14, 1, 1, 1, 1);
		}
	}

	public void setColourFromPercentage(float percentage) {
		if (percentage > 0.5) {
			GL11.glColor3d(1.0 - 2 * (percentage - 0.5), 1.0, 0.0);
		} else {
			GL11.glColor3d(1.0, 2 * (percentage), 0.0);
		}
	}

}
