package binnie.logicpipes;

import java.util.LinkedList;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.World;
import binnie.core.BinnieCore;
import binnie.extrabees.core.ExtraBeeGUI;
import buildcraft.BuildCraftTransport;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.transport.IPipe;
import buildcraft.transport.Gate;
import buildcraft.transport.Pipe;

public class LogicGate extends Gate {

	public LogicGate(Pipe pipe) {
		super(pipe);
		kind = GateKind.None;
	}

	@Override
	public void openGui(EntityPlayer player) {
		if (!BinnieCore.proxy.isSimulating(this.pipe.worldObj)) {
		} else {
			ExtraBees.proxy.openGui(ExtraBeeGUI.LogicPipe, player,
					this.pipe.xCoord, this.pipe.yCoord, this.pipe.zCoord);
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dropGate(World world, int i, int j, int k) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GateConditional getConditional() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addActions(LinkedList<IAction> list) {

		if (pipe.wireSet[IPipe.WireColor.Red.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_2.ordinal())
			list.add(BuildCraftTransport.actionRedSignal);

		if (pipe.wireSet[IPipe.WireColor.Blue.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_3.ordinal())
			list.add(BuildCraftTransport.actionBlueSignal);

		if (pipe.wireSet[IPipe.WireColor.Green.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_4.ordinal())
			list.add(BuildCraftTransport.actionGreenSignal);

		if (pipe.wireSet[IPipe.WireColor.Yellow.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_4.ordinal())
			list.add(BuildCraftTransport.actionYellowSignal);

	}

	@Override
	public void startResolution() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean resolveAction(IAction action) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addTrigger(LinkedList<ITrigger> list) {

		if (pipe.wireSet[IPipe.WireColor.Red.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_2.ordinal()) {
			list.add(BuildCraftTransport.triggerRedSignalActive);
			list.add(BuildCraftTransport.triggerRedSignalInactive);
		}

		if (pipe.wireSet[IPipe.WireColor.Blue.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_3.ordinal()) {
			list.add(BuildCraftTransport.triggerBlueSignalActive);
			list.add(BuildCraftTransport.triggerBlueSignalInactive);
		}

		if (pipe.wireSet[IPipe.WireColor.Green.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_4.ordinal()) {
			list.add(BuildCraftTransport.triggerGreenSignalActive);
			list.add(BuildCraftTransport.triggerGreenSignalInactive);
		}

		if (pipe.wireSet[IPipe.WireColor.Yellow.ordinal()]
				&& kind.ordinal() >= Gate.GateKind.AND_4.ordinal()) {
			list.add(BuildCraftTransport.triggerYellowSignalActive);
			list.add(BuildCraftTransport.triggerYellowSignalInactive);
		}

	}

	@Override
	public int getTexture(boolean isSignalActive) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getGuiFile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
	}

}
