package binnie.botany.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import binnie.core.network.PacketCoordinates;

import net.minecraft.src.ChunkCoordinates;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class PacketFlowerAge extends PacketCoordinates {

	public int age;

	public PacketFlowerAge() {
	}

	public PacketFlowerAge(ChunkCoordinates coordinates, int age) {
		this(coordinates.posX, coordinates.posY, coordinates.posZ, age);
	}

	public PacketFlowerAge(int posX, int posY, int posZ, int age) {
		super(PacketID.FlowerAge.ordinal(), posX, posY, posZ);
		this.age = age;
	}

	@Override
	public void writeData(DataOutputStream data) throws IOException {
		super.writeData(data);
		data.writeInt(age);

	}

	@Override
	public void readData(DataInputStream data) throws IOException {
		super.readData(data);
		age = data.readInt();

	}

	public ChunkCoordinates getCoordinates() {
		return new ChunkCoordinates(posX, posY, posZ);
	}

	public TileEntity getTileEntity(World world) {
		return world.getBlockTileEntity(posX, posY, posZ);
	}

}
