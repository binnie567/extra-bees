package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerIsNotWorking extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 4;
	}

	@Override
	public String getDescription() {
		return "Is Not Working";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			return !machine.doingWork;
		}
		return false;
	}

	public TriggerIsNotWorking() {
		super();
	}

}
