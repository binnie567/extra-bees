package binnie.craftgui.extrabees.database;

import forestry.api.apiculture.IAlleleBeeSpecies;
import binnie.craftgui.controls.ControlText;
import binnie.craftgui.core.IWidget;

public class PageSpeciesMutations extends PageSpecies {

	public PageSpeciesMutations(IWidget parent) {
		super(parent);

		pageSpeciesFurther_Title = new ControlText(this, 72, 8,
				"Further Mutations", ControlText.Alignment.Center);

		pageSpeciesFurther_List = new ControlMutationBox(this, 4, 20, 136, 152,
				ControlMutationBox.Type.Further);
	}

	ControlText pageSpeciesFurther_Title;
	ControlMutationBox pageSpeciesFurther_List;

	@Override
	public void onSpeciesChanged(IAlleleBeeSpecies species) {
		pageSpeciesFurther_List.setSpecies(species);

	}

}
