package binnie.botany.core;

import org.lwjgl.opengl.GL11;

import forestry.plugins.PluginBotany;

import binnie.botany.genetics.IAlleleFlowerSpecies;
import binnie.botany.genetics.IFlower;
import binnie.core.BinnieCore;

import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;
import net.minecraft.src.RenderBlocks;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TileEntity;
import net.minecraft.src.TileEntitySpecialRenderer;

public class RendererPlant extends TileEntitySpecialRenderer {

	public RendererPlant() {
	}

	@Override
	public void renderTileEntityAt(TileEntity entity, double par3, double par5,
			double par7, float par8) {

		BinnieCore.proxy.bindTexture("/gfx/botany/botany.png");

		if (!(entity instanceof TileEntityPlant))
			return;

		TileEntityPlant plant = (TileEntityPlant) entity;

		IFlower flower = plant.getFlower();

		int age = flower.getAge();
		Tessellator var9 = Tessellator.instance;
		int var10 = age + flower
				.getFlowerGenome()
				.getStem()
				.getIconIndex() * 8;

		int var11 = (var10 & 15) << 4;
		int var12 = var10 & 240;
		double var13 = (double) ((float) var11 / 256.0F);
		double var15 = (double) (((float) var11 + 15.99F) / 256.0F);
		double var17 = (double) ((float) var12 / 256.0F);
		double var19 = (double) (((float) var12 + 15.99F) / 256.0F);
		double var21 = par3 + 0.5D - 0.45D;
		double var23 = par3 + 0.5D + 0.45D;
		double var25 = par7 + 0.5D - 0.45D;
		double var27 = par7 + 0.5D + 0.45D;

		var9.startDrawingQuads();

		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var15, var17);

		var9.draw();

		var10 = age
				+ flower.getFlowerGenome().getPrimaryAsFlower()
						.getPrimaryIconIndex() * 8;

		var11 = (var10 & 15) << 4;
		var12 = var10 & 240;
		var13 = (double) ((float) var11 / 256.0F);
		var15 = (double) (((float) var11 + 15.99F) / 256.0F);
		var17 = (double) ((float) var12 / 256.0F);
		var19 = (double) (((float) var12 + 15.99F) / 256.0F);
		var21 = par3 + 0.5D - 0.45D;
		var23 = par3 + 0.5D + 0.45D;
		var25 = par7 + 0.5D - 0.45D;
		var27 = par7 + 0.5D + 0.45D;

		int colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getPrimaryColor();

		int r = colour / (256 * 256);
		colour = colour % (256 * 256);
		int g = colour / 256;
		colour = colour % 256;
		int b = colour;

		var9.startDrawingQuads();

		var9.setColorOpaque(r, g, b);

		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var15, var17);

		var9.draw();

		var10 = age
				+ flower.getFlowerGenome().getPrimaryAsFlower()
						.getSecondaryIconIndex() * 8;

		var11 = (var10 & 15) << 4;
		var12 = var10 & 240;
		var13 = (double) ((float) var11 / 256.0F);
		var15 = (double) (((float) var11 + 15.99F) / 256.0F);
		var17 = (double) ((float) var12 / 256.0F);
		var19 = (double) (((float) var12 + 15.99F) / 256.0F);
		var21 = par3 + 0.5D - 0.45D;
		var23 = par3 + 0.5D + 0.45D;
		var25 = par7 + 0.5D - 0.45D;
		var27 = par7 + 0.5D + 0.45D;

		colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getSecondaryColor();

		r = colour / (256 * 256);
		colour = colour % (256 * 256);
		g = colour / 256;
		colour = colour % 256;
		b = colour;

		var9.startDrawingQuads();

		var9.setColorOpaque(r, g, b);

		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var13, var17);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var13, var19);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var15, var19);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var15, var17);
		var9.addVertexWithUV(var23, par5 + 1.0D, var25, var13, var17);
		var9.addVertexWithUV(var23, par5 + 0.0D, var25, var13, var19);
		var9.addVertexWithUV(var21, par5 + 0.0D, var27, var15, var19);
		var9.addVertexWithUV(var21, par5 + 1.0D, var27, var15, var17);

		var9.draw();

	}

}
