package binnie.craftgui.controls;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;

public class ControlProgress extends Control {

	protected int u1;
	protected int u2;
	protected int v1;
	protected int v2;
	protected float progress;
	protected Direction direction;

	public enum Direction {
		Up, Down, Left, Right
	}

	public ControlProgress(IWidget parent, int x, int y, int width, int height,
			int u1, int v1, int u2, int v2, Direction dir) {
		super(parent, x, y, width, height);
		this.u1 = u1;
		this.u2 = u2;
		this.v1 = v1;
		this.v2 = v2;
		progress = 0.0f;
		direction = dir;
	}

	@Override
	public void onRenderBackground() {
		getRenderer().renderRect(0, 0, (int) getSize().x, (int) getSize().y,
				u1, v1);

		switch (direction) {
		case Right:
			getRenderer().renderRect(0, 0, (int) (getSize().x * progress),
					(int) getSize().y, u2, v2);
			break;
		case Left:
			getRenderer().renderRect((int) (getSize().x * (1.0f - progress)),
					0, (int) (getSize().x * progress), (int) getSize().y, u2,
					v2);
			break;
		case Down:
			getRenderer().renderRect(0, 0, (int) getSize().x,
					(int) (getSize().y * progress), u2, v2);
			break;
		case Up:
			getRenderer().renderRect(0,
					(int) (getSize().y * (1.0f - progress)), (int) getSize().x,
					(int) (getSize().y * progress), u2,
					(int) (v2 + getSize().y * (1.0f - progress)));
			break;
		}

	}

	public void setProgress(float progress) {
		this.progress = progress;
		if (this.progress < 0.0f)
			this.progress = 0.0f;
		else if (this.progress > 1.0f)
			this.progress = 1.0f;
	}

}
