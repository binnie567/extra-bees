package binnie.craftgui.controls.listbox;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import binnie.craftgui.controls.ControlText;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlIndexed;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouseClick;

public class ControlOption<T> extends ControlAbstractOption<T> {

	@Override
	@SideOnly(Side.CLIENT)
	public void onMouseClick(EventMouseClick event) {
		((IControlValue<T>)getParent()).setValue(getValue());
	}

	public ControlOption(IWidget parent, float x, float y, float w, float h, T value) {
		super(parent, x, y, w, h, value);
		if (value != null) {
			canMouseOver = true;
		} else {
			canMouseOver = false;
		}
	}

}
