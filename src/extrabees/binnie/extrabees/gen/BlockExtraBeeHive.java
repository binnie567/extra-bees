package binnie.extrabees.gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import binnie.extrabees.core.ExtraBeeBlock;
import binnie.extrabees.core.ExtraBeeCore;
import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.apiculture.IHiveDrop;

public class BlockExtraBeeHive extends Block {
	public BlockExtraBeeHive(int par1) {
		super(par1, 5, ExtraBeeBlock.materialBeehive);
		setLightValue(0.2F);
		setHardness(1.0F);
		setTickRandomly(true);
		setBlockName("BlockExtraBeeHive");
		setCreativeTab(CreativeTabs.tabBlock);
		setTextureFile(ExtraBeeTexture.Main.getTexture());
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (int i = 0; i < 4; i++)
			itemList.add(new ItemStack(this, 1, i));
	}

	@Override
	public int getBlockTextureFromSideAndMetadata(int i, int metadata) {
		if (i < 2) {
			return 1 + metadata * 2;
		} else {
			return 0 + metadata * 2;
		}
	}

	@Override
	public ArrayList getBlockDropped(World world, int x, int y, int z,
			int metadata, int fortune) {
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();

		ArrayList<IHiveDrop> dropList = ExtraBeeCore.hiveDrops[metadata];

		Collections.shuffle(dropList);
		// Grab a princess
		int tries = 0;
		boolean hasPrincess = false;
		while (tries <= 10 && !hasPrincess) {
			tries++;

			for (IHiveDrop drop : dropList) {
				if (world.rand.nextInt(100) < drop.getChance(world, x, y, z)) {
					ret.add(drop.getPrincess(world, x, y, z, fortune));
					hasPrincess = true;
					break;
				}
			}
		}

		// Grab drones
		for (IHiveDrop drop : dropList) {
			if (world.rand.nextInt(100) < drop.getChance(world, x, y, z)) {
				ret.addAll(drop.getDrones(world, x, y, z, fortune));
				break;
			}
		}
		// Grab anything else on offer
		for (IHiveDrop drop : dropList) {
			if (world.rand.nextInt(100) < drop.getChance(world, x, y, z)) {
				ret.addAll(drop.getAdditional(world, x, y, z, fortune));
				break;
			}
		}

		return ret;
	}

	@Override
	public void addCreativeItems(ArrayList itemList) {
		for (int i = 0; i < 4; i++) {
			itemList.add(new ItemStack(this, 1, i));
		}
	}
}
