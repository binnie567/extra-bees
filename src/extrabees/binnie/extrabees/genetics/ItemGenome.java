package binnie.extrabees.genetics;

import java.util.List;

import binnie.core.BinnieCore;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

import forestry.api.apiculture.BeeManager;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class ItemGenome extends ItemSerumEmpty {

	@Override
	@SideOnly(Side.CLIENT)
	public int getIconFromDamageForRenderPass(int par1, int par2) {
		return iconIndex + 1 + par2;
	}

	@Override
	public int getRenderPasses(int metadata) {
		return 4;
	}

	public ItemGenome(int id) {
		super(id);
		setMaxStackSize(1);
		setIconIndex(128);
		setMaxDamage(16);
	}

	@Override
	public int getColorFromItemStack(ItemStack itemstack, int j) {
		int i = itemstack.getItemDamage();

		if (j == 2) {
			IBeeGenome genome = getGenome(itemstack);
			if (genome == null)
				return 0x000000;
			else
				return genome.getPrimaryAsBee().getPrimaryColor();
		}

		return 0xFFFFFF;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer,
			List list, boolean par4) {
		super.addInformation(itemStack, entityPlayer, list, par4);
		int damage = this.getMaxDamage() - itemStack.getItemDamage();

		if (damage == 0)
			list.add("Empty");
		else if (damage == 1)
			list.add("1 Charge");
		else
			list.add(damage + " Charges");

		ModuleEngineering.getQualityInfo(itemStack, list);

		IBeeGenome genome = getGenome(itemStack);
		if (genome == null)
			return;

		IBee bee = BeeManager.beeInterface.getBee(BinnieCore.proxy.getWorld(),
				genome);

		bee.addTooltip(list);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		String name = getName(itemstack);
		if (name != null && name != "") {
			return name;
		}

		IBeeGenome genome = getGenome(itemstack);
		if (genome == null)
			return "Corrupted Genome";

		IBee bee = BeeManager.beeInterface.getBee(BinnieCore.proxy.getWorld(),
				genome);
		return bee.getDisplayName() + " Genome";

	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {

		// itemList.addAll(ModuleEngineering.getAllGenomes());
		//
	}

	@Override
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	public static IBeeGenome getGenome(ItemStack item) {
		if (!item.hasTagCompound())
			return null;
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("genome")) {
			NBTTagCompound genomeNBT = nbt.getCompoundTag("genome");
			IBeeGenome genome = BeeManager.beeInterface
					.templateAsGenome(BeeManager.breedingManager
							.getDefaultBeeTemplate());
			genome.readFromNBT(genomeNBT);
			return genome;
		} else
			return null;
	}

	public static String getName(ItemStack item) {
		if (!item.hasTagCompound())
			return null;
		NBTTagCompound nbt = item.getTagCompound();
		if (nbt.hasKey("name")) {
			return nbt.getString("name");
		} else
			return null;
	}
}
