package binnie.craftgui.controls.core;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Widget;
import binnie.craftgui.window.Window;

public class Control extends Widget {

	@Override
	public boolean canFocus() {
		return canFocus;
	}

	protected boolean canMouseOver = false;;
	protected boolean canFocus = false;;
	protected String texture = "/gfx/extrabees/craftgui-controls.png";

	protected Control(IWidget parent, float x, float y, float w, float h) {
		super(parent);
		setPosition(x, y);
		setSize(w, h);
	}

	@Override
	public final boolean canMouseOver() {
		return canMouseOver;
	}

	@Override
	public void onRenderBackground() {
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
	}

	@Override
	public String getTextureFile() {
		return texture;
	}

	public Window getWindow() {
		return (Window) this.getSuperParent();
	}

}
