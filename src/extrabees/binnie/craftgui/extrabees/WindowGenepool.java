package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlEnergyBar;
import binnie.craftgui.controls.ControlLiquidTank;
import binnie.craftgui.controls.ControlProgress;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.ControlSlotArray;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityGenepool;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowGenepool extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	ControlLiquidTank tankDNA;

	TileEntityGenepool machine;

	ControlSlot slotBee;
	ControlSlotArray slotReserve;
	ControlExtraBeeProgress progessWidget;

	public WindowGenepool(EntityPlayer player, IInventory inventory, Side side) {
		super(234, 192, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityGenepool) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowGenepool(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Genepool");

		playerInventory = new ControlPlayerInventory(this);

		ControlEnergyBar energyBar = new ControlEnergyBar(this, 12, 116, 14,
				60, ControlEnergyBar.Direction.Up);
		energyBar.setMachine(machine);

		tankDNA = new ControlLiquidTank(this, 204, 28);
		tankDNA.setTankID(0);

		slotBee = new ControlSlot(this, 12 + 36 + 8, 50);

		slotReserve = new ControlSlotArray(this, 12, 32, 2, 3);

		progessWidget = new ControlExtraBeeProgress(this, 12 + 36 + 8 + 18, 48,
				130, 21, 64, 0, 64, 21, ControlProgress.Direction.Right);

		slotBee.create(machine, TileEntityGenepool.SlotBee);
		slotReserve.create(machine, TileEntityGenepool.SlotReserve[0]);

		new ControlErrorState(this, 130, 80);
	}

	@Override
	public void onUpdate() {
		progessWidget.setProgress(machine.getCurrentProgress() / 100.0f);
	}

};
