package binnie.extrabees.machines;

import java.util.List;
import java.util.Random;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ItemStack;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockApiaristMachine extends BlockGeneticMachine {

	enum Machine {
		Acclimatiser, Databank, Indexer
	}

	public BlockApiaristMachine(int id) {
		super(id);
		setBlockName("apiaristMachine");
	}

	@Override
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		for (int i = 0; i < getNumberOfMachines(); i++) {
			itemList.add(new ItemStack(this, 1, i));
		}
	}

	@Override
	public void randomDisplayTick(World world, int x, int y, int z,
			Random random) {
		TileEntity entity = world.getBlockTileEntity(x, y, z);

		if (entity == null)
			return;

		if (entity instanceof TileEntityMachine) {
			if (!((TileEntityMachine) entity).isInProgress())
				return;
		} else {
			return;
		}

		if (!world.isAirBlock(x, y + 1, z))
			return;

		double xPos, yPos, zPos;

		xPos = x + 0.1 + world.rand.nextDouble() * 0.8;
		zPos = z + 0.1 + world.rand.nextDouble() * 0.8;

		/*
		 * if(entity instanceof TileEntityAcclimatiser) {
		 * TileEntityAcclimatiser.Mode mode = ((TileEntityAcclimatiser)
		 * entity).mode; switch(mode) { case WET: yPos = y + 1.0 +
		 * world.rand.nextDouble();
		 * 
		 * world.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * world.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * break; case DRY: yPos = y + 1.0 + world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("depthsuspend", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("suspended", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("depthsuspend", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); world.spawnParticle("suspended", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); break; case HOT: yPos = y + 1.0 +
		 * world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("flame", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * world.spawnParticle("smoke", xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		 * break; case COLD: yPos = y + 1.0 + world.rand.nextDouble()/2.0;
		 * 
		 * world.spawnParticle("snowshovel", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); world.spawnParticle("snowshovel", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D); break; default: break; } } /* WATER
		 * 
		 * double xPos = x + par1World.rand.nextDouble(); double xPos = x +
		 * par1World.rand.nextDouble(); double yPos = y + 1.0 +
		 * par1World.rand.nextDouble(); double zPos = z +
		 * par1World.rand.nextDouble();
		 * 
		 * par1World.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D, 0.0D,
		 * 0.0D); par1World.spawnParticle("dripWater", xPos, yPos, zPos, 0.0D,
		 * 0.0D, 0.0D);
		 */

	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return ModuleMachines.modelMachine;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {
		switch (metadata) {
		case 1:
			return new TileEntityAcclimatiser();
		case 2:
			return new TileEntityDatabank();
		case 3:
			return new TileEntityIndexer();
		default:
			return new TileEntityApiaristMachine();
		}
	}

	@Override
	public String getMachineName(int meta) {
		switch (meta) {
		case 0:
			return "Apiarist Machine";

		case 1:
			return "Acclimatiser";

		case 2:
			return "Apiarist Databank";

		case 3:
			return "Indexer";

		default:
			return "??? Machine";
		}
	}

	@Override
	public int getNumberOfMachines() {
		return Machine.values().length + 1;
	}

	@Override
	public int damageDropped(int par1) {
		return par1;
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return null;
	}

}
