package binnie.extrabees.machines;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import binnie.extrabees.core.ExtraBeeGUI;
import binnie.extrabees.genetics.ModuleEngineering;
import buildcraft.api.power.IPowerReceptor;

public class TileEntityReplicator extends TileEntityMachine implements
		ITankContainer, IInventory, IPowerReceptor {

	public static final int SlotSerum = 2;

	public static final int SlotSerumEmpty = 3;

	public static final int[] SlotSerumsDone = new int[] { 4, 5, 6, 7, 8, 9 };

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/Replicator.png";
	}

	@Override
	public ExtraBeeGUI getGUI() {
		return ExtraBeeGUI.Replicator;
	}

	public TileEntityReplicator() {
		super(25000, 500, 40000);

		addSlot(SlotSerum);

		getSlot(SlotSerum).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));

		addSlot(SlotSerumEmpty);

		getSlot(SlotSerumEmpty).setValidator(
				new SlotValidatorSerum(SlotValidatorSerum.Mode.Empty));

		addSlotArray(SlotSerumsDone);

		for (int slot : SlotSerumsDone) {
			getSlot(slot).setReadOnly();
			getSlot(slot).setValidator(
					new SlotValidatorSerum(SlotValidatorSerum.Mode.Serum));
		}

	}

	public InventorySlot[] getFreeVialSlots() {
		List<InventorySlot> slots = new ArrayList<InventorySlot>();
		for (InventorySlot slot : getSlots(SlotSerumsDone)) {
			if (slot.getItemStack() == null)
				slots.add(slot);
		}
		return slots.toArray(new InventorySlot[0]);
	}

	@Override
	public void onFinishTask() {

		ItemStack original = getSlot(SlotSerum).getItemStack();

		ModuleEngineering.changeQuality(original, -worldObj.rand.nextInt(2));

		ItemStack duplicate = original.copy();

		duplicate.setItemDamage(duplicate.getMaxDamage());

		getSlot(SlotSerumEmpty).decrStackSize(1);

		getFreeVialSlots()[0].setItemStack(duplicate);

	}

	@Override
	public ErrorState canWork() {

		if (getSlot(SlotSerum).getItemStack() == null)
			return new ErrorState("No Serum");

		if (getSlot(SlotSerumEmpty).getItemStack() == null)
			return new ErrorState("No Empty Serums");

		if (getFreeVialSlots().length == 0)
			return new ErrorState("No free vial slots");

		return super.canWork();
	}

	@Override
	public int getTankIndexToFill(ForgeDirection from, LiquidStack resource) {
		return 0;
	}

	@Override
	public int getTankIndexToDrain(ForgeDirection from) {
		return 0;
	}

}
