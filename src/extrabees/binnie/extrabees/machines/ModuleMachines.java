package binnie.extrabees.machines;

//import binnie.extrabees.alveary.BlockAlvearyComponent;
import binnie.core.BinnieCore;
import binnie.extrabees.config.Config;
import binnie.extrabees.core.ExtraBeeBlock;
import binnie.extrabees.core.ExtraBeeItem;
import binnie.extrabees.liquids.ItemLiquidDNA;
import cpw.mods.fml.common.registry.GameRegistry;
import forestry.api.core.ItemInterface;
import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

public class ModuleMachines {

	public static int modelMachine;
	public static int modelNaturalApiary;
	public static int modelAlvearyGlass;
	public static int modelAlveary;

	// public static RendererNaturalApiary rendererNaturalApiary;
	// public static RendererAlvearyGlass rendererAlvearyGlass;
	// public static RendererAlveary rendererAlveary;

	// public static BlockAlvearyComponent alvearyComponent;
	// public static BlockNaturalApiary naturalApiary;

	// public static BlockAlvearyGlass alvearyGlass;
	// public static BlockAlveary alveary;
	// public static BlockAlvearyTop alvearyTop;
	// public static BlockAlvearyCore alvearyCore;

	public static void doInit() {

		ExtraBeeItem.liquidDNA = new ItemLiquidDNA(Config.Main.getItemID(
				"liquidDNA", ExtraBeeItem.liquidDnaID));
		
		LiquidDictionary.getOrCreateLiquid("liquidDNA", new LiquidStack(ExtraBeeItem.liquidDNA, 1000));

		ExtraBeeBlock.apiaristMachine = new BlockApiaristMachine(
				Config.Machines.getBlockId("apiaristMachine", Config.Machines
						.getBlockId("apiaristMachine",
								ExtraBeeBlock.apiaristMachineID)));
		Block apiaristMachine = ExtraBeeBlock.apiaristMachine;
		ModLoader.registerBlock(apiaristMachine, ItemApiaristMachine.class);
		ModLoader.addName(apiaristMachine, "Apiarist Machine");

		ExtraBeeBlock.geneticMachine = new BlockGeneticMachine(
				Config.Machines.getBlockId("geneticMachine",
						Config.Machines.getBlockId("geneticMachine", 4003)));
		Block geneticMachine = ExtraBeeBlock.geneticMachine;
		ModLoader.registerBlock(geneticMachine, ItemGeneticMachine.class);
		ModLoader.addName(geneticMachine, "Genetic Machine");

		ExtraBeeBlock.advGeneticMachine = new BlockAdvancedGeneticMachine(
				Config.Machines.getBlockId("advGeneticMachine",
						Config.Machines.getBlockId("advGeneticMachine", 4004)));
		Block advGeneticMachine = ExtraBeeBlock.advGeneticMachine;
		ModLoader.registerBlock(advGeneticMachine,
				ItemAdvancedGeneticMachine.class);
		ModLoader.addName(advGeneticMachine, "Advanced Genetic Machine");

		modelMachine = BinnieCore.proxy.getUniqueBlockModelID();

		// alvearyComponent = new
		// BlockAlvearyComponent(Config.Machines.getBlockId("alveary", 4004));
		// ModLoader.registerBlock(alvearyComponent);
		// ModLoader.addName(alvearyComponent, "Alveary Mutator");
		/*
		 * 
		 * naturalApiary = new BlockNaturalApiary(Config.Machines.getBlockId(
		 * "naturalApiary", 4004)); ModLoader.registerBlock(naturalApiary,
		 * ItemNaturalApiary.class); ModLoader.addName(naturalApiary,
		 * "Natural Apiary");
		 * 
		 * alvearyGlass = new BlockAlvearyGlass(Config.Machines.getBlockId(
		 * "alvearyGlass", 4005)); ModLoader.registerBlock(alvearyGlass,
		 * ItemAlvearyGlass.class); ModLoader.addName(alvearyGlass,
		 * "Industrial Alveary Glass");
		 * 
		 * alveary = new BlockAlveary(Config.Machines.getBlockId("alveary",
		 * 4006)); ModLoader.registerBlock(alveary, ItemAlveary.class);
		 * ModLoader.addName(alveary, "Industrial Alveary");
		 * 
		 * alvearyTop = new BlockAlvearyTop(Config.Machines.getBlockId(
		 * "alvearyTop", 4007)); ModLoader.registerBlock(alvearyTop);
		 * ModLoader.addName(alvearyTop, "Industrial Alveary");
		 * 
		 * alvearyCore = new BlockAlvearyCore(Config.Machines.getBlockId(
		 * "alvearyCore", 4008)); ModLoader.registerBlock(alvearyCore);
		 * ModLoader.addName(alvearyCore, "Industrial Alveary");
		 * 
		 * modelNaturalApiary = ExtraBees.proxy.getUniqueBlockModelID();
		 * rendererNaturalApiary = new RendererNaturalApiary();
		 * 
		 * modelAlvearyGlass = ExtraBees.proxy.getUniqueBlockModelID();
		 * rendererAlvearyGlass = new RendererAlvearyGlass();
		 * 
		 * modelAlveary = ExtraBees.proxy.getUniqueBlockModelID();
		 * rendererAlveary = new RendererAlveary();
		 */

		Object renderer = BinnieCore.proxy
				.createTileEntityRenderer("binnie.extrabees.machines.RendererMachine");

		BinnieCore.proxy.registerTileEntity(TileEntityApiaristMachine.class,
				"extrabees.apiaristMachine", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityGeneticMachine.class,
				"extrabees.geneticMachine", renderer);

		BinnieCore.proxy.registerTileEntity(
				TileEntityAdvancedGeneticMachine.class,
				"extrabees.advGeneticMachine", renderer);

		BinnieCore.proxy.registerTileEntity(TileEntityAcclimatiser.class,
				"extrabees.acclimatiser", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityDatabank.class,
				"extrabees.databank", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityIndexer.class,
				"extrabees.indexer", renderer);

		BinnieCore.proxy.registerTileEntity(TileEntityGenepool.class,
				"extrabees.genepool", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntitySequencer.class,
				"extrabees.sequencer", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntitySplicer.class,
				"extrabees.splicer", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityAdvancedSequencer.class,
				"extrabees.advsequencer", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityAdvIndexer.class,
				"extrabees.advindexer", renderer);

		BinnieCore.proxy.registerTileEntity(TileEntityIsolator.class,
				"extrabees.isolator", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityReplicator.class,
				"extrabees.replicator", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityPurifier.class,
				"extrabees.purifier", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntityInoculator.class,
				"extrabees.inoculator", renderer);
		BinnieCore.proxy.registerTileEntity(TileEntitySynthesizer.class,
				"extrabees.synthesizer", renderer);

		BinnieCore.proxy.registerBlockRenderer(renderer);

		/*
		 * 
		 * ModLoader.registerTileEntity(TileEntityNaturalApiary.class,
		 * "extrabees.naturalApiary", rendererNaturalApiary);
		 * 
		 * ModLoader.registerTileEntity(TileEntityAlvearyGlass.class,
		 * "extrabees.alvearyGlass", rendererAlvearyGlass);
		 * ModLoader.registerTileEntity(TileEntityAlvearyTop.class,
		 * "extrabees.alvearyTop", rendererAlveary);
		 * ModLoader.registerTileEntity(TileEntityAlvearyCore.class,
		 * "extrabees.alvearyCore", rendererAlveary);
		 * ModLoader.registerTileEntity(TileEntityAlveary.class,
		 * "extrabees.alveary");
		 */
	}

	public static void postInit() {

		Block apiaristMachine = ExtraBeeBlock.apiaristMachine;

		Block geneticMachine = ExtraBeeBlock.geneticMachine;

		Block advGeneticMachine = ExtraBeeBlock.advGeneticMachine;

		GameRegistry.addRecipe(
				new ItemStack(apiaristMachine, 1, 0),
				new Object[] {
						"SRS",
						"SMS",
						"SRS",
						'S',
						ItemInterface.getItem("ingotCopper"),
						'R',
						Item.redstone,
						'M',
						ItemInterface.getItem("sturdyCasing")
						});

		GameRegistry.addRecipe(new ItemStack(geneticMachine, 1, 0),
				new Object[] {
						"SRS",
						"SMS",
						"SES",
						'S',
						Item.ingotIron,
						'R',
						new ItemStack(Item.dyePowder, 1, 4),
						'M',
						new ItemStack(apiaristMachine, 1, 0),
						'E',
						new ItemStack(ItemInterface.getItem("circuitboards")
								.getItem(), 1, 0), });

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 0),
				new Object[] {
						"SRS",
						"SMS",
						"SES",
						'S',
						Item.ingotGold,
						'R',
						Item.diamond,
						'M',
						new ItemStack(geneticMachine, 1, 0),
						'E',
						new ItemStack(ItemInterface.getItem("circuitboards")
								.getItem(), 1, 1),

				});

		// Apiarist Machines

		GameRegistry.addRecipe(
				new ItemStack(apiaristMachine, 1, 1),
				new Object[] { "WRL", "WML", "WRL", 'W',
						ItemInterface.getItem("canWater"), 'R', Item.redstone,
						'M', new ItemStack(apiaristMachine, 1, 0), 'L',
						ItemInterface.getItem("canLava"), });

		GameRegistry.addRecipe(new ItemStack(apiaristMachine, 1, 2),
				new Object[] { "GDG", "GMG", "GRG", 'G', Block.glass, 'R',
						Item.redstone, 'M',
						new ItemStack(apiaristMachine, 1, 0), 'D',
						ExtraBeeItem.dictionary, });

		GameRegistry
				.addRecipe(new ItemStack(apiaristMachine, 1, 3),
						new Object[] { "GDG", "GMG", "GRG", 'G', Block.chest,
								'R', Item.redstone, 'M',
								new ItemStack(apiaristMachine, 1, 0), 'D',
								Item.diamond });

		// Genetic Machines

		Block tankBlock = null;
		;

		try {
			tankBlock = (Block) Class.forName("buildcraft.BuildCraftFactory")
					.getField("tankBlock").get(null);
		} catch (Exception e) {

		}

		ItemStack tank = new ItemStack(Block.glass);

		if (tankBlock != null)
			tank = new ItemStack(tankBlock);

		GameRegistry.addRecipe(new ItemStack(geneticMachine, 1, 1),
				new Object[] { "TGT", "TMT", "gRg", 'T', tank, 'R',
						Item.redstone, 'M',
						new ItemStack(geneticMachine, 1, 0), 'G', Block.glass,
						'g', Item.ingotGold, });

		GameRegistry.addRecipe(new ItemStack(geneticMachine, 1, 2),
				new Object[] { "ggg", "GMG", "LRL", 'G', Block.glass, 'l',
						new ItemStack(Item.dyePowder, 1, 6), 'M',
						new ItemStack(geneticMachine, 1, 0), 'R',
						Item.redstone, 'g', Block.glowStone, });

		GameRegistry
				.addRecipe(new ItemStack(geneticMachine, 1, 3),
						new Object[] { "DDD", "TMT", "GRG", 'G', Block.glass,
								'R', Item.redstone, 'T', tank, 'M',
								new ItemStack(geneticMachine, 1, 0), 'D',
								Item.diamond });

		/*
		 * 
		 * GameRegistry.addRecipe(new ItemStack(geneticMachine, 1, 4), new
		 * Object[] { " B ", "BMB", " B ", 'B', Item.blazeRod, 'M', new
		 * ItemStack(geneticMachine, 1, 3), });
		 * 
		 * 
		 * 
		 * GameRegistry.addRecipe(new ItemStack(geneticMachine, 1, 4), new
		 * Object[] { " D ", "DMD", "CCC", 'C', new
		 * ItemStack(ItemInterface.getItem("circuitboards") .getItem(), 1, 0),
		 * 'D', Item.diamond, 'M', new ItemStack(apiaristMachine, 1, 3), });
		 */

		// Adv Genetic Machine

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 1),
				new Object[] { "KKK", "XMX", "XRX", 'K', Item.emerald, 'R',
						Item.redstone, 'M',
						new ItemStack(advGeneticMachine, 1, 0), 'X',
						Item.blazeRod, });

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 2),
				new Object[] { "SSS", "TMT", "TRT", 'T',
						new ItemStack(ExtraBeeItem.serumEmpty), 'S',
						Item.eyeOfEnder, 'M',
						new ItemStack(advGeneticMachine, 1, 0), 'R',
						Item.redstone, });

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 3),
				new Object[] { "FFF", "TMT", "TRT", 'F', Item.ghastTear, 'R',
						Item.redstone, 'T', tank, 'M',
						new ItemStack(advGeneticMachine, 1, 0), });

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 4),
				new Object[] { "BBB", "BMB", "SRS", 'B', Item.magmaCream, 'R',
						Item.redstone, 'S', ExtraBeeItem.serumEmpty, 'M',
						new ItemStack(advGeneticMachine, 1, 0), });

		GameRegistry.addRecipe(new ItemStack(advGeneticMachine, 1, 5),
				new Object[] { "SGS", "TMT", "SRS", 'G', Block.glass, 'S',
						Block.slowSand, 'M',
						new ItemStack(advGeneticMachine, 1, 0), 'R',
						Item.redstone, 'T', tank, });

		LogicAcclimatiser.postInit();
	}

}
