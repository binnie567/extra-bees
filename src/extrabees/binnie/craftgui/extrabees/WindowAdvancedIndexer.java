package binnie.craftgui.extrabees;

import cpw.mods.fml.common.Side;
import binnie.craftgui.controls.ControlCycle;
import binnie.craftgui.controls.button.ControlEnumButton;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventCycleChanged;
import binnie.craftgui.events.EventValueChanged;
import binnie.craftgui.window.ControlPlayerInventory;
import binnie.craftgui.window.Panel;
import binnie.craftgui.window.Window;
import binnie.extrabees.machines.TileEntityAdvIndexer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;

public class WindowAdvancedIndexer extends Window {

	EntityPlayer player;

	ControlPlayerInventory playerInventory;

	TileEntityAdvIndexer machine;

	ControlSlotIndexerArray slotIndexer;

	ControlCycle pageCycle;

	ControlEnumButton<TileEntityAdvIndexer.Mode> modeButton;

	public WindowAdvancedIndexer(EntityPlayer player, IInventory inventory,
			Side side) {
		super(320, 240, player, inventory, side);
		this.player = player;
		this.machine = (TileEntityAdvIndexer) inventory;
	}

	public static Window create(EntityPlayer player, IInventory inventory,
			Side side) {
		if (player == null || inventory == null)
			return null;
		return new WindowAdvancedIndexer(player, inventory, side);
	}

	@Override
	public void initialize() {

		setTitle("Gene Indexer");

		playerInventory = new ControlPlayerInventory(this);
		playerInventory.setPosition(16, playerInventory.getPosition().y);

		slotIndexer = new ControlSlotIndexerArray(this, 16, 24, 16, 4);

		modeButton = new ControlEnumButton<TileEntityAdvIndexer.Mode>(this, 66,
				104, 64, 18, TileEntityAdvIndexer.Mode.class);

		pageCycle = new ControlCycle(this, 50, 130, 96, 18, 24);

		slotIndexer.create(machine);

		new Panel(this, 190, 110, 110, 120, Panel.Type.Gray);

	}

	@Override
	public void onHandleEvent(Event event) {
		super.onHandleEvent(event);
		if (event instanceof EventCycleChanged && event.isOrigin(pageCycle)) {
			int page = ((EventCycleChanged) event).getValue();
			slotIndexer.setPage(page);
		}
		if (event instanceof EventValueChanged && event.isOrigin(modeButton)) {
			machine.setMode(modeButton.getValue());
		}
	}

	@Override
	public void onUpdate() {
		modeButton.setValue(machine.getMode());
		pageCycle.setMax(machine.getNumberOfPages(64));
	}

};
