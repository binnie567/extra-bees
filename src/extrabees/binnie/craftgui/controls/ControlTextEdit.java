package binnie.craftgui.controls;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import binnie.craftgui.controls.core.Control;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventKeyTyped;
import binnie.craftgui.events.EventTextEdit;
import binnie.craftgui.window.IRendererWindow;

public class ControlTextEdit extends Control {

	char[] allowedCharacters = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'T', 'Z', ' ', '(', ')', '[', ']' };

	@Override
	public String getTextureFile() {
		return "/gfx/extrabees/craftgui-panel.png";
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void onKeyTyped(EventKeyTyped event) {
		if (getSuperParent().isFocused(this)) {
			if (event.getKey() == Keyboard.KEY_BACK
					&& text.getText().length() > 0) {
				text.setText(text.getText().substring(0,
						text.getText().length() - 1));
			} else {
				char character = event.getCharacter();
				for (char allowed : allowedCharacters) {
					if (character == allowed) {
						text.setText(text.getText() + allowed);
					}
				}
			}
			this.callEvent(new EventTextEdit(this, text.getText()));
		}
	}

	@Override
	public void onRenderBackground() {
		((IRendererWindow) getSuperParent().getRenderer()).renderPanelGray(0,
				0, (int) this.getSize().x, (int) this.getSize().y);
	}

	@Override
	public void onRenderForeground() {
		// TODO Auto-generated method stub
		super.onRenderForeground();
	}

	ControlText text;

	public ControlTextEdit(IWidget parent, float x, float y, float width) {
		super(parent, x, y, width, 12);
		this.text = new ControlText(this, 2, 2, "", ControlText.Alignment.Left);
		text.setColour(0x000000);
		canFocus = true;
		canMouseOver = true;
	}

	public void setText(String text2) {
		text.setText(text2);
	}

}
