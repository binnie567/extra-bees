package binnie.craftgui.controls.listbox;

import java.util.List;

import binnie.craftgui.controls.core.Control;
import binnie.craftgui.controls.core.IControlIndexed;
import binnie.craftgui.controls.core.IControlValue;
import binnie.craftgui.core.IWidget;

public abstract class ControlAbstractListBox<T> extends Control implements IControlValue<T> {

	protected final ControlAbstractOptionSet<T> controlOptions;
	
	protected ControlAbstractListBox(IWidget parent, float x, float y, float w,
			float h) {
		super(parent, x, y, w, h);
		controlOptions = createOptionSet();
	}

	@Override
	public final T getValue() {
		return controlOptions.getValue();
	}

	@Override
	public final void setValue(T value) {
		controlOptions.setValue(value);
	}
	
	protected abstract ControlAbstractOptionSet createOptionSet();
	
	public abstract IWidget createOption(int index, int height, T value);
	
	public void setOptions(List<T> options) {
		controlOptions.setOptions(options);
	}

}
