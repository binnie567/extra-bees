package binnie.extrabees.fx;

import net.minecraft.src.EntityFX;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import binnie.core.fx.EntityBinnieFX;
import binnie.extrabees.core.ExtraBeeTexture;
import binnie.extrabees.machines.TileEntitySequencer;
import binnie.extrabees.machines.TileEntitySplicer;

public class EntitySplicerFX extends EntityBinnieFX {

	public int blendmode = 1;
	private String texture = ExtraBeeTexture.FX.getTexture();

	TileEntity tile;
	
	public EntitySplicerFX(TileEntitySplicer tileEntitySplicer, int rot) {

		super(tileEntitySplicer.worldObj, new Vector3f((float) tileEntitySplicer.xCoord + 0.2f + 0.6f
				* tileEntitySplicer.worldObj.rand.nextFloat(), (float) tileEntitySplicer.yCoord + 1f,
				(float) tileEntitySplicer.zCoord + 0.2f + 0.6f
						* tileEntitySplicer.worldObj.rand.nextFloat()), new Vector3f(0f, 0f,
				0f));
		
		this.tile = tileEntitySplicer;
		
		angle = (float) (Math.PI * rot);
		

		this.setParticleTextureIndex(0);
		this.setSize(0.1F, 0.1F);
		this.particleScale *= 0.6F;
		this.particleMaxAge = (int) (20.0D / (Math.random() * 0.8D + 0.2D));
		this.noClip = true;

		this.motionX *= 0.0;
		this.motionY *= 0.119999999552965164D;
		this.motionZ *= 0.0;

		this.motionY = 0.01f;

		this.particleMaxAge = 80;
		
		setParticleTextureIndex(4);
		
		setColor(colors[worldObj.rand.nextInt(4)]);
		
		this.lastTickPosX = tile.xCoord + 0.5f + 0.3f * Math.sin(angle);
		this.lastTickPosZ = tile.zCoord + 0.5f + 0.3f * Math.cos(angle);

	}
	
	float angle = 0.0f;

	int[] colors = new int[] { 0xFF0000, 0xFFFF00, 0x00FF00, 0x0000FF };

	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate() {

		//this.posX = tile.xCoord + 0.5f + 0.5f * Math.sin(angle);
		//this.posZ = tile.zCoord + 0.5f + 0.5f * Math.cos(angle);
		this.setPosition(tile.xCoord + 0.5d + 0.3d * Math.sin(angle), posY, tile.zCoord + 0.5f + 0.3f * Math.cos(angle));
		
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		this.moveEntity(this.motionX, this.motionY, this.motionZ);
		
		angle += 0.1f;
		
		if (this.particleMaxAge-- <= 0)
			this.setDead();
		
	}
	
//	public void onUpdate() {
//
//		this.prevPosX = this.posX;
//		this.prevPosY = this.posY;
//		this.prevPosZ = this.posZ;
//		this.moveEntity(this.motionX, this.motionY, this.motionZ);
//
//		if(this.particleMaxAge < 20) {
//			this.setSize(0.005f*particleMaxAge, 0.005f*particleMaxAge);
//		}
//		
//		if (this.particleMaxAge-- <= 0)
//			this.setDead();
//		
//
//	}

}
