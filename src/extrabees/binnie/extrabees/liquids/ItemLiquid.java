package binnie.extrabees.liquids;

import java.util.List;

import binnie.extrabees.core.ExtraBeeTexture;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.ExtraBees;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import forestry.api.core.ItemInterface;

public class ItemLiquid extends Item {
	public ItemLiquid.EnumType type;

	public static ItemLiquid[] liquids;

	public static ItemLiquidContainer waxCapsule;
	public static ItemLiquidContainer refactoryCapsule;
	public static ItemLiquidContainer can;
	public static ItemLiquidContainer bucket;
	public static ItemLiquidContainer bottle;

	public ItemLiquid(int par1, ItemLiquid.EnumType type) {
		super(par1);
		setMaxDamage(0);
		setIconIndex(type.ordinal());
		this.type = type;
		setTextureFile(ExtraBeeTexture.Liquids.getTexture());
		setCreativeTab(CreativeTabs.tabMaterials);
	}

	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return type.name;
	}

	public enum EnumType {
		@Deprecated
		CREOSOTE, ACID, POISON, GLACIAL

		;

		public boolean deprecated = false;

		public void deprecate() {
			deprecated = true;
		}

		public String name;
		public int red, green, blue;
		public int colour;

		private EnumType() {
			name = "??? Liquid";
			red = 255;
			green = 255;
			blue = 255;
			colour = 0xFFFFFF;
		}

		public void init(String name, int r, int g, int b) {
			this.name = name;
			red = r;
			green = g;
			blue = b;
			colour = red * 256 * 256 + green * 256 + blue;
		}

		;

		public static EnumType getTypeFromID(int id) {
			if (id >= ItemLiquid.EnumType.values().length) {
				ModLoader.getLogger().severe(
						"Tried to get Extra Bees Liquid with ID " + id
								+ " which does not exist!");
			}

			return ItemLiquid.EnumType.class.getEnumConstants()[id];
		}

		public LiquidStack getLiquidStack(int amount) {
			return new LiquidStack(liquids[this.ordinal()], amount);
		}
	}

	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		if (!EnumType.values()[type.ordinal()].deprecated)
			itemList.add(new ItemStack(this, 1, 0));
	}

	public static void setup(int id, int idContainer) {
		ItemLiquid.EnumType.CREOSOTE.init("Creosote Oil", 115, 115, 25);
		ItemLiquid.EnumType.CREOSOTE.deprecate();
		ItemLiquid.EnumType.ACID.init("Acid", 175, 235, 25);
		ItemLiquid.EnumType.POISON.init("Poison", 235, 20, 235);
		ItemLiquid.EnumType.GLACIAL.init("Liquid Nitrogen", 150, 200, 200);
		liquids = new ItemLiquid[EnumType.values().length];

		for (ItemLiquid.EnumType liquid : ItemLiquid.EnumType.values()) {
			liquids[liquid.ordinal()] = new ItemLiquid(id + liquid.ordinal(),
					liquid);
			ExtraBees.proxy.addLiquidFX(liquid);
		}

		waxCapsule = new ItemLiquidContainer(idContainer++, "Capsule", 16, 32);
		refactoryCapsule = new ItemLiquidContainer(idContainer++, "Capsule",
				17, 32);
		can = new ItemLiquidContainer(idContainer++, "Can", 18, 32);
		bucket = new ItemLiquidContainer(idContainer++, "Bucket", 19, 35);
		bottle = new ItemLiquidContainer(idContainer++, "Bottle", 20, 36);

		for (ItemLiquid.EnumType liquid : ItemLiquid.EnumType.values()) {

			LiquidStack liquidStack = LiquidDictionary.getOrCreateLiquid(liquid
					.toString().toLowerCase(), liquid.getLiquidStack(1000));

			liquidStack.amount = 1000;

			ModuleLiquids.injectLiquidContainer(new LiquidContainerData(
					liquidStack,
					new ItemStack(waxCapsule, 1, liquid.ordinal()),
					ItemInterface.getItem("waxCapsule")));

			ModuleLiquids.injectLiquidContainer(new LiquidContainerData(
					liquidStack, new ItemStack(refactoryCapsule, 1, liquid
							.ordinal()), ItemInterface
							.getItem("refractoryEmpty")));

			ModuleLiquids.injectLiquidContainer(new LiquidContainerData(
					liquidStack, new ItemStack(can, 1, liquid.ordinal()),
					ItemInterface.getItem("canEmpty")));

			ModuleLiquids.injectLiquidContainer(new LiquidContainerData(liquid
					.getLiquidStack(1000), new ItemStack(bucket, 1, liquid
					.ordinal()), new ItemStack(Item.bucketEmpty, 1, 0)));

			ModuleLiquids.injectLiquidContainer(new LiquidContainerData(
					liquidStack, new ItemStack(bottle, 1, liquid.ordinal()),
					new ItemStack(Item.glassBottle, 1, 0)));
		}

	}
}
