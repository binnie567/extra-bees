package binnie.extrabees.machines;

public class TileEntityAdvancedGeneticMachine extends TileEntityMachine {

	@Override
	public String getRenderTexture() {
		return "/gfx/extrabees/AdvancedGeneticMachine.png";
	}

}
