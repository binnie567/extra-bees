package binnie.craftgui.extrabees;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;
import net.minecraft.src.IInventory;
import binnie.craftgui.controls.ControlSlot;
import binnie.craftgui.controls.CustomSlot;
import binnie.craftgui.core.IWidget;
import binnie.craftgui.events.EventMouseClick;
import binnie.craftgui.window.Window;

public class ControlSlotIndexer extends ControlSlot {
	
	@Override
	public CustomSlot createSlot(IInventory inventory, int index) {
		return new SlotIndexer(inventory, index, 0, 0, pageLength);
	}

	public void create(IInventory inventory) {
		super.create(inventory, 0);
	}

	int pageLength;

	public ControlSlotIndexer(IWidget parent, int x, int y, int pageLength) {
		super(parent, x, y);
		this.pageLength = pageLength;
	}

	public void setPage(int page) {
		((SlotIndexer) this.slot).setPage(page);
	}

}
