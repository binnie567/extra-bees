package binnie.extrabees.triggers;

import binnie.extrabees.core.ExtraBeeTexture;
import buildcraft.api.gates.Action;
import buildcraft.api.gates.ActionManager;
import buildcraft.api.gates.IAction;

public class ExtraBeeAction extends Action {

	public static int incrementalID = 800;

	public static IAction actionPauseProcess;
	public static IAction actionCancelTask;

	public ExtraBeeAction() {
		super(incrementalID++);
	}

	@Override
	public String getTexture() {
		return ExtraBeeTexture.Triggers.getTexture();
	}

	public static void setup() {

		actionPauseProcess = new ActionPauseProcess();
		actionCancelTask = new ActionCancelTask();

		ActionManager.registerActionProvider(new ActionProvider());
	}

}
