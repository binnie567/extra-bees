package extrabees.liquids;

import extrabees.core.ExtraBeeCore;
import extrabees.platform.ExtraBeePlatform;
import forestry.api.core.ItemInterface;
import forestry.api.fuels.EngineBronzeFuel;
import forestry.api.fuels.FuelManager;
import forge.ITextureProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import net.minecraft.server.*;
import net.minecraft.server.ModLoader;
import extrabees.liquids.ItemLiquid;
import extrabees.liquids.ItemLiquid.EnumType;
import railcraft.common.api.ItemRegistry;

public class ItemLiquid extends Item implements ITextureProvider
{
	public enum EnumType {
		CREOSOTE, ACID, POISON, GLACIAL

		;

		public String name;
		public int red, green, blue;
		public int colour;

		private EnumType() {
			name = "??? Liquid";
			red = 255;
			green = 255;
			blue = 255;
			colour = 0xFFFFFF;
		}

		public void init(String name, int r, int g, int b) {
			this.name = name;
			red = r;
			green = g;
			blue = b;
			colour = red*256*256 + green*256 + blue;
		}

		;

		public static EnumType getTypeFromID(int id) {
			if (id >= ItemLiquid.EnumType.values().length) {
				ModLoader.getLogger().severe(
						"Tried to get Extra Bees Liquid with ID " + id
								+ " which does not exist!");
			}

			return ItemLiquid.EnumType.class.getEnumConstants()[id];
		}
	}

    public EnumType type;
    public static ItemLiquid liquids[];
    public static ItemLiquidContainer waxCapsule;
    public static ItemLiquidContainer refactoryCapsule;
    public static ItemLiquidContainer can;
    public static ItemLiquidContainer bucket;
    public static ItemLiquidContainer bottle;
    public static HashMap containers;

    public ItemLiquid(int i, EnumType enumtype)
    {
        super(i);
        setMaxDurability(0);
        d(enumtype.ordinal());
        type = enumtype;
    }

    public String getItemDisplayName(ItemStack itemstack)
    {
        return type.name;
    }

    public String getTextureFile()
    {
        return ExtraBeeCore.getTextureFileLiquid();
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        arraylist.add(new ItemStack(this, 1, 0));
    }

    public static void setup(int i, int j)
    {
        EnumType.CREOSOTE.init("Creosote Oil", 115, 115, 25);
        EnumType.ACID.init("Acid", 175, 235, 25);
        EnumType.POISON.init("Poison", 235, 20, 235);
        EnumType.GLACIAL.init("Glacial Water", 200, 235, 235);
        liquids = new ItemLiquid[EnumType.values().length];
        EnumType aenumtype[] = EnumType.values();
        int k = aenumtype.length;

        for (int l = 0; l < k; l++)
        {
            EnumType enumtype = aenumtype[l];
            liquids[enumtype.ordinal()] = new ItemLiquid(i + enumtype.ordinal(), enumtype);
            ExtraBeePlatform.addLiquidFX(enumtype);
        }

        waxCapsule = new ItemLiquidContainer(j++, "Capsule", 16, 32);
        refactoryCapsule = new ItemLiquidContainer(j++, "Capsule", 17, 32);
        can = new ItemLiquidContainer(j++, "Can", 18, 32);
        bucket = new ItemLiquidContainer(j++, "Bucket", 19, 35);
        bottle = new ItemLiquidContainer(j++, "Bottle", 20, 36);
        containers = new HashMap();
        aenumtype = EnumType.values();
        k = aenumtype.length;

        for (int i1 = 0; i1 < k; i1++)
        {
            EnumType enumtype1 = aenumtype[i1];
            HashMap hashmap = new HashMap();
            hashmap.put(waxCapsule, new EBLiquidContainer(enumtype1, 1000, waxCapsule, ItemInterface.getItem("waxCapsule")));
            hashmap.put(refactoryCapsule, new EBLiquidContainer(enumtype1, 1000, refactoryCapsule, ItemInterface.getItem("refractoryEmpty")));
            hashmap.put(can, new EBLiquidContainer(enumtype1, 1000, can, ItemInterface.getItem("canEmpty")));
            hashmap.put(bucket, new EBLiquidContainer(enumtype1, 1000, bucket, new ItemStack(Item.BUCKET, 1, 0)));

            if (enumtype1 == EnumType.CREOSOTE && ExtraBeeCore.modRailcraft)
            {
                ItemStack itemstack = ItemRegistry.getItem("item.creosote", 1);

                if (itemstack != null)
                {
                    ModLoader.getLogger().fine("Found Creosote Oil from Railcraft, using instead of Creosote Oil Bottle");
                    hashmap.put(bottle, new EBLiquidContainer(enumtype1, 1000, itemstack.getItem(), new ItemStack(Item.GLASS_BOTTLE, 1, 0)));
                }
                else
                {
                    hashmap.put(bottle, new EBLiquidContainer(enumtype1, 1000, bottle, new ItemStack(Item.GLASS_BOTTLE, 1, 0)));
                }
            }
            else
            {
                hashmap.put(bottle, new EBLiquidContainer(enumtype1, 1000, bottle, new ItemStack(Item.GLASS_BOTTLE, 1, 0)));
            }
        }

        FuelManager.bronzeEngineFuel.put(Integer.valueOf(liquids[EnumType.CREOSOTE.ordinal()].id), new EngineBronzeFuel(new ItemStack(liquids[EnumType.CREOSOTE.ordinal()]), 4, 16000, 1));
    }
}
