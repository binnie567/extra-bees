package binnie.botany.core;

import binnie.botany.genetics.IFlower;
import binnie.botany.genetics.IFlowerGenome;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;

public interface IFlowerInterface {

	boolean isFlower(ItemStack stack);

	IFlower getFlower(ItemStack stack);

	IFlower getFlower(IFlowerGenome genome, int age);

	ItemStack getFlowerStack(IFlower flower);

	IChromosome[] templateAsChromosomes(IAllele[] template);

	IChromosome[] templateAsChromosomes(IAllele[] templateActive,
			IAllele[] templateInactive);

	IFlowerGenome templateAsGenome(IAllele[] template);

	IFlowerGenome templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive);

	IAllele[] getDefaultFlowerTemplate();

	IFlowerGenome getDefaultFlowerGenome();

}
