package binnie.craftgui.controls;

import java.util.ArrayList;
import java.util.List;

import binnie.craftgui.core.IWidget;
import binnie.craftgui.core.Widget;
import binnie.craftgui.events.Event;
import binnie.craftgui.events.EventValueChanged;

public class ControlTabBar<T extends Enum<T>> extends Widget {

	public ControlTabBar(IWidget parent, int x, int y, int width, int height,
			Class<T> enumClass) {
		super(parent);
		setPosition(x, y);
		setSize(width, height);

		if (enumClass.getEnumConstants() != null) {
			int length = enumClass.getEnumConstants().length;
			int tabHeight = height / length;
			for (int i = 0; i < length; i++) {
				enumConstants.add(enumClass.getEnumConstants()[i]);
				IWidget tab = new ControlTab<T>(this,
						enumClass.getEnumConstants()[i]);
				tab.setPosition(0, i * tabHeight);
				tab.setSize(width, tabHeight);
			}
			selectedTab = enumConstants.get(0);
		}
	}

	List<T> enumConstants = new ArrayList<T>();

	public T selectedTab = null;

	@Override
	public void onRenderBackground() {
	}

	@Override
	public void onRenderForeground() {
	}

	@Override
	public void onRenderOverlay() {
	}

	@Override
	public void onUpdate() {
	}

	@Override
	public String getTextureFile() {
		return "";
	}

	@Override
	public void onHandleEvent(Event event) {
		if (event instanceof EventValueChanged
				&& this.getWidgets().contains(event.getOrigin())) {
			selectedTab = ((EventValueChanged<T>) event).getValue();
		}
	}

	public IWidget getTab(T tab) {
		for (IWidget child : this.getWidgets())
			if (child instanceof ControlTab
					&& ((ControlTab<T>) child).getValue() == tab)
				return child;
		return null;
	}

}
