package binnie.extrabees.nei;

import org.lwjgl.input.Keyboard;

import binnie.craftgui.extrabees.WindowNEIBreeder;
import binnie.craftgui.extrabees.database.WindowDictionary;
import binnie.extrabees.core.ExtraBeeGUI;

import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.forge.IContainerInputHandler;
import cpw.mods.fml.common.Side;

import net.minecraft.src.ExtraBees;
import net.minecraft.src.GuiContainer;
import net.minecraft.src.GuiScreen;

public class ExtraBeeNEIInputHandler implements IContainerInputHandler {

	@Override
	public boolean mouseClicked(GuiContainer gui, int mousex, int mousey,
			int button) {
		return false;
	}

	public static void init() {
		GuiContainerManager.addInputHandler(new ExtraBeeNEIInputHandler());
	}

	@Override
	public void onKeyTyped(GuiContainer gui, char keyChar, int keyCode) {
		
	}

	@Override
	public boolean lastKeyTyped(GuiContainer gui, char keyChar, int keyID) {
		if (keyID == Keyboard.KEY_D && gui.mc.playerController.isInCreativeMode()) {
			ExtraBees.proxy.openGui(ExtraBeeGUI.DatabaseNEI, gui.mc.thePlayer,
					(int) gui.mc.thePlayer.posX, (int) gui.mc.thePlayer.posY, (int) gui.mc.thePlayer.posZ);
			return true;
		}
		return false;
	}

	@Override
	public void onMouseClicked(GuiContainer gui, int mousex, int mousey,
			int button) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMouseUp(GuiContainer gui, int mousex, int mousey, int button) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean mouseScrolled(GuiContainer gui, int mousex, int mousey,
			int scrolled) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onMouseScrolled(GuiContainer gui, int mousex, int mousey,
			int scrolled) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyTyped(GuiContainer gui, char keyChar, int keyCode) {
		// TODO Auto-generated method stub
		return false;
	}

}
