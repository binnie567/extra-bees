package binnie.craftgui.core;

import net.minecraft.src.ItemStack;

public interface IRenderer {

	public void renderRect(int x, int y, int width, int height, int u, int v);

	public void renderRectTiled(int x, int y, int width, int height, int u,
			int v, int textWidth, int textHeight);

	public void renderRectBordered(int x, int y, int width, int height, int u,
			int v, int textWidth, int textHeight, int borderLeft,
			int borderRight, int borderTop, int borderBottom);

	public void setTexture(String texture);

	public int getTextWidth(String text);

	public int getTextHeight();

	public void setColour(int c);

	public void renderText(int x, int y, String text, int colour);

	public void renderOutline(int x, int y, int w, int h, int colour);

	public void renderSolidRect(int x, int y, int w, int h, int colour);

	public void renderItem(int x, int y, ItemStack item);

	public void renderGradientRect(int i, int j, int k, int l, int m, int n);

	public void limitArea(float x, float y, float x2, float y2);

}
