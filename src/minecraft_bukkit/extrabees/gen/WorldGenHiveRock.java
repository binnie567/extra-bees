package extrabees.gen;

import extrabees.core.ExtraBeeBlock;
import java.util.Random;
import net.minecraft.server.*;

public class WorldGenHiveRock extends WorldGenerator
{
    public WorldGenHiveRock()
    {
    }

    public boolean a(World world, Random random, int i, int j, int k)
    {
        BiomeBase biomebase = world.getWorldChunkManager().getBiome(i, k);
        int l = i;
        int i1 = j;
        int j1 = k;

        if (world.getTypeId(l, i1, j1) == Block.STONE.id)
        {
            world.setRawTypeIdAndData(l, i1, j1, ExtraBeeBlock.hive.id, 1);
        }

        return true;
    }
}
