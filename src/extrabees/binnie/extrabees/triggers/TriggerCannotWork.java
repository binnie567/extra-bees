package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerCannotWork extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 6;
	}

	@Override
	public String getDescription() {
		return "Cannot Work";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			return machine.canWork() != null || machine.canProgress() != null;
		}
		return false;
	}

	public TriggerCannotWork() {
		super();
	}

}
