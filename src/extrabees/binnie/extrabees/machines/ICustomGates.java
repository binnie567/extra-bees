package binnie.extrabees.machines;

import java.util.LinkedList;

import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionReceptor;
import buildcraft.api.gates.ITrigger;

public interface ICustomGates extends IActionReceptor {

	public LinkedList<IAction> getCustomActions();

	public LinkedList<ITrigger> getCustomTriggers();
}
