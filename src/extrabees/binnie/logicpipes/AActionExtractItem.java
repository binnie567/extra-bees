package binnie.logicpipes;

import java.util.ArrayList;
import java.util.List;

public class AActionExtractItem extends LogicAction {

	@Override
	public String getTextureFile() {
		return "/gfx/buildcraft/gui/triggers.png";
	}

	@Override
	public int getIndexInTexture() {
		return 0;
	}

	@Override
	public String getDescription() {
		return "Extract Item";
	}

	@Override
	public List<String> getTooltip() {
		List<String> list = new ArrayList<String>();
		list.add("First item in inventory");
		list.add("1 item per second");
		return list;
	}

}
