package binnie.extrabees.triggers;

import binnie.extrabees.machines.TileEntityMachine;
import buildcraft.api.gates.ITriggerParameter;
import net.minecraft.src.TileEntity;

public class TriggerPowerFull extends ExtraBeeTrigger {

	@Override
	public int getIndexInTexture() {
		return 14;
	}

	@Override
	public String getDescription() {
		return "Full Energy";
	}

	@Override
	public boolean isTriggerActive(TileEntity tile, ITriggerParameter parameter) {
		if (tile instanceof TileEntityMachine) {
			TileEntityMachine machine = (TileEntityMachine) tile;
			double percentage = machine.getEnergyStored()
					/ machine.getMaxEnergyStored();
			return percentage > 0.95f;
		}
		return false;
	}

	public TriggerPowerFull() {
		super();
	}

}
