package binnie.craftgui.controls;

import binnie.extrabees.machines.IInventorySlots;
import binnie.extrabees.machines.InventorySlot;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Slot;

public class CustomSlot extends Slot {

	@Override
	public boolean isItemValid(ItemStack par1ItemStack) {
		if (slot == null)
			return super.isItemValid(par1ItemStack);
		return slot.isItemValid(par1ItemStack);
	}

	InventorySlot slot = null;

	public CustomSlot(IInventory par1iInventory, int par2, int par3, int par4) {
		super(par1iInventory, par2, par3, par4);
		if (par1iInventory instanceof IInventorySlots) {
			slot = ((IInventorySlots) par1iInventory).getSlot(par2);
		}
	}

}
