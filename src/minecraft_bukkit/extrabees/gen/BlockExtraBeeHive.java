package extrabees.gen;

import extrabees.core.ExtraBeeBlock;
import extrabees.core.ExtraBeeCore;
import forestry.api.apiculture.*;
import forestry.api.core.ItemInterface;
import forge.ITextureProvider;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Logger;
import net.minecraft.server.*;

public class BlockExtraBeeHive extends Block implements ITextureProvider
{
    public BlockExtraBeeHive(int i)
    {
        super(i, 5, ExtraBeeBlock.materialBeehive);
        a(0.2F);
        c(1.0F);
        a(true);
        a("BlockExtraBeeHive");
    }

    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    public int a(int i, int j)
    {
        if (i < 2)
        {
            return 1 + j * 2;
        }
        else
        {
            return 0 + j * 2;
        }
    }

    public String getTextureFile()
    {
        return ExtraBeeCore.getTextureFileMain();
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean a()
    {
        return false;
    }

    public ArrayList getBlockDropped(World world, int i, int j, int k, int l, int i1)
    {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = ExtraBeeCore.hiveDrops[l];
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        boolean flag = true;
        boolean flag1 = true;
        Collections.shuffle(arraylist1);

        while (flag || flag1)
        {
            Item item;

            if (flag)
            {
                item = ItemInterface.getItem("beePrincessGE").getItem();
            }
            else
            {
                item = ItemInterface.getItem("beeDroneGE").getItem();
            }

            Iterator iterator = arraylist1.iterator();

            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }

                IHiveDrop ihivedrop = (IHiveDrop)iterator.next();

                if (world.random.nextInt(100) < ihivedrop.getChance(world, i, j, k))
                {
                    try
                    {
                        Class class1 = Class.forName("forestry.apiculture.genetics.Bee");
                        Class class2 = Class.forName("forestry.apiculture.genetics.BeeTemplates");
                        Method method = class2.getMethod("templateAsGenome", new Class[]
                                {
                                    forestry.api.genetics.IAllele[].class
                                });
                        Object obj = method.invoke(null, new Object[]
                                {
                                    ihivedrop.getTemplate()
                                });
                        Constructor constructor = class1.getConstructor(new Class[]
                                {
                                    forestry.api.apiculture.IBeeGenome.class
                                });
                        IBee ibee = (IBee)constructor.newInstance(new Object[]
                                {
                                    obj
                                });
                        ItemStack itemstack = new ItemStack(item, 1, ibee.getMeta());
                        ibee.b(nbttagcompound);
                        itemstack.setTag(nbttagcompound);
                        arraylist.add(itemstack);
                    }
                    catch (Exception exception)
                    {
                        ModLoader.getLogger().warning("Extra Bees Hive failed to drop bees, due to java reflection failing!");
                        return arraylist;
                    }

                    if (flag)
                    {
                        flag = false;
                    }
                    else
                    {
                        flag1 = false;
                    }
                }
            }
            while (flag || flag1);
        }

        return arraylist;
    }

    public void addCreativeItems(ArrayList arraylist)
    {
        for (int i = 0; i < 4; i++)
        {
            arraylist.add(new ItemStack(this, 1, i));
        }
    }
}
