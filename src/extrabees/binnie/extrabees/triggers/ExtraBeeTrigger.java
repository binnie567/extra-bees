package binnie.extrabees.triggers;

import binnie.extrabees.core.ExtraBeeTexture;
import buildcraft.api.gates.ActionManager;
import buildcraft.api.gates.ITrigger;
import buildcraft.api.gates.Trigger;

public class ExtraBeeTrigger extends Trigger {

	public static int incrementalID = 800;

	public static ITrigger triggerNoBlankTemplate;
	public static ITrigger triggerNoTemplate;

	public static ITrigger triggerIsWorking;
	public static ITrigger triggerIsNotWorking;
	public static ITrigger triggerCanWork;
	public static ITrigger triggerCannotWork;

	public static ITrigger triggerPowerNone;
	public static ITrigger triggerPowerLow;
	public static ITrigger triggerPowerMedium;
	public static ITrigger triggerPowerHigh;
	public static ITrigger triggerPowerFull;

	public static ITrigger triggerAcclimatiserNone;
	public static ITrigger triggerAcclimatiserHot;
	public static ITrigger triggerAcclimatiserCold;
	public static ITrigger triggerAcclimatiserWet;
	public static ITrigger triggerAcclimatiserDry;

	public ExtraBeeTrigger() {
		super(incrementalID++);
	}

	@Override
	public String getTextureFile() {
		return ExtraBeeTexture.Triggers.getTexture();
	}

	public static void setup() {
		triggerNoBlankTemplate = new TriggerNoBlankTemplate();

		triggerNoTemplate = new TriggerNoTemplate();

		triggerIsWorking = new TriggerIsWorking();
		triggerIsNotWorking = new TriggerIsNotWorking();
		triggerCanWork = new TriggerCanWork();
		triggerCannotWork = new TriggerCannotWork();

		triggerPowerNone = new TriggerPowerNone();
		triggerPowerLow = new TriggerPowerLow();
		triggerPowerMedium = new TriggerPowerMedium();
		triggerPowerHigh = new TriggerPowerHigh();
		triggerPowerFull = new TriggerPowerFull();

		ActionManager.registerTriggerProvider(new TriggerProvider());

	}

}
