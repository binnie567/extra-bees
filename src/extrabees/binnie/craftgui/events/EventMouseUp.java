package binnie.craftgui.events;

import binnie.craftgui.core.IWidget;

public class EventMouseUp extends Event {

	int x;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getButton() {
		return button;
	}

	public EventMouseUp(IWidget origin, int x, int y, int button) {
		super(origin);
		this.x = x;
		this.y = y;
		this.button = button;
	}

	int y;
	int button;

}
